// Karma configuration
// Generated on Mon Jun 09 2014 16:50:42 GMT+0200 (CEST)

module.exports = function(config) {
  config.set({

    browserConsoleLogOptions: {
      terminal: true
    },


    // base path, that will be used to resolve files and exclude
    basePath: '',


    // frameworks to use
    frameworks: [ 'mocha', 'sinon-chai', 'chai' ],


    // list of files / patterns to load in the browser
    files: [
      'node_modules/moment/moment.js',
      'app/bower_components/jquery/dist/jquery.js',
      'app/bower_components/angular/angular.js',
      'app/bower_components/angular-scroll/angular-scroll.js',
      'app/bower_components/angular-gettext/dist/angular-gettext.js',
      'app/bower_components/angular-mocks/angular-mocks.js',
      'app/bower_components/sass-bootstrap/dist/js/bootstrap.js',
      'app/bower_components/angular-resource/angular-resource.js',
      'app/bower_components/angular-cookies/angular-cookies.js',
      'app/bower_components/angular-sanitize/angular-sanitize.js',
      'app/bower_components/angular-route/angular-route.js',
      'app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'app/bower_components/Case/dist/Case.js',
      'app/bower_components/ngDialog/js/ngDialog.js',
			'app/bower_components/ngDialog/js/ngDialog.js',
			'app/bower_components/angular-once/once.js',
      'app/bower_components/angular-elastic/elastic.js',
			'app/bower_components/angular-local-storage/dist/angular-local-storage.js',
			'app/bower_components/ng-file-upload/angular-file-upload.js',
      'app/bower_components/angular-ui-tree/dist/angular-ui-tree.min.js',
      'app/bower_components/angular-ui-mask/dist/mask.js',
      'app/bower_components/isteven-angular-multiselect/isteven-multi-select.js',
      'app/bower_components/intro.js/intro.js',
      'app/bower_components/ng-tags-input/ng-tags-input.js',
      'app/bower_components/ngprogress/build/ngprogress.min.js',
      'app/services/**/*Module.js',
      'app/services/**/index.js',
      'app/services/**/*.js',
      'app/scripts/*.js',
      'app/services/**/*.js',
      'app/scripts/controllers/**/index.js',
      'app/scripts/controllers/**/*.js',
      'app/scripts/modules/*.js',
      'app/scripts/components/**/*.js',
      'app/scripts/modules/dde/**/*.js',
      'test/**/*.js',
      'app/views/*.html',
      'app/views/**/*.html',
      '.tmp/scripts/bsScripts.br.js'
    ],

    // list of files to exclude
    exclude: [
      'app/scripts/browserify/*.js'
    ],

    // coverage reporter generates the coverage
    reporters: [ 'mocha', 'junit', 'coverage' ],

    preprocessors: {
      // source files, that you wanna generate coverage for
      // do not include tests or libraries
      // (these files will be instrumented by Istanbul)
      'app/scripts/*.js': 'coverage',
      'app/scripts/controllers/**/*.js': 'coverage',
      'app/scripts/services/**/*.js': 'coverage',
      'app/scripts/modules/!(workbenchModule).js': 'coverage',
      'app/services/**/*.js': 'coverage',

      // generate js files from html templates
      'app/views/**/*.html': 'ng-html2js',
      'app/views/*.html': 'ng-html2js'
    },

    junitReporter: {
      outputFile: 'test-results.xml'
    },

    coverageReporter: {
      dir:'coverage/',
      reporters:[
        {type: 'cobertura'},
        {type: 'clover'},
        {type: 'html'}
      ]
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: 'app/',
    },


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera (has to be installed with `npm install karma-opera-launcher`)
    // - Safari (only Mac; has to be installed with `npm install karma-safari-launcher`)
    // - PhantomJS
    // - IE (only Windows; has to be installed with `npm install karma-ie-launcher`)
    browsers: ['PhantomJS'],


    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: true
  });
};
