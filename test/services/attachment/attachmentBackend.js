/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('attachmentBackend', function () {

  var $httpBackend, ATTACHMENT_URL;
  var AttachmentBackend;

  beforeEach(module('gettext'));
  beforeEach(module('documentmodel')); // for underscore
  beforeEach(module('services.attachment'));

  beforeEach(inject(function (_$httpBackend_, _ATTACHMENT_URL_, _AttachmentBackend_) {
    $httpBackend = _$httpBackend_;
    ATTACHMENT_URL = _ATTACHMENT_URL_;
    AttachmentBackend = _AttachmentBackend_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.getAttachmentInfo()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(ATTACHMENT_URL + 'info/abc123');
      callback = sinon.spy();
    });

    it('should retrieve attachment info by id' , function () {
      requestHandler.respond({ info: {} });

      AttachmentBackend.getAttachmentInfo('abc123').then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({ info: {} });
    });

    it('should report any error while retrieving attachment info by id' , function () {
      requestHandler.respond(401);

      AttachmentBackend.getAttachmentInfo('abc123').catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });
  });

  describe('.getAttachmentData()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(ATTACHMENT_URL + 'download/abc123');
      callback = sinon.spy();
    });

    it('should retrieve attachment data by id' , function () {
      requestHandler.respond({ data: {} });

      AttachmentBackend.getAttachmentData('abc123').then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({ data: {} });
    });

  });

  describe('.uploadAttachment()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPOST(
        ATTACHMENT_URL + 'upload/abc123');
      callback = sinon.spy();
    });

    it('should upload an attachment' , function () {
      var workflowID = 123;
      var attachmentID = 'abc123';
      var file = 'this is a file';
      requestHandler.respond({ data: {} });

      AttachmentBackend.uploadAttachment(workflowID, attachmentID, file).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({ data: {} });
    });

  });

  describe('.deleteAttachment()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectDELETE(ATTACHMENT_URL + 'delete/abc123');
      callback = sinon.spy();
    });

    it('should delete an attachment by id' , function () {
      requestHandler.respond({});

      AttachmentBackend.deleteAttachment('abc123').then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({});
    });
  });

});
