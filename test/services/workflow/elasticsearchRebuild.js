/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('ElasticsearchRebuild', function () {

  var ElasticsearchRebuild, $httpBackend, ELASTIC_REFRESH_URL;

  beforeEach(module('services.workflow'));

  beforeEach(inject(function (_ElasticsearchRebuild_, _$httpBackend_, _ELASTIC_REFRESH_URL_) {
    $httpBackend = _$httpBackend_;
    ElasticsearchRebuild = _ElasticsearchRebuild_;
    ELASTIC_REFRESH_URL = _ELASTIC_REFRESH_URL_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.rebuildCollection()', function () {

    // A common http request handler. The exepectations are define in the
    // beforeEach() block, whereas the responses are provided by the tests
    // themselves.
    var requestHandler;

    // A common callback function to spy on.
    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPOST(ELASTIC_REFRESH_URL + '/rebuild?organizationId=1&dataCollectionGroup=TEST');
      callback = sinon.spy();
    });

    it('should start an elasticsearch index rebuild' , function () {
      requestHandler.respond(200);

      ElasticsearchRebuild.rebuildCollection(1, 'TEST').then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.be.undefined;
    });

    it('should report any error trying to rebuild the index' , function () {
      requestHandler.respond(401, { error: 'some message' });

      ElasticsearchRebuild.rebuildCollection(1, 'TEST').catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not rebuild Elasticsearch for "TEST"');
    });

  });

  describe('.status()', function () {

    // A common http request handler. The exepectations are define in the
    // beforeEach() block, whereas the responses are provided by the tests
    // themselves.
    var requestHandler;

    // A common callback function to spy on.
    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(ELASTIC_REFRESH_URL + '/status/advanced');
      callback = sinon.spy();
    });

    it('should query the elasticsearch index rebuild status' , function () {
      requestHandler.respond(200, { status: 'OK' });

      ElasticsearchRebuild.status().then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal({ status: 'OK' });
    });

    it('should report any error trying to query the status' , function () {
      requestHandler.respond(500);

      ElasticsearchRebuild.status().catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been
        .calledWith('Could not retrieve Elasticsearch rebuilding status');
    });

  });
});
