/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('makeSearchQuery', function () {

  var SkryvSession;
  var makeSearchQuery;

  beforeEach(function () {
    SkryvSession = {
      getLoggedUser: sinon.stub()
    };
  });

  describe('on HD4DP', function () {

    beforeEach(module('services.workflow', function ($provide) {
      $provide.constant('SkryvSession', SkryvSession);
      $provide.constant('PLATFORM', 'hd4dp');
    }));

    beforeEach(inject(function (_makeSearchQuery_) {
      makeSearchQuery = _makeSearchQuery_;
    }));

    it('should return a default search query', function () {
      var searchParams = {};
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          filtered: {
            filter: {
              bool: {
                must: [],
                must_not: [
                  {
                    term: {
                      status: 'DELETED'
                    }
                  }
                ]
              }
            }
          }
        }
      });
    });

    it('should filter on last update', function () {
      var searchParams = {
        lastUpdatedAfter: '2011-11-23',
        lastUpdatedBefore: '2014-11-23'
      };
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          filtered: {
            filter: {
              bool: {
                must: [
                  {
                    range: {
                      updatedOn: {
                        gte: '2011-11-23',
                        lte: '2014-11-23'
                      }
                    }
                  }
                ],
                must_not: [
                  {
                    term: {
                      status: 'DELETED'
                    }
                  }
                ]
              }
            }
          }
        }
      });
    });

    it('should filter on the current user', function () {
      var searchParams = {
        onlyMine: true
      };
      var username = 'whatever';
      SkryvSession.getLoggedUser.returns({ username: username });
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          filtered: {
            filter: {
              bool: {
                must: [
                  {
                    nested: {
                      path: 'history',
                      filter: {
                        term: {
                          'history.executedBy': username
                        }
                      }
                    }
                  }
                ],
                must_not: [
                  {
                    term: {
                      status: 'DELETED'
                    }
                  }
                ]
              }
            }
          }
        }
      });
    });

    it('should filter multiple statuses', function () {
      var searchParams = {
        statuses: [ { status: 'A' }, { status: 'B' } ]
      };
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          filtered: {
            filter: {
              bool: {
                must: [
                  {
                    terms: {
                      status: [ 'A', 'B' ]
                    }
                  }
                ],
                must_not: [
                  {
                    term: {
                      status: 'DELETED'
                    }
                  }
                ]
              }
            }
          }
        }
      });
    });

    it('should filter on status flags', function () {
      var searchParams = {
        statuses: [ { status: 'A', options: { flag: 'someFlag' } }, { status: 'B' } ]
      };
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          filtered: {
            filter: {
              bool: {
                must: [
                  {
                    terms: {
                      status: [ 'A', 'B' ]
                    }
                  },
                  {
                    bool: {
                      should: [
                        {
                          not: {
                            term: {
                              status: 'A'
                            }
                          }
                        },
                        {
                          term: {
                            'flags.someFlag': true
                          }
                        }
                      ]
                    }
                  }
                ],
                must_not: [
                  {
                    term: {
                      status: 'DELETED'
                    }
                  }
                ]
              }
            }
          }
        }
      });
    });

    it('should filter on follow ups', function () {
      var searchParams = {
        followUps: [
          { name: 'A', planned: true },
          { name: 'B', active: true, submitted: true }
        ]
      };
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          filtered: {
            filter: {
              bool: {
                must: [
                  {
                    nested: {
                      path: 'context.followUps',
                      filter: {
                        bool: {
                          must: [
                            {
                              exists: {
                                field: 'context.followUps.activationDate'
                              }
                            },
                            {
                              term: {
                                'context.followUps.name': 'A'
                              }
                            }
                          ],
                          must_not: [
                            {
                              exists: {
                                field: 'context.followUps.submittedOn'
                              }
                            },
                            {
                              term: {
                                'context.followUps.active': true
                              }
                            }
                          ],
                          should: []
                        }
                      }
                    }
                  },
                  {
                    nested: {
                      path: 'context.followUps',
                      filter: {
                        bool: {
                          must: [ { term: { 'context.followUps.name': 'B' } } ],
                          must_not: [],
                          should: [
                            {
                              term: {
                                'context.followUps.active': true
                              }
                            },
                            {
                              exists: {
                                field: 'context.followUps.submittedOn'
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                ],
                must_not: [ { term: { status: 'DELETED' } } ]
              }
            }
          }
        }
      });
    });

    it('should sort on workflow fields', function () {
      var searchParams = {
        sort: { sortColumn: 'warnings' }
      };
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          filtered: {
            filter: {
              bool: {
                must: [],
                must_not: [
                  {
                    term: {
                      status: 'DELETED'
                    }
                  }
                ]
              }
            }
          }
        },
        sort: {
          'document.documentContent.warnings': {
            unmapped_type: 'string',
            order: 'asc',
            missing: '_last'
          }
        }
      });
    });

    it('should sort on document fields', function () {
      var searchParams = {
        sort: { sortColumn: { name: 'someField' }}
      };
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          filtered: {
            filter: {
              bool: {
                must: [],
                must_not: [
                  {
                    term: {
                      status: 'DELETED'
                    }
                  }
                ]
              }
            }
          }
        },
        sort: {
          'document.documentContent.display.nested.someField.value': {
            unmapped_type: 'string',
            order: 'asc',
            missing: '_last'
          }
        }
      });
    });

    it('should support full text search', function () {
      var searchParams = {
        queryText: 'hello'
      };
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          filtered: {
            filter: {
              bool: {
                must: [],
                must_not: [
                  {
                    term: {
                      status: 'DELETED'
                    }
                  }
                ]
              }
            },
            query: {
              match_phrase_prefix: {
                _all: 'hello'
              }
            }
          }
        }
      });
    });

  });

  describe('on HD4RES', function () {

    beforeEach(module('services.workflow', function ($provide) {
      $provide.constant('SkryvSession', SkryvSession);
      $provide.constant('PLATFORM', 'hd4res');
    }));

    beforeEach(inject(function (_makeSearchQuery_) {
      makeSearchQuery = _makeSearchQuery_;
    }));

    it('should return a default search query', function () {
      var searchParams = {};
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0
      });
    });

    it('should support full text search', function () {
      var searchParams = {
        queryText: 'hello'
      };
      var query = makeSearchQuery(searchParams, 1, 10);
      expect(query).to.deep.equal({
        size: 10,
        from: 0,
        query: {
          match_phrase_prefix: {
            _all: 'hello'
          }
        }
      });
    });

  });

});
