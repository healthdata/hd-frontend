/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('WorkflowBackend', function () {

  var $httpBackend, $timeout, WORKFLOW_SERVER_URL, SEARCH_SERVER_URL, stripTrailingSlash, PLATFORM, WorkflowRequest;

  beforeEach(module('services.workflow'));

  beforeEach(inject(function (_$httpBackend_, _$timeout_, _WORKFLOW_SERVER_URL_, _SEARCH_SERVER_URL_, _stripTrailingSlash_, _PLATFORM_, _WorkflowRequest_) {
    $httpBackend = _$httpBackend_;
    $timeout = _$timeout_;
    WORKFLOW_SERVER_URL = _WORKFLOW_SERVER_URL_;
    SEARCH_SERVER_URL = _SEARCH_SERVER_URL_;
    stripTrailingSlash = _stripTrailingSlash_;
    PLATFORM = _PLATFORM_;
    WorkflowRequest = _WorkflowRequest_;
  }));

  // Normally, $httpBackend.flush() will check all expectations. The following code
  // makes sure that no test passes because we forgot to flush.
  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.get()', function () {

    // A common http request handler. The exepectations are define in the
    // beforeEach() block, whereas the responses are provided by the tests
    // themselves.
    var requestHandler;

    // A common callback function to spy on.
    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(WORKFLOW_SERVER_URL + '1');
      callback = sinon.spy();
    });

    it('should retrieve a workflow by id' , function () {
      var workflow = { someInfo: 'abc' };
      requestHandler.respond(workflow);

      WorkflowRequest.get(1).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('someInfo', workflow.someInfo);
    });

    it('should report any error while retrieving a workflow by id' , function () {
      requestHandler.respond(401);

      WorkflowRequest.get(1).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Registration not found');
    });
  });

  describe('.save()', function () {

    var requestHandler, callback;

    var BODY = { a: 'A', b: 'B' };

    beforeEach(function () {
      requestHandler = $httpBackend.expect('POST',
        stripTrailingSlash(WORKFLOW_SERVER_URL),
        BODY);
      callback = sinon.spy();
    });

    it('should save a workflow' , function () {
      var workflow = { someInfo: 'abc' };
      requestHandler.respond(workflow);

      WorkflowRequest.save(BODY).then(callback);

      $httpBackend.flush();
      $timeout.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('someInfo', workflow.someInfo);
    });

    it('should report any error while saving a workflow' , function () {
      requestHandler.respond(401, {});

      WorkflowRequest.save(BODY).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not save the registration');
    });

    it('should report an error when the data collection is not open', function () {
      requestHandler.respond(40, {
        error: 'INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION',
        error_description: '... not open for 123abc'
      });

      WorkflowRequest.save(BODY).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Data collection is not open for 123abc');
    });
  });

  describe('.getByDatacollection()', function () {

    var DATACOLLECTION_NAME = 'DC';
    var QUERY = {
      elasticsearch: 'query'
    };

    var requestHandler, callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expect('POST',
        SEARCH_SERVER_URL + DATACOLLECTION_NAME,
        QUERY);
      callback = sinon.spy();
    });

    it('should retrieve all workflows for a given data collection name', function () {
      var response = { elasticsearch: 'response' };
      requestHandler.respond(response);

      WorkflowRequest.getByDatacollection(DATACOLLECTION_NAME, QUERY)
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal(response);
    });

    it('should report any error while retrieving registrations', function () {
      requestHandler.respond(500);

      WorkflowRequest.getByDatacollection(DATACOLLECTION_NAME, QUERY)
        .catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not get registrations');
    });
  });

  describe('.applyAction()', function () {

    var requestHandler, callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expect('POST',
        WORKFLOW_SERVER_URL + 'actions',
        { action: 'information' });
      callback = sinon.spy();
    });

    it('should post an action to the workflow', function () {
      var response = { action: 'response' };
      requestHandler.respond(response);

      WorkflowRequest.applyAction({ action: 'information' })
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith(response);
    });

    it('should report any error while applying the action', function () {
      requestHandler.respond(500, { error: 'explanation' });

      WorkflowRequest.applyAction({ action: 'information' })
        .catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('explanation');
    });
  });

  describe('.downloadAll()', function () {

    var DEFINITION_ID = 123;

    var requestHandler, callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(
        WORKFLOW_SERVER_URL + 'download/' + DEFINITION_ID);
      callback = sinon.spy();
    });

    it('should retrieve all workflows for a given data collection id', function () {
      var response = { csv: 'response' };
      requestHandler.respond(response);

      WorkflowRequest.downloadAll({ id: DEFINITION_ID })
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith(response);
    });

    it('should report any error while retrieving workflows', function () {
      requestHandler.respond(500);

      WorkflowRequest.downloadAll({ id: DEFINITION_ID })
        .catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Failed downloading registrations');
    });
  });

  describe('.bulkDelete()', function () {

    var requestHandler, callback;
    var BODY = [ { id: 1 }, { id: 2 } ];

    beforeEach(function () {
      requestHandler = $httpBackend.expect('DELETE',
        WORKFLOW_SERVER_URL,
        [ 1, 2 ]);
      callback = sinon.spy();
    });

    it('should delete workflows', function () {
      requestHandler.respond(200, 'Success');

      WorkflowRequest.bulkDelete(BODY)
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Success');
    });

    it('should report any error while deleting registrations', function () {
      requestHandler.respond(500);

      WorkflowRequest.bulkDelete(BODY)
        .catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Failed deleting registrations');
    });
  });

});
