/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('WorkflowActions', function () {

  var $q, $rootScope, $timeout, WORKFLOW_SERVER_URL, SEARCH_SERVER_URL, PLATFORM, SkryvSession, $httpBackend;

  beforeEach(module('services.workflow'));

  beforeEach(function () {
    // Mock filter
    SkryvSession = {
      getDefinition: function () {
        return {};
      }
    };

    module(function ($provide) {
      $provide.value('SkryvSession', SkryvSession);
    });
  });

  beforeEach(inject(function (_$q_, _$rootScope_, _$timeout_, _WORKFLOW_SERVER_URL_, _SEARCH_SERVER_URL_, _PLATFORM_, _$httpBackend_) {
    $q = _$q_;
    $rootScope = _$rootScope_;
    $timeout = _$timeout_;
    WORKFLOW_SERVER_URL = _WORKFLOW_SERVER_URL_;
    SEARCH_SERVER_URL = _SEARCH_SERVER_URL_;
    PLATFORM = _PLATFORM_;
    $httpBackend = _$httpBackend_;
  }));

  describe('WorkflowActions', function () {

    var WorkflowActions;

    beforeEach(inject(function (_WorkflowActions_) {
      WorkflowActions = _WorkflowActions_;
    }));

    describe('.applyAction()', function () {

      var theAction;
      var callback;

      var NOTIFICATIONS = {
        errorMessage: 'some error message'
      };

      beforeEach(function () {
        theAction = sinon.stub();
        callback = sinon.spy();
      });

      it('should post an action to a given workflow', function () {
        $httpBackend.expectGET('views/dialog/simpleMessage.html').respond(201);
        var workflow = { thisIs: 'a workflow' };
        theAction.returns($q.when(workflow));

        WorkflowActions.applyAction(theAction, NOTIFICATIONS)
          .then(callback);

        $rootScope.$digest();
        $timeout.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.have.property('thisIs', workflow.thisIs);
      });

      it('should report any error while posting an action', function () {
        $httpBackend.expectGET('views/dialog/simpleMessage.html').respond(201);
        theAction.returns($q.reject('oops'));
        WorkflowActions.applyAction(theAction, NOTIFICATIONS)
          .catch(callback);

        $rootScope.$digest();
        $timeout.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith(NOTIFICATIONS.errorMessage);
      });

    });

  });

});
