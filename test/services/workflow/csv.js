/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('Csv', function () {

  var SkryvCsv, Notifications, $httpBackend, SESSION_SERVER_URL, requestHandler, callback;

  beforeEach(module('services.workflow'));

  beforeEach(inject(function (_SkryvCsv_, _Notifications_, _$httpBackend_, _SESSION_SERVER_URL_) {
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    $httpBackend = _$httpBackend_;
    SkryvCsv = _SkryvCsv_;
    Notifications = _Notifications_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.uploadCsv()', function () {

    var csv = 'patient_code;collect_year;\n\n;2014';

    it('should notify when csv is empty', function () {
      var csv = '';
      SkryvCsv.uploadCsv(csv, 'BCFR');
      expect(Notifications.notifications[0].content).to.equal('CSV contains no data');
    });

    it('should notify when csv is 1 row', function () {
      var csv = 'patient_code;collect_year;';
      SkryvCsv.uploadCsv(csv, 'BCFR');
      expect(Notifications.notifications[0].content).to.equal('CSV contains no data');
    });

    it('should trim last newline', function () {
      var csv = 'patient_code;collect_year;\n';
      SkryvCsv.uploadCsv(csv, 'BCFR');
      expect(Notifications.notifications[0].content).to.equal('CSV contains no data');
    });

    it('should notify upload success', function () {
      requestHandler = $httpBackend.expectPOST(SESSION_SERVER_URL + '/preloading');

      callback = sinon.spy();

      requestHandler.respond({ status: [ { status: 'ok' }, { status: 'ok' }, { status: 'error' } ] });

      SkryvCsv.uploadCsv(csv, 'BCFR');

      $httpBackend.flush();
    });

    it('should notify upload failed', function () {
      requestHandler = $httpBackend.expectPOST(SESSION_SERVER_URL + '/preloading');
      callback = sinon.spy();

      requestHandler.respond(401);

      SkryvCsv.uploadCsv(csv, 'BCFR');

      $httpBackend.flush();
      expect(Notifications.notifications[0].content).to.equal('CSV upload error');
    });
  });

  describe('.downloadCsv()', function () {
    it('should do nothing when no workflows', function () {
      SkryvCsv.downloadCsv([], 'BCFR');
      expect(Notifications.notifications).to.lengthOf(0);
    });

    it('should catch errors', function () {
      SkryvCsv.downloadCsv([ '....' ], 'BCFR');
      expect(Notifications.notifications[0].content).to.equal('CSV download failed');
    });
  });

});
