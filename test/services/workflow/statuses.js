/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('Statuses', function () {

  var Statuses;

  beforeEach(module('services.workflow'));

  beforeEach(inject(function (_Statuses_) {
    Statuses = _Statuses_;
  }));

  it('should expose a list of statuses', function () {
    expect(Statuses.statuses()).to.have.length.above(5);
  });

  it('should contain the correct properties for each status', function () {
    angular.forEach(Statuses.statuses(), function (status) {
      expect(status).to.have.property('name');
      expect(status).to.have.property('label');
      expect(status).to.have.property('platform');
    });
  });

});
