/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('DocumentBackend', function () {

  var $httpBackend, Notifications, WORKFLOW_SERVER_URL, DocumentBackend;

  beforeEach(module('services.workflow'));

  beforeEach(inject(function (_$httpBackend_, _Notifications_, _WORKFLOW_SERVER_URL_, _SkryvDoc_) {
    $httpBackend = _$httpBackend_;
    WORKFLOW_SERVER_URL = _WORKFLOW_SERVER_URL_;
    Notifications = _Notifications_;
    DocumentBackend = _SkryvDoc_;
  }));

  var request;
  var callback;

  beforeEach(function () {
    request = $httpBackend.expectGET(WORKFLOW_SERVER_URL + '1/documents');
    callback = sinon.spy();
  });

  it('should return document obj' , function () {
    request.respond({ docContent: 'Test' });

    DocumentBackend.byId(1).then(callback);

    $httpBackend.flush();

    expect(callback).to.have.been.calledOnce;
    expect(callback.firstCall.args[0])
      .to.have.property('docContent', 'Test');
  });

  it('should return unable to get document' , function () {
    request.respond(401, '');

    DocumentBackend.byId(1).catch(callback);

    $httpBackend.flush();

    expect(callback).to.have.been.calledWith('Unable to fetch registration');
  });

});
