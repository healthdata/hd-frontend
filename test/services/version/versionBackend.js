/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('versionModule', function () {

  var $httpBackend, SESSION_SERVER_URL, PLATFORM;

  beforeEach(module('skryv.version'));

  beforeEach(inject(function (_$httpBackend_, _SESSION_SERVER_URL_) {
    $httpBackend = _$httpBackend_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('SkryvVersion', function () {

    var SkryvVersion;

    beforeEach(inject(function (_SkryvVersion_, _PLATFORM_) {
      SkryvVersion = _SkryvVersion_;
      PLATFORM = _PLATFORM_;
    }));

    describe('.get()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/about/' + PLATFORM.toUpperCase() + '_VERSION');
        callback = sinon.spy();
      });

      it('should retrieve version of platform' , function () {
        requestHandler.respond({ value: '1.1.1' });

        SkryvVersion.get().then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.have.property('value', '1.1.1');
      });
    });
  });
});
