/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('notificationManager', function () {

  var Notifications, $timeout;

  beforeEach(module('services.notification'));

  beforeEach(inject(function (_Notifications_, _$timeout_) {
    Notifications = _Notifications_;
    $timeout = _$timeout_;
  }));

  it('should clear notifications', function () {
    Notifications.notifications = [ '1', '2' ];
    Notifications.clear();
    expect(Notifications.notifications).to.deep.equal([]);
  });

  describe('Notifications', function () {
    beforeEach(function () {
      Notifications.addNotification = sinon.spy();
    });

    it('should create info notification', function () {
      Notifications.info('test', 1);
      expect(Notifications.addNotification).to.have.been.calledOnce;
      expect(Notifications.addNotification).to.have.been.calledWith('info', 'info', 'test', 1);
    });

    it('should create error notification', function () {
      Notifications.error('test');
      expect(Notifications.addNotification).to.have.been.calledOnce;
      expect(Notifications.addNotification).to.have.been.calledWith('error', 'times-circle-o', 'test', undefined);
    });

    it('should create warning notification', function () {
      Notifications.warning('test', 1);
      expect(Notifications.addNotification).to.have.been.calledOnce;
      expect(Notifications.addNotification).to.have.been.calledWith('warning', 'exclamation-triangle', 'test', 1);
    });

    it('should create success notification', function () {
      Notifications.success('test', 1);
      expect(Notifications.addNotification).to.have.been.calledOnce;
      expect(Notifications.addNotification).to.have.been.calledWith('success', 'check-circle-o', 'test', 1);
    });
  });

  describe('NotificationHelpers', function () {
    it('should add notification to queue', function () {
      Notifications.addNotification('info', 'info', 'test', 1);
      expect(Notifications.notifications).to.deep.equal([ { type: 'info', icon: 'info', content: 'test', duration: 1 } ]);

      $timeout.flush();
      expect(Notifications.notifications).to.deep.equal([]);
    });

    it('should add sticky notification to queue', function () {
      Notifications.addNotification('info', 'info', 'test', 'sticky');
      expect(Notifications.notifications).to.deep.equal([ { type: 'info', icon: 'info', content: 'test', duration: 'sticky' } ]);

      $timeout.verifyNoPendingTasks();
    });

    it('should remove notification from queue', function () {
      Notifications.notifications = [ '1', '2', '3' ];
      Notifications.removeNotification('2');
      expect(Notifications.notifications).to.deep.equal([ '1', '3' ]);
    });
  });

});
