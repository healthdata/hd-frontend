/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('pdfContentService', function () {

  var $q, $rootScope;
  var SkryvPdf;
  var PdfTemplateBackend;
  var WorkflowRequest;

  beforeEach(module('services.pdf'));

  beforeEach(function () {
    PdfTemplateBackend = {
      get: sinon.stub()
    };
    WorkflowRequest = {
      getByDatacollection: sinon.stub()
    };
    module({
      PdfTemplateBackend: PdfTemplateBackend,
      WorkflowRequest: WorkflowRequest
    });
  });

  beforeEach(inject(function (_$q_, _$rootScope_, _SkryvPdf_) {
    $q = _$q_;
    $rootScope = _$rootScope_;
    SkryvPdf = _SkryvPdf_;
  }));

  function flush() {
    $rootScope.$digest();
  }

  describe('.getWorkflowPdf()', function () {

    it('should render the handlebars template with the data from elasticsearch' , function () {
      var workflowId = 123;
      var dataCollectionDefinitionId = 'def456';
      var workflow = {
        id: workflowId,
        dataCollectionDefinitionId: dataCollectionDefinitionId
      };
      var pdfTemplate = 'Foo: {{ document.bar }}';
      var expectedQuery = {
        query: {
          filtered: {
            filter: {
              bool: {
                must: [
                  { term: { id: workflowId }}
                ]
              }
            }
          }
        }
      };
      var esWorkflowData = {
        hits: {
          hits: [
            {
              _source: {
                document: {
                  documentContent: {
                    display: {
                      bar: 'Hello, world!'
                    }
                  }
                }
              }
            }
          ]
        }
      };
      WorkflowRequest.getByDatacollection
        .withArgs(dataCollectionDefinitionId, expectedQuery)
        .returns($q.when(esWorkflowData));

      var callback = sinon.spy();

      PdfTemplateBackend.get.returns($q.when(pdfTemplate));

      SkryvPdf.getWorkflowPdf(workflow).then(callback);

      flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Foo: Hello, world!');
    });

  });

});
