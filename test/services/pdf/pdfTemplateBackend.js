/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('pdfTemplateBackend', function () {

  var $httpBackend;
  var PdfTemplateBackend;
  var SESSION_SERVER_URL;

  beforeEach(module('services.pdf'));

  beforeEach(inject(function (_$httpBackend_, _SESSION_SERVER_URL_, _PdfTemplateBackend_) {
    $httpBackend = _$httpBackend_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    PdfTemplateBackend = _PdfTemplateBackend_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.getTemplate()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/pdf/templates/1');
      callback = sinon.spy();
    });

    it('should retrieve a pdf template by id' , function () {
      requestHandler.respond({ pdfTemplate: {} });

      PdfTemplateBackend.get(1).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({ pdfTemplate: {} });
    });

    it('should report any error while retrieving the pdf template' , function () {
      requestHandler.respond(401);

      PdfTemplateBackend.get(1).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not get pdf template');
    });
  });

});
