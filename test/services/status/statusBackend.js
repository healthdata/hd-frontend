/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('StatusBackend', function () {
  var SkryvStatus, Notifications, $httpBackend, requestHandler, callback, SESSION_SERVER_URL;

  beforeEach(module('gettext'));
  beforeEach(module('services.notification'));
  beforeEach(module('services.status'));

  beforeEach(inject(function (_SkryvStatus_, _Notifications_, _$httpBackend_, _SESSION_SERVER_URL_) {
    $httpBackend = _$httpBackend_;
    Notifications = _Notifications_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    SkryvStatus = _SkryvStatus_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should get all messages', function () {
    requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/statistics/registrations');
    callback = sinon.spy();
    requestHandler.respond({
      TEST: [
        {
          version: { major: 1 },
          dataProviders: [ {
            identification: { value: '123', name: 'TEST' },
            registrations: {
              IN_PROGRESS: 123,
              SUBMITTED: 6
            }
          } ]
        }
      ]
    });

    var messages;
    SkryvStatus.getStatusMessages().then(function (result) {
      messages = result;
    });

    $httpBackend.flush();
    expect(messages).to.deep.equal({
      TEST: [
        {
          version: { major: 1 },
          dataProviders: [ {
            identification: { value: '123', name: 'TEST' },
            registrations: {
              IN_PROGRESS: 123,
              SUBMITTED: 6
            }
          } ]
        }
      ]
    });
  });

  it('should get all messages by date', function () {
    requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/statusmessages?createdOn=02-02-2000');
    callback = sinon.spy();
    requestHandler.respond([ { test: 1 } ]);

    var messages;
    SkryvStatus.getStatusMessagesByDate('02-02-2000').then(function (result) {
      messages = result;
    });

    $httpBackend.flush();
    expect(messages).to.deep.equal([ { test: 1 } ]);
  });

  it('should fail get all messages', function () {
    requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/statistics/registrations');
    callback = sinon.spy();
    requestHandler.respond(401);

    SkryvStatus.getStatusMessages();

    $httpBackend.flush();
    expect(Notifications.notifications[0].content).to.equal('Could not retrieve status messages');
  });

  it('should get status api', function () {
    requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/status');
    callback = sinon.spy();
    requestHandler.respond([ { catalogue: 'UP' } ]);

    var api;
    SkryvStatus.getStatusAPI().then(function (result) {
      api = result;
    });

    $httpBackend.flush();
    expect(api).to.deep.equal([ { catalogue: 'UP' } ]);
  });

  it('should fail get status api', function () {
    requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/status');
    callback = sinon.spy();
    requestHandler.respond(401);

    var err;
    SkryvStatus.getStatusAPI().catch(function (error) {
      err = error;
    });

    $httpBackend.flush();
    expect(err).to.equal('Could not get status of API\'s');
  });

});
