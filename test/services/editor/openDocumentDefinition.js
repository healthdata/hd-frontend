/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('openDocumentDefinition', function () {

  var openDocumentDefinition;

  beforeEach(module('services.editor'));

  beforeEach(inject(function (_openDocumentDefinition_) {
    openDocumentDefinition = _openDocumentDefinition_;
  }));

  it('should not allow invalid document definitions', function () {
    expect(function () {
      openDocumentDefinition('this is not JSON');
    }).to.throw();
  });

  it('should return the description and component', function () {
    var result = openDocumentDefinition({});
    expect(result).to.have.property('description')
      .that.is.an('object');
    expect(result).to.have.property('component')
      .that.is.an('object');
  });

});
