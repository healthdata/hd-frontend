/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('displayValue', function () {

  // FIXME ideally we should only load the list view module here.
  beforeEach(module('frontendApp'));

  describe('displayValue', function () {

    var displayValue;

    beforeEach(inject(function (_displayValue_) {
      displayValue = _displayValue_;
    }));

    function prop(name) {
      return DOCUMENT_DATA[name];
    }

    it('should render numbers fields', function () {
      expect(displayValue(prop('someNumber'))).to.equal(42);
    });

    it('should render text fields', function () {
      expect(displayValue(prop('someText'))).to.equal('Hello, world!');
    });

    it('should render boolean fields', function () {
      expect(displayValue(prop('someBoolean'))).to.equal('Yes');
      expect(displayValue(prop('someOtherBoolean'))).to.equal('No');
    });

    it('should render invalid booleans as empty', function () {
      expect(displayValue(prop('invalidBoolean'))).to.equal('');
    });

    it('should render date fields', function () {
      expect(displayValue(prop('someDate'))).to.equal('11-03-2015');
    });

    it('should render choice fields', function () {
      expect(displayValue(prop('someChoice'))).to.equal('Option B');
    });

    it('should render empty choice fields', function () {
      expect(displayValue(prop('emptyChoice'))).to.equal('');
    });

    it('should render multichoice fields', function () {
      expect(displayValue(prop('someMultiChoice'))).to.equal('Option A\nOption C');
    });

    it('should render empty multichoice fields', function () {
      expect(displayValue(prop('emptyMultiChoice'))).to.equal('');
    });

    it('should render referencelist fields', function () {
      expect(displayValue(prop('placeOfResidence'))).to.equal('9000 - Ghent');
    });

    it('should render empty referencelist fields', function () {
      expect(displayValue(prop('emptyPlaceOfResidence'))).to.equal('');
    });

    it('should render non-binding referencelist fields', function () {
      expect(displayValue(prop('nonBindingRefList'))).to.equal('something');
    });

    it('should render lists', function () {
      expect(displayValue(prop('someList'))).to.equal(
        'Some number in a list: 42\n' +
        'Some text in a list: hello world\n' +
        'Some reflist in a list: 9000\n' +
        '\n' +
        'Some number in a list: 101\n' +
        'Some text in a list: foo bar\n' +
        'Some reflist in a list: 1000');
    });

    it('should render lists with limited nesting', function () {
      var result = displayValue(prop('listWithNesting'));
      expect(result).to.equal(
        'Some number in a list: 42\n' +
        'Some multichoice in a list: Option A, Option B\n' +
        '\n' +
        'Some number in a list: 101\n' +
        'Some multichoice in a list: Option A, Option B');
    });

  });

  describe('documentDisplayValues', function () {

    var documentDisplayValues;

    beforeEach(inject(function (_documentDisplayValues_) {
      documentDisplayValues = _documentDisplayValues_;
    }));

    it('should convert all the fields in a document to their display value', function () {
      expect(documentDisplayValues(EXAMPLE_DOCUMENT)).to.deep.equal(EXPECTED_RESULT);
    });

  });

});

var DOCUMENT_DATA = {
  someNumber: {
    contentType: 'number',
    value: 42
  },
  someText: {
    contentType: 'text',
    value: 'Hello, world!'
  },
  someBoolean: {
    contentType: 'boolean',
    value: true
  },
  someOtherBoolean: {
    contentType: 'boolean',
    value: false
  },
  invalidBoolean: {
    contentType: 'boolean'
  },
  someDate: {
    contentType: 'date',
    value: '2015-03-11T09:58:26.399Z'
  },
  someChoice: {
    contentType: 'choice',
    selectedOptionLabel: {
      nl: 'Option B',
      fr: 'Option B',
      en: 'Option B'
    }
  },
  emptyChoice: {
    contentType: 'choice'
  },
  someMultiChoice: {
    contentType: 'multichoice',
    selectedOptionsLabels: [
      { nl: 'Option A', fr: 'Option A', en: 'Option A' },
      { nl: 'Option C', fr: 'Option C', en: 'Option C' }
    ]
  },
  emptyMultiChoice: {
    contentType: 'multichoice'
  },
  placeOfResidence: {
    contentType: 'referencelist',
    labels: {
      nl: '9000 - Gent',
      fr: '9000 - Gand',
      en: '9000 - Ghent'
    }
  },
  emptyPlaceOfResidence: {
    contentType: 'referencelist'
  },
  nonBindingRefList: {
    contentType: 'referencelist',
    value: 'something'
  },
  someList: {
    contentType: 'list',
    elements: [
      {
        contentType: 'object',
        nested: {
          someNumberInAList: {
            contentType: 'number',
            fieldLabel: {
              en: 'Some number in a list'
            },
            value: 42
          },
          someTextInAList: {
            contentType: 'text',
            fieldLabel: {
              en: 'Some text in a list'
            },
            value: 'hello world'
          },
          someRefListInAList: {
            contentType: 'referencelist',
            fieldLabel: {
              en: 'Some reflist in a list'
            },
            value: '9000'
          }
        }
      },
      {
        contentType: 'object',
        nested: {
          someNumberInAList: {
            contentType: 'number',
            fieldLabel: {
              en: 'Some number in a list'
            },
            value: 101
          },
          someTextInAList: {
            contentType: 'text',
            fieldLabel: {
              en: 'Some text in a list'
            },
            value: 'foo bar'
          },
          someRefListInAList: {
            contentType: 'referencelist',
            fieldLabel: {
              en: 'Some reflist in a list'
            },
            value: '1000'
          }
        }
      }
    ]
  },
  listWithNesting: {
    contentType: 'list',
    elements: [
      {
        contentType: 'object',
        nested: {
          someNumberInAList: {
            contentType: 'number',
            fieldLabel: {
              en: 'Some number in a list'
            },
            value: 42
          },
          multiChoiceInList: {
            contentType: 'multichoice',
            fieldLabel: {
              en: 'Some multichoice in a list'
            },
            selectedOptionsLabels: [
              { en: 'Option A' },
              { en: 'Option B' }
            ]
          }
        }
      },
      {
        contentType: 'object',
        nested: {
          someNumberInAList: {
            contentType: 'number',
            fieldLabel: {
              en: 'Some number in a list'
            },
            value: 101
          },
          multiChoiceInList: {
            contentType: 'multichoice',
            fieldLabel: {
              en: 'Some multichoice in a list'
            },
            selectedOptionsLabels: [
              { en: 'Option A' },
              { en: 'Option B' }
            ]
          }
        }
      }
    ]
  }
};

var EXAMPLE_DOCUMENT = {
  fieldLabel: {
    en: 'NSIH-SEP',
    fr: 'NSIH-SEP',
    nl: 'NSIH-SEP'
  },
  isActive: true,
  contentType: 'object',
  nested: {
    treating_physician: {
      fieldLabel: {
        en: 'Treating physician',
        fr: 'Médecin traitant',
        nl: 'Behandelende arts'
      },
      isActive: true,
      contentType: 'list',
      value: 1,
      elements: [
        {
          isActive: true,
          contentType: 'object',
          nested: {
            nihdicode_of_the_treating_physician: {
              fieldLabel: {
                en: 'NIHDI-code of the treating physician',
                fr: 'Code INAMI unique du médecin traitant',
                nl: 'RIZIV-code van de behandelende arts'
              },
              isActive: true,
              contentType: 'referencelist',
              value: '10002678140',
              labels: {
                en: '10002678140 - DURNEZ JOKE',
                nl: '10002678140 - DURNEZ JOKE',
                fr: '10002678140 - DURNEZ JOKE'
              }
            }
          }
        }
      ]
    },
    patient_id: {
      fieldLabel: {
        en: 'National registry ID of the patient',
        fr: 'Numéro de registre national du patient',
        nl: 'Rijksregisternummer van de patiënt'
      },
      isActive: true,
      contentType: 'patientID',
      nested: {
        internal_patient_id: {
          fieldLabel: {
            en: 'Internal patient ID',
            fr: 'Numéro de patient interne',
            nl: 'Intern patiënt ID'
          },
          isActive: true,
          contentType: 'text'
        },
        name: {
          fieldLabel: {
            en: 'Name',
            fr: 'Nom',
            nl: 'Naam'
          },
          isActive: true,
          contentType: 'text',
          value: 'Mostaza'
        },
        first_name: {
          fieldLabel: {
            en: 'First name',
            fr: 'Prénom',
            nl: 'Voornaam'
          },
          isActive: true,
          contentType: 'text',
          value: 'Chips'
        },
        date_of_birth: {
          fieldLabel: {
            en: 'Date of birth',
            fr: 'Date de naissance',
            nl: 'Geboortedatum'
          },
          isActive: true,
          contentType: 'date',
          value: '2000-11-09',
          rendered: '09-11-2000'
        },
        sex: {
          fieldLabel: {
            en: 'Sex',
            fr: 'Sexe',
            nl: 'Geslacht'
          },
          isActive: true,
          contentType: 'choice',
          value: 'M',
          selectedOptionLabel: {
            en: 'Male',
            fr: 'Homme',
            nl: 'Man'
          }
        },
        deceased: {
          fieldLabel: {
            en: 'Deceased?',
            fr: 'Décédé ?',
            nl: 'Overleden?'
          },
          isActive: true,
          contentType: 'boolean',
          value: false
        },
        date_of_death: {
          fieldLabel: {
            en: 'Date of death',
            fr: 'Date de décès',
            nl: 'Datum van overlijden'
          },
          isActive: false,
          contentType: 'date'
        },
        place_of_residence: {
          fieldLabel: {
            en: 'Place of residence',
            fr: 'Lieu de résidence',
            nl: 'Woonplaats'
          },
          isActive: true,
          contentType: 'referencelist',
          labels: {}
        }
      },
      value: '20001109MOCHM'
    },
    admission_id: {
      fieldLabel: {
        en: 'Admission ID',
        fr: 'Code unique d’hospitalisation',
        nl: 'Unieke hospitalisatiecode'
      },
      isActive: true,
      contentType: 'text'
    },
    admission_date: {
      fieldLabel: {
        en: 'Admission date in hospital',
        fr: 'Date d\'hospitalisation',
        nl: 'Opnamedatum ziekenhuis'
      },
      isActive: true,
      contentType: 'date',
      value: '2017-01-07',
      rendered: '07-01-2017'
    },
    ward_admission_date: {
      fieldLabel: {
        en: 'Admission date in ward where bloodstream infection was diagnosed',
        fr: 'Date d’admission dans  le service où la septicémie a été diagnostiquée',
        nl: 'Opnamedatum in dienst waar bloedstroominfectie werd gediagnosticeerd'
      },
      isActive: true,
      contentType: 'date',
      value: '2017-01-11',
      rendered: '11-01-2017'
    },
    installation_id: {
      fieldLabel: {
        en: 'NIHDI-code of the data provider institution',
        fr: 'Code INAMI de l\'institution du fournisseur de données',
        nl: 'RIZIV-code van de data provider instelling'
      },
      isActive: true,
      contentType: 'text',
      value: '11111111'
    },
    site_number: {
      fieldLabel: {
        en: 'Campus number',
        fr: 'Numéro du site',
        nl: 'Vestigingsnummer campus'
      },
      isActive: true,
      contentType: 'referencelist',
      value: '1000',
      labels: {
        en: '1000 - ALGEMEEN ZIEKENHUIS ST. LUCAS',
        nl: '1000 - ALGEMEEN ZIEKENHUIS ST. LUCAS',
        fr: '1000 - ALGEMEEN ZIEKENHUIS ST. LUCAS'
      }
    },
    ward_id: {
      fieldLabel: {
        en: 'ID of ward where bloodstream infection was diagnosed',
        fr: 'ID du service où la septicémie a été diagnostiquée',
        nl: 'ID van dienst waar bloedstroominfectie werd gediagnosticeerd'
      },
      isActive: true,
      contentType: 'referencelist',
      value: '2300',
      labels: {
        en: '2300 - VE Ped - 89150',
        nl: '2300 - VE Ped - 89150',
        fr: '2300 - VE Ped - 89150'
      }
    },
    ward_type: {
      fieldLabel: {
        en: 'Specialty of the ward where bloodstream infection was diagnosed',
        fr: 'Spécialité du service où la septicémie a été diagnostiquée',
        nl: 'Specialiteit van dienst waar bloedstroominfectie werd gediagnosticeerd'
      },
      isActive: true,
      contentType: 'referencelist',
      value: '309945009',
      labels: {
        en: '309945009 - Pediatric department',
        nl: '309945009 - Pediatric department',
        fr: '309945009 - Pediatric department'
      }
    },
    efu_dt: {
      fieldLabel: {
        en: 'Discharge date from hospital or end date of follow-up',
        fr: 'Date de sortie de l\'hôpital ou date de fin de follow-up',
        nl: 'Ontslagdatum uit het ziekenhuis of datum van einde follow-up'
      },
      isActive: true,
      contentType: 'date',
      value: '2017-01-30',
      rendered: '30-01-2017'
    },
    infection_date: {
      fieldLabel: {
        en: 'Date of infection',
        fr: 'Date de l’infection',
        nl: 'Infectiedatum'
      },
      isActive: true,
      contentType: 'date',
      value: '2017-01-15',
      rendered: '15-01-2017'
    },
    sep_cl: {
      fieldLabel: {
        en: 'Classification other bloodstream infections episodes than those appearing two days or more after common hospital admission',
        fr: 'Classement de l\'episode des septicémies AUTRES que ceux survenant 2 jours ou plus après hospitalisation classique',
        nl: 'Classificatie van ANDERE bloedstroominfectie-episodes dan de episodes die 2 dagen of meer na de klassieke ziekenhuisopname optreden'
      },
      isActive: false,
      contentType: 'choice',
      nested: {
        specify: {
          fieldLabel: {
            en: 'Classification of episode',
            fr: 'Classement de l\'épisode',
            nl: 'Classificatie van episode'
          },
          isActive: false,
          contentType: 'text'
        }
      }
    },
    case_def: {
      fieldLabel: {
        en: 'Case definition of bloodstream infection',
        fr: 'Définition de cas de septicémie',
        nl: 'Definitie bloedstroominfectie'
      },
      isActive: true,
      contentType: 'choice',
      value: '1',
      selectedOptionLabel: {
        en: 'At least one hemoculture positive for a recognised pathogen',
        fr: 'Au moins 1 hémoculture positive avec un germe pathogène reconnu',
        nl: 'Minstens 1 positieve hemocultuur voor een erkend pathogeen micro-organisme'
      }
    },
    infection_source: {
      fieldLabel: {
        en: 'Suspected source of origin of bloodstream infection',
        fr: 'Porte d’entrée suspectée de la septicémie',
        nl: 'Vermoedelijke oorsprong bloedstroominfectie'
      },
      isActive: true,
      contentType: 'choice',
      value: '9',
      selectedOptionLabel: {
        en: 'Skin / soft tissue',
        fr: 'Peau / tissus mous',
        nl: 'Huid / weke weefsels'
      },
      nested: {
        specify: {
          fieldLabel: {
            en: 'Specify',
            fr: 'Veuillez préciser',
            nl: 'Gelieve te specificeren'
          },
          isActive: false,
          contentType: 'text'
        }
      }
    },
    inv_man: {
      fieldLabel: {
        en: 'In case of non-surgical invasive procedure, suspected invasive manipulation as cause of bloodstream infection',
        fr: 'Si porte d’entrée suspectée manipulation invasive non-chirurgicale, manipulation invasive présumée en cause de la septicémie',
        nl: 'Indien vermoedelijke oorsprong niet-chirurgische invasieve procedure, vermoedelijke invasieve manipulatie als oorzaak van bloedstroominfectie'
      },
      isActive: false,
      contentType: 'choice',
      nested: {
        specify: {
          fieldLabel: {
            en: 'Specify',
            fr: 'Veuillez préciser',
            nl: 'Gelieve te specificeren'
          },
          isActive: false,
          contentType: 'text'
        }
      }
    },
    inv_dev1: {
      fieldLabel: {
        en: 'In case of urinary tract source, did the patient have a catheter or was the patient catheterised in the 2 days preceding the infection?',
        fr: 'Si porte d’entrée suspectée tractus urinaire, patient sondé, ou ayant été sondé dans les 2 jours précédents?',
        nl: 'Indien vermoedelijke oorsprong urinewegen, patiënt met sonde of gesondeerd in de 2 dagen voorafgaande de infectie?'
      },
      isActive: false,
      contentType: 'choice'
    },
    inv_dev2: {
      fieldLabel: {
        en: 'In case of pleuro-pulmonary source, was the patient tracheotomised or intubated in the 2 days preceding the infection?',
        fr: 'Si porte d’entrée suspectée pleuro-pulmonaire, patient intubé, ou trachéotomisé dans les 2 jours précédents?',
        nl: 'Indien vermoedelijke oorsprong pleuro-pulmonair, geïntubeerde of getracheotomiseerde patiënt in de 2 dagen voorafgaande de infectie?'
      },
      isActive: false,
      contentType: 'choice'
    },
    cvc: {
      fieldLabel: {
        en: 'In case of CL, MBI, or unknown source, was CL present in the two days preceding the infection?',
        fr: 'Si porte d’entrée CVC, MBI ou inconnue, CVC présent dans les 2 jours avant l’infection?',
        nl: 'Indien oorsprong CVC, MBI of onbekend, CVC aanwezig in de 2 dagen voorafgaande de infectie?'
      },
      isActive: false,
      contentType: 'choice'
    },
    cvc_dt: {
      fieldLabel: {
        en: 'Date of insertion CL',
        fr: 'Date d’insertion du CVC',
        nl: 'Datum plaatsing CVC'
      },
      isActive: false,
      contentType: 'date'
    },
    cvc_type: {
      fieldLabel: {
        en: 'Type of CL',
        fr: 'Type CVC',
        nl: 'Type CVC'
      },
      isActive: false,
      contentType: 'choice',
      nested: {
        specify: {
          fieldLabel: {
            en: 'Specify',
            fr: 'Veuillez préciser',
            nl: 'Gelieve te specificeren'
          },
          isActive: false,
          contentType: 'text'
        }
      }
    },
    mo_doc: {
      fieldLabel: {
        en: 'Source with microbiological documentation',
        fr: 'Porte d’entrée avec documentation biologique',
        nl: 'Oorsprong met microbiologische documentatie'
      },
      isActive: true,
      contentType: 'choice',
      value: '1',
      selectedOptionLabel: {
        en: 'Yes',
        fr: 'Oui',
        nl: 'Ja'
      }
    },
    efu_st: {
      fieldLabel: {
        en: 'Status end follow-up',
        fr: 'Status fin de follow-up',
        nl: 'Status einde follow-up'
      },
      isActive: true,
      contentType: 'choice',
      value: '3',
      selectedOptionLabel: {
        en: 'Discharged from the hospital',
        fr: 'Sorti de l\'hôpital',
        nl: 'Ontslagen uit het ziekenhuis'
      }
    },
    sample_id: {
      fieldLabel: {
        en: 'Sample ID',
        fr: 'Identification de l\'échantillon',
        nl: 'Identificatie van het staal'
      },
      isActive: true,
      contentType: 'text',
      value: 'fjieomazoiejfaeoriaoriejfapozejf'
    },
    microorganism_1: {
      fieldLabel: {
        en: 'Microorganism 1',
        fr: 'Microorganisme 1',
        nl: 'Micro-organisme 1'
      },
      isActive: true,
      contentType: 'object',
      nested: {
        pathogen_1: {
          fieldLabel: {
            en: 'Microorganism 1',
            fr: 'Microorganisme 1',
            nl: 'Micro-organisme 1'
          },
          isActive: true,
          contentType: 'referencelist',
          value: '4668009',
          labels: {
            en: '4668009 - Genus Yersinia',
            nl: '4668009 - Genus Yersinia',
            fr: '4668009 - Genus Yersinia'
          }
        },
        antibiotic_resistance_1: {
          fieldLabel: {
            en: 'Antimicrobial resistance 1',
            fr: 'Résistance aux antibiotiques 1',
            nl: 'Antimicrobiële resistentie 1'
          },
          isActive: true,
          contentType: 'questionnaire'
        },
        amoxicillin: {
          fieldLabel: {
            en: '18816-5 - Amoxicillin',
            fr: '18816-5 - Amoxicillin',
            nl: '18816-5 - Amoxicillin'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Susceptible',
            fr: 'Sensible',
            nl: 'Gevoelig'
          }
        },
        amoxicillin_clavulanate: {
          fieldLabel: {
            en: '18862-3 - Amoxicillin + Clavulanate',
            fr: '18862-3 - Amoxicillin + Clavulanate',
            nl: '18862-3 - Amoxicillin + Clavulanate'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Intermediate',
            fr: 'Intermédiaire',
            nl: 'Intermediair'
          }
        },
        ampicillin: {
          fieldLabel: {
            en: '18864-9 - Ampicillin',
            fr: '18864-9 - Ampicillin',
            nl: '18864-9 - Ampicillin'
          },
          isActive: false,
          contentType: 'question'
        },
        ampicillin_sulbactam: {
          fieldLabel: {
            en: '18865-6 - Ampicillin + Sulbactam',
            fr: '18865-6 - Ampicillin + Sulbactam',
            nl: '18865-6 - Ampicillin + Sulbactam'
          },
          isActive: false,
          contentType: 'question'
        },
        cefotaxime: {
          fieldLabel: {
            en: '18886-2 - Cefotaxime',
            fr: '18886-2 - Cefotaxime',
            nl: '18886-2 - Cefotaxime'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Resistant',
            fr: 'Résistant',
            nl: 'Resistent'
          }
        },
        ceftazidime: {
          fieldLabel: {
            en: '18893-8 - Ceftazidime',
            fr: '18893-8 - Ceftazidime',
            nl: '18893-8 - Ceftazidime'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Unknown',
            fr: 'Inconnu',
            nl: 'Onbekend'
          }
        },
        ceftriaxone: {
          fieldLabel: {
            en: '18895-3 - Ceftriaxone',
            fr: '18895-3 - Ceftriaxone',
            nl: '18895-3 - Ceftriaxone'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Resistant',
            fr: 'Résistant',
            nl: 'Resistent'
          }
        },
        colistin: {
          fieldLabel: {
            en: '18912-6 - Colistin',
            fr: '18912-6 - Colistin',
            nl: '18912-6 - Colistin'
          },
          isActive: false,
          contentType: 'question'
        },
        imipenem: {
          fieldLabel: {
            en: '18932-4 - Imipenem',
            fr: '18932-4 - Imipenem',
            nl: '18932-4 - Imipenem'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Intermediate',
            fr: 'Intermédiaire',
            nl: 'Intermediair'
          }
        },
        meropenem: {
          fieldLabel: {
            en: '18943-1 - Meropenem',
            fr: '18943-1 - Meropenem',
            nl: '18943-1 - Meropenem'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Susceptible',
            fr: 'Sensible',
            nl: 'Gevoelig'
          }
        },
        oxacillin: {
          fieldLabel: {
            en: '18961-3 - Oxacillin',
            fr: '18961-3 - Oxacillin',
            nl: '18961-3 - Oxacillin'
          },
          isActive: false,
          contentType: 'question'
        },
        piperacillin: {
          fieldLabel: {
            en: '18969-6 - Piperacillin',
            fr: '18969-6 - Piperacillin',
            nl: '18969-6 - Piperacillin'
          },
          isActive: false,
          contentType: 'question'
        },
        piperacillin_tazobactam: {
          fieldLabel: {
            en: '18970-4 - Piperacillin + Tazobactam',
            fr: '18970-4 - Piperacillin + Tazobactam',
            nl: '18970-4 - Piperacillin + Tazobactam'
          },
          isActive: false,
          contentType: 'question'
        },
        teicoplanin: {
          fieldLabel: {
            en: '18989-4 - Teicoplanin',
            fr: '18989-4 - Teicoplanin',
            nl: '18989-4 - Teicoplanin'
          },
          isActive: false,
          contentType: 'question'
        },
        vancomycin: {
          fieldLabel: {
            en: '19000-9 - Vancomycin',
            fr: '19000-9 - Vancomycin',
            nl: '19000-9 - Vancomycin'
          },
          isActive: false,
          contentType: 'question'
        },
        esbl_producing: {
          fieldLabel: {
            en: '6984-9 - Extended Spectrum Beta-Lactamase producing',
            fr: '6984-9 - Extended Spectrum Beta-Lactamase producing',
            nl: '6984-9 - Extended Spectrum Beta-Lactamase producing'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Intermediate',
            fr: 'Intermédiaire',
            nl: 'Intermediair'
          }
        }
      }
    },
    microorganism_2: {
      fieldLabel: {
        en: 'Microorganism 2',
        fr: 'Microorganisme 2',
        nl: 'Micro-organisme 2'
      },
      isActive: true,
      contentType: 'object',
      nested: {
        pathogen_2: {
          fieldLabel: {
            en: 'Microorganism 2',
            fr: 'Microorganisme 2',
            nl: 'Micro-organisme 2'
          },
          isActive: true,
          contentType: 'referencelist',
          value: '7527002',
          labels: {
            en: '7527002 - Genus Legionella',
            nl: '7527002 - Genus Legionella',
            fr: '7527002 - Genus Legionella'
          }
        },
        antibiotic_resistance_2: {
          fieldLabel: {
            en: 'Antimicrobial resistance 2',
            fr: 'Résistance aux antibiotiques 2',
            nl: 'Antimicrobiële resistentie 2'
          },
          isActive: true,
          contentType: 'questionnaire'
        },
        amoxicillin: {
          fieldLabel: {
            en: '18816-5 - Amoxicillin',
            fr: '18816-5 - Amoxicillin',
            nl: '18816-5 - Amoxicillin'
          },
          isActive: false,
          contentType: 'question'
        },
        amoxicillin_clavulanate: {
          fieldLabel: {
            en: '18862-3 - Amoxicillin + Clavulanate',
            fr: '18862-3 - Amoxicillin + Clavulanate',
            nl: '18862-3 - Amoxicillin + Clavulanate'
          },
          isActive: false,
          contentType: 'question'
        },
        ampicillin: {
          fieldLabel: {
            en: '18864-9 - Ampicillin',
            fr: '18864-9 - Ampicillin',
            nl: '18864-9 - Ampicillin'
          },
          isActive: false,
          contentType: 'question'
        },
        ampicillin_sulbactam: {
          fieldLabel: {
            en: '18865-6 - Ampicillin + Sulbactam',
            fr: '18865-6 - Ampicillin + Sulbactam',
            nl: '18865-6 - Ampicillin + Sulbactam'
          },
          isActive: false,
          contentType: 'question'
        },
        cefotaxime: {
          fieldLabel: {
            en: '18886-2 - Cefotaxime',
            fr: '18886-2 - Cefotaxime',
            nl: '18886-2 - Cefotaxime'
          },
          isActive: false,
          contentType: 'question'
        },
        ceftazidime: {
          fieldLabel: {
            en: '18893-8 - Ceftazidime',
            fr: '18893-8 - Ceftazidime',
            nl: '18893-8 - Ceftazidime'
          },
          isActive: false,
          contentType: 'question'
        },
        ceftriaxone: {
          fieldLabel: {
            en: '18895-3 - Ceftriaxone',
            fr: '18895-3 - Ceftriaxone',
            nl: '18895-3 - Ceftriaxone'
          },
          isActive: false,
          contentType: 'question'
        },
        colistin: {
          fieldLabel: {
            en: '18912-6 - Colistin',
            fr: '18912-6 - Colistin',
            nl: '18912-6 - Colistin'
          },
          isActive: false,
          contentType: 'question'
        },
        imipenem: {
          fieldLabel: {
            en: '18932-4 - Imipenem',
            fr: '18932-4 - Imipenem',
            nl: '18932-4 - Imipenem'
          },
          isActive: false,
          contentType: 'question'
        },
        meropenem: {
          fieldLabel: {
            en: '18943-1 - Meropenem',
            fr: '18943-1 - Meropenem',
            nl: '18943-1 - Meropenem'
          },
          isActive: false,
          contentType: 'question'
        },
        oxacillin: {
          fieldLabel: {
            en: '18961-3 - Oxacillin',
            fr: '18961-3 - Oxacillin',
            nl: '18961-3 - Oxacillin'
          },
          isActive: false,
          contentType: 'question'
        },
        piperacillin: {
          fieldLabel: {
            en: '18969-6 - Piperacillin',
            fr: '18969-6 - Piperacillin',
            nl: '18969-6 - Piperacillin'
          },
          isActive: false,
          contentType: 'question'
        },
        piperacillin_tazobactam: {
          fieldLabel: {
            en: '18970-4 - Piperacillin + Tazobactam',
            fr: '18970-4 - Piperacillin + Tazobactam',
            nl: '18970-4 - Piperacillin + Tazobactam'
          },
          isActive: false,
          contentType: 'question'
        },
        teicoplanin: {
          fieldLabel: {
            en: '18989-4 - Teicoplanin',
            fr: '18989-4 - Teicoplanin',
            nl: '18989-4 - Teicoplanin'
          },
          isActive: false,
          contentType: 'question'
        },
        vancomycin: {
          fieldLabel: {
            en: '19000-9 - Vancomycin',
            fr: '19000-9 - Vancomycin',
            nl: '19000-9 - Vancomycin'
          },
          isActive: false,
          contentType: 'question'
        },
        esbl_producing: {
          fieldLabel: {
            en: '6984-9 - Extended Spectrum Beta-Lactamase producing',
            fr: '6984-9 - Extended Spectrum Beta-Lactamase producing',
            nl: '6984-9 - Extended Spectrum Beta-Lactamase producing'
          },
          isActive: false,
          contentType: 'question'
        }
      }
    },
    microorganism_3: {
      fieldLabel: {
        en: 'Microorganism 3',
        fr: 'Microorganisme 3',
        nl: 'Micro-organisme 3'
      },
      isActive: true,
      contentType: 'object',
      nested: {
        pathogen_3: {
          fieldLabel: {
            en: 'Microorganism 3',
            fr: 'Microorganisme 3',
            nl: 'Micro-organisme 3'
          },
          isActive: true,
          contentType: 'referencelist',
          value: '5595000',
          labels: {
            en: '5595000 - Salmonella Typhi (not specified)',
            nl: '5595000 - Salmonella Typhi (not specified)',
            fr: '5595000 - Salmonella Typhi (not specified)'
          }
        },
        antibiotic_resistance_3: {
          fieldLabel: {
            en: 'Antimicrobial resistance 3',
            fr: 'Résistance aux antibiotiques 3',
            nl: 'Antimicrobiële resistentie 3'
          },
          isActive: true,
          contentType: 'questionnaire'
        },
        amoxicillin: {
          fieldLabel: {
            en: '18816-5 - Amoxicillin',
            fr: '18816-5 - Amoxicillin',
            nl: '18816-5 - Amoxicillin'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Susceptible',
            fr: 'Sensible',
            nl: 'Gevoelig'
          }
        },
        amoxicillin_clavulanate: {
          fieldLabel: {
            en: '18862-3 - Amoxicillin + Clavulanate',
            fr: '18862-3 - Amoxicillin + Clavulanate',
            nl: '18862-3 - Amoxicillin + Clavulanate'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Susceptible',
            fr: 'Sensible',
            nl: 'Gevoelig'
          }
        },
        ampicillin: {
          fieldLabel: {
            en: '18864-9 - Ampicillin',
            fr: '18864-9 - Ampicillin',
            nl: '18864-9 - Ampicillin'
          },
          isActive: false,
          contentType: 'question'
        },
        ampicillin_sulbactam: {
          fieldLabel: {
            en: '18865-6 - Ampicillin + Sulbactam',
            fr: '18865-6 - Ampicillin + Sulbactam',
            nl: '18865-6 - Ampicillin + Sulbactam'
          },
          isActive: false,
          contentType: 'question'
        },
        cefotaxime: {
          fieldLabel: {
            en: '18886-2 - Cefotaxime',
            fr: '18886-2 - Cefotaxime',
            nl: '18886-2 - Cefotaxime'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Intermediate',
            fr: 'Intermédiaire',
            nl: 'Intermediair'
          }
        },
        ceftazidime: {
          fieldLabel: {
            en: '18893-8 - Ceftazidime',
            fr: '18893-8 - Ceftazidime',
            nl: '18893-8 - Ceftazidime'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Intermediate',
            fr: 'Intermédiaire',
            nl: 'Intermediair'
          }
        },
        ceftriaxone: {
          fieldLabel: {
            en: '18895-3 - Ceftriaxone',
            fr: '18895-3 - Ceftriaxone',
            nl: '18895-3 - Ceftriaxone'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Resistant',
            fr: 'Résistant',
            nl: 'Resistent'
          }
        },
        colistin: {
          fieldLabel: {
            en: '18912-6 - Colistin',
            fr: '18912-6 - Colistin',
            nl: '18912-6 - Colistin'
          },
          isActive: false,
          contentType: 'question'
        },
        imipenem: {
          fieldLabel: {
            en: '18932-4 - Imipenem',
            fr: '18932-4 - Imipenem',
            nl: '18932-4 - Imipenem'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Resistant',
            fr: 'Résistant',
            nl: 'Resistent'
          }
        },
        meropenem: {
          fieldLabel: {
            en: '18943-1 - Meropenem',
            fr: '18943-1 - Meropenem',
            nl: '18943-1 - Meropenem'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Unknown',
            fr: 'Inconnu',
            nl: 'Onbekend'
          }
        },
        oxacillin: {
          fieldLabel: {
            en: '18961-3 - Oxacillin',
            fr: '18961-3 - Oxacillin',
            nl: '18961-3 - Oxacillin'
          },
          isActive: false,
          contentType: 'question'
        },
        piperacillin: {
          fieldLabel: {
            en: '18969-6 - Piperacillin',
            fr: '18969-6 - Piperacillin',
            nl: '18969-6 - Piperacillin'
          },
          isActive: false,
          contentType: 'question'
        },
        piperacillin_tazobactam: {
          fieldLabel: {
            en: '18970-4 - Piperacillin + Tazobactam',
            fr: '18970-4 - Piperacillin + Tazobactam',
            nl: '18970-4 - Piperacillin + Tazobactam'
          },
          isActive: false,
          contentType: 'question'
        },
        teicoplanin: {
          fieldLabel: {
            en: '18989-4 - Teicoplanin',
            fr: '18989-4 - Teicoplanin',
            nl: '18989-4 - Teicoplanin'
          },
          isActive: false,
          contentType: 'question'
        },
        vancomycin: {
          fieldLabel: {
            en: '19000-9 - Vancomycin',
            fr: '19000-9 - Vancomycin',
            nl: '19000-9 - Vancomycin'
          },
          isActive: false,
          contentType: 'question'
        },
        esbl_producing: {
          fieldLabel: {
            en: '6984-9 - Extended Spectrum Beta-Lactamase producing',
            fr: '6984-9 - Extended Spectrum Beta-Lactamase producing',
            nl: '6984-9 - Extended Spectrum Beta-Lactamase producing'
          },
          isActive: true,
          contentType: 'question',
          answerLabel: {
            en: 'Unknown',
            fr: 'Inconnu',
            nl: 'Onbekend'
          }
        }
      }
    }
  }
};

var EXPECTED_RESULT = {
  treating_physician: {
    value: 'NIHDI-code of the treating physician: 10002678140 - DURNEZ JOKE',
    nested: {
      nihdicode_of_the_treating_physician: {
        value: '10002678140 - DURNEZ JOKE',
        nested: {}
      }
    }
  },
  patient_id: {
    value: '20001109MOCHM',
    nested: {
      internal_patient_id: {
        value: '',
        nested: {}
      },
      name: {
        value: 'Mostaza',
        nested: {}
      },
      first_name: {
        value: 'Chips',
        nested: {}
      },
      date_of_birth: {
        value: '09-11-2000',
        nested: {}
      },
      sex: {
        value: 'Male',
        nested: {}
      },
      deceased: {
        value: 'No',
        nested: {}
      },
      date_of_death: {
        value: '',
        nested: {}
      },
      place_of_residence: {
        value: '',
        nested: {}
      }
    }
  },
  admission_id: {
    value: '',
    nested: {}
  },
  admission_date: {
    value: '07-01-2017',
    nested: {}
  },
  ward_admission_date: {
    value: '11-01-2017',
    nested: {}
  },
  installation_id: {
    value: '11111111',
    nested: {}
  },
  site_number: {
    value: '1000 - ALGEMEEN ZIEKENHUIS ST. LUCAS',
    nested: {}
  },
  ward_id: {
    value: '2300 - VE Ped - 89150',
    nested: {}
  },
  ward_type: {
    value: '309945009 - Pediatric department',
    nested: {}
  },
  efu_dt: {
    value: '30-01-2017',
    nested: {}
  },
  infection_date: {
    value: '15-01-2017',
    nested: {}
  },
  sep_cl: {
    value: '',
    nested: {
      specify: {
        value: '',
        nested: {}
      }
    }
  },
  case_def: {
    value: 'At least one hemoculture positive for a recognised pathogen',
    nested: {}
  },
  infection_source: {
    value: 'Skin / soft tissue',
    nested: {
      specify: {
        value: '',
        nested: {}
      }
    }
  },
  inv_man: {
    value: '',
    nested: {
      specify: {
        value: '',
        nested: {}
      }
    }
  },
  inv_dev1: {
    value: '',
    nested: {}
  },
  inv_dev2: {
    value: '',
    nested: {}
  },
  cvc: {
    value: '',
    nested: {}
  },
  cvc_dt: {
    value: '',
    nested: {}
  },
  cvc_type: {
    value: '',
    nested: {
      specify: {
        value: '',
        nested: {}
      }
    }
  },
  mo_doc: {
    value: 'Yes',
    nested: {}
  },
  efu_st: {
    value: 'Discharged from the hospital',
    nested: {}
  },
  sample_id: {
    value: 'fjieomazoiejfaeoriaoriejfapozejf',
    nested: {}
  },
  microorganism_1: {
    value: '',
    nested: {
      pathogen_1: {
        value: '4668009 - Genus Yersinia',
        nested: {}
      },
      antibiotic_resistance_1: {
        value: '',
        nested: {}
      },
      amoxicillin: {
        value: 'Susceptible',
        nested: {}
      },
      amoxicillin_clavulanate: {
        value: 'Intermediate',
        nested: {}
      },
      ampicillin: {
        value: '',
        nested: {}
      },
      ampicillin_sulbactam: {
        value: '',
        nested: {}
      },
      cefotaxime: {
        value: 'Resistant',
        nested: {}
      },
      ceftazidime: {
        value: 'Unknown',
        nested: {}
      },
      ceftriaxone: {
        value: 'Resistant',
        nested: {}
      },
      colistin: {
        value: '',
        nested: {}
      },
      imipenem: {
        value: 'Intermediate',
        nested: {}
      },
      meropenem: {
        value: 'Susceptible',
        nested: {}
      },
      oxacillin: {
        value: '',
        nested: {}
      },
      piperacillin: {
        value: '',
        nested: {}
      },
      piperacillin_tazobactam: {
        value: '',
        nested: {}
      },
      teicoplanin: {
        value: '',
        nested: {}
      },
      vancomycin: {
        value: '',
        nested: {}
      },
      esbl_producing: {
        value: 'Intermediate',
        nested: {}
      }
    }
  },
  microorganism_2: {
    value: '',
    nested: {
      pathogen_2: {
        value: '7527002 - Genus Legionella',
        nested: {}
      },
      antibiotic_resistance_2: {
        value: '',
        nested: {}
      },
      amoxicillin: {
        value: '',
        nested: {}
      },
      amoxicillin_clavulanate: {
        value: '',
        nested: {}
      },
      ampicillin: {
        value: '',
        nested: {}
      },
      ampicillin_sulbactam: {
        value: '',
        nested: {}
      },
      cefotaxime: {
        value: '',
        nested: {}
      },
      ceftazidime: {
        value: '',
        nested: {}
      },
      ceftriaxone: {
        value: '',
        nested: {}
      },
      colistin: {
        value: '',
        nested: {}
      },
      imipenem: {
        value: '',
        nested: {}
      },
      meropenem: {
        value: '',
        nested: {}
      },
      oxacillin: {
        value: '',
        nested: {}
      },
      piperacillin: {
        value: '',
        nested: {}
      },
      piperacillin_tazobactam: {
        value: '',
        nested: {}
      },
      teicoplanin: {
        value: '',
        nested: {}
      },
      vancomycin: {
        value: '',
        nested: {}
      },
      esbl_producing: {
        value: '',
        nested: {}
      }
    }
  },
  microorganism_3: {
    value: '',
    nested: {
      pathogen_3: {
        value: '5595000 - Salmonella Typhi (not specified)',
        nested: {}
      },
      antibiotic_resistance_3: {
        value: '',
        nested: {}
      },
      amoxicillin: {
        value: 'Susceptible',
        nested: {}
      },
      amoxicillin_clavulanate: {
        value: 'Susceptible',
        nested: {}
      },
      ampicillin: {
        value: '',
        nested: {}
      },
      ampicillin_sulbactam: {
        value: '',
        nested: {}
      },
      cefotaxime: {
        value: 'Intermediate',
        nested: {}
      },
      ceftazidime: {
        value: 'Intermediate',
        nested: {}
      },
      ceftriaxone: {
        value: 'Resistant',
        nested: {}
      },
      colistin: {
        value: '',
        nested: {}
      },
      imipenem: {
        value: 'Resistant',
        nested: {}
      },
      meropenem: {
        value: 'Unknown',
        nested: {}
      },
      oxacillin: {
        value: '',
        nested: {}
      },
      piperacillin: {
        value: '',
        nested: {}
      },
      piperacillin_tazobactam: {
        value: '',
        nested: {}
      },
      teicoplanin: {
        value: '',
        nested: {}
      },
      vancomycin: {
        value: '',
        nested: {}
      },
      esbl_producing: {
        value: 'Unknown',
        nested: {}
      }
    }
  }
};
