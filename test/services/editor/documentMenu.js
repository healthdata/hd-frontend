/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('DocumentMenu', function () {

  beforeEach(module('LocalStorageModule'));
  beforeEach(module('skryv.conf'));
  beforeEach(module('gettext'));
  beforeEach(module('skryv.listView')); // for filterDate
  beforeEach(module('services.editor'));

  var CurrentDocument;

  beforeEach(function () {

    CurrentDocument = {
      currentWorkflow: sinon.stub(),
      currentDefinition: sinon.stub(),
      isValidDocument: sinon.stub()
    };

    module({
      ngDialog: {},
      $route: {},
      WorkflowActions: {},
      CurrentDocument: CurrentDocument
    });
  });

  var DocumentMenu;
  var _;

  beforeEach(inject(function (_DocumentMenu_, ___) {
    DocumentMenu = _DocumentMenu_;
    _ = ___;
  }));

  describe('.makeMenu()', function () {

    function expectPresent(items, name) {
      var names = _.pluck(items, 'name');
      expect(name).to.be.oneOf(names);
    }

    function expectAbsent(items, name) {
      var names = _.pluck(items, 'name');
      expect(name).to.not.be.oneOf(names);
    }

    function expectEmpty(items) {
      expect(items).to.have.lengthOf(0);
    }

    function expectItemClass(items, name, expectedResult) {
      var item = _.findWhere(items, { name: name });
      var itemClass = item.class;
      if (_.isFunction(itemClass)) {
        itemClass = itemClass();
      }
      expect(itemClass).to.deep.equal(expectedResult);
    }

    function expectDisabledHelpText(items, name, expectedPartialResult) {
      var item = _.findWhere(items, { name: name });
      var itemDisabledHelptext = item.disabledHelptext;
      if (_.isFunction(itemDisabledHelptext)) {
        itemDisabledHelptext = itemDisabledHelptext();
      }
      expect(itemDisabledHelptext).to.contain(expectedPartialResult);
    }

    it('should produce no items when there is no workflow', function () {
      CurrentDocument.currentWorkflow.returns(null);
      var items = DocumentMenu.makeMenu();
      expect(items).to.have.lengthOf(0);
    });

    it('should produce no items for a new workflow in a collection without a start date', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: null
        }
      };
      var workflow = {
        status: 'IN_PROGRESS',
        newWorkflow: true
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectEmpty(items);
    });

    it('should produce no items for a new workflow in a collection that has not started yet', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '2123-11-12' // well in the future
        }
      };
      var workflow = {
        status: 'IN_PROGRESS',
        newWorkflow: true
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectEmpty(items);
    });

    it('should produce no items for a new workflow in a closed collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12',         // well in the past
          endDateSubmission: '1989-11-12', // also in the past
          endDateComments: '1989-11-12'    // also in the past
        }
      };
      var workflow = {
        status: 'IN_PROGRESS',
        newWorkflow: true
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectEmpty(items);
    });

    it('should return "Save" and (disabled) "Submit" items for a new workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12' // well in the past
        }
      };
      var workflow = {
        status: 'IN_PROGRESS',
        newWorkflow: true
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Save');
      expectPresent(items, 'Submit');
      expectAbsent(items, 'Delete');
      expectItemClass(items, 'Submit', { disabled: true });
      expectDisabledHelpText(items, 'Submit', 'validation errors');
    });

    it('should return enabled "Submit" item for a _valid_ new workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12' // well in the past
        }
      };
      var workflow = {
        status: 'IN_PROGRESS',
        newWorkflow: true
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      CurrentDocument.isValidDocument.returns(true);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Save');
      expectPresent(items, 'Submit');
      expectItemClass(items, 'Submit', { disabled: false });
    });

    it('should return "Save" and (disabled) "Submit" items for an IN_PROGRESS workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12' // well in the past
        }
      };
      var workflow = {
        status: 'IN_PROGRESS'
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Save');
      expectPresent(items, 'Submit');
      expectItemClass(items, 'Submit', { disabled: true });
      expectDisabledHelpText(items, 'Submit', 'validation errors');
    });

    it('should return enabled "Submit" item for a _valid_ IN_PROGRESS workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12',        // well in the past
          endDateSubmission: '2123-11-12' // well in the future
        }
      };
      var workflow = {
        status: 'IN_PROGRESS'
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      CurrentDocument.isValidDocument.returns(true);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Save');
      expectPresent(items, 'Submit');
      expectItemClass(items, 'Submit', { disabled: false });
    });

    it('should return "Reopen" item for a SUBMITTED workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12' // well in the past
        }
      };
      var workflow = {
        status: 'SUBMITTED'
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Reopen');
      expectAbsent(items, 'Save');
      expectAbsent(items, 'Submit');
      expectAbsent(items, 'Delete');
    });

    it('should return "Print" item for a SUBMITTED workflow in a closed collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12',         // well in the past
          endDateSubmission: '1989-11-12', // also in the past
          endDateComments: '1989-11-12'    // also in the past
        }
      };
      var workflow = {
        status: 'SUBMITTED'
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Print');
      expectAbsent(items, 'Reopen');
      expectAbsent(items, 'Save');
      expectAbsent(items, 'Submit');
    });

    it('should return (disabled) "Re-submit" item for a SUBMISSION_FAILED workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12' // well in the past
        }
      };
      var workflow = {
        status: 'SUBMISSION_FAILED'
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Print');
      expectPresent(items, 'Re-submit');
      expectAbsent(items, 'Reopen');
      expectAbsent(items, 'Save');
      expectAbsent(items, 'Submit');
      expectItemClass(items, 'Re-submit', { disabled: true });
    });

    it('should return enabled "Re-submit" item for a _valid_ SUBMISSION_FAILED workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12' // well in the past
        }
      };
      var workflow = {
        status: 'SUBMISSION_FAILED'
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Print');
      expectPresent(items, 'Re-submit');
      expectAbsent(items, 'Reopen');
      expectAbsent(items, 'Save');
      expectAbsent(items, 'Submit');
      expectItemClass(items, 'Re-submit', { disabled: true });
    });

    it('should return "Delete" item for a deletable workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12' // well in the past
        }
      };
      var workflow = {
        status: 'IN_PROGRESS',
        deletable: true
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Delete');
      expectPresent(items, 'Submit');
      expectPresent(items, 'Save');
      expectItemClass(items, 'Delete', 'btn-new-second');
    });

    it('should return a (disabled) "Re-submit" item for an ACTION_NEEDED workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12' // well in the past
        }
      };
      var workflow = {
        status: 'ACTION_NEEDED'
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Save');
      expectPresent(items, 'Re-submit');
      expectAbsent(items, 'Submit');
      expectItemClass(items, 'Re-submit', { disabled: true });
      expectDisabledHelpText(items, 'Re-submit', 'validation errors');
    });

    it('should return a enabled "Re-submit" item for a _valid_ ACTION_NEEDED workflow in an open collection', function () {
      var definition = {
        dataCollectionGroup: {
          startDate: '1987-11-12',      // well in the past
          endDateComments: '2123-11-12' // well in the future
        }
      };
      var workflow = {
        status: 'ACTION_NEEDED'
      };
      CurrentDocument.currentDefinition.returns(definition);
      CurrentDocument.currentWorkflow.returns(workflow);
      CurrentDocument.isValidDocument.returns(true);
      var items = DocumentMenu.makeMenu();
      expectPresent(items, 'Save');
      expectPresent(items, 'Re-submit');
      expectAbsent(items, 'Submit');
      expectItemClass(items, 'Re-submit', { disabled: false });
    });

  });

});
