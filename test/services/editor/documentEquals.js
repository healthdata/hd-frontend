/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('documentEquals', function () {

  beforeEach(module('services.editor'));

  var documentEquals;

  beforeEach(inject(function (_documentEquals_) {
    documentEquals = _documentEquals_;
  }));

  function expectEqual(left, right) {
    expect(documentEquals(left, right)).to.be.true;
  }

  function expectDistinct(left, right) {
    expect(documentEquals(left, right)).to.be.false;
  }

  it('should perform a deep comparison', function () {
    var left = {
      patientID: {},
      content: {
        value: {
          foo: 42
        }
      }
    };
    var right = {
      patientID: {},
      content: {
        value: {
          foo: 42
        }
      }
    };
    expectEqual(left, right);
  });

  it('should detect different field values', function () {
    var left = {
      content: {
        value: {
          foo: 42
        }
      }
    };
    var right = {
      content: {
        value: {
          foo: 101
        }
      }
    };
    expectDistinct(left, right);
  });

  it('should ignore UPDATED_ON in patientID', function () {
    var left = {
      patientID: {
        UPDATED_ON: 'foo'
      },
      content: {
        value: {}
      }
    };
    var right = {
      patientID: {
        UPDATED_ON: 'bar'
      },
      content: {
        value: {}
      }
    };
    expectEqual(left, right);
  });

  it('should normalize date_of_birth', function () {
    var left = {
      patientID: {
        date_of_birth: '"1987-11-23T00:00:00+05:00"'
      },
      content: {
        value: {}
      }
    };
    var right = {
      patientID: {
        date_of_birth: '"1987-11-23T01:00:00+06:00"'
      },
      content: {
        value: {}
      }
    };
    expectEqual(left, right);
  });

  it('should identity empty and absent PATIENT_IDENTIFIER', function () {
    var left = {
      patientID: {
        PATIENT_IDENTIFIER: ''
      },
      content: {
        value: {}
      }
    };
    var right = {
      patientID: {},
      content: {
        value: {}
      }
    };
    expectEqual(left, right);
  });

  it('should look for sex in content', function () {
    var left = {
      patientID: {
        sex: '{"selectedOption":"F"}'
      },
      content: {
        value: {
          patient_id: {
            sex: { selectedOption: 'F' }
          }
        }
      }
    };
    var right = {
      patientID: {},
      content: {
        value: {
          patient_id: {
            sex: { selectedOption: 'F' }
          }
        }
      }
    };
    expectEqual(left, right);
  });

  it('should detect different private field values', function () {
    var left = {
      content: {
        value: {}
      },
      private: {
        foo: 42
      }
    };
    var right = {
      content: {
        value: {}
      },
      private: {
        foo: 101
      }
    };
    expectDistinct(left, right);
  });

});
