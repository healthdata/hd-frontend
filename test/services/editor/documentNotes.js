/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('documentNotes', function () {

  beforeEach(module('skryv.conf'));
  beforeEach(module('services.workflow'));
  beforeEach(module('services.editor'));

  var $rootScope;
  var $q;

  var CurrentDocument;
  var WorkflowNotes;
  var SkryvSession;
  var DocumentNotes;

  beforeEach(function () {

    SkryvSession = {
      getLoggedUser: sinon.stub(),
      loadApp: sinon.stub()
    };

    CurrentDocument = {
      currentWorkflow: sinon.stub(),
      saveItAnyway: sinon.stub()
    };

    WorkflowNotes = {
      save: sinon.mock(),
      update: sinon.mock(),
      delete: sinon.mock()
    };

    module({
      CurrentDocument: CurrentDocument,
      WorkflowNotes: WorkflowNotes,
      SkryvSession: SkryvSession
    });
  });

  beforeEach(inject(function (_$rootScope_, _$q_, _DocumentNotes_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    DocumentNotes = _DocumentNotes_;
  }));

  function flush() {
    $rootScope.$digest();
  }

  describe('.addNode()', function () {

    it('should add a note to an new workflow', function () {
      var workflowID = 'abc123';
      var workflow = { /* no id yet */ };

      var title = 'Note title';
      var content = 'Note content';

      var expectedNote = {
        title: title,
        comments: [ { content: content } ]
      };

      DocumentNotes.resetInternalState([]);

      CurrentDocument.currentWorkflow.returns(workflow);

      var workflowAfterSaving = { id: workflowID };
      CurrentDocument.saveItAnyway.returns($q.when(workflowAfterSaving));

      var savedNote = {
        markerForTest: {}
      };

      WorkflowNotes.save
        .once()
        .withArgs(workflowID, expectedNote)
        .returns($q.when(savedNote));

      var callback = sinon.spy();

      DocumentNotes.addNote(title, content).then(callback);

      flush();

      WorkflowNotes.save.verify();
      expect(callback).to.have.been.calledOnce;
    });

    it('should add a note to an existing workflow', function () {
      var workflowID = 'abc123';
      var workflow = { id: workflowID };

      var title = 'Note title';
      var content = 'Note content';

      var expectedNote = {
        title: title,
        comments: [ { content: content } ]
      };

      DocumentNotes.resetInternalState([]);

      CurrentDocument.currentWorkflow.returns(workflow);

      var savedNote = {
        markerForTest: {}
      };

      WorkflowNotes.save
        .once()
        .withArgs(workflowID, expectedNote)
        .returns($q.when(savedNote));

      var callback = sinon.spy();

      DocumentNotes.addNote(title, content).then(callback);

      flush();

      WorkflowNotes.save.verify();
      expect(callback).to.have.been.calledOnce;
      expect(DocumentNotes.notes()).to.contain(savedNote);
    });

  });

  describe('.addComment()', function () {

    it('should add a comment to an existing note', function () {

      var note = {
        comments: []
      };
      var originalNote = angular.copy(note);

      var comment = 'Hello';

      DocumentNotes.resetInternalState([ note ]);

      var expectedNoteForUpdate = {
        comments: [ { content: comment } ]
      };

      var updatedNote = {
        markerForTest: {},
        comments: [ { content: comment } ]
      };

      WorkflowNotes.update
        .once()
        .withArgs(expectedNoteForUpdate)
        .returns($q.when(updatedNote));

      var callback = sinon.spy();

      DocumentNotes.addComment(comment, note).then(callback);

      flush();

      expect(callback).to.have.been.calledOnce;
      WorkflowNotes.update.verify();
      expect(DocumentNotes.notes()[0]).to.equal(updatedNote);
      expect(note).to.deep.equal(originalNote);

    });

    it('should replace the correct note and leave the rest unchanged', function () {

      var firstNote = {};
      var thirdNote = {};
      var note = {
        comments: []
      };

      var allNotes = [ firstNote, note, thirdNote ];
      var originalNote = angular.copy(note);

      DocumentNotes.resetInternalState(allNotes);

      var updatedNote = {
        markerForTest: {}
      };

      WorkflowNotes.update
        .returns($q.when(updatedNote));

      var callback = sinon.spy();

      DocumentNotes.addComment('Hello', note).then(callback);

      flush();

      expect(callback).to.have.been.calledOnce;

      var newNotes = DocumentNotes.notes();
      expect(newNotes).to.have.lengthOf(3);
      expect(newNotes[0]).to.equal(firstNote);
      expect(newNotes[1]).to.equal(updatedNote);
      expect(newNotes[2]).to.equal(thirdNote);

      expect(note).to.deep.equal(originalNote);

    });

  });

  describe('.editComment()', function () {

    it('should update the content of an existing comment in a note', function () {

      var note = {
        comments: [
          { content: 'one' },
          { content: 'two' },
          { content: 'three' }
        ]
      };
      var originalNote = angular.copy(note);

      var comment = note.comments[1];

      var newContent = 'Hello';

      DocumentNotes.resetInternalState([ note ]);

      var expectedNoteForUpdate = {
        comments: [
          { content: 'one' },
          { content: newContent },
          { content: 'three' }
        ]
      };

      var updatedNote = {
        markerForTest: {},
        comments: []
      };

      WorkflowNotes.update
        .once()
        .withArgs(expectedNoteForUpdate)
        .returns($q.when(updatedNote));

      var callback = sinon.spy();

      DocumentNotes.editComment(newContent, comment, note).then(callback);

      flush();

      expect(callback).to.have.been.calledOnce;
      WorkflowNotes.update.verify();
      expect(DocumentNotes.notes()[0]).to.equal(updatedNote);
      expect(note).to.deep.equal(originalNote);

    });

  });

  describe('.deleteComment()', function () {

    it('should delete a comment from an existing note', function () {
      var note = {
        comments: [
          { content: 'one' },
          { content: 'two' },
          { content: 'three' }
        ]
      };
      var originalNote = angular.copy(note);

      var comment = note.comments[1];

      DocumentNotes.resetInternalState([ note ]);

      var expectedNoteForUpdate = {
        comments: [
          { content: 'one' },
          { content: 'three' }
        ]
      };

      var updatedNote = {
        markerForTest: {},
        comments: []
      };

      WorkflowNotes.update
        .once()
        .withArgs(expectedNoteForUpdate)
        .returns($q.when(updatedNote));

      var callback = sinon.spy();

      DocumentNotes.deleteComment(comment, note).then(callback);

      flush();

      expect(callback).to.have.been.calledOnce;
      WorkflowNotes.update.verify();
      expect(DocumentNotes.notes()[0]).to.equal(updatedNote);
      expect(note).to.deep.equal(originalNote);
    });

    it('should delete the entire note when the last comment is deleted', function () {
      var note = {
        comments: [
          { content: 'one' }
        ]
      };

      var comment = note.comments[1];

      DocumentNotes.resetInternalState([ note ]);

      var expectedNoteForDelete = {
        comments: [
          { content: 'one' }
        ]
      };

      WorkflowNotes.delete
        .once()
        .withArgs(expectedNoteForDelete)
        .returns($q.when());

      var callback = sinon.spy();

      DocumentNotes.deleteComment(comment, note).then(callback);

      flush();

      expect(callback).to.have.been.calledOnce;
      WorkflowNotes.delete.verify();
      expect(DocumentNotes.notes()).to.be.empty;
    });

  });

  describe('.toggleSolved()', function () {

    it('should set "resolved" from true to false', function () {
      var note = {
        resolved: true
      };
      var originalNote = angular.copy(note);

      DocumentNotes.resetInternalState([ note ]);

      var expectedNoteForUpdate = {
        resolved: false
      };

      var updatedNote = {
        markerForTest: {}
      };

      WorkflowNotes.update
        .once()
        .withArgs(expectedNoteForUpdate)
        .returns($q.when(updatedNote));

      var callback = sinon.spy();

      DocumentNotes.toggleSolved(note).then(callback);

      flush();

      expect(callback).to.have.been.calledOnce;
      WorkflowNotes.update.verify();
      expect(DocumentNotes.notes()[0]).to.equal(updatedNote);
      expect(note).to.deep.equal(originalNote);
    });

    it('should set "resolved" from false to true', function () {
      var note = {
        resolved: false
      };
      var originalNote = angular.copy(note);

      DocumentNotes.resetInternalState([ note ]);

      var expectedNoteForUpdate = {
        resolved: true
      };

      var updatedNote = {
        markerForTest: {}
      };

      WorkflowNotes.update
        .once()
        .withArgs(expectedNoteForUpdate)
        .returns($q.when(updatedNote));

      var callback = sinon.spy();

      DocumentNotes.toggleSolved(note).then(callback);

      flush();

      expect(callback).to.have.been.calledOnce;
      WorkflowNotes.update.verify();
      expect(DocumentNotes.notes()[0]).to.equal(updatedNote);
      expect(note).to.deep.equal(originalNote);
    });

    it('should set "resolved" from undefined to true', function () {
      var note = {
        resolved: undefined
      };
      var originalNote = angular.copy(note);

      DocumentNotes.resetInternalState([ note ]);

      var expectedNoteForUpdate = {
        resolved: true
      };

      var updatedNote = {
        markerForTest: {}
      };

      WorkflowNotes.update
        .once()
        .withArgs(expectedNoteForUpdate)
        .returns($q.when(updatedNote));

      var callback = sinon.spy();

      DocumentNotes.toggleSolved(note).then(callback);

      flush();

      expect(callback).to.have.been.calledOnce;
      WorkflowNotes.update.verify();
      expect(DocumentNotes.notes()[0]).to.equal(updatedNote);
      expect(note).to.deep.equal(originalNote);
    });

  });

});
