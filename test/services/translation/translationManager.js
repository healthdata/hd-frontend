/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('translationModule', function () {

  var $httpBackend, SESSION_SERVER_URL, SkryvTranslation, callback, $q;

  beforeEach(module('gettext'));
  beforeEach(module('skryv.translation'));

  beforeEach(inject(function (_$httpBackend_, _SESSION_SERVER_URL_, _SkryvTranslation_, _$q_) {
    $httpBackend = _$httpBackend_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    SkryvTranslation = _SkryvTranslation_;
    $q = _$q_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  beforeEach(function () {
    callback = sinon.spy();
  });

  describe('.getTranslations()', function () {
    it('should get all translations' , function () {
      var requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/translations');
      var translations = [ 1, 2, 3 ];
      requestHandler.respond([ 1, 2, 3 ]);
      SkryvTranslation.getTranslations().then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal(translations);
    });

    it('should get all translations for a language' , function () {
      var requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/translations' + '?language=nl');
      var translations = [ 1, 2, 3 ];
      requestHandler.respond([ 1, 2, 3 ]);
      SkryvTranslation.getTranslations('nl').then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal(translations);
    });

    it('should fail getting all translation' , function () {
      var requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/translations');
      requestHandler.respond(401);
      SkryvTranslation.getTranslations().catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Translations not found');
    });
  });

  describe('.storeTranslations()', function () {
    it('should store a translation' , function () {
      var requestHandler = $httpBackend.expectPOST(SESSION_SERVER_URL + '/translations/manager');
      var translations = [ 1, 2, 3 ];
      requestHandler.respond({ data: [ 1, 2, 3 ] });
      SkryvTranslation.storeTranslations({ key: 'test' }).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result.data).to.deep.equal(translations);
    });

    it('should fail storing a translation' , function () {
      var requestHandler = $httpBackend.expectPOST(SESSION_SERVER_URL + '/translations/manager');
      requestHandler.respond(401);
      SkryvTranslation.storeTranslations({ key: 'test' }).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not save translations');
    });
  });

  describe('.updateTranslations()', function () {
    it('should update all translations' , function () {
      var requestHandler = $httpBackend.expectPUT(SESSION_SERVER_URL + '/translations/manager');
      var translations = [ 1, 2, 3 ];
      requestHandler.respond({ data: [ 1, 2, 3 ] });
      SkryvTranslation.updateTranslations({ key: 'test' }).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result.data).to.deep.equal(translations);
    });

    it('should fail updating a translation' , function () {
      var requestHandler = $httpBackend.expectPUT(SESSION_SERVER_URL + '/translations/manager');
      requestHandler.respond(401);
      SkryvTranslation.updateTranslations({ key: 'test' }).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not update translations');
    });
  });
});
