/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('SkryvParticipation', function () {
  var SkryvParticipation, $httpBackend, requestHandler, callback, PARTICIPATION_SERVER_URL;

  beforeEach(module('gettext'));
  beforeEach(module('services.participation'));

  beforeEach(inject(function (_SkryvParticipation_, _$httpBackend_, _PARTICIPATION_SERVER_URL_) {
    $httpBackend = _$httpBackend_;
    PARTICIPATION_SERVER_URL = _PARTICIPATION_SERVER_URL_;
    SkryvParticipation = _SkryvParticipation_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.get()', function () {

    it('should get a single participation', function () {
      requestHandler = $httpBackend.expectGET(PARTICIPATION_SERVER_URL + '3');
      callback = sinon.spy();
      requestHandler.respond({ id: 3, name: 'TEST' });
      SkryvParticipation.get(3).then(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal({ id: 3, name: 'TEST' });
    });

    it('should fail get a single participation', function () {
      requestHandler = $httpBackend.expectGET(PARTICIPATION_SERVER_URL + '3');
      callback = sinon.spy();
      requestHandler.respond(401);
      SkryvParticipation.get(3).catch(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Participation not found');
    });

  });

  describe('.getAll()', function () {

    it('should get all participations (in progress, finished)', function () {
      requestHandler = $httpBackend.expectGET(PARTICIPATION_SERVER_URL + '?dataCollectionDefinitionId=3');
      callback = sinon.spy();
      requestHandler.respond([ { id: 1, name: 'TEST' } ]);
      SkryvParticipation.getAll({ dataCollectionDefinitionId: 3 }).then(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal([ { id: 1, name: 'TEST' } ]);
    });

    it('should fail getting all participations (in progress, finished)', function () {
      requestHandler = $httpBackend.expectGET(PARTICIPATION_SERVER_URL + '?dataCollectionDefinitionId=3');
      callback = sinon.spy();
      requestHandler.respond(401);
      SkryvParticipation.getAll({ dataCollectionDefinitionId: 3 }).catch(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not load participations');
    });

  });

  describe('.getNonParticipants()', function () {

    it('should get all participations (non-participating)', function () {
      requestHandler = $httpBackend.expectGET(PARTICIPATION_SERVER_URL + 'nonparticipating?dataCollectionDefinitionId=3');
      callback = sinon.spy();
      requestHandler.respond([ { id: 1, name: 'TEST' } ]);
      SkryvParticipation.getNonParticipants({ dataCollectionDefinitionId: 3 }).then(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal([ { id: 1, name: 'TEST' } ]);
    });

    it('should fail getting all participations (non-participating)', function () {
      requestHandler = $httpBackend.expectGET(PARTICIPATION_SERVER_URL + 'nonparticipating?dataCollectionDefinitionId=3');
      callback = sinon.spy();
      requestHandler.respond(401);
      SkryvParticipation.getNonParticipants({ dataCollectionDefinitionId: 3 }).catch(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not load non-participants');
    });

  });

  describe('.getDocument()', function () {

    it('should get participation document', function () {
      requestHandler = $httpBackend.expectGET(PARTICIPATION_SERVER_URL + '2/documents');
      callback = sinon.spy();
      requestHandler.respond({ name: 'doc' });
      SkryvParticipation.getDocument(2).then(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal({ name: 'doc' });
    });

    it('should fail participation document', function () {
      requestHandler = $httpBackend.expectGET(PARTICIPATION_SERVER_URL + '2/documents');
      callback = sinon.spy();
      requestHandler.respond(401, 'Not working');
      SkryvParticipation.getDocument(2).catch(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result.data).to.deep.equal('Not working');
    });

  });

  describe('.action()', function () {

    it('should do participation action', function () {
      requestHandler = $httpBackend.expectPOST(PARTICIPATION_SERVER_URL + 'actions', { action: 'SAVE' });
      callback = sinon.spy();
      requestHandler.respond({ name: 'doc' });
      SkryvParticipation.action({ action: 'SAVE' }).then(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal({ name: 'doc' });
    });

    it('should fail doing action', function () {
      requestHandler = $httpBackend.expectPOST(PARTICIPATION_SERVER_URL + 'actions', { action: 'SAVE' });
      callback = sinon.spy();
      requestHandler.respond(401, 'Not working');
      SkryvParticipation.action({ action: 'SAVE' }).catch(callback);
      $httpBackend.flush();
      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result.data).to.deep.equal('Not working');
    });

  });
});
