/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('EnvironmentConfig', function () {

  beforeEach(module('services.configurations'));

  var SkryvConfig;

  beforeEach(function () {

    SkryvConfig = {
      getConfigByKey: sinon.stub()
    };

    module({
      SkryvConfig: SkryvConfig
    });
  });

  var EnvironmentConfig;
  var $q, $rootScope;

  beforeEach(inject(function (_$rootScope_, _$q_, _EnvironmentConfig_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    EnvironmentConfig = _EnvironmentConfig_;
  }));

  function flush() {
    $rootScope.$digest();
  }

  describe('.isDdeEnabled()', function () {

    it('should return false when USE_DATA_DEFINITION_EDITOR is not set', function () {
      SkryvConfig.getConfigByKey
        .withArgs('USE_DATA_DEFINITION_EDITOR')
        .returns($q.when([]));

      var callback = sinon.spy();
      EnvironmentConfig.isDdeEnabled().then(callback);

      flush();

      expect(callback).to.be.calledOnce;
      expect(callback).to.be.calledWithExactly(false);
    });

    it('should return true when USE_DATA_DEFINITION_EDITOR is set', function () {
      SkryvConfig.getConfigByKey
        .withArgs('USE_DATA_DEFINITION_EDITOR')
        .returns($q.when([ { value: 'True' } ]));

      var callback = sinon.spy();
      EnvironmentConfig.isDdeEnabled().then(callback);

      flush();

      expect(callback).to.be.calledOnce;
      expect(callback).to.be.calledWithExactly(true);
    });

    it('should report any error', function () {
      SkryvConfig.getConfigByKey
        .withArgs('USE_DATA_DEFINITION_EDITOR')
        .returns($q.reject('Something went wrong'));

      var callback = sinon.spy();
      EnvironmentConfig.isDdeEnabled().catch(callback);

      flush();

      expect(callback).to.be.calledOnce;
      expect(callback).to.be.calledWithExactly('Something went wrong');
    });

    it('should remember its result for a second invocation', function () {
      SkryvConfig.getConfigByKey
        .withArgs('USE_DATA_DEFINITION_EDITOR')
        .returns($q.when([ { value: 'True' } ]));

      var callback = sinon.spy();
      EnvironmentConfig.isDdeEnabled().then(callback);

      flush();

      EnvironmentConfig.isDdeEnabled().then(callback);

      flush();

      expect(callback).to.be.calledTwice;
      expect(callback).to.be.calledWithExactly(true);

      expect(SkryvConfig.getConfigByKey).to.be.calledOnce;
    });

  });

  describe('.isPublishEnabled()', function () {

    it('should return false when USE_DATA_DEFINITION_PUBLISH is not set', function () {
      SkryvConfig.getConfigByKey
        .withArgs('USE_DATA_DEFINITION_PUBLISH')
        .returns($q.when([]));

      var callback = sinon.spy();
      EnvironmentConfig.isPublishEnabled().then(callback);

      flush();

      expect(callback).to.be.calledOnce;
      expect(callback).to.be.calledWithExactly(false);
    });

    it('should return true when USE_DATA_DEFINITION_PUBLISH is set', function () {
      SkryvConfig.getConfigByKey
        .withArgs('USE_DATA_DEFINITION_PUBLISH')
        .returns($q.when([ { value: 'True' } ]));

      var callback = sinon.spy();
      EnvironmentConfig.isPublishEnabled().then(callback);

      flush();

      expect(callback).to.be.calledOnce;
      expect(callback).to.be.calledWithExactly(true);
    });

    it('should report any error', function () {
      SkryvConfig.getConfigByKey
        .withArgs('USE_DATA_DEFINITION_PUBLISH')
        .returns($q.reject('Something went wrong'));

      var callback = sinon.spy();
      EnvironmentConfig.isPublishEnabled().catch(callback);

      flush();

      expect(callback).to.be.calledOnce;
      expect(callback).to.be.calledWithExactly('Something went wrong');
    });

    it('should remember its result for a second invocation', function () {
      SkryvConfig.getConfigByKey
        .withArgs('USE_DATA_DEFINITION_PUBLISH')
        .returns($q.when([ { value: 'True' } ]));

      var callback = sinon.spy();
      EnvironmentConfig.isPublishEnabled().then(callback);

      flush();

      EnvironmentConfig.isPublishEnabled().then(callback);

      flush();

      expect(callback).to.be.calledTwice;
      expect(callback).to.be.calledWithExactly(true);

      expect(SkryvConfig.getConfigByKey).to.be.calledOnce;
    });

  });

});
