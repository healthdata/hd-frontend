/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('SkryvConfig', function () {
  var $httpBackend, $rootScope, SESSION_SERVER_URL, SkryvConfig;

  beforeEach(module('services.configurations'));
  beforeEach(module('gettext'));

  beforeEach(inject(function (_$httpBackend_, _$rootScope_, _SESSION_SERVER_URL_, _SkryvConfig_) {
    $httpBackend = _$httpBackend_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    $rootScope = _$rootScope_;
    SkryvConfig = _SkryvConfig_;
  }));

  var callback;

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  beforeEach(function () {
    callback = sinon.spy();
  });

  describe('.getConfig()', function () {
    it('should retrieve all configurations', function () {
      var requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/configurations');
      var configs = [ 1, 2, 3 ];
      requestHandler.respond([ 1, 2, 3 ]);
      SkryvConfig.getConfig().then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(angular.equals(result, configs)).to.be.true;
    });
    it('should report any error while getting configurations' , function () {
      var requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/configurations');
      requestHandler.respond(401);
      SkryvConfig.getConfig().catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('No configurations found');
    });
  });

  describe('.deleteConfig()', function () {
    it('should delete a single configuration by id', function () {
      var requestHandler = $httpBackend.expectDELETE(SESSION_SERVER_URL + '/configurations/5');
      var config = { name: 'conf' };
      requestHandler.respond(config);
      SkryvConfig.deleteConfig(5).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(angular.equals(result, config)).to.be.true;
    });
    it('should report any error while deleting a configuration by id' , function () {
      var requestHandler = $httpBackend.expectDELETE(SESSION_SERVER_URL + '/configurations/5');
      requestHandler.respond(401);
      SkryvConfig.deleteConfig(5).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Configuration could not be deleted');
    });
  });

  describe('.editConfig()', function () {
    it('should edit a single configuration by id', function () {
      var requestHandler = $httpBackend.expectPUT(SESSION_SERVER_URL + '/configurations/5');
      var config = { id: 5, name: 'conf' };
      requestHandler.respond(config);
      SkryvConfig.editConfig(config).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(angular.equals(result, config)).to.be.true;
    });
    it('should report any error while editing a configuration by id' , function () {
      var requestHandler = $httpBackend.expectPUT(SESSION_SERVER_URL + '/configurations/5');
      var config = { id: 5, name: 'conf' };
      requestHandler.respond(401);
      SkryvConfig.editConfig(config).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not update configuration');
    });
  });

  describe('.storeConfig()', function () {
    it('should store a single configuration', function () {
      var requestHandler = $httpBackend.expectPOST(SESSION_SERVER_URL + '/configurations');
      var config = { name: 'conf' };
      requestHandler.respond(config);
      SkryvConfig.storeConfig(config).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(angular.equals(result, config)).to.be.true;
    });
    it('should report any error while storing a configuration' , function () {
      var requestHandler = $httpBackend.expectPOST(SESSION_SERVER_URL + '/configurations');
      var config = { name: 'conf' };
      requestHandler.respond(401);
      SkryvConfig.storeConfig(config).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not create configuration');
    });
  });

});
