/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('OrganizationBackend', function () {

  var $httpBackend, ORGANIZATION_SERVER_URL, Organization;

  beforeEach(module('services.organization'));

  beforeEach(inject(function (_$httpBackend_, _ORGANIZATION_SERVER_URL_, _Organization_) {
    $httpBackend = _$httpBackend_;
    ORGANIZATION_SERVER_URL = _ORGANIZATION_SERVER_URL_;
    Organization = _Organization_;
  }));

  var request;
  var callback;

  beforeEach(function () {
    request = $httpBackend.expectGET(ORGANIZATION_SERVER_URL);
    callback = sinon.spy();
  });

  it('should return array of organizations' , function () {
    request.respond([ { id: 1, identificationType: 'NIHII-HOSPITAL', identificationValue: '12345678', name: 'TEST hospital' } ]);

    Organization.get().then(callback);

    $httpBackend.flush();

    expect(callback).to.have.been.calledOnce;
    expect(callback.firstCall.args[0][0])
      .to.have.property('name', 'TEST hospital');
  });

  it('should return unable to get organizations' , function () {
    request.respond(401, '');

    Organization.get().catch(callback);

    $httpBackend.flush();

    expect(callback).to.have.been.calledWith('Unable to fetch organizations');
  });

});
