'use strict';

describe('listViewDocColumns', function () {
  beforeEach(module('services.listview'));
  beforeEach(module('services.editor'));
  beforeEach(module('documentmodel'));
  beforeEach(module('services.notification'));

  var $scope, ListViewDocColumns, SkryvSession, PLATFORM;

  var dc = {
    dataCollectionName: 'TEST',
    content: {
      label: 'TEST',
      fields: [
        {
          type: 'text',
          label: 'Favourite colors',
          name: 'favourite_colors'
        },
        {
          type: 'text',
          label: 'Sticky field',
          name: 'sticky_field',
          sticky: true
        },
        {
          type: 'patientID',
          label: 'Patient ID',
          name: 'some_patient_id',
          fields: [
            {
              label: 'Name',
              name: 'name',
              type: 'text',
              keep: 'coded'
            }
          ]
        },
        {
          type: 'fieldset',
          label: 'Fieldset',
          name: 'field_set',
          fields: [
            {
              label: 'Fieldset Child',
              name: 'field_set_child',
              type: 'text'
            }
          ]
        }
      ],
      sections: [ {
        id: '4f1e3781-ad83-4ebb-b6c4-bdab593d0e09',
        label: 'Sample',
        name: 'sample',
        fields: [
          {
            type: 'text',
            label: 'Sample child',
            name: 'sample_child'
          }
        ]
      } ]
    }
  };

  beforeEach(function () {
    SkryvSession =  {
      getDefinition: function () {
        return dc;
      }
    } ;

    module(function ($provide) {
      $provide.value('SkryvSession', SkryvSession);
    });
  });

  beforeEach(inject(function (_ListViewDocColumns_, $rootScope, _PLATFORM_) {
    $scope = $rootScope.$new();
    ListViewDocColumns = _ListViewDocColumns_;
    PLATFORM = _PLATFORM_;
    ListViewDocColumns.resetInternalState(dc);
  }));

  /*
  Docdef contains:
  -------------------
  - 3 fields with: - 1 sticky,
                   - 1 fieldset with 1 field
                   - 1 patientID with 1 field
  - 1 section with: - 1 field
  */

  it('should set normal field from docdef', function () {
    var color = ListViewDocColumns.docColumns().fields.favourite_colors;
    expect(color.name).to.equal('favourite_colors');
    expect(color.label).to.equal('Favourite colors');
    expect(color.type).to.equal('text');
    expect(color.key).to.equal('favourite_colors.value');
    expect(color.active).to.be.false;
  });

  it('should set sticky field active from docdef', function () {
    var sticky = ListViewDocColumns.docColumns().fields.sticky_field;
    expect(sticky.active).to.be.true;
  });

  it('should toggle field', function () {
    expect(ListViewDocColumns.docColumns().fields.favourite_colors.active).to.be.false;
    ListViewDocColumns.docColumns().fields.favourite_colors.toggleActive();
    expect(ListViewDocColumns.docColumns().fields.favourite_colors.active).to.be.true;
  });

  it('should set sections from docdef', function () {
    var sample = ListViewDocColumns.docColumns().sections.sample;
    expect(sample.label).to.equal('Sample');
    expect(sample.fields.sample_child).to.not.be.undefined;
    expect(sample.fields.sample_child.key).to.equal('sample_child.value');
  });

  it('should set fieldset from docdef as section', function () {
    var fieldset = ListViewDocColumns.docColumns().sections.field_set;
    expect(fieldset).to.not.be.undefined;

    expect(fieldset.label).to.equal('Fieldset');
    expect(fieldset.fields.field_set_child).to.not.be.undefined;
    expect(fieldset.fields.field_set_child.key).to.equal('field_set.nested.field_set_child.value');
  });

  it('should set patientID dropdown ', function () {
    var patientID = ListViewDocColumns.docColumns().sections.some_patient_id;

    expect(patientID).to.not.be.undefined;
    // the 'name' field should be available both in HD4DP and HD4RES, because
    // it has keep: 'coded'.
    expect(patientID.fields.name).to.not.be.undefined;
    expect(patientID.fields.patient_id).to.not.be.undefined;

    expect(patientID.fields.patient_id.key).to.equal('some_patient_id.value');
    expect(patientID.fields.name.key).to.equal('some_patient_id.nested.name.value');

    if (PLATFORM === 'hd4dp') {
      expect(patientID.fields.first_name).to.not.be.undefined;
    } else if (PLATFORM === 'hd4res') {
      // in hd4res, the first name should not be availbale
      expect(patientID.fields.first_name).to.be.undefined;
    }
  });

  it('should get active columns', function () {
    expect(ListViewDocColumns.docActiveColumns().length).to.equal(1);
    ListViewDocColumns.docColumns().fields.favourite_colors.toggleActive();
    expect(ListViewDocColumns.docActiveColumns().length).to.equal(2);
    ListViewDocColumns.docColumns().fields.favourite_colors.toggleActive();
    expect(ListViewDocColumns.docActiveColumns().length).to.equal(1);
  });

  it('should get active keys', function () {
    ListViewDocColumns.docColumns().fields.favourite_colors.toggleActive();
    expect(ListViewDocColumns.getActiveKeys()).to.deep.equal([ 'favourite_colors.value', 'sticky_field.value' ]);
  });

  it('should clear active columns', function () {
    expect(ListViewDocColumns.docActiveColumns().length).to.equal(1);
    ListViewDocColumns.docColumns().fields.favourite_colors.toggleActive();
    expect(ListViewDocColumns.docActiveColumns().length).to.equal(2);
    ListViewDocColumns.clearActiveColumns();
    expect(ListViewDocColumns.docActiveColumns().length).to.equal(0);
  });

  it('should set default columns', function () {
    expect(ListViewDocColumns.docColumns().fields.sticky_field.active).to.be.true;
    ListViewDocColumns.docColumns().fields.sticky_field.toggleActive();
    expect(ListViewDocColumns.docColumns().fields.sticky_field.active).to.be.false;
    ListViewDocColumns.setDefaultColumns();
    expect(ListViewDocColumns.docColumns().fields.sticky_field.active).to.be.true;
  });

  it('should set active keys', function () {
    expect(ListViewDocColumns.docColumns().sections.field_set.fields.field_set_child.active).to.be.false;
    ListViewDocColumns.setActiveKeys([ 'field_set.nested.field_set_child.value' ]);
    expect(ListViewDocColumns.docColumns().sections.field_set.fields.field_set_child.active).to.be.true;
    expect(ListViewDocColumns.docColumns().fields.sticky_field.active).to.be.false;
  });

});
