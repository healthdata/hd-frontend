'use strict';

describe('listViewExtraColumns', function () {
  beforeEach(module('services.listview'));
  beforeEach(module('documentmodel'));
  beforeEach(module('gettext'));

  var $scope, ListViewExtraColumns, SkryvSession;

  beforeEach(function () {
    SkryvSession =  {
      getDefinition: function () {
        return {
          name: 'Test'
        };
      }
    };

    module(function ($provide) {
      $provide.value('SkryvSession', SkryvSession);
    });
  });

  beforeEach(inject(function (_ListViewExtraColumns_, $rootScope) {
    $scope = $rootScope.$new();
    ListViewExtraColumns = _ListViewExtraColumns_;
  }));

  // We check the quality columns for the complete test

  it('should set extra columns', function () {
    expect(Object.keys(ListViewExtraColumns.extraColumns()).length).to.be.above(0);
  });

  it('should transform quality to dropdown', function () {
    var quality = ListViewExtraColumns.extraColumns().qualities;
    expect(quality).to.not.be.undefined;

    expect(quality.label).to.equal = 'Data quality items';
    expect(quality.key).to.equal = 'qualities';
    expect(quality.type).to.equal = 'dropdown';

    expect(Object.keys(quality.columns).length).to.be.above(0);
  });

  it('should toggle a dropdown field', function () {
    expect(ListViewExtraColumns.extraColumns().qualities.columns.errors).to.not.be.undefined;
    expect(ListViewExtraColumns.extraColumns().qualities.columns.errors.active).to.be.false;
    ListViewExtraColumns.extraColumns().qualities.columns.errors.toggleActive();
    expect(ListViewExtraColumns.extraColumns().qualities.columns.errors.active).to.be.true;
  });

  it('should get active extra columns + defaults', function () {
    ListViewExtraColumns.extraColumns().qualities.columns.errors.toggleActive();
    expect(ListViewExtraColumns.getActiveKeys()).to.deep.equal([ 'id', 'errors' ]);
  });

  it('should set active extra columns', function () {
    expect(ListViewExtraColumns.extraColumns().qualities.columns.errors.active).to.be.false;
    ListViewExtraColumns.setActiveKeys([ 'errors' ]);
    expect(ListViewExtraColumns.extraColumns().qualities.columns.errors.active).to.be.true;

  });

  it('should clear active extra columns', function () {
    ListViewExtraColumns.extraColumns().qualities.columns.errors.toggleActive();
    expect(ListViewExtraColumns.extraColumns().qualities.columns.errors.active).to.be.true;
    ListViewExtraColumns.clearExtraColumns();
    expect(ListViewExtraColumns.extraColumns().qualities.columns.errors.active).to.be.false;
  });

});
