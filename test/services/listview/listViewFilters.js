'use strict';

describe('listViewFilters', function () {
  beforeEach(module('services.listview'));
  beforeEach(module('documentmodel'));
  beforeEach(module('gettext'));

  var $scope, ListViewFilters, Statuses, PLATFORM, SkryvSession;

  beforeEach(function () {
    Statuses =  {
      statuses: function () {
        return [
          {
            name: 'IN_PROGRESS',
            label: 'In progress',
            platform: 'hd4dp'
          }, {
            name: 'IN_PROGRESS',
            label: 'In progress',
            platform: 'hd4res'
          }
          ];
      }
    };

    SkryvSession =  {
      getDefinition: function () {
        return {
          name: 'Test'
        };
      }
    };

    module(function ($provide) {
      $provide.value('Statuses', Statuses);
      $provide.value('SkryvSession', SkryvSession);
    });
  });

  beforeEach(inject(function (_ListViewFilters_, $rootScope, _PLATFORM_) {
    $scope = $rootScope.$new();
    ListViewFilters = _ListViewFilters_;
    PLATFORM = _PLATFORM_;
  }));

  // We check the status filter for the complete test

  it('should set filters', function () {
    expect(Object.keys(ListViewFilters.filters()).length).to.be.above(0);
  });

  it('should transform status to object', function () {
    var inProgress = ListViewFilters.filters().statuses.fields.IN_PROGRESS;

    expect(inProgress.label).to.equal = 'In progress';
    expect(inProgress.type).to.equal = 'checkbox';
    expect(inProgress.value).to.equal = false;

    inProgress.toggleValue();

    expect(inProgress.value).to.equal = false;
  });

  it('should set dropdown active', function () {
    var dropdown = ListViewFilters.filters().statuses;
    expect(dropdown.isActive()).to.be.false;

    var inProgress = ListViewFilters.filters().statuses.fields.IN_PROGRESS;
    inProgress.toggleValue();

    expect(dropdown.isActive()).to.be.true;
  });

  it('should get active filters', function () {
    expect(ListViewFilters.getActiveFilters()).to.deep.equal({});
    ListViewFilters.filters().statuses.fields.IN_PROGRESS.toggleValue();
    expect(ListViewFilters.getActiveFilters()).to.deep.equal({ statuses: { IN_PROGRESS: true } });
  });

  it('should set filters from object', function () {
    expect(ListViewFilters.getActiveFilters()).to.deep.equal({});

    ListViewFilters.setActiveFilters({ statuses: { IN_PROGRESS: true } });

    expect(ListViewFilters.getActiveFilters()).to.deep.equal({ statuses: { IN_PROGRESS: true } });
  });

  it('should clear filters', function () {
    ListViewFilters.filters().statuses.fields.IN_PROGRESS.toggleValue();
    expect(ListViewFilters.filters().statuses.fields.IN_PROGRESS.value).to.be.true;

    ListViewFilters.clearFilters();

    expect(ListViewFilters.filters().statuses.fields.IN_PROGRESS.value).to.be.false;
  });

});
