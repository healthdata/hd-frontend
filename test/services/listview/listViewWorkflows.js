/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('list view', function () {

  // FIXME the following declarations should not be here
  // they are required because skryv.editor does not properly list its dependencies
  beforeEach(module('services.dcd'));
  beforeEach(module('ngDialog'));
  beforeEach(module('skryv.configurations'));
  beforeEach(module('LocalStorageModule'));
  beforeEach(module('skryv.listView'));
  beforeEach(module('services.notification'));

  describe('ListViewWorkflowsStore', function () {

    var ListViewWorkflowsStore;

    beforeEach(inject(function (_ListViewWorkflowsStore_) {
      ListViewWorkflowsStore = _ListViewWorkflowsStore_;
    }));

    it('should be in progress after LoadWorkflows event', function () {
      expect(ListViewWorkflowsStore.isLoading()).to.be.false;
      var searchParams = {};
      ListViewWorkflowsStore.onLoadWorkflows(searchParams);
      expect(ListViewWorkflowsStore.isLoading()).to.be.true;
    });

    it('should not be in progress after LoadWorkflowsSuccess action', function () {
      var data = [];
      var complete = false;
      var searchParams = {};
      ListViewWorkflowsStore.onLoadWorkflows(searchParams);
      ListViewWorkflowsStore.onLoadWorkflowsSuccess(data, complete);
      expect(ListViewWorkflowsStore.isLoading()).to.be.false;
    });

    it('should not be in progress after LoadWorkflowsFailure action', function () {
      var searchParams = {};
      ListViewWorkflowsStore.onLoadWorkflows(searchParams);
      ListViewWorkflowsStore.onLoadWorkflowsFailure();
      expect(ListViewWorkflowsStore.isLoading()).to.be.false;
    });

    it('should expose the error after LoadWorkflowsFailure action', function () {
      var error = { thisIs: 'an error' };
      var searchParams = {};
      ListViewWorkflowsStore.onLoadWorkflows(searchParams);
      ListViewWorkflowsStore.onLoadWorkflowsFailure(error);
      expect(ListViewWorkflowsStore.hasError()).to.be.true;
      expect(ListViewWorkflowsStore.error()).to.equal(error);
    });

    it('should replace received workflows after multiple LoadWorkflowsSuccess actions', function () {
      var data = [ { workflow: 1 }, { workflow: 2 } ];
      var searchParams = {};
      ListViewWorkflowsStore.onLoadWorkflows(searchParams);
      ListViewWorkflowsStore.onLoadWorkflowsSuccess(data);
      expect(ListViewWorkflowsStore.workflows()).to.deep.equal(data);

      var moreData = [ { workflow: 3 }, { workflow: 4 } ];
      ListViewWorkflowsStore.onLoadWorkflows(searchParams);
      ListViewWorkflowsStore.onLoadWorkflowsSuccess(moreData);
      expect(ListViewWorkflowsStore.workflows()).to.deep.equal(moreData);
    });

    it('should track the total number of worklfows in a search', function () {
      var data = [ { workflow: 1 }, { workflow: 2 } ];
      var searchParams = {};
      ListViewWorkflowsStore.onLoadWorkflows(searchParams);
      ListViewWorkflowsStore.onLoadWorkflowsSuccess(data, 123);
      expect(ListViewWorkflowsStore.total()).to.equal(123);
    });

    it('should clear all loaded workflows after onLoadWorkflows action', function () {
      var data = [ { workflow: 1 }, { workflow: 2 } ];
      var searchParams = {};
      ListViewWorkflowsStore.onLoadWorkflows(searchParams);
      ListViewWorkflowsStore.onLoadWorkflowsSuccess(data);
      expect(ListViewWorkflowsStore.workflows()).to.deep.equal(data);

      var otherSearchParams = {};
      ListViewWorkflowsStore.onLoadWorkflows(otherSearchParams);
      expect(ListViewWorkflowsStore.workflows()).to.deep.equal([]);
    });

  });

  describe('ListViewWorkflowsActions', function () {

    var $q, $rootScope, _;
    var ListViewWorkflowsActions, ListViewWorkflowsStore, WorkflowRequest;
    var interceptedEvents;

    // mocking ListViewWorkflowsStore
    // events are logged in an array
    beforeEach(function () {
      interceptedEvents = [];
      function mockEvent(name) {
        return function () {
          interceptedEvents.push({ name: name, args: arguments });
        };
      }
      ListViewWorkflowsStore = {
        searchParams: sinon.stub(),
        isLoading: sinon.stub(),
        workflows: sinon.stub(),
        processAndCache: sinon.stub(),
        onLoadWorkflows: mockEvent('onLoadWorkflows'),
        onLoadWorkflowsSuccess: mockEvent('onLoadWorkflowsSuccess'),
        onLoadWorkflowsFailure: mockEvent('onLoadWorkflowsFailure')
      };
      WorkflowRequest = {
        getByDatacollection: sinon.stub()
      };
      module(function ($provide) {
        $provide.value('ListViewWorkflowsStore', ListViewWorkflowsStore);
        $provide.value('WorkflowRequest', WorkflowRequest);
      });
    });

    function checkInterceptedEvent(pos, name, f) {
      var eventInfo = interceptedEvents[pos];
      expect(eventInfo).to.have.property('name', name);
      f.apply(null, eventInfo.args);
    }

    beforeEach(inject(function (___, _$q_, _$rootScope_, _ListViewWorkflowsActions_) {
      _ = ___; // it looks weird, but it is just injecting underscore
      $q = _$q_;
      $rootScope = _$rootScope_;
      ListViewWorkflowsActions = _ListViewWorkflowsActions_;
    }));

    describe('.loadWorkflows()', function () {

      var searchParams;
      var requestDeferred;
      var workflows;

      beforeEach(function () {
        searchParams = {};
        workflows = [];
        ListViewWorkflowsStore.isLoading.returns(false);
        ListViewWorkflowsStore.searchParams.returns(searchParams);
        ListViewWorkflowsStore.workflows.returns(workflows);
        requestDeferred = $q.defer();
        WorkflowRequest.getByDatacollection.returns(requestDeferred.promise);
      });

      describe('with basic search parameters', function () {

        var dataCollectionId;

        beforeEach(function () {
          dataCollectionId = searchParams.dataCollectionId = 'FUBAR';
        });

        it('should invoke getByDatacollection() correctly', function () {
          ListViewWorkflowsActions.loadWorkflows(searchParams, 3, 10);
          expect(WorkflowRequest.getByDatacollection).to.have.been.calledOnce;
          expect(WorkflowRequest.getByDatacollection)
            .to.have.been.calledWith('FUBAR', {
              from: 20,
              size: 10,
              query: {
                filtered: {
                  filter: {
                    bool: {
                      must: [],
                      must_not: [ { term: { status: 'DELETED' } } ]
                    }
                  }
                }
              }
            });
        });

        it('should trigger a loadWorkflows event', function () {
          ListViewWorkflowsActions.loadWorkflows(searchParams);
          expect(interceptedEvents).to.have.lengthOf(1);
          checkInterceptedEvent(0, 'onLoadWorkflows', function (_searchParams_) {
            expect(_searchParams_).to.equal(searchParams);
          });
        });

        it('should trigger a LoadWorkflowsSuccess event when the request succeeds', function () {
          requestDeferred.resolve({
            hits: {
              total: 987,
              hits: []
            }
          });

          ListViewWorkflowsActions.loadWorkflows(searchParams, 3, 10);

          $rootScope.$apply(); // promises are resolved/dispatched only on next $digest cycle

          expect(interceptedEvents).to.have.lengthOf(2);
          checkInterceptedEvent(1, 'onLoadWorkflowsSuccess',
            function (workfows, total) {
              expect(workfows).to.deep.equal([]);
              expect(total).to.equal(987);
            });
        });

        it('should trigger a LoadWorkflowsFailure event when the request fails', function () {
          requestDeferred.reject('the reason');

          ListViewWorkflowsActions.loadWorkflows(searchParams);

          $rootScope.$apply(); // promises are resolved/dispatched only on next $digest cycle

          expect(interceptedEvents).to.have.lengthOf(2);
          checkInterceptedEvent(1, 'onLoadWorkflowsFailure',
            function (error) {
              expect(error).to.equal('the reason');
            });
          });
      });

      it('should take into account the dateOfBirthAfter parameter', function () {
        searchParams.dataCollectionId = 'FUBAR';
        searchParams.dateOfBirthAfter = '1987-12-12T00:00:00+01:00';

        ListViewWorkflowsActions.loadWorkflows(searchParams, 3, 10);

        expect(WorkflowRequest.getByDatacollection)
          .to.have.been.calledWith('FUBAR', {
            from: 20,
            size: 10,
            query: {
              filtered: {
                filter: {
                  bool: {
                    must: [ {
                      range: {
                        'document.documentContent.patientID.birthdate': {
                          gte: searchParams.dateOfBirthAfter,
                          lte: undefined
                        }
                      }
                    } ],
                    must_not: [ { term: { status: 'DELETED' } } ]
                  }
                }
              }
            }
          });
      });

      it('should take into account the statuses parameter', function () {
        searchParams.dataCollectionId = 'FUBAR';
        searchParams.statuses = [ { status: 'A' }, { status: 'C' } ];

        ListViewWorkflowsActions.loadWorkflows(searchParams, 3, 10);

        expect(WorkflowRequest.getByDatacollection)
          .to.have.been.calledWith('FUBAR', {
            from: 20,
            size: 10,
            query: {
              filtered: {
                filter: {
                  bool: {
                    must: [ { terms: { status: [ 'A', 'C' ] } } ],
                    must_not: [ { term: { status: 'DELETED' } } ]
                  }
                }
              }
            }
          });
      });

    });

  });

});
