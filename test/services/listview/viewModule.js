'use strict';

describe('viewModule', function () {

  beforeEach(module('skryv.view'));
  beforeEach(module('services.notification'));
  beforeEach(module('documentmodel'));
  beforeEach(module('gettext'));

  // modules
  var $httpBackend, $q, SkryvView, SkryvSession, SkryvUser,
    SkryvUserView, SkryvDefinition, $scope, Notifications;

  // promises
  var requestDeferred, deleteViewPromise, requestDeferredDc, getViewPromise, saveViewPromise, updateViewPromise;

  var dc = { dataCollectionName: 'TEST', id: 12 };

  describe('With no views stored', function () {

    beforeEach(function () {
      SkryvSession = {
        getLoggedUser:  function () { return { username: 'John', metaData: { lastView: '{ "name": "testView", "dataCollectionName": "TEST", "dataCollectionId": 12  }' }}; },
        getDefinition:  function () { return dc; }
      };

      SkryvUser = {
        updateMetadata: sinon.stub()
      };

      SkryvDefinition = {
        get: sinon.stub()
      };

      SkryvUserView = {
        get: sinon.stub(),
        save: sinon.stub(),
        update: sinon.stub(),
        delete: sinon.stub()
      };

      module(function ($provide) {
        $provide.value('SkryvSession', SkryvSession);
        $provide.value('SkryvUser', SkryvUser);
        $provide.value('SkryvDefinition', SkryvDefinition);
        $provide.value('SkryvUserView', SkryvUserView);
      });
    });

    beforeEach(inject(function (_$httpBackend_, _$q_, _SkryvView_, $rootScope, _Notifications_) {
      $httpBackend = _$httpBackend_;
      $scope = $rootScope.$new();
      $q = _$q_;
      SkryvView = _SkryvView_;
      Notifications = _Notifications_;

      requestDeferred = $q.defer();
      SkryvUser.updateMetadata.returns(requestDeferred.promise);

      getViewPromise = $q.defer();
      SkryvUserView.get.returns(getViewPromise.promise);

      saveViewPromise = $q.defer();
      SkryvUserView.save.returns(saveViewPromise.promise);

      getViewPromise.resolve([]);
      SkryvView.resetInternalState(dc);
      $scope.$digest();
    }));

    it('should toggle initiated', function () {
      expect(SkryvView.initiated()).to.deep.equal(false);

      SkryvView.setInitiated();

      expect(SkryvView.initiated()).to.deep.equal(true);
    });

    it('should set last active view of DCD', function () {
      expect(SkryvView.getLastView()).to.deep.equal({ name: 'testView', dataCollectionName: 'TEST', dataCollectionId: 12 });
    });

    it('should save last view', function () {
      requestDeferred.resolve({ });

      SkryvView.setLastView({ name: 'updatedView', dataCollectionName: 'TEST' });
      expect(SkryvUser.updateMetadata).to.have.been.calledOnce;
      expect(SkryvUser.updateMetadata).to.have.been.calledWith({
        username: 'John',
        metaData: {
          lastView: '{"name":"updatedView","dataCollectionName":"TEST","content":{"name":"updatedView","dataCollectionName":"TEST"}}'
        }
      });

      $scope.$digest();

      expect(SkryvView.activeView()).to.deep.equal({
        name: 'updatedView',
        dataCollectionName: 'TEST',
        content: {
          name: 'updatedView',
          dataCollectionName: 'TEST'
        }
      });
    });

    it('should add a new view', function () {
      var callback = sinon.spy();

      requestDeferred.resolve({});
      saveViewPromise.resolve('View Saved');

      SkryvView.addView({ name: 'newView', dataCollectionName: 'TEST', dataCollectionId: 12 }).then(callback);

      expect(SkryvUserView.save).to.have.been.calledOnce;
      expect(SkryvUserView.save).to.have.been.calledWith(12, 'newView', {
        dataCollectionId: 12, dataCollectionName: 'TEST', name: 'newView'
      });

      $scope.$digest();
      expect(Notifications.notifications[0].content).to.equal('Your view has been saved');

      var data = callback.firstCall.args[0];
      expect(data).to.deep.equal('View Saved');
      expect(SkryvView.views()).to.deep.equal([ 'View Saved' ]);
    });

    it('should fail adding a new view', function () {
      saveViewPromise.reject('error');

      SkryvView.addView({ name: 'newView', dataCollectionName: 'TEST', dataCollectionId: 12 });

      expect(SkryvUserView.save).to.have.been.calledOnce;
      expect(SkryvUserView.save).to.have.been.calledWith(12, 'newView', {
        dataCollectionId: 12, dataCollectionName: 'TEST', name: 'newView'
      });

      $scope.$digest();
      expect(Notifications.notifications[0].content).to.equal('Failed to save your view');
    });

  });

  describe('With new views stored', function () {

    beforeEach(function () {

      SkryvSession = {
        getLoggedUser:  function () {
          return {
            username: 'John',
            metaData: {
              lastView: '{ "name": "testView", "dataCollectionName": "TEST" }'
            }
          };
        },
        getDefinition:  function () { return { dataCollectionName: 'TEST', id: 12 }; }
      };

      SkryvUser = {
        updateMetadata: sinon.stub()
      };

      SkryvDefinition = {
        get: sinon.stub()
      };

      SkryvUserView = {
        get: sinon.stub(),
        save: sinon.stub(),
        delete: sinon.stub(),
        update: sinon.stub()
      };

      module(function ($provide) {
        $provide.value('SkryvSession', SkryvSession);
        $provide.value('SkryvUser', SkryvUser);
        $provide.value('SkryvDefinition', SkryvDefinition);
        $provide.value('SkryvUserView', SkryvUserView);
      });
    });

    beforeEach(inject(function (_$httpBackend_, _$q_, _SkryvView_, $rootScope, _Notifications_) {
      $httpBackend = _$httpBackend_;
      $scope = $rootScope.$new();
      $q = _$q_;
      SkryvView = _SkryvView_;
      Notifications = _Notifications_;

      requestDeferred = $q.defer();
      SkryvUser.updateMetadata.returns(requestDeferred.promise);

      requestDeferredDc = $q.defer();
      SkryvDefinition.get.returns(requestDeferredDc.promise);

      getViewPromise = $q.defer();
      SkryvUserView.get.returns(getViewPromise.promise);

      updateViewPromise = $q.defer();
      SkryvUserView.update.returns(updateViewPromise.promise);

      deleteViewPromise = $q.defer();
      SkryvUserView.delete.returns(deleteViewPromise.promise);

      saveViewPromise = $q.defer();
      SkryvUserView.save.returns(saveViewPromise.promise);

      getViewPromise.resolve([ { name: 'testView1' }, { name: 'testView2' }, { name: 'testView3' } ]);
      SkryvView.resetInternalState(dc);
      $scope.$digest();
    }));

    it('should delete a view', function () {
      deleteViewPromise.resolve({});

      expect(SkryvView.views().length).to.equal(3);
      SkryvView.deleteView(SkryvView.views()[1]);

      $scope.$digest();

      expect(SkryvUserView.delete).to.have.been.calledOnce;
      expect(SkryvUserView.delete).to.have.been.calledWith(12, 'testView2');

      expect(Notifications.notifications[0].content).to.equal('Your view has been deleted');
      expect(SkryvView.views().length).to.equal(2);
      expect(SkryvView.views()).to.deep.equal([ { name: 'testView1' }, { name: 'testView3' } ]);
    });

    it('should fail deleting a view', function () {
      deleteViewPromise.reject('error deleting');

      expect(SkryvView.views().length).to.equal(3);
      expect(SkryvView.deleteView(SkryvView.views()[1]));

      $scope.$digest();

      expect(Notifications.notifications[0].content).to.equal('error deleting');
      expect(SkryvView.views().length).to.equal(3);
    });

    it('should update a view', function () {
      updateViewPromise.resolve({});

      var update = SkryvView.views()[0];
      update.filters = { sentOn: true };

      expect(SkryvView.updateView(update));

      $scope.$digest();

      expect(SkryvUserView.update).to.have.been.calledOnce;
      expect(SkryvUserView.update).to.have.been.calledWith(12, 'testView1', update);

      expect(Notifications.notifications[0].content).to.equal('Your view has been updated');
      expect(SkryvView.views()).to.deep.equal([
        { name: 'testView1', filters: { sentOn: true }}, { name: 'testView2' }, { name: 'testView3' } ]);
    });

    it('should fail updating a view', function () {
      requestDeferred.reject('error');
      updateViewPromise.reject('error');

      var update = SkryvView.views()[0];
      update.filters = { sentOn: true };

      var originalViews = SkryvView.views();

      expect(SkryvView.updateView(update));

      $scope.$digest();

      expect(Notifications.notifications[0].content).to.equal('error');
      expect(SkryvView.views()).to.deep.equal(originalViews);
    });

    it('should warn when adding a duplicate view', function () {
      var callback = sinon.spy();

      saveViewPromise.reject({ status: 409 });

      SkryvView.addView({ name: 'test', dataCollectionName: 'TEST', dataCollectionId: 12 }).catch(callback);

      $scope.$digest();

      expect(Notifications.notifications[0].content).to.equal('View with that name already exists');
    });
  });

  describe('With older views stored', function () {

    beforeEach(function () {
      SkryvSession = {
        getLoggedUser:  function () {
          return {
            username: 'John',
            metaData: {
              'views': [],
              'VIEWS-BEWSD': JSON.stringify([
                {
                  statuses: {
                    IN_PROGRESS: true
                  },
                  dateOfBirthBefore: '5',
                  dateOfBirthAfter: '',
                  onlyMyWorkflows: true,
                  columns: {
                    '20': {
                      order: 20,
                      name: 'color',
                      label: 'Color',
                      $$hashKey: 'object:539'
                    }
                  },
                  qualities: {},
                  sourceOfData: {},
                  identifications: {},
                  timings: {
                    createdOn: 'Creation date'
                  },
                  sendStatus: false,
                  dataCollection: 'BEWSD',
                  name: 'test'
                }
              ])
            }
          };
        },
        getDefinition:  function () { return { dataCollectionName: 'BEWSD', id: 12 }; }
      };

      SkryvUser = {
        updateMetadata: sinon.stub()
      };

      SkryvDefinition = {
        get: sinon.stub()
      };

      SkryvUserView = {
        get: sinon.stub(),
        save: sinon.stub()
      };

      module(function ($provide) {
        $provide.value('SkryvSession', SkryvSession);
        $provide.value('SkryvUser', SkryvUser);
        $provide.value('SkryvDefinition', SkryvDefinition);
        $provide.value('SkryvUserView', SkryvUserView);
      });
    });

    beforeEach(inject(function (_$httpBackend_, _$q_, _SkryvView_, $rootScope, _Notifications_) {
      $httpBackend = _$httpBackend_;
      $scope = $rootScope.$new();
      $q = _$q_;
      SkryvView = _SkryvView_;
      Notifications = _Notifications_;

      requestDeferred = $q.defer();
      requestDeferredDc = $q.defer();
      SkryvUser.updateMetadata.returns(requestDeferred.promise);
      SkryvDefinition.get.returns(requestDeferredDc.promise);

      getViewPromise = $q.defer();
      SkryvUserView.get.returns(getViewPromise.promise);

      saveViewPromise = $q.defer();
      SkryvUserView.save.returns(saveViewPromise.promise);
    }));

    // it('should get views and transform old ones', function () {
    //   saveViewPromise.resolve({});
    //   SkryvView.resetInternalState({ dataCollectionName: 'BEWSD', id: 12 });
    //
    //   getViewPromise.resolve([]);
    //   $scope.$digest();
    //
    //   var expected =  {
    //     filters: {
    //       IN_PROGRESS: true,
    //       onlyMyWorkflows: true,
    //       dateOfBirthBefore: '5'
    //     },
    //     extraColumns: [ 'createdOn', 'sendStatus' ],
    //     docColumns: [ 'color.value' ],
    //     dataCollectionName: 'BEWSD',
    //     dataCollectionDefinitionId: 12,
    //     name: 'test'
    //   };
    //
    //   expect(SkryvUserView.save).to.have.been.calledOnce;
    //   expect(SkryvUserView.save).to.have.been.calledWith(12, 'test', expected);
    // });

  });
});
