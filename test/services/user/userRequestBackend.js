/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('userRequestBackend', function () {

  var $httpBackend;
  var SkryvUserRequest;
  var USER_REQUEST_SERVER_URL;

  beforeEach(module('gettext'));
  beforeEach(module('services.user'));

  beforeEach(inject(function (_$httpBackend_, _USER_REQUEST_SERVER_URL_, _SkryvUserRequest_) {
    $httpBackend = _$httpBackend_;
    USER_REQUEST_SERVER_URL = _USER_REQUEST_SERVER_URL_;
    SkryvUserRequest = _SkryvUserRequest_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.getAll()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(USER_REQUEST_SERVER_URL);
      callback = sinon.spy();
    });

    it('should retrieve a all requests' , function () {
      requestHandler.respond([ { userRequest: {} }, { userRequest: {} } ]);

      SkryvUserRequest.getAll().then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith([
        { userRequest: {} },
        { userRequest: {} }
      ]);
    });

    it('should report any error while retrieving all user requests' , function () {
      requestHandler.respond(401);

      SkryvUserRequest.getAll().catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not load user requests');
    });
  });

  describe('.get()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(USER_REQUEST_SERVER_URL + '1');
      callback = sinon.spy();
    });

    it('should retrieve a request by id' , function () {
      requestHandler.respond({ userRequest: {} });

      SkryvUserRequest.get(1).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({ userRequest: {} });
    });

    it('should report any error while retrieving the user request' , function () {
      requestHandler.respond(401);

      SkryvUserRequest.get(1).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('User request not found');
    });
  });

  describe('.accept()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPOST(USER_REQUEST_SERVER_URL + '1/accept');
      callback = sinon.spy();
    });

    it('should accept a user request by id' , function () {
      requestHandler.respond({});

      SkryvUserRequest.accept(1).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({});
    });

    it('should report any error while accepting a view' , function () {
      requestHandler.respond(401);

      SkryvUserRequest.accept(1).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not edit user');
    });
  });

  describe('.reject()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPOST(USER_REQUEST_SERVER_URL + '1/reject');
      callback = sinon.spy();
    });

    it('should reject a user request by id' , function () {
      requestHandler.respond({});

      SkryvUserRequest.reject(1).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({});
    });

    it('should report any error while rejecting a view' , function () {
      requestHandler.respond(401);

      SkryvUserRequest.reject(1).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not reject user request');
    });
  });

  describe('.update()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPUT(
        USER_REQUEST_SERVER_URL + '1',
        { id: 1, updatedRequest: {} });
      callback = sinon.spy();
    });

    it('should edit a user request' , function () {
      var updatedRequest = { id: 1, updatedRequest: {} };
      requestHandler.respond(updatedRequest);

      SkryvUserRequest.update(updatedRequest).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith(updatedRequest);
    });

    it('should report any error while updating a user request' , function () {
      var updatedRequest = { id: 1, updatedRequest: {} };
      requestHandler.respond(401);

      SkryvUserRequest.update(updatedRequest).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not edit user');
    });
  });

  describe('.save()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPOST(
        USER_REQUEST_SERVER_URL,
        { savedUserRequest: {} });
      callback = sinon.spy();
    });

    it('should save a vied by DCD id and name' , function () {
      var savedUserRequest = { savedUserRequest: {} };
      requestHandler.respond(savedUserRequest);

      SkryvUserRequest.save(savedUserRequest).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith(savedUserRequest);
    });

    it('should report any error while saving a view' , function () {
      var savedUserRequest = { savedUserRequest: {} };
      requestHandler.respond(409);

      SkryvUserRequest.save(savedUserRequest).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not edit user');
    });
  });

});
