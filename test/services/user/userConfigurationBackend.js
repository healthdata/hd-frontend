/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('userConfigurationBackend', function () {

  var $httpBackend;
  var SkryvUserConfiguration;
  var USER_CONFIGURATION_SERVER_URL;

  beforeEach(module('gettext'));
  beforeEach(module('services.user'));

  beforeEach(inject(function (_$httpBackend_, _USER_CONFIGURATION_SERVER_URL_, _SkryvUserConfiguration_) {
    $httpBackend = _$httpBackend_;
    USER_CONFIGURATION_SERVER_URL = _USER_CONFIGURATION_SERVER_URL_;
    SkryvUserConfiguration = _SkryvUserConfiguration_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.get()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(USER_CONFIGURATION_SERVER_URL);
      callback = sinon.spy();
    });

    it('should retrieve the user configurations' , function () {
      requestHandler.respond({});

      SkryvUserConfiguration.get().then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({});
    });

    it('should report any error while retrieving the user configurations' , function () {
      requestHandler.respond(401);

      SkryvUserConfiguration.get(1).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not get user configurations');
    });
  });

  describe('.update()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPUT(
        USER_CONFIGURATION_SERVER_URL + '1',
        { id: 1, updatedConfiguration: {} });
      callback = sinon.spy();
    });

    it('should edit a user configuration' , function () {
      var updatedConfig = { id: 1, updatedConfiguration: {} };
      requestHandler.respond(updatedConfig);

      SkryvUserConfiguration.update(updatedConfig).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith(updatedConfig);
    });

    it('should report any error while updating a user configuration' , function () {
      var updatedConfig = { id: 1, updatedConfiguration: {} };
      requestHandler.respond(401);

      SkryvUserConfiguration.update(updatedConfig).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not update user configurations');
    });
  });

});
