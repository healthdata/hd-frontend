/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('userViewBackend', function () {

  var $httpBackend, USER_SERVER_URL;
  var SkryvUserView;
  var USER_VIEW_SERVER_URL;

  beforeEach(module('gettext'));
  beforeEach(module('services.user'));

  beforeEach(inject(function (_$httpBackend_, _USER_SERVER_URL_, _SkryvUserView_) {
    $httpBackend = _$httpBackend_;
    USER_SERVER_URL = _USER_SERVER_URL_;
    SkryvUserView = _SkryvUserView_;
  }));

  beforeEach(function () {
    USER_VIEW_SERVER_URL = USER_SERVER_URL + 'views/';
  });

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.get()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(USER_VIEW_SERVER_URL + '1');
      callback = sinon.spy();
    });

    it('should retrieve all views by DCD id' , function () {
      var views = [ { testMarker: {} }, { testMarker: {} } ];
      requestHandler.respond(views);

      SkryvUserView.get(1).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith(views);
    });

    it('should report any error while retrieving views by DCD id' , function () {
      requestHandler.respond(401);

      SkryvUserView.get(1).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not get views');
    });
  });

  describe('.delete()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectDELETE(USER_VIEW_SERVER_URL + '1/someView');
      callback = sinon.spy();
    });

    it('should delete a view by DCD id and name' , function () {
      requestHandler.respond({});

      SkryvUserView.delete(1, 'someView').then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({});
    });

    it('should report any error while deleting a view' , function () {
      requestHandler.respond(401);

      SkryvUserView.delete(1, 'someView').catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not update view');
    });
  });

  describe('.update()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPUT(
        USER_VIEW_SERVER_URL + '1/someView',
        { updatedView: {} });
      callback = sinon.spy();
    });

    it('should edit a view by DCD id and name' , function () {
      var updatedView = { updatedView: {} };
      requestHandler.respond(updatedView);

      SkryvUserView.update(1, 'someView', updatedView).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith(updatedView);
    });

    it('should report any error while editing a view' , function () {
      var updatedView = { updatedView: {} };
      requestHandler.respond(401);

      SkryvUserView.update(1, 'someView', updatedView).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not update view');
    });
  });

  describe('.save()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPOST(
        USER_VIEW_SERVER_URL + '1/someView',
        { savedView: {} });
      callback = sinon.spy();
    });

    it('should save a vied by DCD id and name' , function () {
      var savedView = { savedView: {} };
      requestHandler.respond(savedView);

      SkryvUserView.save(1, 'someView', savedView).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith(savedView);
    });

    it('should report any error while saving a view' , function () {
      var savedView = { savedView: {} };
      requestHandler.respond(409);

      SkryvUserView.save(1, 'someView', savedView).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var err = callback.firstCall.args[0];
      expect(err).to.have.property('status', 409);
    });
  });

});
