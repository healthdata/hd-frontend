/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('hd4dpUsersBackend', function () {

  var $httpBackend;
  var HD4DPUsers;
  var USER_HD4DP_SERVER_URL;

  beforeEach(module('gettext'));
  beforeEach(module('services.user'));

  beforeEach(inject(function (_$httpBackend_, _USER_HD4DP_SERVER_URL_, _HD4DPUsers_) {
    $httpBackend = _$httpBackend_;
    USER_HD4DP_SERVER_URL = _USER_HD4DP_SERVER_URL_;
    HD4DPUsers = _HD4DPUsers_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.getHD4DP()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(USER_HD4DP_SERVER_URL + '?dataCollection=IQED');
      callback = sinon.spy();
    });

    it('should retrieve all HD4DP users' , function () {
      requestHandler.respond([ {}, {} ]);

      HD4DPUsers.getHD4DP({ dataCollection: 'IQED' }).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith([ {}, {} ]);
    });

    it('should report any error while retrieving the users' , function () {
      requestHandler.respond(401);

      HD4DPUsers.getHD4DP({ dataCollection: 'IQED' }).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Users not found');
    });
  });

});
