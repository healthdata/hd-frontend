/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('userBackend', function () {

  var $httpBackend, USER_SERVER_URL, LDAP_SERVER_URL;
  var SkryvSession;
  var SkryvUser;

  beforeEach(module('gettext'));
  beforeEach(module('services.user'));

  beforeEach(function () {
    SkryvSession = {
      setLoggedUser: sinon.spy()
    };
    module({
      SkryvSession: SkryvSession
    });
  });

  beforeEach(inject(function (_$httpBackend_, _USER_SERVER_URL_, _LDAP_SERVER_URL_, _SkryvUser_) {
    $httpBackend = _$httpBackend_;
    USER_SERVER_URL = _USER_SERVER_URL_;
    LDAP_SERVER_URL = _LDAP_SERVER_URL_;
    SkryvUser = _SkryvUser_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.get()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(USER_SERVER_URL + '1');
      callback = sinon.spy();
    });

    it('should retrieve a user by id' , function () {
      var user = { username: 'johnDoe' };
      requestHandler.respond(user);

      SkryvUser.get(1).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('username', user.username);
    });

    it('should report any error while retrieving a user by id' , function () {
      requestHandler.respond(401);

      SkryvUser.get(1).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('User not found');
    });
  });

  describe('.delete()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectDELETE(USER_SERVER_URL + '1');
      callback = sinon.spy();
    });

    it('should delete a user by id' , function () {
      var user = { username: 'johnDoe' };
      requestHandler.respond(user);

      SkryvUser.delete(1).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('username', user.username);
    });

    it('should report any error while deleting a user by id' , function () {
      requestHandler.respond(401);

      SkryvUser.delete(1).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not delete user');
    });
  });

  describe('.update()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPUT(USER_SERVER_URL + '1');
      callback = sinon.spy();
    });

    it('should edit a user by id' , function () {
      var user = { id: 1, username: 'johnDoe' };
      requestHandler.respond(user);

      SkryvUser.update(user).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('username', user.username);
    });

    it('should report any error while editing a user by id' , function () {
      var user = { id: 1, username: 'johnDoe' };
      requestHandler.respond(401);

      SkryvUser.update(user).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not edit user');
    });
  });

  describe('.save()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPOST(USER_SERVER_URL);
      callback = sinon.spy();
    });

    it('should edit a user by id' , function () {
      var user = { username: 'johnDoe' };
      requestHandler.respond(user);

      SkryvUser.save(user).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('username', user.username);
    });

    it('should report when user already exists' , function () {
      var user = { username: 'johnDoe' };
      requestHandler.respond(409,
        { error: 'USERNAME_ALREADY_EXISTS' }
      );

      SkryvUser.save(user).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Username already exists');
    });

    it('should report when LDAP user not exists' , function () {
      var user = { username: 'johnDoe' };
      requestHandler.respond(409,
        { error: 'LDAP_USER_REQUEST_EXCEPTION' }
      );

      SkryvUser.save(user).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('LDAP not configured correctly to save this user');
    });

    it('should report any error while editing a user by id' , function () {
      var user = { username: 'johnDoe' };
      requestHandler.respond(401);

      SkryvUser.save(user).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not create user');
    });
  });

  describe('.updateMetadata()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPUT(USER_SERVER_URL + 'metadata/1');
      callback = sinon.spy();
    });

    it('should edit a user metadata by id' , function () {
      var user = { id: 1, username: 'johnDoe' };
      requestHandler.respond(user);

      SkryvUser.updateMetadata(user).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(SkryvSession.setLoggedUser).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('username', user.username);
    });

    it('should report any error while editing the metadata of a user by id' , function () {
      var user = { id: 1, username: 'johnDoe' };
      requestHandler.respond(401);

      SkryvUser.updateMetadata(user).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Could not edit user metadata');
    });
  });

  describe('.updatePassword()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPUT(USER_SERVER_URL + 'password/1?oldPassword=john&newPassword=doe');
      callback = sinon.spy();
    });

    it('should edit a user password by id' , function () {
      var user = { id: 1, oldPassword: 'john', newPassword: 'doe' };
      requestHandler.respond(user);

      SkryvUser.updatePassword(user).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('newPassword', user.newPassword);
    });

    it('should report any error while editing password from a user by id' , function () {
      var user = { id: 1, oldPassword: 'john', newPassword: 'doe' };
      requestHandler.respond(401);

      SkryvUser.updatePassword(user).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      // expect(callback).to.have.been.calledWith('Password could not be changed');
    });
  });

  describe('.getLdap()', function () {

    var requestHandler;

    var callback;

    it('should all ldap users that contains string' , function () {
      requestHandler = $httpBackend.expectGET(LDAP_SERVER_URL  + '?search=johnDoe');
      callback = sinon.spy();
      var user = { username: 'johnDoe' };
      requestHandler.respond(user);

      SkryvUser.getAllLdapUsers('johnDoe').then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('username', user.username);
    });

    it('should search a ldap user' , function () {
      requestHandler = $httpBackend.expectGET(LDAP_SERVER_URL  + 'johnDoe');
      callback = sinon.spy();

      var user = { username: 'johnDoe' };
      requestHandler.respond(user);

      SkryvUser.getLdapUser('johnDoe').then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.have.property('username', user.username);
    });

    it('should report any error while retrieving all ldap users by string' , function () {

      requestHandler = $httpBackend.expectGET(LDAP_SERVER_URL  + '?search=johnDoe');
      callback = sinon.spy();

      requestHandler.respond(401);

      SkryvUser.getAllLdapUsers('johnDoe').catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('Users not found');
    });

    it('should report any error while retrieving a ldap user by username' , function () {
      requestHandler = $httpBackend.expectGET(LDAP_SERVER_URL  + 'johnDoe');
      callback = sinon.spy();
      requestHandler.respond(401);

      SkryvUser.getLdapUser('johnDoe').catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('User not found');
    });
  });

});
