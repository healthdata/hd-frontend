/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('makeMenu', function () {

  var MainMenu;
  var SkryvSession;
  var _;

  beforeEach(module('documentmodel')); // for underscore
  beforeEach(module('gettext'));

  beforeEach(function () {
    SkryvSession = {
      getLoggedUser: sinon.stub()
    };
  });

  function menuItemNames(items) {
    return _.map(items, function (item) {
      return item.name;
    });
  }

  describe('on HD4DP', function () {

    beforeEach(module('services.navigation', function ($provide) {
      $provide.constant('SkryvSession', SkryvSession);
      $provide.constant('PLATFORM', 'hd4dp');
    }));

    beforeEach(inject(function (_MainMenu_, ___) {
      MainMenu = _MainMenu_;
      _ = ___;
    }));

    it('should list all the right menu items', function () {
      var items = MainMenu.mainMenu();
      expect(menuItemNames(items)).to.deep.equal([
        'participation',
        'data collection',
        'dashboard',
        'reporting',
        'guide',
        'help',
        'settings',
        'user'
      ]);
    });

    it('should show the username if no full name is known', function () {
      var userName = 'whatever';
      var user = { username: userName };
      SkryvSession.getLoggedUser.returns(user);
      var items = MainMenu.mainMenu();
      var userItem = _.findWhere(items, { name: 'user' });
      expect(userItem).to.have.property('label', userName);
    });

    it('should show the full name of the user', function () {
      var firstName = 'first';
      var lastName = 'last';
      var fullName = firstName + ' ' + lastName;
      var user = {
        firstName: firstName,
        lastName: lastName
      };
      SkryvSession.getLoggedUser.returns(user);
      var items = MainMenu.mainMenu();
      var userItem = _.findWhere(items, { name: 'user' });
      expect(userItem).to.have.property('label', fullName);
    });

    it('should have the right dropdown items for help', function () {
      var items = MainMenu.mainMenu();
      var helpItem = _.findWhere(items, { name: 'help' });
      var dropdownItems = helpItem.dropdownItems;
      expect(menuItemNames(dropdownItems)).to.deep.equal([
        'support',
        'contact'
      ]);
    });
  });

  describe('on HD4RES', function () {

    beforeEach(module('services.navigation', function ($provide) {
      $provide.constant('SkryvSession', SkryvSession);
      $provide.constant('PLATFORM', 'hd4res');
    }));

    beforeEach(inject(function (_MainMenu_, ___) {
      MainMenu = _MainMenu_;
      _ = ___;
    }));

    it('should list all the right menu items', function () {
      var items = MainMenu.mainMenu();
      expect(menuItemNames(items)).to.deep.equal([
        'participation',
        'data collection',
        'dashboard',
        'reporting',
        'guide',
        'help',
        'settings',
        'user'
      ]);
    });

    it('should have the right dropdown items for help', function () {
      var items = MainMenu.mainMenu();
      var helpItem = _.findWhere(items, { name: 'help' });
      var dropdownItems = helpItem.dropdownItems;
      expect(menuItemNames(dropdownItems)).to.deep.equal([
        'stabledata',
        'support',
        'contact'
      ]);
    });
  });

});
