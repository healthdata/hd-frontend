/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('dcdBackend', function () {

  var $httpBackend, SESSION_SERVER_URL, SkryvDefinition, SkryvDefinitionManager;

  beforeEach(module('gettext'));
  beforeEach(module('services.dcd'));

  beforeEach(inject(function (_$httpBackend_, _SESSION_SERVER_URL_, _SkryvDefinition_, _SkryvDefinitionManager_) {
    $httpBackend = _$httpBackend_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    SkryvDefinition = _SkryvDefinition_;
    SkryvDefinitionManager = _SkryvDefinitionManager_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('SkryvDefinition', function () {
    describe('.getVersions()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollectiondefinitions/latestmajors/TEST');
        callback = sinon.spy();
      });

      it('should retrieve all data collections definitions' , function () {
        requestHandler.respond([ 'CRRD', 'BCFR', 'BNMDR' ]);

        SkryvDefinition.getVersions('TEST').then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal([ 'CRRD', 'BCFR', 'BNMDR' ]);
      });

      it('should report any error while retrieving data collection definitions' , function () {
        requestHandler.respond(401);

        SkryvDefinition.getVersions('TEST').catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('Could not get data collections');
      });
    });
    describe('.get()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollectiondefinitions/1');
        callback = sinon.spy();
      });

      it('should retrieve a single data collections definition by id' , function () {
        requestHandler.respond('BCFR');

        SkryvDefinition.get(1).then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal('BCFR');
      });

      it('should report any error while retrieving data collection definition' , function () {
        requestHandler.respond(401);

        SkryvDefinition.get(1).catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('Could not get data collection');
      });
    });
    describe('.getLatest()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollectiondefinitions/latest/BCFR');
        callback = sinon.spy();
      });

      it('should retrieve a single data collections definition by name' , function () {
        requestHandler.respond({ name: 'BCFR' });

        SkryvDefinition.getLatest('BCFR').then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal({ name: 'BCFR' });
      });

      it('should report any error while retrieving data collection definition' , function () {
        requestHandler.respond(401);

        SkryvDefinition.getLatest('BCFR').catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('No data collection found with name: BCFR');
      });
    });
  });

  describe('SkryvDefinitionManager', function () {
    describe('.delete()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expectDELETE(SESSION_SERVER_URL + '/datacollectiondefinitions/manager/1');
        callback = sinon.spy();
      });

      it('should delete a data collection definition by name' , function () {
        requestHandler.respond({ name: 'BCFR' });

        SkryvDefinitionManager.delete(1).then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal({ name: 'BCFR' });
      });

      it('should report any error while deleting data collection definition' , function () {
        requestHandler.respond(401);

        SkryvDefinitionManager.delete(1).catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('Data collection could not be deleted');
      });
    });
    describe('.save()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expect('POST', SESSION_SERVER_URL + '/datacollectiondefinitions/manager/', 'Test');
        callback = sinon.spy();
      });

      it('should retrieve a single data collections definition by name' , function () {
        requestHandler.respond('TEST');

        SkryvDefinitionManager.save('Test').then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal('TEST');
      });

      it('should report any error while retrieving data collection definition' , function () {
        requestHandler.respond(401);

        SkryvDefinitionManager.save('Test').catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('It was not possible to save the data collection');
      });
    });
    describe('.publish()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expect('POST', SESSION_SERVER_URL + '/datacollectiondefinitions/manager/1/publish');
        callback = sinon.spy();
      });

      it('should retrieve a single data collections definition by name' , function () {
        requestHandler.respond('TEST');

        SkryvDefinitionManager.publish(1).then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal('TEST');
      });

      it('should report any error while retrieving data collection definition' , function () {
        requestHandler.respond(401);

        SkryvDefinitionManager.publish(1).catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('It was not possible to publish the data collection');
      });
    });
    describe('.update()', function () {

      var requestHandler;

      var callback;

      it('should update a data collection' , function () {
        requestHandler = $httpBackend.expect('PUT', SESSION_SERVER_URL + '/datacollectiondefinitions/manager/1',
          { dataCollectionDefinitionId: 1, minorVersion: '2', published: true });
        callback = sinon.spy();

        requestHandler.respond({ dataCollectionDefinitionId: 1, minorVersion: '2', published: true });

        SkryvDefinitionManager.update({ dataCollectionDefinitionId: 1, minorVersion: '2', published: true }).then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal({ dataCollectionDefinitionId: 1, minorVersion: '2', published: true });
      });

      it('should report any error while updating a data collection' , function () {
        requestHandler = $httpBackend.expect('PUT', SESSION_SERVER_URL + '/datacollectiondefinitions/manager/1',
          { dataCollectionDefinitionId: 1, 'version': '2','published': false });
        callback = sinon.spy();

        requestHandler.respond(401);

        SkryvDefinitionManager.update({ dataCollectionDefinitionId: 1, version: '2', 'published': false }).catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('It was not possible to update the data collection');
      });
    });
    describe('.getAll()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollectiondefinitions/manager/');
        callback = sinon.spy();
      });

      it('should retrieve all data collections definitions' , function () {
        requestHandler.respond([ 'CRRD', 'BCFR', 'BNMDR' ]);

        SkryvDefinitionManager.getAll().then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal([ 'CRRD', 'BCFR', 'BNMDR' ]);
      });

      it('should report any error while retrieving data collection definitions' , function () {
        requestHandler.respond(401);

        SkryvDefinitionManager.getAll().catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('Could not get data collections');
      });
    });
    describe('.get()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollectiondefinitions/manager/1');
        callback = sinon.spy();
      });

      it('should retrieve a single data collections definition by id' , function () {
        requestHandler.respond('BCFR');

        SkryvDefinitionManager.get(1).then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal('BCFR');
      });

      it('should report any error while retrieving data collection definition' , function () {
        requestHandler.respond(401);

        SkryvDefinitionManager.get(1).catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('Could not get data collection');
      });
    });
    describe('.getByName()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollectiondefinitions/manager/name/BCFR');
        callback = sinon.spy();
      });

      it('should retrieve a single data collections definition by id' , function () {
        requestHandler.respond('BCFR');

        SkryvDefinitionManager.getByName('BCFR').then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal('BCFR');
      });

      it('should report any error while retrieving data collection definition' , function () {
        requestHandler.respond(401);

        SkryvDefinitionManager.getByName('BCFR').catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('Could not get data collections');
      });
    });
    describe('.createWip()', function () {

      var requestHandler;

      var callback;

      beforeEach(function () {
        requestHandler = $httpBackend.expect('POST', SESSION_SERVER_URL + '/datacollectiondefinitions/manager/1/history', 'Test');
        callback = sinon.spy();
      });

      it('should retrieve a single data collections definition by name' , function () {
        requestHandler.respond('TEST');

        SkryvDefinitionManager.createWip(1, 'Test').then(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        var result = callback.firstCall.args[0];
        expect(result).to.deep.equal('TEST');
      });

      it('should report any error while retrieving data collection definition' , function () {
        requestHandler.respond(401);

        SkryvDefinitionManager.createWip(1, 'Test').catch(callback);

        $httpBackend.flush();

        expect(callback).to.have.been.calledOnce;
        expect(callback).to.have.been.calledWith('It was not possible to save your changes');
      });
    });
  });
});
