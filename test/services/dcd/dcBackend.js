/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('dcBackend', function () {

  var $httpBackend, SESSION_SERVER_URL, _, SkryvCollection;

  beforeEach(module('documentmodel'));
  beforeEach(module('gettext'));
  beforeEach(module('services.dcd'));

  beforeEach(inject(function (_$httpBackend_, _SESSION_SERVER_URL_, ___, _SkryvCollection_) {
    $httpBackend = _$httpBackend_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    _ = ___;
    SkryvCollection = _SkryvCollection_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.getCollections()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollections');
      callback = sinon.spy();
    });

    it('should retrieve all data collections' , function () {
      requestHandler.respond([ 'CRRD', 'BCFR', 'BNMDR' ]);

      SkryvCollection.getCollections().then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal([ 'CRRD', 'BCFR', 'BNMDR' ]);
    });

    it('should report any error while retrieving data collections' , function () {
      requestHandler.respond(401);

      SkryvCollection.getCollections().catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('No data collections found');
    });
  });

  describe('.getDefinition()', function () {

    var requestHandler;

    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expect('POST' ,SESSION_SERVER_URL + '/datacollections/organization', { name: 'hospital' });
      callback = sinon.spy();
    });

    it('should retrieve all collections for organization' , function () {
      requestHandler.respond({ 'CRRD': [ 'CRRD' ], 'BCFR': [ 'BCFR' ], 'BNMDR': [ 'BNMDR' ] });

      SkryvCollection.getByOrganization({ name: 'hospital' }).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      var result = callback.firstCall.args[0];
      expect(result).to.deep.equal([
        { name: 'BCFR', children: [ 'BCFR' ] },
        { name: 'BNMDR', children: [ 'BNMDR' ] },
        { name: 'CRRD', children: [ 'CRRD' ] }
      ]);
    });

    it('should report any error while retrieving data collections' , function () {
      requestHandler.respond(401);

      SkryvCollection.getByOrganization({ name: 'hospital' }).catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith('No data collections found for organization: hospital');
    });
  });
});
