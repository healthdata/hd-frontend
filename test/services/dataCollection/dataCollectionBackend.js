/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('dataCollectionBackend', function () {

  var $httpBackend;
  var dataCollectionManager;
  var ENVIRONMENT_URL;

  beforeEach(module('gettext'));
  beforeEach(module('services.dataCollection'));

  beforeEach(inject(function (_$httpBackend_, _ENVIRONMENT_URL_, _dataCollectionManager_) {
    $httpBackend = _$httpBackend_;
    ENVIRONMENT_URL = _ENVIRONMENT_URL_;
    dataCollectionManager = _dataCollectionManager_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.getDataCollectionRequests()', function () {

    var callback;

    beforeEach(function () {
      callback = sinon.spy();
    });

    it('should retrieve a all data collection access requests' , function () {
      $httpBackend
        .expectGET(
          ENVIRONMENT_URL + '/organizations/org123/dataCollectionAccessRequests?')
        .respond([ { accessRequest: {} }, { accessRequest: {} } ]);

      dataCollectionManager.getDataCollectionRequests('org123')
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith([
        { accessRequest: {} },
        { accessRequest: {} }
      ]);
    });

    it('should retrieve a all processed data collection access requests' , function () {
      $httpBackend
        .expectGET(
          ENVIRONMENT_URL + '/organizations/org123/dataCollectionAccessRequests?processed=true')
        .respond([]);

      dataCollectionManager.getDataCollectionRequests('org123', true)
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });

    it('should retrieve a all collection access requests by user' , function () {
      $httpBackend
        .expectGET(
          ENVIRONMENT_URL + '/organizations/org123/dataCollectionAccessRequests?&userId=someUser')
        .respond([]);

      dataCollectionManager.getDataCollectionRequests('org123', undefined, 'someUser')
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });

    it('should retrieve a all processed collection access requests by user' , function () {
      $httpBackend
        .expectGET(
          ENVIRONMENT_URL + '/organizations/org123/dataCollectionAccessRequests?processed=false&userId=someUser')
        .respond([]);

      dataCollectionManager.getDataCollectionRequests('org123', false, 'someUser')
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });

    it('should report any error while retrieving all user requests' , function () {
      $httpBackend
        .expectGET(
          ENVIRONMENT_URL + '/organizations/org123/dataCollectionAccessRequests?')
        .respond(401);

      dataCollectionManager.getDataCollectionRequests('org123')
        .catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });
  });

  describe('.postDataCollectionRequest()', function () {

    var requestHandler;
    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectPOST(
        ENVIRONMENT_URL + '/organizations/org123/dataCollectionAccessRequests',
        { dataCollections: [ 1, 2 ] });
      callback = sinon.spy();
    });

    it('should submit new collection access requests' , function () {
      requestHandler.respond({});

      dataCollectionManager.postDataCollectionRequest('org123', [ 1, 2 ])
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({});
    });

    it('should report any error ' , function () {
      requestHandler.respond(401);

      dataCollectionManager.postDataCollectionRequest('org123', [ 1, 2 ])
        .catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });
  });

  describe('.getDataProvidersForDataCollection()', function () {

    var requestHandler;
    var callback;

    beforeEach(function () {
      requestHandler = $httpBackend.expectGET(
        ENVIRONMENT_URL + '/datacollections/ABC/dataproviders');
      callback = sinon.spy();
    });

    it('should retrieve the data providers' , function () {
      requestHandler.respond([ {}, {} ]);

      dataCollectionManager.getDataProvidersForDataCollection('ABC')
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith([ {}, {} ]);
    });

    it('should report any error while retrieving all user requests' , function () {
      requestHandler.respond(401);

      dataCollectionManager.getDataProvidersForDataCollection('ABC')
        .catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });
  });

  describe('.decide()', function () {

    var requestHandler;
    var callback;
    var request;

    beforeEach(function () {
      request = {
        id: 123,
        dataCollections: [
          { key: 'ABC', value: true },
          { key: 'XYZ', value: false },
          { key: 'UVW', value: null }
        ]
      };
      requestHandler = $httpBackend.expectPOST(
        ENVIRONMENT_URL + '/organizations/org123/dataCollectionAccessRequests/123/decision',
        { ABC: true, XYZ: false, UVW: false });
      callback = sinon.spy();
    });

    it('should submit collection access decisions' , function () {
      requestHandler.respond({});

      dataCollectionManager.decide('org123', request)
        .then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
      expect(callback).to.have.been.calledWith({});
    });

    it('should report any error ' , function () {
      requestHandler.respond(401);

      dataCollectionManager.decide('org123', request)
        .catch(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });
  });

});
