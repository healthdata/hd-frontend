/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('SkryvOauth', function () {

  var $httpBackend, OAUTH_SERVER_URL, SkryvOauth;

  beforeEach(module('skryv.session'));
  beforeEach(module('skryv.user'));
  beforeEach(module('ngDialog'));
  beforeEach(module('services.notification'));
  beforeEach(module('LocalStorageModule'));
  beforeEach(module('services.dcd'));
  beforeEach(module('skryv.oauth'));
  beforeEach(module('gettext'));
  beforeEach(module('skryv.configurations'));
  beforeEach(module('documentmodel'));

  beforeEach(inject(function (_$httpBackend_, _OAUTH_SERVER_URL_, _SkryvOauth_) {
    $httpBackend = _$httpBackend_;
    OAUTH_SERVER_URL = _OAUTH_SERVER_URL_;
    SkryvOauth = _SkryvOauth_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('.getAccessToken()', function () {

    var requestHandler;

    var callback;

    it('should retrieve an accesstoken' , function () {
      requestHandler = $httpBackend.expectPOST(OAUTH_SERVER_URL + '?username=Test&password=Test&client_id=123&organization_id=321&grant_type=organization_password');
      callback = sinon.spy();

      var username = 'Test';
      var password = 'Test';
      var clientId = 123;
      var organizationId = 321;

      var response = {
        access_token: '5f75c6ed-fe17-4331-a610-354ee43391b5',
        expires_in: 10746,
        refresh_token: 'c2ea1702-0d72-4151-a224-47d6a14fffb1',
        scope: 'read trust write',
        token_type: 'bearer'
      };

      requestHandler.respond(response);

      SkryvOauth.getAccessToken(username, password, clientId, organizationId).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });

    it('should retrieve an accesstoken with special chars' , function () {
      requestHandler = $httpBackend.expectPOST(
        OAUTH_SERVER_URL +
        '?username=Test&password=%2BTest%26&client_id=123&organization_id=321&grant_type=organization_password');
      callback = sinon.spy();

      var username = 'Test';
      var password = '+Test&';
      var clientId = 123;
      var organizationId = 321;

      var response = {
        access_token: '5f75c6ed-fe17-4331-a610-354ee43391b5',
        expires_in: 10746,
        refresh_token: 'c2ea1702-0d72-4151-a224-47d6a14fffb1',
        scope: 'read trust write',
        token_type: 'bearer'
      };

      requestHandler.respond(response);

      SkryvOauth.getAccessToken(username, password, clientId, organizationId).then(callback);

      $httpBackend.flush();

      expect(callback).to.have.been.calledOnce;
    });
  });
});
