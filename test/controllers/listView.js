/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

//describe('listViewCtrl', function () {
//  var controller = null, $scope = null;var element; var $rootScopeMock;
//
//  beforeEach(function () {
//    module('frontendApp');
//  });
//
//  beforeEach(inject(function ($controller, $rootScope) {
//    $scope = $rootScope.$new();
//    $rootScopeMock = $rootScope.$new();
//    $rootScopeMock.activeDatacollection = { id: 1 };
//
//    controller = $controller('ListViewCtrl', {
//      $rootScope: $rootScopeMock,
//      $scope: $scope
//    });
//  }));
//
//  // Statuses
//  describe('Statuses', function () {
//    it('Status are loaded', function () {
//      expect($scope.statuses).not.to.have.lengthOf(0);
//    });
//    it('Should have the correct properties', function () {
//      $scope.statuses.forEach(function (item) {
//        item.should.have.property('name');
//        item.should.have.property('label');
//        item.should.have.property('icon');
//      });
//    });
//  });
//
//  // Table Settings
//  describe('Table Settings', function () {
//    it('Sort by column', function () {
//      $scope.currentPage = 2;
//      $scope.sortByColumn('id');
//      // Columns
//      expect($scope.columnSort.sortColumn).to.equal('id');
//      expect($scope.columnSort.reverse).to.be.false;
//      $scope.sortByColumn('id');
//      expect($scope.columnSort.reverse).to.be.true;
//      // Current page
//      expect($scope.currentPage).to.equal(1);
//    });
//  });
//
//  // Pagination
//  describe('Pagination', function () {
//    it('Page starts at 1', function () {
//      expect($scope.currentPage).to.equal(1);
//    });
//    it('Should set new page', function () {
//      $scope.setPage(2);
//      expect($scope.currentPage).to.equal(2);
//    });
//    it('Items per page should not be nul', function () {
//      $scope.setPage(2);
//      expect($scope.itemsPerPage).not.to.equal(0);
//    });
//    it('Should set new items per page', function () {
//      $scope.setItemsPerPage(2);
//      expect($scope.itemsPerPage).to.equal(2);
//    });
//  });
//
//  // Toolbar
//  describe('Toolbar', function () {
//    it('Statuses should be checked', function () {
//      $scope.statuses =  [ { name: 'IN_PROGRESS', icon: 'In Progress', label: 'In progress' } ];
//      $scope.selectedStatuses = {};
//      $scope.addStatuses();
//      expect($scope.statuses).to.have.lengthOf(1);
//      expect($scope.selectedStatuses.IN_PROGRESS).to.be.true;
//    });
//    it('Status filter', function () {
//      var workflow = { status: 'IN_PROGRESS' };
//      var result = $scope.statusFilter(workflow);
//      expect(result).to.be.true;
//    })
//  });
//});
