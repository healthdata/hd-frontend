/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('StatusListCtrl', function () {
  var $scope, SkryvStatus, requestDeferred, $q, controller, $httpBackend, ngDialog, $filter;

  beforeEach(function () {
    module('skryv.status');
    module('ngDialog');
    module('documentmodel');
    module('frontendApp');
  });

  beforeEach(function () {
    // Mock Session
    SkryvStatus = {
      getStatusMessagesByDate: sinon.stub()
    };

    module(function ($provide) {
      $provide.value('SkryvStatus', SkryvStatus);
    });
  });

  beforeEach(inject(function ($controller, $rootScope, _$q_, _$httpBackend_, _ngDialog_, _$filter_) {
    $q = _$q_;
    $filter = _$filter_;
    requestDeferred = $q.defer();
    $httpBackend = _$httpBackend_;
    ngDialog = _ngDialog_;
    SkryvStatus.getStatusMessagesByDate.returns(requestDeferred.promise);
    $scope = $rootScope.$new();
    controller =  $controller('StatusListCtrl', {
      $scope: $scope
    });
  }));

  it('should set the messages on page and process them', function () {
    requestDeferred.resolve([
      { content: { validation: { statuses: { status: 'NOK', error: [ 'First error', 'Second error' ] } } } },
      { content: { validation: { statuses: { status: 'OK' } } } }
    ]);
    $scope.$digest();
    expect(SkryvStatus.getStatusMessagesByDate).have.been.calledOnce;
    expect($scope.messages).to.have.lengthOf(2);

    var message = $scope.messages[0].content.validation.statuses.error.join('\n\n');

    expect($scope.messages).to.deep.equal([
      { content: { validation: { statuses: { status: 'NOK', error: [ 'First error', 'Second error' ], errorMessage: message } } } },
      { content: { validation: { statuses: { status: 'OK' } } } }
    ])
  });

  it('should set the messages after date picked', function () {
    requestDeferred.resolve([
      { content: { validation: { statuses: { status: 'OK' } } } }
    ]);
    $scope.dateOfMessages = new Date();
    $scope.searchFiltersChanged();
    $scope.$digest();
    expect(SkryvStatus.getStatusMessagesByDate).have.been.calledTwice;
    expect($scope.messages).to.have.lengthOf(1);
  });

  it('should sort on column', function () {
    $scope.sortByColumn('Test');
    expect($scope.columnSort.sortColumn).to.equal('Test');
    expect($scope.columnSort.reverse).to.equal(false);

    $scope.sortByColumn('Other');
    expect($scope.columnSort.sortColumn).to.equal('Other');
    expect($scope.columnSort.reverse).to.equal(false);

    $scope.sortByColumn('Other');
    expect($scope.columnSort.reverse).to.equal(true);
  });

  it('should check if string is date', function () {
    $scope.columnSort.sortColumn = 'test';
    expect($scope.orderByDate({ test: 'test' })).to.equal('test');
    var data = $scope.orderByDate({ test: '14-06-1999 12:30:33' });
    expect(data._isAMomentObject).to.be.true;
  });

  it('should open date picker', function () {
    var $event = {
      preventDefault: function () {},
      stopPropagation: function () {}
    };
    $scope.open($event, 'date');
    expect($scope.date).to.equal(true);
  });

  it('should filter empty values to bottom', function () {
    var messages = [ { test: 2 }, { test: 1 } ];

    expect($filter('nullToBottomStatus')({}, 'test')).to.deep.equal(undefined);
    expect($filter('nullToBottomStatus')(messages, 'test')).to.deep.equal([ { test: 2 }, { test: 1 } ]);

    messages = [ { metaData: { test: 1 } } ];
    expect($filter('nullToBottomStatus')(messages, 'test')).to.deep.equal([ { metaData: { test: 1 } } ]);
  })

});
