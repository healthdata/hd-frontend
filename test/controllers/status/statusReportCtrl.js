'use strict';

describe('StatusReportCtrl', function () {
  var $scope, SkryvStatus, requestDeferred, $q, controller, $httpBackend, ngDialog, $filter, Statuses, SkryvCollection, requestDeferredDC;

  beforeEach(function () {
    module('skryv.status');
    module('ngDialog');
    module('documentmodel');
    module('frontendApp');
  });

  beforeEach(function () {
    // Mock Session
    SkryvStatus = {
      getStatusMessages: sinon.stub()
    };

    Statuses = {
      withProperty: function () {
        return [ { status: 'IN_PROGRESS', displayInStatus: true } ];
      }
    };

    SkryvCollection = {
      getCollections: sinon.stub()
    };

    module(function ($provide) {
      $provide.value('SkryvStatus', SkryvStatus);
      $provide.value('Statuses', Statuses);
      $provide.value('SkryvCollection', SkryvCollection);
    });
  });

  beforeEach(inject(function ($controller, $rootScope, _$q_, _$httpBackend_, _ngDialog_, _$filter_) {
    $q = _$q_;
    $filter = _$filter_;
    requestDeferred = $q.defer();
    requestDeferredDC = $q.defer();
    $httpBackend = _$httpBackend_;
    ngDialog = _ngDialog_;
    SkryvStatus.getStatusMessages.returns(requestDeferred.promise);
    SkryvCollection.getCollections.returns(requestDeferredDC.promise);
    $scope = $rootScope.$new();
    controller =  $controller('StatusReportCtrl', {
      $scope: $scope
    });
  }));

  it('should set the statuses on scope', function () {
    expect($scope.statuses).to.have.lengthOf(1);
    expect($scope.statuses).to.deep.equal([ { status: 'IN_PROGRESS',  displayInStatus: true } ]);
  });

  it('should set the data collection definitions', function () {
    requestDeferredDC.resolve([ 'TEST', 'BEWSD' ]);
    $scope.$digest();
    expect($scope.collections).to.deep.equal([ 'TEST', 'BEWSD' ]);
  });

  it('should set the messages on page', function () {
    requestDeferred.resolve([
      { content: { validation: { statuses: { status: 'NOK', error: [ 'First error', 'Second error' ] } } } },
      { content: { validation: { statuses: { status: 'OK' } } } }
    ]);
    $scope.$digest();
    expect(SkryvStatus.getStatusMessages).have.been.calledOnce;
    expect($scope.messages).to.have.lengthOf(2);

    var message = $scope.messages[0].content.validation.statuses.error.join('\n\n');

    expect($scope.messages).to.deep.equal([
      { content: { validation: { statuses: { status: 'NOK', error: [ 'First error', 'Second error' ] } } } },
      { content: { validation: { statuses: { status: 'OK' } } } }
    ])
  });

  it('should sort on column', function () {
    $scope.sortByColumn('Test');
    expect($scope.columnSort.sortColumn).to.equal('Test');
    expect($scope.columnSort.reverse).to.equal(false);

    $scope.sortByColumn('Other');
    expect($scope.columnSort.sortColumn).to.equal('Other');
    expect($scope.columnSort.reverse).to.equal(false);

    $scope.sortByColumn('Other');
    expect($scope.columnSort.reverse).to.equal(true);
  });

  it('should filter empty values to bottom', function () {
    var messages = [ { test: 2 }, { test: 1 } ];

    expect($filter('nullToBottomStatus')({}, 'test')).to.deep.equal(undefined);
    expect($filter('nullToBottomStatus')(messages, 'test')).to.deep.equal([ { test: 2 }, { test: 1 } ]);

    messages = [ { metaData: { test: 1 } } ];
    expect($filter('nullToBottomStatus')(messages, 'test')).to.deep.equal([ { metaData: { test: 1 } } ]);
  })

});
