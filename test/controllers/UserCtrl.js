/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('UserCtrl', function () {
  var Authorities, SkryvCollection, SkryvUser, Notifications, ngDialog,
    controller, $scope, $q, requestDeferred, $location, SkryvSession, transFilter, requestDefinitions, requestAuth;

  beforeEach(module('gettext'));
  beforeEach(module('services.notification'));
  beforeEach(module('skryv.user'));
  beforeEach(module('skryv.session'));
  beforeEach(module('ngDialog'));
  beforeEach(module('documentmodel'));
  beforeEach(module('LocalStorageModule'));
  beforeEach(module('services.dcd'));

  beforeEach(function () {
    // Mock Session
    SkryvCollection = {
      getByOrganization: sinon.stub()
    };

    // Mock Authorities
    Authorities = {
      Authorities:  function () {
        return [ 'auths' ];
      }
    };

    SkryvSession = {
      getLoggedUser: function () {
        return { organization: 'test' };
      },
      getAuthType: sinon.stub()
    };

    // Mock User
    SkryvUser = {
      get: sinon.stub(),
      save: sinon.stub(),
      delete: sinon.stub(),
      update: sinon.stub(),
      getAllLdapUsers: sinon.stub()
    };

    module(function ($provide) {
      $provide.value('SkryvCollection', SkryvCollection);
      $provide.value('Authorities', Authorities);
      $provide.value('SkryvUser', SkryvUser);
      $provide.value('SkryvSession', SkryvSession);
    });
  });

  beforeEach(inject(function ($controller, $rootScope, _Notifications_, _$q_, _$location_, _ngDialog_, _transFilter_) {
    $q = _$q_;
    $scope = $rootScope.$new();
    Notifications = _Notifications_;
    $location = _$location_;
    ngDialog = _ngDialog_;
    transFilter = _transFilter_;
  }));

  describe('UserCtrl: userListCtrl', function () {

    beforeEach(inject(function ($controller) {
      requestDeferred = $q.defer();
      SkryvUser.get.returns(requestDeferred.promise);
      SkryvUser.delete.returns(requestDeferred.promise);
      controller =  $controller('userListCtrl', {
        $scope: $scope
      });
    }));

    it('should sort on username', function () {
      expect($scope.columnSort).to.deep.equal({ reverse: false, sortColumn: 'username' })
    });

    it('should get all users and authorities', function () {
      requestDeferred.resolve([ { username: 'John', authorities: [ { authority: 'ROLE_USER'} ] }, { username: 'Doe' } ]);
      $scope.$digest();
      expect(SkryvUser.get).to.have.been.calledOnce;
      expect($scope.users).to.have.lengthOf(2);
      expect($scope.users[0]).to.deep.equal( { username: 'John', authorities: [ { authority: 'ROLE_USER'} ], authority: 'user' } );
    });

    it('should notify when loading users fails', function () {
      requestDeferred.reject('FAILED');
      $scope.$digest();
      expect(SkryvUser.get).to.have.been.calledOnce;
      expect(Notifications.notifications[0].content).to.equal('Users not found');
    });

    it('should open dialog with 2 buttons for user removal', function () {
      $scope.remove();

      $scope.$digest();
      var dialogContent = ngDialog.$result;
      expect(dialogContent.find('button')).to.have.lengthOf(2);
    });

    it('should remove user and navigate', function () {
      requestDeferred.resolve({ id: 1 });
      $scope.userEdit = { id: '1' };

      $scope.remove(true);

      $scope.$digest();

      expect(SkryvUser.delete).to.have.been.calledOnce;
      expect(Notifications.notifications[0].content).to.equal('User has been deleted!');
    });

    it('should notify when failed to remove user', function () {
      requestDeferred.reject('FAILED');
      $scope.userEdit = { id: '1' };

      $scope.remove(true);

      $scope.$digest();
      expect(SkryvUser.delete).to.have.been.calledOnce;
      var notifications = Notifications.notifications.map(function (n) { return n.content; });
      expect(notifications).to.include('Users not found');
      expect(notifications).to.include('FAILED');
    });

    it('should sort the columns', function () {
      $scope.sortByColumn('Test');
      expect($scope.columnSort.sortColumn).to.equal('Test');
      expect($scope.columnSort.reverse).to.equal(false);

      $scope.sortByColumn('Other');
      expect($scope.columnSort.sortColumn).to.equal('Other');
      expect($scope.columnSort.reverse).to.equal(false);

      $scope.sortByColumn('Other');
      expect($scope.columnSort.reverse).to.equal(true);
    });

  });

  describe('UserCtrl: userEditCtrl', function () {
    var ngDialog;

    beforeEach(inject(function ($controller, $rootScope, _ngDialog_) {
      requestDeferred = $q.defer();
      requestAuth = $q.defer();
      requestDefinitions = $q.defer();
      SkryvCollection.getByOrganization.returns(requestDefinitions.promise);
      SkryvUser.get.returns(requestDeferred.promise);
      SkryvUser.delete.returns(requestDeferred.promise);
      SkryvUser.update.returns(requestDeferred.promise);
      SkryvUser.get.returns(requestDeferred.promise);
      SkryvSession.getAuthType.returns(requestAuth.promise)
      requestAuth.resolve('DATABASE');
      $scope = $rootScope.$new();
      ngDialog = _ngDialog_;
      controller =  $controller('userEditCtrl', {
        $scope: $scope
      });
    }));

    it('should get and set user on scope', function () {
      requestDeferred.resolve({ username: 'John', password: '1234567', enabled: true, dataCollectionNames: [ 'BCFR', 'CRRD' ] });
      $scope.$digest();
      expect(SkryvUser.get).to.have.been.calledOnce;
      expect($scope.userEdit).to.deep.equal({ username: 'John', enabled: true, dataCollectionNames: [ 'BCFR', 'CRRD' ] });
    });

    it('should notify when loading user fails', function () {
      requestDeferred.reject('FAILED');
      $scope.$digest();
      expect(SkryvUser.get).to.have.been.calledOnce;
      expect(Notifications.notifications[0].content).to.equal('FAILED');
    });

    it('should have default values for checkboxes', function () {
      //requestDefinitions.resolve( [ 'BCFR', 'CRRD', 'TEST', 'BNMDR', 'IQED' ] );
      requestDefinitions.resolve( [
        { name: 'BCFR', children: [ 'BCFR' ] },
        { name: 'BNMDR', children: [ 'BNMDR' ] },
        { name: 'CRRD', children: [ 'CRRD' ] },
        { name: 'IQED', children: [ 'IQED' ] },
        { name: 'TEST', children: [ 'TEST' ] }
      ] );
      requestDeferred.resolve({ username: 'John', enabled: true, password: '1234567', dataCollectionNames: [ 'BCFR', 'CRRD' ] });
      $scope.$digest();
      // Filter DC assigned to user
      expect($scope.dataCollections).to.deep.equal([
        { name: 'BCFR', children: [ 'BCFR' ] },
        { name: 'BNMDR', children: [ 'BNMDR' ] },
        { name: 'CRRD', children: [ 'CRRD' ] },
        { name: 'IQED', children: [ 'IQED' ] },
        { name: 'TEST', children: [ 'TEST' ] }
      ]);
      // Filter Actives
      expect($scope.userDataCollections).to.deep.equal({ BCFR: 'BCFR', CRRD: 'CRRD' });
    });

    it('should have default values for multiselect', function () {
      requestDefinitions.resolve([
        { name: 'BCFR' },
        { name: 'BNMDR' },
        { name: 'CRRD' },
        { name: 'IQED' },
        { name: 'IQEDFOOT' },
        { name: 'TEST' }
      ]);
      $scope.$digest();
      requestDeferred.resolve({ username: 'John', password: '1234567', dataCollectionNames: [ 'BCFR', 'CRRD' ] });
      $scope.$digest();
      // Filter DC assigned to user
      // Should mark the assigned DC
      expect($scope.dataCollections).to.deep.equal([
        { name: 'BCFR', ticked: true },
        { name: 'BNMDR' },
        { name: 'CRRD', ticked: true },
        { name: 'IQED' },
        { name: 'IQEDFOOT' },
        { name: 'TEST' }
      ]);
      // Filter Actives (only used for checkboxes, the multiselect uses multiDataCollections as input)
      expect($scope.userDataCollections).to.deep.equal({});
    });

    it('should notify when loading datacollections fails', function () {
      requestDeferred.resolve({ username: 'John', password: '1234567', enabled: true, dataCollectionNames: [ 'BCFR', 'CRRD' ] });
      requestDefinitions.reject('FAILED');
      $scope.$digest();
      expect(SkryvUser.get).to.have.been.calledOnce;
      expect(Notifications.notifications[0].content).to.equal('FAILED');
    });

    it('should open dialog with 2 buttons for user removal', function () {
      requestDeferred.resolve({ id: 1 });
      $scope.$digest();
      expect($scope.userEdit.id).to.equal(1);

      $scope.remove();

      $scope.$digest();
      var dialogContent = ngDialog.$result;
      expect(dialogContent.find('button')).to.have.lengthOf(2);
    });

    it('should remove user and navigate', function () {
      requestDeferred.resolve({ id: 1 });
      $scope.userEdit = { id: '1' };

      $scope.remove(true);

      $scope.$digest();

      expect(SkryvUser.delete).to.have.been.calledOnce;
      expect(Notifications.notifications[0].content).to.equal('User has been deleted!');

      $scope.$apply();

      expect($location.path()).to.equal('/user/list');
    });

    it('should notify when failed to remove user', function () {
      requestDeferred.reject('FAILED');
      $scope.userEdit = { id: '1' };

      $scope.remove(true);

      $scope.$digest();
      expect(SkryvUser.delete).to.have.been.calledOnce;
      expect(Notifications.notifications[0].content).to.equal('FAILED');
    });

    it('should update user and navigate', function () {
      var userEdit = {
        id: '12',
        username: 'John',
        dataCollectionNames: ['BCFR'],
        passwordRepeat: 'Test',
        email: 'john@doe.com',
        authorities: [ { authority: 'ROLE_USER' } ]
      };

      requestDeferred.resolve(userEdit);
      requestDefinitions.resolve([ { name: 'BCFR' } ]);

      $scope.$digest();

      $scope.userEdit = userEdit;
      $scope.user_form = {
        $valid: true
      };

      $scope.save();
      $scope.$digest();

      expect(SkryvUser.update).to.have.been.calledOnce;
      expect(SkryvUser.update).to.have.been.calledWith({
        authorities: [{ authority: "ROLE_USER" }],
        dataCollectionNames: [ "BCFR" ],
        id: "12",
        email: 'john@doe.com',
        username: "John"
      });
      $scope.$apply();

      expect($location.path()).to.equal('/user/list');
      expect(Notifications.notifications[0].content).to.equal('The user was successfully edited!');
    });

    it('should update admin and navigate', function () {
      var userEdit = {
        id: '12',
        username: 'John',
        passwordRepeat: 'Test',
        email: 'john@doe.com',
        authorities: [ { authority: 'ROLE_ADMIN' } ]
      };

      requestDeferred.resolve(userEdit);
      requestDefinitions.resolve([{ name: 'BCFR', children: ['BCFR'] }]);

      $scope.$digest();

      $scope.userEdit = userEdit;
      $scope.user_form = {
        $valid: true
      };

      $scope.save();
      $scope.$digest();

      expect(SkryvUser.update).to.have.been.calledOnce;
      expect(SkryvUser.update).to.have.been.calledWith({
        authorities: [{ authority: "ROLE_ADMIN" }],
        dataCollectionNames: [],
        id: "12",
        email: 'john@doe.com',
        username: "John"
      });
      $scope.$apply();

      expect($location.path()).to.equal('/user/list');
      expect(Notifications.notifications[0].content).to.equal('The user was successfully edited!');
    });

    it('should notify when failed to update user', function () {
      $scope.userEdit = {
        id: '12',
        username: 'John',
        dataCollectionNames: ['BCFR'],
        passwordRepeat: 'Test',
        email: 'john@doe.com',
        authorities: [ { authority: 'ROLE_USER' } ]
      };

      $scope.dataCollections = [{ name: 'BCFR' }];
      $scope.userDataCollections = { BCFR: 'BCFR' };
      $scope.$digest();

      requestDeferred.reject('User could not be updated');

      $scope.user_form = {
        $valid: true
      };

      $scope.save();
      $scope.$digest();

      expect(SkryvUser.update).to.have.been.calledOnce;
      expect(Notifications.notifications[0].content).to.equal('User could not be updated');
    });

    it('should toggle password edit', function () {
      expect($scope.editPassword).to.be.false;
      $scope.togglePassword();
      expect($scope.editPassword).to.be.true;
      $scope.togglePassword();
      expect($scope.editPassword).to.be.false;
    });

    it('should notify when wrong password', function () {
      $scope.userEdit = { username: 'test', password: 'yes', email: 'john@doe.com', authorities: [ { authority: 'ROLE_USER' } ] };
      $scope.editPassword = true;
      $scope.user_form = {
        $valid: true
      };
      $scope.userEdit.passwordRepeat = 'no';

      $scope.save();
      expect(Notifications.notifications[0].content).to.equal('Repeat of new password is not the same');
    });

  });

  describe('UserCtrl: userNewCtrl', function () {
    beforeEach(inject(function ($controller, $rootScope) {
      requestDeferred = $q.defer();
      SkryvUser.save.returns(requestDeferred.promise);
      requestDefinitions = $q.defer();
      SkryvCollection.getByOrganization.returns(requestDefinitions.promise);
      $scope = $rootScope.$new();
      controller =  $controller('userNewCtrl', {
        $scope: $scope,
        prefilledUser: null
      });
    }));

    it('should have default values', function () {
      expect($scope.userEdit).to.deep.equal({ organization: 'test' });
      expect($scope.userDataCollections).to.deep.equal({});
      expect($scope.authorities).to.deep.equal([ 'auths' ]);
    });

    it('should create new user and navigate', function () {
      requestDeferred.resolve({ username: 'John' });
      $scope.user_form = {
        $valid: true
      };

      $scope.userDataCollections = { IQED: 'IQED', TEST: 'TEST', BCFR: 'BCFR' };
      $scope.userEdit = { username: 'John', password: 'test', email: 'john@doe.com', enabled: 'enabled', passwordRepeat:'test', authorities: [ { authority: 'ROLE_USER' } ], organization: { name: 'Hospital'} };

      $scope.dataCollections = [{ name: 'BCFR' }, { name: 'TEST' }];

      $scope.save();
      $scope.$digest();

      expect(SkryvUser.save).to.have.been.calledOnce;
      expect(SkryvUser.save).to.have.been.calledWith({
        authorities: [{ authority: "ROLE_USER" }],
        dataCollectionNames: ["BCFR", "TEST"],
        enabled: 'enabled',
        email: 'john@doe.com',
        organization: { name: "Hospital" },
        password: "test",
        username: "John"
      });
      $scope.$apply();

      expect($location.path()).to.equal('/user/list');
      expect(Notifications.notifications[0].content).to.equal('The user was successfully created!');
    });

    it('should create new admin and navigate', function () {
      requestDeferred.resolve({ username: 'John' });
      $scope.user_form = {
        $valid: true
      };
      $scope.userEdit = { username: 'John', password: 'test', enabled: 'enabled', email: 'john@doe.com', passwordRepeat:'test', authorities: [ { authority: 'ROLE_ADMIN' } ], organization: { name: 'Hospital'} };
      $scope.save();
      $scope.$digest();

      expect(SkryvUser.save).to.have.been.calledOnce;
      expect(SkryvUser.save).to.have.been.calledWith({
        authorities: [{ authority: "ROLE_ADMIN" }],
        dataCollectionNames: [],
        enabled: 'enabled',
        email: 'john@doe.com',
        organization: { name: "Hospital" },
        password: "test",
        username: "John"
      });
      $scope.$apply();

      expect($location.path()).to.equal('/user/list');
      expect(Notifications.notifications[0].content).to.equal('The user was successfully created!');
    });

    it('should notify when failed to create new user', function () {
      requestDeferred.reject('User could not be created');

      $scope.user_form = {
        $valid: true
      };
      $scope.userEdit = { username: 'John', email: 'john@doe.com', password: 'test', passwordRepeat:'tst',enabled: 'enabled', authorities: [ { authority: 'ROLE_USER' } ], organization: { name: 'Hospital'} };
      $scope.userDataCollections = { IQED: 'IQED', TEST: 'TEST', BCFR: 'BCFR' };
      $scope.dataCollections = [{ name: 'BCFR' }, { name: 'TEST' }];
      $scope.save();
      $scope.$digest();

      expect(Notifications.notifications[0].content).to.equal('Repeat of new password is not the same');
      $scope.userEdit.passwordRepeat = 'test';

      $scope.save();
      $scope.$digest();

      expect(SkryvUser.save).to.have.been.calledOnce;
      expect(Notifications.notifications[1].content).to.equal('User could not be created');
    });

    it('should get the definitions', function () {
      requestDefinitions.resolve([{ name: 'BCFR', children: [ 'BCFR' ] }]);
      $scope.getDefinitions();
      $scope.$digest();
      expect($scope.dataCollections).to.deep.equal([ { name: 'BCFR', children: [ 'BCFR' ] } ]);
    });

    it('should fail get the definitions', function () {
      requestDefinitions.reject('FAILED');
      $scope.getDefinitions();
      $scope.$digest();
      expect(Notifications.notifications[0].content).to.equal('FAILED');
    });

  });

  describe('UserCtrl: userSearchCtrl', function () {
    beforeEach(inject(function ($controller) {
      requestDeferred = $q.defer();
      requestAuth = $q.defer();
      SkryvUser.save.returns(requestDeferred.promise);
      SkryvUser.getAllLdapUsers.returns(requestDeferred.promise);
      SkryvSession.getAuthType.returns(requestAuth.promise)
      requestAuth.resolve('DATABASE');
      controller =  $controller('userSearchCtrl', {
        $scope: $scope
      });
    }));

    it('should search for ldap user', function () {
      requestDeferred.resolve([ { username: 'John' } ]);

      expect($scope.loading).to.equal(false);
      expect($scope.searched).to.equal(false);

      $scope.search();
      expect($scope.loading).to.equal(true);

      $scope.$digest();
      $scope.$digest();

      expect($scope.users).to.deep.equal([ { username: 'John' } ]);
    });

    it('should fail searching for ldap user', function () {
      requestDeferred.reject('no users found');

      expect($scope.loading).to.equal(false);
      expect($scope.searched).to.equal(false);

      $scope.search();
      expect($scope.loading).to.equal(true);

      $scope.$digest();

      expect($scope.loading).to.equal(false);

      expect(Notifications.notifications[0].content).to.equal('no users found');
    });
  });
});
