/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('Platform Style', function () {
  describe('HeaderCtrl', function () {
    var $controller, $scope;

    beforeEach(function () {
      module('frontendApp');
    });

    beforeEach(inject(function (_$controller_, $rootScope) {
      $scope = $rootScope.$new();
      $controller = _$controller_;
    }));

    it('should support platform HD4DP', function () {
      $controller('HeaderCtrl', {
        $scope: $scope,
        PLATFORM: 'hd4dp'
      });
      expect($scope.platform).to.equal('hd4dp');
      expect($scope.isHD4DP).to.be.true;
      expect($scope.isHD4RES).to.be.false;
    });

    it('should support platform HD4RES', function () {
      $controller('HeaderCtrl', {
        $scope: $scope,
        PLATFORM: 'hd4res'
      });
      expect($scope.platform).to.equal('hd4res');
      expect($scope.isHD4DP).to.be.false;
      expect($scope.isHD4RES).to.be.true;
    });
  });
});
