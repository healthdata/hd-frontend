/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('Footer', function () {
  describe('FooterCtrl', function () {
    var controller, $scope, mockBackend, $location, SkryvVersion, requestDeferred, $q, $rootScope;

    beforeEach(function () {
      module('frontendApp');
    });

    beforeEach(function () {
      // Mock Session
      SkryvVersion = {
        get: sinon.stub()
      };

      module(function ($provide) {
        $provide.value('SkryvVersion', SkryvVersion);
      });
    });

    beforeEach(inject(function (_$httpBackend_, $controller, _$rootScope_, _$location_, _$q_) {
      $rootScope = _$rootScope_;
      $q = _$q_;
      requestDeferred = $q.defer();
      SkryvVersion.get.returns(requestDeferred.promise);
      $scope = $rootScope.$new();
      mockBackend = _$httpBackend_;
      $location = _$location_;
      $controller('FooterCtrl', {
        $scope: $scope
      });
    }));

    it('should be default visible', function () {
      expect($scope.visible).to.be.true;
    });

    it('should get version', function () {
      requestDeferred.resolve({ value: '1.1.1' });
      $scope.$digest();
      expect($scope.version).to.equal('1.1.1')
    });

    it('should keep footer visible', function () {
      $location.path('/document/5');
      $rootScope.$broadcast('$routeChangeSuccess', {});
      expect($scope.visible).to.be.false;
    });
  });
});
