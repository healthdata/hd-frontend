/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe.skip('ListViewCtrl', function () {

  var Statuses, SkryvSession, SkryvUser, SkryvView, controller, ngDialog, $compile, $timeout, WORKFLOW_SERVER_URL,
    $scope, $location, $q, requestDeferred, SkryvCSV, Notifications, $filter, WorkflowActions, $httpBackend, ListViewWorkflowsActions, SESSION_SERVER_URL;

  beforeEach(module('gettext'));
  beforeEach(module('services.notification'));
  beforeEach(module('ngDialog'));
  beforeEach(module('frontendApp'));

  beforeEach(function () {
    var ListViewDocColumns = {
      documentDefinition: function () {},
      listen: function () {},
      resetInternalState : function () {},
      setDefaultColumns: function () {}
    };

    // Mock Session
    SkryvSession = {
      getCollections: function () {
        return [ { name: 'BCFR', label: 'BCFR', versions: [] } ];
      },
      getDefinition: sinon.stub(),
      getLoggedUser: sinon.stub(),
      loadApp: function () {
        return {
          then: function() {
            return
          }
        }
      },
      getUserTokens: function () {
        return true;
      }
    };

    // Mock SkryvView
    SkryvView = {
      addViewDialog: sinon.stub(),
      updateViewDialog: sinon.stub(),
      deleteViewDialog: sinon.stub(),
      saveLastView: sinon.stub(),
      setLastView: function () {},
      resetInternalState : function () {},
      getLastView: function () { return {} },
      listen: function () {},
      getViews: function () {
        return [ 'view1', 'view2' ];
      }
    };

    WorkflowActions = {
      applyAction: sinon.stub()
    };

    // Mock Statuses
    Statuses = {
      statuses:  function () {
        return [ {
          label: 'In progress',
          name: 'IN_PROGRESS',
          platform: 'hd4dp'
        }, {
          label: 'Submitted',
          name: 'SUBMITTED',
          platform: 'hd4res'
        }, {
          label: 'Corrections needed',
          name: 'CORRECTIONS_NEEDED',
          platform: 'hd4dp'
        } ];
      }
    };

    // Mock User
    SkryvUser = {
      get: sinon.stub(),
      save: sinon.stub(),
      delete: sinon.stub(),
      update: sinon.stub()
    };

    // Mock Skryv CSV
    SkryvCSV = {
      downloadCsv: sinon.stub(),
      uploadCsv: sinon.stub()
    };

    ListViewWorkflowsActions = {
      listen: sinon.stub(),
      loadWorkflows: sinon.stub()
    };


    module(function ($provide) {
      $provide.value('SkryvSession', SkryvSession);
      $provide.value('Statuses', Statuses);
      $provide.value('SkryvUser', SkryvUser);
      $provide.value('SkryvView', SkryvView);
      $provide.value('SkryvCSV', SkryvCSV);
      $provide.value('WorkflowActions', WorkflowActions);
      $provide.value('ListViewWorkflowsActions', ListViewWorkflowsActions);
      $provide.value('ListViewDocColumns', ListViewDocColumns);
    });
  });

  beforeEach(inject(function ($controller, $rootScope, _$location_, _$q_, _$httpBackend_, _WORKFLOW_SERVER_URL_,
                              _Notifications_, _$filter_, _ngDialog_, _WorkflowActions_, _$compile_, _$timeout_ , _SESSION_SERVER_URL_) {
    $scope = $rootScope.$new();
    WORKFLOW_SERVER_URL = _WORKFLOW_SERVER_URL_;
    $location = _$location_;
    $q = _$q_;
    Notifications = _Notifications_;
    $filter = _$filter_;
    ngDialog = _ngDialog_;
    WorkflowActions = _WorkflowActions_;
    $httpBackend = _$httpBackend_;
    $compile = _$compile_;
    $timeout = _$timeout_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
  }));

  describe('ListViewCtrl', function () {

    beforeEach(inject(function ($controller) {
      requestDeferred = $q.defer();
      SkryvView.addViewDialog.returns(requestDeferred.promise);
      SkryvView.updateViewDialog.returns(requestDeferred.promise);
      SkryvView.deleteViewDialog.returns(requestDeferred.promise);
      SkryvView.saveLastView.returns(requestDeferred.promise);
      WorkflowActions.applyAction.returns(requestDeferred.promise);
      SkryvCSV.downloadCsv.returns(false);
      SkryvCSV.uploadCsv.returns(true);
      SkryvSession.getLoggedUser.returns(false);
      controller =  $controller('ListViewCtrl', {
        $scope: $scope,
        WORKFLOWS_COUNT_MAXIMUM: 10,
        PLATFORM: 'hd4dp'
      });
    }));

    describe('Function', function () {
      it('should open datepicker dialog', function () {
        var $event = {
          preventDefault: function () {},
          stopPropagation: function () {}
        };
        $scope.open($event, 'date1');
        expect($scope.openDatePicker.date1).to.be.true;
      });

      it('should have default values', function () {
        // Constants
        expect($scope.platform).to.equal('hd4dp');
        expect($scope.datepicker).to.deep.equal({ format: 'dd-MM-yyyy' });
      });

      it('should redirect to document: $scope.goToDoc', function () {
        $scope.goToDoc(5);
        $scope.$digest();
        expect($location.path()).to.equal('/document/5');
      });
    });

    describe('Table Settings', function () {
      it('should sort on column', function () {
        $scope.sortByColumn('Test');
        expect($scope.columnSort.sortColumn).to.equal('Test');
        expect($scope.currentPage).to.equal(1);
        expect($scope.columnSort.reverse).to.equal(false);

        $scope.sortByColumn('Other');
        expect($scope.columnSort.sortColumn).to.equal('Other');
        expect($scope.columnSort.reverse).to.equal(false);

        $scope.sortByColumn('Other');
        expect($scope.columnSort.reverse).to.equal(true);
      });
    });

    describe('CSV Management', function () {
      it('should upload a CSV, fails on reader', function () {
        $httpBackend.expectGET('views/popupLoader.html').respond(201);
        $scope.files = [ { name:'foo.csv' } ];
        $scope.$digest();
        // PhantomJS doesn't trigger the reader.onload
        if (window.navigator.userAgent.indexOf('PhantomJS') == -1) {
          expect(Notifications.notifications[0].content).to.equal('CSV upload failed');
        }
      });

      it('should not upload CSV-type', function () {
        $scope.files = [ { name: 'BCFR.txt' } ];
        $scope.$digest();
        expect(Notifications.notifications[0].content).to.equal('Please select a CSV file.');
      });
    });

    describe('Actions', function () {
      it('should show bulk submit dialog', function () {
        $scope.validSubmitDate = true;
        $scope.selectedRegistrations = [ 'workflow' ];
        $scope.selectedValidSubmit = [ 'workflow' ];
        $scope.submitSelectedDialog();
        $scope.$digest();

        expect($scope.bulkWorkflows).to.equal(0);
        expect(ngDialog.$result.length).to.equal(1);
        expect(ngDialog.$result[0].innerHTML).to.have.string('submitSelected()');
      });

      it('should show bulk delete dialog', function () {
        $scope.validSubmitDate = true;
        $scope.selectedRegistrations = [ 'workflow' ];
        $scope.selectedValidDelete = [ 'workflow' ];
        $scope.deleteSelectedDialog();
        $scope.$digest();

        expect($scope.bulkWorkflows).to.equal(0);
        expect(ngDialog.$result.length).to.equal(1);
        expect(ngDialog.$result[0].innerHTML).to.have.string('deleteSelected()');
      });

      it('should bulk delete', function () {
        $httpBackend.expectGET('views/popupLoader.html').respond(201);
        requestDeferred.resolve('');

        $scope.deleteSelected();
        expect(Notifications.notifications[1].content).to.equal('There are no registrations that can be deleted.');

        $scope.selectedValidDelete = [ 'doc1', 'doc2' ];
        $scope.validSubmitDate = true;

        $httpBackend.expectDELETE(WORKFLOW_SERVER_URL).respond(201);
        $scope.deleteSelected();

        $httpBackend.flush();

        $timeout.flush();

        expect(Notifications.notifications[0].content).to.equal('2 registration(s) deleted');
      });

      it('should fail bulk delete', function () {
        $httpBackend.expectGET('views/popupLoader.html').respond(201);
        $httpBackend.expectDELETE(WORKFLOW_SERVER_URL).respond(500);

        $scope.deleteSelected();
        expect(Notifications.notifications[1].content).to.equal('There are no registrations that can be deleted.');

        $scope.selectedValidDelete = [ { errors: 1 }, { errors: 0 } ];
        $scope.validSubmitDate = true;

        $scope.deleteSelected();
        $httpBackend.flush();

        expect(Notifications.notifications[2].content).to.equal('Could not be deleted');
      });

      //TODO: check if content of dialog is good
      it.skip('should bulk submit without errors in document', function () {
        $scope.validSubmitDate = true;
        $scope.selectedValidSubmit = [ { errors: 0 }, { errors: 0 } ];

        $httpBackend.expectGET('views/dialog/listview/submit.html').respond(201);
        requestDeferred.resolve('');

        $scope.submitSelected();
        $scope.$digest();
        //TODO: check if content of dialog is good
      });

      it.skip('should fail bulk submit', function () {
        $scope.submitSelected();
        expect(Notifications.notifications[0].content).to.equal('There are no registrations that can be submitted.');

        $scope.selectedValidSubmit = [ { errors: 1 }, { errors: 0 } ];
        $scope.submitSelected();
        expect(Notifications.notifications[1].content).to.equal('There are errors in some of the selected registrations. Please correct them before submitting.');
      });
    });

    describe('Toggles', function () {
      it('should check all documents', function () {
        $scope.shownWorkflows = [ { name: 'doc1' }, { name: 'doc2' } ];
        $scope.$digest;
        $scope.checked = true; // value set by the checkbox model
        $scope.clickMainCheckbox();
        expect($scope.shownWorkflows).to.deep.equal([ { name: 'doc1', checked: true }, { name: 'doc2', checked: true } ]);
      });
    });
  });

  describe('ListViewCtrl filters', function () {
    it('should a date to chosen format', function () {
      // In order have a timezone indepent unit test, we should
      // create a date in the local timezone. For this, we rely on
      // JavaScript's parsing of YYYY/MM/DD, which uses local timezone.
      var date = new Date('2015/04/27').toUTCString();
      var format = 'YYYY-MM-DD HH:mm:ss.sss';

      var result = $filter('filterDate')(date, format);

      expect(result).to.equal('2015-04-27 00:00:00.000');

      date = undefined;
      result = $filter('filterDate')(date, format);

      expect(result).to.equal(undefined);
    });

    it('should order items by property value', function () {
      var items  = {
        0: { order: 3, fruit: 'apple' },
        1: { order: 1 , fruit: 'melon' },
        2: { order: 2, fruit: 'grapefruit' }
      };
      var field = 'order';
      var result = $filter('orderObjectBy')(items, field, false);

      expect(result[0]).to.deep.equal({ order: 1, fruit: 'melon' });
      expect(result[1]).to.deep.equal({ order: 2, fruit: 'grapefruit' });
      expect(result[2]).to.deep.equal({ order: 3, fruit: 'apple' });

      // Reverse on
      field = 'fruit';
      result = $filter('orderObjectBy')(items, field, true);
      expect(result[0]).to.deep.equal({ order: 1, fruit: 'melon' });
      expect(result[1]).to.deep.equal({ order: 2, fruit: 'grapefruit' });
      expect(result[2]).to.deep.equal({ order: 3, fruit: 'apple' });
    });

    it('should get correct data out of document', function () {

    });
  });
});
