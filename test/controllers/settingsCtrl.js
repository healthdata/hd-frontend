/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('Settings', function () {
  describe('SettingsCtrl', function () {
    var $controller, $controller2, $scope, $scope2, localStorageService, $compile, mockBackend, $rootScopeMock;

    beforeEach(function () {
      module('frontendApp');
    });

    beforeEach(inject(function (_$controller_, $rootScope, _localStorageService_, _$compile_, _$httpBackend_) {
      $compile = _$compile_;
      $scope = $rootScope.$new();
      $scope2 = $rootScope.$new();
      $rootScopeMock = $rootScope;
      $controller = _$controller_;
      $controller2 = _$controller_;
      localStorageService = _localStorageService_;
      mockBackend = _$httpBackend_
    }));

    it('should have default values', function () {
      $controller('SettingsCtrl', {
        $scope: $scope
      });
      expect($scope.platform).to.be.defined;
      expect($scope.password).to.be.false;
      expect($scope.loggedUser).to.equal(undefined);
      expect($scope.authorities).to.have.lengthOf(2);
    });

    it('should have default selected language', function () {
      localStorageService.set('language', 'fr');
      $controller('SettingsCtrl', {
        $scope: $scope
      });
      expect($scope.selectedLang).to.equal('fr');
    });

    it('should have default selected language fallback', function () {
      localStorageService.remove('language');
      $controller('SettingsCtrl', {
        $scope: $scope
      });
      expect($scope.selectedLang).to.equal('en');
    });

    it('should have platform bool', function () {
      $controller('SettingsCtrl', {
        $scope: $scope,
        PLATFORM: 'hd4res'
      });
      $controller2('SettingsCtrl', {
        $scope: $scope2,
        PLATFORM: 'hd4dp'
      });
      expect($scope.platform).to.be.true;
      expect($scope2.platform).to.be.false;
    });

    describe.skip('passwordChange', function () {
      beforeEach(function () {
        $controller('SettingsCtrl', {
          $scope: $scope
        });
      });

      it('should pop dialog with inputs and buttons ', function () {
        $scope.changePassword();
        var view = $compile(angular.element(html))($scope);
        var dialog = view.find('.ngdialog-content');
      });

      //it('should pop dialog', function () {
      //  // Set template
      //  element = $compile('<form><input value="t" type="password" ng-model="passwords">' +
      //  '<input type="password" value="test" class="input" same-as="passwords" ng-model="password_confirmation">' +
      //  '<button type="submit">Submit</button></form>') ($scope);
      //
      //  // Get elements
      //  var button = element.find('button[type="submit"]');
      //  var input = element.find('.input');
      //
      //  // Click button
      //  button.click();
      //
      //  var input2 = element.find('.input');
      //  console.log(input2);
      //
      //  expect(input.hasClass('ng-invalid-same-as')).to.be.true;
      //
      //});
    });


  });
});
