/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('ConfigurationCtrl', function () {
  var $scope, SkryvConfig, SkryvSession, requestDeferred, requestDeferredEdit, $q, controller, $httpBackend, $filter, Notifications;

  beforeEach(function () {
    module('skryv.configurations');
    module('frontendApp');
    module('services.notification');
  });

  beforeEach(function () {
    // Mock Session
    SkryvConfig = {
      getConfig: sinon.stub(),
      editConfig:  sinon.stub()
    };

    SkryvSession = {
      getLoggedUser: function() {
        return { organization: { main: true } };
      }
    };

    module(function ($provide) {
      $provide.value('SkryvConfig', SkryvConfig);
      $provide.value('SkryvSession', SkryvSession);
    });
  });

  beforeEach(inject(function ($controller, $rootScope, _$q_, _$httpBackend_, _$filter_, _Notifications_) {
    $q = _$q_;
    $filter = _$filter_;
    requestDeferred = $q.defer();
    requestDeferredEdit = $q.defer();
    $httpBackend = _$httpBackend_;
    Notifications = _Notifications_;
    SkryvConfig.getConfig.returns(requestDeferred.promise);
    SkryvConfig.editConfig.returns(requestDeferredEdit.promise);
    requestDeferred.resolve([ { config: 'DATABASE', type: 'INTEGER', value: '5' }, { username: 'USER_TYPES', type: 'TEXT', value: '5' } ]);
    $scope = $rootScope.$new();
    controller =  $controller('ConfigurationCtrl', {
      $scope: $scope,
      PLATFORM: 'hd4dp'
    });
  }));

  it('should set variables on scope', function () {
    expect($scope.loading).to.be.true;
    expect($scope.skip).to.be.false;
    expect($scope.inputFilter).to.deep.equal({ advanced: false, key: '' });
    expect($scope.platform).to.equal('hd4dp');
    expect($scope.columnSort).to.deep.equal({ reverse: false, sortColumn: 'key' });
    expect($scope.itemsPerPage).to.equal(10);
    expect($scope.currentPage).to.equal(1);
    expect($scope.isMainOrg).to.be.true;
    expect($scope.datepicker).to.deep.equal({ format: 'dd-MM-yyyy' });
    $scope.$digest();
    expect($scope.loading).to.be.false;
  });

  it('should change advanced filter', function () {
    $scope.inputFilter = { advanced: false, key: 'test' };

    $scope.changeAdvanced(true);
    expect($scope.inputFilter).to.deep.equal({ advanced: true, key: '' });
    expect($scope.skip).to.be.true;
    $scope.$digest();
    // After watch
    expect($scope.skip).to.be.false;
    expect($scope.inputFilter).to.deep.equal({ advanced: true, key: '' });
  });

  it('should change key filter', function () {
    $scope.inputFilter = { advanced: false };
    $scope.$digest();
    expect($scope.inputFilter).to.deep.equal({ advanced: false });
    $scope.inputFilter = { advanced: true, key: 'test' };
    $scope.$digest();
    expect($scope.inputFilter).to.deep.equal({ key: 'test' });
    $scope.inputFilter = { key: '' };
    $scope.$digest();
    expect($scope.inputFilter).to.deep.equal({ key: '', advanced: false });
  });

  it('should convert strings to integers for type "INT"', function () {
    $scope.$digest();
    expect($scope.configs).to.deep.equal([ { config: 'DATABASE', type: 'INTEGER', value: 5 }, { username: 'USER_TYPES', type: 'TEXT', value: '5' } ]);
  });

  it('should filter int from string', function () {
    var int = $filter('num') ('5');
    expect(angular.isNumber(int)).to.be.true;
    expect(int).to.equal(5);

    int = $filter('num') (true);
    expect(angular.isNumber(int)).to.be.false;
    expect(int).to.equal(true);
  });
  it('should set page to 1', function () {
    $scope.currentPage = 5;
    $scope.reload();
    expect($scope.currentPage).to.equal(1);
  });

  it('should set default config', function () {
    $scope.editable = { value: '5', type: 'TEXT', defaultValue: '7' };
    $scope.defaultConfig();
    expect($scope.editable.value).to.equal('7');
    $scope.editable.type = 'INTEGER';
    $scope.defaultConfig();
    expect($scope.editable.value).to.equal(7);
  });

  it('should set edit mode', function () {
    var config = { config: 'DATABASE' };
    $scope.editConfig(config);
    expect($scope.editable).to.deep.equal(config);
  });

  it('should open datepicker dialog', function () {
    var $event = {
      preventDefault: function () {},
      stopPropagation: function () {}
    };
    $scope.config = { config: 'DATABSE' }
    $scope.open($event, $scope.config);
    expect($scope.config.isOpen).to.be.true;
  });

  it('should cancel edit mode', function () {
    $scope.editable = 'test';
    $scope.cancelConfig();
    expect($scope.editable).to.equal(undefined);
  });

  it('should sort on column', function () {
    $scope.sortByColumn('Test');
    expect($scope.columnSort.sortColumn).to.equal('Test');
    expect($scope.currentPage).to.equal(1);
    expect($scope.columnSort.reverse).to.equal(false);

    $scope.sortByColumn('Other');
    expect($scope.columnSort.sortColumn).to.equal('Other');
    expect($scope.columnSort.reverse).to.equal(false);

    $scope.sortByColumn('Other');
    expect($scope.columnSort.reverse).to.equal(true);
  });

  it('should edit a config', function () {
    $scope.$digest();
    var config = { config: 'DATABASE', type: 'INTEGER', value: '5', useFromMain: true };
    $scope.editable = { config: 'DATABASE', type: 'INTEGER', value: '6', useFromMain: false };
    requestDeferredEdit.resolve($scope.editable);
    $scope.saveConfig(config);
    $scope.$digest();
    expect(Notifications.notifications[0].content).to.equal('Configuration has been edited');
    expect(config.value).to.equal(6);
    expect(config.useFromMain).to.equal(false);
    expect( $scope.editable).to.equal(undefined);
  });

  it('should fail on editing a config', function () {
    $scope.$digest();
    requestDeferredEdit.reject(401);
    $scope.saveConfig();
    $scope.$digest();
    expect(Notifications.notifications[0].content).to.equal('Configuration could not be edited');
  });

});
