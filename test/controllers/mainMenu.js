/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Do not complain things like expect(foo).to.be.undefined
/* jshint expr: true */

describe('MainMenu', function () {
    beforeEach(module('frontendApp'));

    describe('MainMenuCtrl', function () {
      var controller, $scope, rootScope, $location;
      var def = { endDateComments: '2015-04-01T18:00:00Z', endDateCreation: '2015-04-01T18:00:00Z', endDateSubmission: '2015-04-01T18:00:00Z', id: 222, startDate: '2015-04-01T18:00:00Z', content: {} };

      var ListViewDocColumns = {};

      var SkryvView = {};

      var SkryvSession = {
        getLoggedUser:  function () {
          return {
            lastName: 'user',
            firstName: 'user',
            dataCollectionNames: [ 'TEST', 'BCFR' ]
          };
        },
        getDefinition:  function () {
          return def;
        },
        setDefinition:  function () {
          return def;
        },
        getUserSession: function () {
          return true;
        },
        isAuth: function () {
          return true;
        },
        isAdmin: function () {
          return true;
        },
        getCollections: function () {
          return [ 'TEST', 'BCFR' ];
        }
      };

      var SkryvDatacollections = {
        getDefinition: function (item) {
          return {
            then: function () {
              return true;
            }
          }
        }
      };

      beforeEach(inject(function ($controller, $rootScope, _$location_) {
        $scope = $rootScope.$new();
        rootScope = $rootScope.$new();
        $location = _$location_;
        controller = $controller('MainMenuCtrl', {
          $scope: $scope,
          $rootScope: rootScope,
          SkryvSession: SkryvSession,
          ListViewDocColumns: ListViewDocColumns,
          SkryvView: SkryvView
        });
      }));

      it('should have multiple menuItems', function () {
        // Has many menuItems | is an array
        expect($scope.mainMenu).not.to.have.lengthOf(0);
      });

      // MenuItems
      it('should have valid menu items', function () {
        $scope.mainMenu.forEach(function (item) {
          item.should.have.property('name');
          item.should.have.property('label');
        });
      });

      it('should render item classes', function () {
        var item = { class: function () { return 'valid' } };
        var item2 = { class: 'valid' };

        expect($scope.itemClass(item)).to.equal('valid');
        expect($scope.itemClass(item2)).to.equal('valid');
      });
    });
});