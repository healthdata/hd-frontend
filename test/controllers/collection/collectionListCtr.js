/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('collectionListCtrl', function () {
  var SkryvDefinitionManager, Notifications, controller, $scope, $q, requestDeferred, requestDeferredDDE, $location, SESSION_SERVER_URL, $httpBackend, EnvironmentConfig;

  beforeEach(module('skryv.collection'));
  beforeEach(module('ngDialog'));
  beforeEach(module('services.notification'));

  beforeEach(function () {
    // Mock Session
    SkryvDefinitionManager = {
      getMajors: sinon.stub()
    };

    EnvironmentConfig = {
      isDdeEnabled: sinon.stub()
    };

    module(function ($provide) {
      $provide.value('SkryvDefinitionManager', SkryvDefinitionManager);
      $provide.value('EnvironmentConfig', EnvironmentConfig);
    });
  });

  beforeEach(inject(function ($controller, $rootScope, _Notifications_, _$q_, _$location_, _SESSION_SERVER_URL_, _$httpBackend_) {
    $q = _$q_;
    requestDeferred = $q.defer();
    requestDeferredDDE = $q.defer();
    $scope = $rootScope.$new();
    Notifications = _Notifications_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    $httpBackend = _$httpBackend_;
    $location = _$location_;
    SkryvDefinitionManager.getMajors.returns(requestDeferred.promise);
    SkryvDefinitionManager.getMajors.returns(requestDeferred.promise);
    EnvironmentConfig.isDdeEnabled.returns(requestDeferredDDE.promise);
    requestDeferredDDE.resolve(true);
    $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollectiondefinitions/manager/latestmajors').respond([ 'CRRD', 'BCFR', 'ABC' ]);
    controller = $controller('collectionListCtrl', {
      $scope: $scope
    });
  }));


  it('should redirect', function () {
    expect($location.path()).to.equal('');
    $scope.new();
    $scope.$digest();
    expect($location.path()).to.equal('/collection/new');
  });

  it('should sort on data collection name', function () {
    expect($scope.columnSort).to.deep.equal({reverse: false, sortColumn: 'dataCollectionName'});
  });

  it('should get all data collections', function () {
    expect($scope.loading).to.be.true;

    requestDeferred.resolve([ { dataCollectionName: 'TEST', content: '{}' }, { dataCollectionName: 'BCFR', content: '{}' } ]);
    $scope.$digest();
    expect($scope.loading).to.be.false;
    expect(SkryvDefinitionManager.getMajors).to.have.been.calledOnce;
    expect($scope.collections).to.have.lengthOf(2);
    expect($scope.collections[0]).to.deep.equal({ dataCollectionName: 'TEST', content: '{}', period: { active: false }, active: 'inactive'});
  });

  it('should notify when loading data collections fails', function () {
    requestDeferred.reject('FAILED');
    $scope.$digest();
    expect(SkryvDefinitionManager.getMajors).to.have.been.calledOnce;
    expect($scope.collections).to.have.lengthOf(0);
    expect(Notifications.notifications[0].content).to.equal('FAILED');
  });

  it('should navigate to view', function () {
    $scope.viewCollection('1');
    expect($location.path()).to.equal('/collection/version/1');
  });

  it('should sort the columns', function () {
    $scope.sortByColumn('Test');
    expect($scope.columnSort.sortColumn).to.equal('Test');
    expect($scope.columnSort.reverse).to.equal(false);

    $scope.sortByColumn('Other');
    expect($scope.columnSort.sortColumn).to.equal('Other');
    expect($scope.columnSort.reverse).to.equal(false);

    $scope.sortByColumn('Other');
    expect($scope.columnSort.reverse).to.equal(true);
  });

});
