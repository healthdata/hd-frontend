/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('collection', function () {
  var $filter;

  beforeEach(module('skryv.collection'));

  beforeEach(inject(function (_$filter_) {
    $filter = _$filter_;
  }));

  describe('Filters: collectionPeriod', function () {
    it('should set period', function () {
      var collection = { content: {} };
      expect($filter('collectionPeriod')(collection)).to.deep.equal({ active: false });
    });

    it('period not active', function () {
      var collection = { content: { period: { end: '', start: '' } } };
      expect($filter('collectionPeriod')(collection)).to.deep.equal({ end: '', start: '', active: false });
    });

    it('period active', function () {
      var collection = { content: { period: { end: '2999-01-01', start: '1000-01-01' } } };
      expect($filter('collectionPeriod')(collection)).to.deep.equal({ end: '2999-01-01', start: '1000-01-01', active: true });
    });
  });

  describe('Filters: collectionActive', function () {
    it('should be active when there are no dates', function () {
      var collection = { startDate: '1000-12-31T23:00:00Z' };
      expect($filter('collectionActive')(collection)).to.equal('active');
    });

    it('should be inactive when there is no start date', function () {
      var collection = {};
      expect($filter('collectionActive')(collection)).to.equal('inactive');
    });

    it('should be active when today is before creation date', function () {
      var collection = { startDate: '1000-12-31T23:00:00Z', endDateCreation: '2999-12-31T23:00:00Z' };
      expect($filter('collectionActive')(collection)).to.equal('active');
    });

    it('should be "no new creations" when creation is before submission date', function () {
      var collection = { startDate: '1000-12-31T23:00:00Z', endDateCreation: '1001-12-31T23:00:00Z', endDateSubmission: '2999-12-31T23:00:00Z' };
      expect($filter('collectionActive')(collection)).to.equal('active, no new creations');
    });

    it('should be "active, no new submissions" when creation is before submission date', function () {
      var collection = { startDate: '1000-12-31T23:00:00Z', endDateCreation: '1001-12-31T23:00:00Z', endDateSubmission: '1002-12-31T23:00:00Z', endDateComments: '2999-12-31T23:00:00Z' };
      expect($filter('collectionActive')(collection)).to.equal('active, no new submissions');
    });
  });
});
