/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('collectionEditCtrl', function () {
  var Notifications, controller, $scope, $q, SESSION_SERVER_URL, $httpBackend, requestDeferred, requestDeferredDde, definitionPromise, $location, SkryvDefinitionManager, EnvironmentConfig, collectionsPromise, publishConfigPromise;

  beforeEach(module('skryv.collection'));
  beforeEach(module('skryv.translation'));
  beforeEach(module('skryv.configurations'));
  beforeEach(module('LocalStorageModule'));
  beforeEach(module('skryv.user'));
  beforeEach(module('skryv.session'));
  beforeEach(module('services.notification'));
  beforeEach(module('ngDialog'));

  // Mock
  SkryvDefinitionManager = {
    get: sinon.stub(),
    update: sinon.stub(),
    publish: sinon.stub(),
    getByName: sinon.stub()
  };

  EnvironmentConfig = {
    isDdeEnabled: sinon.stub(),
    isPublishEnabled: sinon.stub()
  };

  beforeEach(inject(function ($controller, $rootScope, _SESSION_SERVER_URL_, _Notifications_, _$q_, _$httpBackend_, _$location_) {
    $q = _$q_;
    requestDeferred = $q.defer();
    requestDeferredDde = $q.defer();
    collectionsPromise = $q.defer();
    publishConfigPromise = $q.defer();
    definitionPromise = $q.defer();
    SkryvDefinitionManager.get.returns(definitionPromise.promise);
    SkryvDefinitionManager.update.returns(requestDeferred.promise);
    SkryvDefinitionManager.publish.returns(requestDeferred.promise);
    SkryvDefinitionManager.getByName.returns(collectionsPromise.promise);
    EnvironmentConfig.isDdeEnabled.returns(requestDeferred.promise);
    EnvironmentConfig.isPublishEnabled.returns(publishConfigPromise.promise);
    requestDeferredDde.resolve(true);
    $scope = $rootScope.$new();
    Notifications = _Notifications_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    $location = _$location_;
    controller = $controller('collectionEditCtrl', {
      $scope: $scope,
      EnvironmentConfig: EnvironmentConfig,
      SkryvDefinitionManager: SkryvDefinitionManager,
      skryvcollection: { dataCollectionGroupId: 1, content: {}, minorVersion: 1, published: true }
    });
    $scope.skryv_collection_form = {
      $valid: true
    };
    $httpBackend = _$httpBackend_;
  }));

  it('should have default values', function () {
    expect($scope.collection).to.deep.equal({ dataCollectionGroupId: 1, content: {}, minorVersion: 1, published: true });
  });

  it('should update a data collection', function () {
    requestDeferred.resolve({ dataCollectionName: 'BCFR' });
    collectionsPromise.resolve([ { version: { major: 1, minor: 1 }} ]);

    $scope.collection = {
      dataCollectionName: 'BCFR',
      description: 'dc bcfr'
    };

    $scope.save();
    $scope.$digest();

    expect(SkryvDefinitionManager.update).to.have.been.calledOnce;
    expect(SkryvDefinitionManager.update).to.have.been.calledWith({
      dataCollectionName: 'BCFR',
      description: 'dc bcfr'
    });

    $scope.$digest();

    expect(Notifications.notifications[0].content).to.equal('The data collection was successfully saved!');
  });

  it('should fail updating a data collection', function () {
    requestDeferred.reject('Data collection could not be created');

    $scope.collection = {
      dataCollectionName: 'BCFR',
      description: 'dc bcfr'
    };

    $scope.save();
    $scope.$digest();

    expect(Notifications.notifications[0].content).to.equal('Data collection could not be created');
  });

  it('should fail updating a data collection without name or description', function () {
    $scope.collection = {
      description: 'dc bcfr'
    };

    $scope.skryv_collection_form.$valid = false;

    $scope.save();
    $scope.$digest();

    expect(Notifications.notifications[0].content).to.equal('Please fill in all fields');
  });

  it('should publish a data collection', function () {
    requestDeferred.resolve({ dataCollectionName: 'BCFR' });

    $scope.collection = {
      dataCollectionName: 'BCFR',
      id: 5,
      description: 'dc bcfr'
    };

    $scope.publishCollection();
    $scope.$digest();

    expect(SkryvDefinitionManager.publish).to.have.been.calledOnce;

    $scope.$apply();

    expect(Notifications.notifications[0].content).to.equal('The data collection was successfully published!');
  });

  it('should fail publishing a data collection', function () {
    requestDeferred.reject('Data collection could not be published');

    $scope.collection = {
      dataCollectionName: 'BCFR',
      id: 5,
      description: 'dc bcfr'
    };

    $scope.publishCollection();
    $scope.$digest();

    expect(Notifications.notifications[0].content).to.equal('Data collection could not be published');
  });

  // irrelevant, publish should be on group
  it.skip('should show republish button', function () {
    expect($scope.showRepublish).to.be.false;
    $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollectiongroups/manager/1').respond(201);
    publishConfigPromise.resolve(true);
    $scope.$digest();
    collectionsPromise.resolve([ { published: false, minorVersion: 1 } ]);
    $scope.$digest();

    expect($scope.showRepublish).to.be.true;
  });

  // irrelevant, republish should be on group
  it.skip('should not show republish button', function () {
    expect($scope.showRepublish).to.be.false;
    $httpBackend.expectGET(SESSION_SERVER_URL + '/datacollectiongroups/manager/1').respond(201);
    publishConfigPromise.resolve(true);
    $scope.$digest();
    collectionsPromise.resolve([ { published: true, minorVersion: 1 } ]);
    $scope.$digest();

    expect($scope.showRepublish).to.be.false;
  });
});
