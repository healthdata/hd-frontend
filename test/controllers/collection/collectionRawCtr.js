/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('collectionRawCtrl', function () {
  var Notifications, controller, $scope, $q, $httpBackend, requestDeferred, updateCollectionPromise, $location, SkryvDefinitionManager;

  beforeEach(module('skryv.collection'));
  beforeEach(module('services.notification'));
  beforeEach(module('ngDialog'));

  // Mock User
  SkryvDefinitionManager = {
    update: sinon.stub(),
    getHistoryById: sinon.stub()
  };

  var collection = { content: {},
    dataCollectionDefinitionId: 5,
    minorVersion: 0,
    published: false
  };

  beforeEach(inject(function ($controller, $rootScope, _Notifications_, _$q_, _$location_, _$httpBackend_) {
    $q = _$q_;
    requestDeferred = $q.defer();
    updateCollectionPromise = $q.defer();
    SkryvDefinitionManager.getHistoryById.returns(requestDeferred.promise);
    SkryvDefinitionManager.update.returns(updateCollectionPromise.promise);
    $scope = $rootScope.$new();
    Notifications = _Notifications_;
    $location = _$location_;
    $httpBackend = _$httpBackend_;
    controller = $controller('collectionRawCtrl', {
      skryvcollection: collection,
      SkryvDefinitionManager: SkryvDefinitionManager,
      $scope: $scope
    });
  }));

  it('should have default values', function () {
    var json = JSON.stringify({ "label": "test" }, undefined, 4);
    requestDeferred.resolve([ { content: JSON.parse(json), description: 'smth', createdOn: new Date() } ]);

    $scope.$apply();

    expect($scope.latestContent).to.deep.equal(json);
    expect($scope.collection).to.deep.equal(collection);
    expect($scope.isValid).to.deep.equal(true);
    expect($scope.isDirty).to.deep.equal(false);
  });

  it('should display if json is valid', function () {
    // requestDeferred.resolve([ { content: JSON.parse(json), description: 'smth', createdOn: new Date() } ]);
    // $scope.$apply();

    expect($scope.isValid).to.deep.equal(true);
    expect($scope.isDirty).to.deep.equal(false);

    $scope.latestContent = '{ "ee" }';
    $scope.changeRaw();

    expect($scope.isValid).to.deep.equal(false);
    expect($scope.isDirty).to.deep.equal(true);
  });

  // irrelevant, WIP is saved by the backend
  it.skip('should save a wip', function () {
    requestDeferred.resolve({ content: { } });

    $scope.createFromRaw();
    $scope.$digest();

    expect(SkryvDefinitionManager.createWip).to.have.been.calledOnce;

    $scope.$apply();
    expect(Notifications.notifications[0].content).to.equal('The data collection was successfully saved!');
  });

  it('should fail updating a data collection', function () {
    requestDeferred.resolve({ content: { } });
    updateCollectionPromise.reject('Data collection could not be created');

    $scope.createFromRaw();
    $scope.$digest();

    expect(Notifications.notifications[0].content).to.equal('Data collection could not be created');
  });
});
