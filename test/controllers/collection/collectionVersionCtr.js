/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('collectionVersionCtrl', function () {
  var SkryvDefinitionManager, Notifications, controller, $scope, $q, requestDeferred, requestDeferredDDE, $location, EnvironmentConfig;

  beforeEach(module('skryv.collection'));
  beforeEach(module('services.notification'));
  beforeEach(module('ngDialog'));

  beforeEach(function () {
    // Mock Session
    SkryvDefinitionManager = {
      getByName: sinon.stub(),
      update: sinon.stub()
    };

    EnvironmentConfig = {
      isDdeEnabled: sinon.stub()
    };

    module(function ($provide) {
      $provide.value('SkryvDefinitionManager', SkryvDefinitionManager);
      $provide.value('EnvironmentConfig', EnvironmentConfig);
    });
  });

  beforeEach(inject(function ($controller, $rootScope, _Notifications_, _$q_, _$location_) {
    $q = _$q_;
    requestDeferred = $q.defer();
    requestDeferredDDE = $q.defer();
    $scope = $rootScope.$new();
    Notifications = _Notifications_;
    $location = _$location_;
    SkryvDefinitionManager.getByName.returns(requestDeferred.promise);
    EnvironmentConfig.isDdeEnabled.returns(requestDeferred.promise);
    requestDeferredDDE.resolve(true);
    controller = $controller('collectionVersionCtrl', {
      $scope: $scope,
      PLATFORM: 'HD4DP',
      $routeParams: { name: 'BCFR' }
    });
  }));

  it('should have default values', function () {
    expect($scope.loading).to.equal(true);
    expect($scope.platform).to.equal('HD4DP');
    expect($scope.collectionName).to.equal('BCFR');
  });

  it('should navigate to edit view of collection', function () {
    $scope.viewCollection({ name: 'BCFR', id: 1 });
    expect($location.path()).to.equal('/collection/edit/1');
  });

  it('should set all versions to scope', function () {
    $scope.$digest();
    requestDeferred.resolve([ { dataCollectionName: 'BCFR', version: { major: 3, minor: 0}, content: {}, id: 1 },
      { dataCollectionName: 'BCFR', version: { major: 2, minor: 0}, content: {}, id: 2 },
      { dataCollectionName: 'BCFR', version: { major: 1, minor: 0}, content: {}, id: 3 } ]);

    expect(SkryvDefinitionManager.getByName).to.have.been.calledOnce;
    expect(SkryvDefinitionManager.getByName).to.have.been.calledWith('BCFR');

    $scope.$digest();

    $scope.$digest();

    expect($scope.collections).to.deep.equal([ { dataCollectionName: 'BCFR', version: { major: 3, minor: 0}, id: 1, content: {}, period: { active: false }, active: 'inactive'  },
      { dataCollectionName: 'BCFR', version: { major: 2, minor: 0}, content: {}, id: 2, period: { active: false }, active: 'inactive' },
      { dataCollectionName: 'BCFR', version: { major: 1, minor: 0}, content: {}, id: 3, period: { active: false }, active: 'inactive' } ]);
    expect($scope.loading).to.be.false;
  });

  it('should fail get all versions', function () {
    $scope.$digest();
    requestDeferred.reject('Data collections could not be retreived');

    expect(SkryvDefinitionManager.getByName).to.have.been.calledOnce;
    expect(SkryvDefinitionManager.getByName).to.have.been.calledWith('BCFR');

    $scope.$digest();

    expect(Notifications.notifications[0].content).to.equal('Data collections could not be retreived');
  });
});
