/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('collectionNewCtrl', function () {
  var Notifications, controller, $scope, $q, requestDeferred, $location, SkryvDefinitionManager, $httpBackend, ENVIRONMENT_URL;

  beforeEach(module('skryv.collection'));
  beforeEach(module('services.notification'));

  // Mock User
  SkryvDefinitionManager = {
    save: sinon.stub()
  };

  beforeEach(inject(function ($controller, $rootScope, _ENVIRONMENT_URL_, _Notifications_, _$q_, _$location_, _$httpBackend_) {
    $q = _$q_;
    requestDeferred = $q.defer();
    SkryvDefinitionManager.save.returns(requestDeferred.promise);
    $scope = $rootScope.$new();
    Notifications = _Notifications_;
    $location = _$location_;
    $httpBackend = _$httpBackend_;
    ENVIRONMENT_URL = _ENVIRONMENT_URL_;
    controller = $controller('collectionNewCtrl', {
      $scope: $scope,
      SkryvDefinitionManager: SkryvDefinitionManager,
      $routeParams: { id: '123' }
    });

    $scope.skryv_collection_form = {
      $valid: true
    };
  }));

  it('should have default values', function () {
    expect($scope.collection).to.deep.equal({
      dataCollectionGroupId: '123',
      content: {},
      published: false,
      minorVersion: 0
    });
  });

  it('should save a data collection', function () {
    $httpBackend.expectGET(ENVIRONMENT_URL + '/datacollectiongroups/manager/123').respond(200);

    requestDeferred.resolve({ dataCollectionDefinitionId: 123, dataCollectionName: 'BCFR' });

    $scope.collection = {
      dataCollectionName: 'BCFR',
      description: 'dc bcfr'
    };

    $scope.save();
    $scope.$digest();

    expect(SkryvDefinitionManager.save).to.have.been.calledOnce;
    expect(SkryvDefinitionManager.save).to.have.been.calledWith({
      dataCollectionName: 'BCFR',
      description: 'dc bcfr'
    });

    $scope.$apply();

    expect($location.path()).to.equal('/collection/edit/123');
    expect(Notifications.notifications[0].content).to.equal('The data collection was successfully created!');
  });

  it('should fail saving a data collection', function () {
    $httpBackend.expectGET(ENVIRONMENT_URL + '/datacollectiongroups/manager/123').respond(200);
    requestDeferred.reject('Data collection could not be created');

    $scope.collection = {
      dataCollectionName: 'BCFR',
      description: 'dc bcfr'
    };

    $scope.save();
    $scope.$digest();

    expect(Notifications.notifications[0].content).to.equal('Data collection could not be created');
  });

  it('should fail saving a data collection without name or description', function () {
    $httpBackend.expectGET(ENVIRONMENT_URL + '/datacollectiongroups/manager/123').respond(200);
    $scope.collection = {
      description: 'dc bcfr'
    };

    $scope.skryv_collection_form = {
      $valid: false
    };

    $scope.save();
    $scope.$digest();

    expect(Notifications.notifications[0].content).to.equal('Please fill in all fields');
  });

});
