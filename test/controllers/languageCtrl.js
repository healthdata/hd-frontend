/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('Language Session', function () {
  describe('Language', function () {
    var $controller, $scope, localStorageService;

    beforeEach(function () {
      module('frontendApp');
    });

    beforeEach(inject(function (_$controller_, $rootScope, _localStorageService_) {
      $scope = $rootScope.$new();
      $controller = _$controller_;
      localStorageService = _localStorageService_;
    }));

    it('should be cookie value', function () {
      localStorageService.set('language', 'fr');
      $controller('LanguageCtrl', {
        $scope: $scope
      });
      expect($scope.lang).to.equal('fr');
    });

    it('should be browser default', function () {
      // Remove lang cookie
      localStorageService.remove('language');
      $controller('LanguageCtrl', {
        $scope: $scope,
        $window: { navigator: { language: 'nl' } }
      });
      expect($scope.lang).to.equal('nl');
    });

    it('should be en fallback', function () {
      localStorageService.remove('language');
      $controller('LanguageCtrl', {
        $scope: $scope,
        $window: { navigator: { language: 'nl-be' } }
      });
      expect($scope.lang).to.equal('en');
    });
  });
});
