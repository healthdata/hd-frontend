'use strict';

describe('listView', function () {
  var $filter;
  var Organization;

  beforeEach(module('skryv.listView'));
  beforeEach(module('frontendApp'));
  beforeEach(module('services.organization'))

  beforeEach(inject(function (_$filter_) {
    $filter = _$filter_;
  }));

  describe('Filters: getNestedColumns', function () {
    it('should get object with all nested columns', function () {
      var columns = {
        field_1: {
          type:'dropdown',
          columns: {
            nested_1: {
              name: 'nested_1',
              type: 'checkbox',
              active: false
            },
            nested_2: {
              name: 'nested_2',
              type: 'checkbox',
              active: true
            }
          }
        },
        field_2: {
          name: 'field_2',
          type: 'checkbox',
          active: true
        },
        field_3: {
          name: 'field_3',
          type: 'checkbox',
          active: false
        }
      };

      var filtered = $filter('getNestedColumns')(columns);

      var expected = {
        nested_1: { name: 'nested_1', type: 'checkbox', active: false, parent: 'field_1', valueLocation: undefined },
        nested_2: { name: 'nested_2', type: 'checkbox', active: true, parent: 'field_1', valueLocation: undefined },
        field_2: { name: 'field_2', type: 'checkbox', active: true},
        field_3: {name: 'field_3', type: 'checkbox', active: false }
      };

      expect(filtered).to.deep.equal(expected);
    });
  });
});
