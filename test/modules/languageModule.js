/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('SkryvLanguage', function () {
    var mockGettextCatalog, mockCookieStore,mockSkryvLanguage;
    beforeEach(module('skryv.language'));
    beforeEach(module('gettext'));
    beforeEach(module('ngCookies'));
    beforeEach(module('LocalStorageModule'));

    beforeEach(inject(function(gettextCatalog, $cookieStore,SkryvLanguage ) {
        mockGettextCatalog = gettextCatalog;
        mockCookieStore = $cookieStore;
        mockSkryvLanguage = SkryvLanguage;
    }));

///////////////////////////
// Methods

    describe('SkryvLanguage Methods', function () {
      // Debug
      it('Debug should be off', function () {
        var debug = mockSkryvLanguage.getDebug();
        expect(debug).to.be.false;
      });

      it('Toggle debug', function () {
        mockSkryvLanguage.setDebug(true);
        var debug = mockSkryvLanguage.getDebug();
        expect(debug).to.be.true;
      });

      // Language
      it('Should set and get language', function () {
        mockSkryvLanguage.setLanguage('nl');
        var lang = mockSkryvLanguage.getLanguage();
        expect(lang).to.equal('nl');
      });

      // Translate
      it('translate', function () {
        mockSkryvLanguage.setLanguage('fr');
        mockGettextCatalog.setStrings('fr', {"Add": "Ajouter"});
        var translation = mockSkryvLanguage.translate('Add');
        expect(translation).to.equal("Ajouter");
      });
    });
});

