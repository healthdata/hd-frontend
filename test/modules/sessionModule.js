/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('SkryvSession', function () {
  var SkryvSession, SkryvUser, SkryvLanguage, SkryvCollection , mockLocalStorage, $q, $rootScope, SESSION_SERVER_URL, SkryvConfig,
    $httpBackend, ngDialog, SkryvDefinition, Notifications;

  var requestDeferredUser, requestDeferredDC, requestDeferredDCD, requestDeferredVersions, configPromise;
  beforeEach(module('skryv.conf'));
  beforeEach(module('services.dcd'));
  beforeEach(module('ngCookies'));
  beforeEach(module('skryv.session'));
  beforeEach(module('ngDialog'));
  beforeEach(module('skryv.user'));
  beforeEach(module('gettext'));
  beforeEach(module('LocalStorageModule'));
  beforeEach(module('skryv.configurations'));
  beforeEach(module('services.notification'));
  beforeEach(module('documentmodel'));

  beforeEach(function () {
    // Mock Session
    SkryvUser = {
      get: sinon.stub()
    };

    SkryvCollection = {
      getDataCollectionGroups: sinon.stub(),
      getCollections: sinon.stub(),
      getDefinition: sinon.stub()
    };

    SkryvDefinition = {
      getLatest: sinon.stub(),
      getVersions: sinon.stub()
    };

    SkryvConfig = {
      getConfigByKey: sinon.stub(),
    };

    SkryvLanguage = {
      getLanguage: function () { return 'nl' }
    };

    module(function ($provide) {
      $provide.value('SkryvUser', SkryvUser);
      $provide.value('SkryvCollection', SkryvCollection);
      $provide.value('SkryvLanguage', SkryvLanguage);
      $provide.value('SkryvDefinition', SkryvDefinition);
      $provide.value('SkryvConfig', SkryvConfig);
    });
  });


  beforeEach(inject(function (_$rootScope_, _$httpBackend_, _SkryvSession_, _ngDialog_, localStorageService, _$q_, _Notifications_, _SESSION_SERVER_URL_, _SkryvDefinition_) {
    SkryvSession = _SkryvSession_;
    $httpBackend = _$httpBackend_;
    SESSION_SERVER_URL = _SESSION_SERVER_URL_;
    $rootScope = _$rootScope_;
    mockLocalStorage = localStorageService;
    $q = _$q_;
    SkryvDefinition = _SkryvDefinition_;
    ngDialog = _ngDialog_;
    Notifications = _Notifications_;
    requestDeferredUser = $q.defer();
    requestDeferredDC = $q.defer();
    requestDeferredDCD = $q.defer();
    requestDeferredVersions = $q.defer();
    configPromise = $q.defer();
    SkryvUser.get.returns(requestDeferredUser.promise);
    SkryvCollection.getDataCollectionGroups.returns(requestDeferredDC.promise);
    SkryvCollection.getCollections.returns(requestDeferredDC.promise);
    SkryvCollection.getDefinition.returns(requestDeferredDCD.promise);
    SkryvDefinition.getLatest.returns(requestDeferredDCD.promise);
    SkryvDefinition.getVersions.returns(requestDeferredVersions.promise);
    SkryvConfig.getConfigByKey.returns(configPromise.promise);
    mockLocalStorage.set('userInfo', { 'userName': 'user', dataCollectionNames: [] });
  }));

  describe('userSession', function () {
    it('storeUserTokens', function () {
      // Stored user
      SkryvSession.storeUserTokens('123');

      // See if cookie is set
      var cookie = mockLocalStorage.get('userInfo');
      expect(cookie).to.deep.equal({'accessToken': '123'});
    });

    it('destroyUserSession', function () {
      // Store user
      var cookie = mockLocalStorage.get('userInfo');
      expect(cookie).to.deep.equal({ 'userName': 'user', dataCollectionNames: [] });

      // Destroy user
      SkryvSession.destroyUserSession();

      // Destroy cookie
      cookie = mockLocalStorage.get('userInfo');
      expect(cookie).to.be.null;
    });

    it('getUserTokens', function () {
      // Stored user
      SkryvSession.storeUserTokens(123);
      // Get user
      var userSession = SkryvSession.getUserTokens();
      expect(userSession.accessToken).to.equal(123);
    });
  });

  describe('loggedUser', function () {
    it('should get setted loggedUser', function () {
      SkryvSession.setLoggedUser({'userName': 'user'});
      var loggedUser = SkryvSession.getLoggedUser();
      expect(loggedUser).to.deep.equal({'userName': 'user'});
    })
  });

  describe('loadApp', function () {
    it('should reject when no accessToken', function () {
      mockLocalStorage.remove('userInfo');

      var result;
      SkryvSession.loadApp().then(function (app) {
        result = app;
      }, function (err) {
        result = err;
      });

      $rootScope.$apply();
      expect(result).to.equal('No local user');
    });

    it('should skip if session active', function () {
      $rootScope.SessionActive = true;

      var result;
      SkryvSession.loadApp().then(function (app) {
        result = app;
      }, function (err) {
        result = err;
      });
      $rootScope.$apply();
      expect(result).to.equal('Session active');

    });
    it.skip('should set logged user, datacollections, translations and installationId', function () {
      $httpBackend.expectGET('views/popupLoader.html').respond(201);
      $httpBackend.expectGET(SESSION_SERVER_URL + '/translations?language=nl').respond(201);

      SkryvSession.loadApp();
      $httpBackend.flush();

      requestDeferredUser.resolve({'userName': 'user', authorities: [ { authority: 'ROLE_USER' } ],dataCollectionNames: ["CRRD", "TEST"], metaData:{}});
      requestDeferredDC.resolve({ "TEST": [ { "dataCollectionGroupName": "TEST", "dataCollectionGroupId": 8224, "majorVersion":1, "label": { "en": "TEST", "fr": "TEST", "nl": "TEST" }, "dataCollectionDefinitions": [               {
        "dataCollectionName":"TEST",
        "dataCollectionDefinitionId":8308,
        "minorVersion":0,
        "label":{
          "en":"TEST",
          "fr":"TEST",
          "nl":"TEST"
        },
        "description":{
          "en":"TEST",
          "fr":"TEST",
          "nl":"TEST"
        }
      }] } ] });
      configPromise.resolve([ { value: 'TEST' } ]);
      $rootScope.$digest();
      requestDeferredVersions.resolve([ {id:5, dataCollectionName: 'CRRD', version: { major: 1 }},{ id:12, dataCollectionName: 'CRRD', version: { major: 2 } }]);
      $rootScope.$digest();

      expect(SkryvUser.get).have.been.calledOnce;
      expect(SkryvCollection.getCollections).have.been.called;
      expect(SkryvConfig.getConfigByKey).have.been.calledOnce;

      console.log(JSON.stringify(SkryvSession.getCollections()));

      expect(SkryvSession.getLoggedUser()).to.deep.equal({'userName': 'user', authorities: [ { authority: 'ROLE_USER' }], dataCollectionNames: ["CRRD", "TEST"], metaData:{}});
      expect(SkryvSession.getInstallationId()).to.deep.equal('TEST');
      expect(SkryvSession.getCollections()).to.deep.equal([
        {"label":"CRRD",
          "children":["CRRD"],
          "versions":[
            {"id":5,"dataCollectionName":"CRRD","version":{"major":1},"status":"inactive"},
            {"id":12,"dataCollectionName":"CRRD","version":{"major":2},"status":"inactive"}
          ]
        }
      ]);
    });

    it('should error logged user and datacollections', function () {
      $httpBackend.expectGET('views/popupLoader.html').respond(201);
      $httpBackend.expectGET(SESSION_SERVER_URL + '/translations?language=nl').respond(201);

      SkryvSession.loadApp();

      $httpBackend.flush();

      requestDeferredUser.reject('error');
      requestDeferredDC.reject('error');
      $rootScope.$digest();
      expect(SkryvUser.get).have.been.calledOnce;
      expect(SkryvCollection.getCollections).have.been.called;

      expect(Notifications.notifications[0].content).to.equal('Could not retrieve user or data collections');

    });
    it('should not reset logged user and datacollections', function () {
      $httpBackend.expectGET('views/popupLoader.html').respond(201);
      $httpBackend.expectGET(SESSION_SERVER_URL + '/translations?language=nl').respond(201);

      SkryvSession.setLoggedUser({'userName': 'user'});
      SkryvSession.setCollections({'name': 'CRRD'});

      expect(SkryvUser.get).have.been.not.called;
      expect(SkryvCollection.getCollections).have.been.not.called;

      SkryvSession.loadApp();

      expect(SkryvSession.getLoggedUser()).to.deep.equal({'userName': 'user'});
      expect(SkryvSession.getCollections()).to.deep.equal({'name': 'CRRD'});
    });
    it('should set admin and no data collections (admin)', function () {
      $httpBackend.expectGET('views/popupLoader.html').respond(201);
      $httpBackend.expectGET(SESSION_SERVER_URL + '/translations?language=nl').respond(201);

      SkryvSession.loadApp();

      $httpBackend.flush();

      requestDeferredUser.resolve({'userName': 'user', authorities: [ { authority: 'ROLE_ADMIN' } ], metaData: {}});
      requestDeferredDC.resolve({'name': 'CRRD'});
      configPromise.resolve([ { value: 'TEST' } ]);
      $rootScope.$digest();

      expect(SkryvUser.get).have.been.calledOnce;
      expect(SkryvCollection.getCollections).have.been.called;
      expect(SkryvConfig.getConfigByKey).have.been.calledOnce;

      $rootScope.$digest();

      expect(SkryvSession.isAdmin()).to.be.true;
      expect($rootScope.SessionActive).to.be.true;

    });
    it.skip('should set user with default data collection (user)', function () {
      $httpBackend.expectGET('views/popupLoader.html').respond(201);
      $httpBackend.expectGET(SESSION_SERVER_URL + '/translations?language=nl').respond(201);
      $httpBackend.whenGET(SESSION_SERVER_URL + '/datacollectiondefinitions/TEST/1').respond(201);

      SkryvSession.loadApp();

      $httpBackend.flush();

      requestDeferredUser.resolve({'userName': 'user', authorities: [ { authority: 'ROLE_USER' } ], metaData: {}, dataCollectionNames: [ 'TEST' ] });
      requestDeferredDC.resolve([ 'TEST' ]);
      requestDeferredDCD.resolve({'name': 'TEST'});
      configPromise.resolve([ { value: 'TEST' } ]);
      $rootScope.$digest();
      $rootScope.$digest();

      requestDeferredVersions.resolve([ {id:5, dataCollectionName: 'CRRD', version: { major: 1 }},{ id:12, dataCollectionName: 'CRRD', version: { major: 2 } }]);

      $rootScope.$digest();

      expect(SkryvSession.isAdmin()).to.be.false;
      expect($rootScope.SessionActive).to.be.true;
    });

    it.skip('should set user with active collection (user)', function () {
      $httpBackend.expectGET('views/popupLoader.html').respond(201);
      $httpBackend.expectGET(SESSION_SERVER_URL + '/translations?language=nl').respond(201);
      $httpBackend.whenGET(SESSION_SERVER_URL + '/datacollectiondefinitions/TEST/1').respond(201);

      SkryvSession.loadApp();

      $httpBackend.flush();

      requestDeferredUser.resolve({'userName': 'user', authorities: [ { authority: 'ROLE_USER' } ], metaData: { activeDatacollectionName: 'TEST', activeDatacollectionId: 5 }, dataCollectionNames: [ 'TEST' ] });
      requestDeferredDC.resolve([ 'TEST' ]);
      configPromise.resolve([ { value: 'TEST' } ]);
      $rootScope.$digest();
      $rootScope.$digest();

      requestDeferredVersions.resolve([ {id:5, dataCollectionName: 'TEST', version: { major: 1 }},{ id:12, dataCollectionName: 'CRRD', version: { major: 2 } }]);
      $rootScope.$digest();

      expect($rootScope.SessionActive).to.be.true;
    });

    it.skip('should set user with active collection that has not the same capitalisation as user\'s collections', function () {
      $httpBackend.expectGET('views/popupLoader.html').respond(201);
      $httpBackend.expectGET(SESSION_SERVER_URL + '/translations?language=nl').respond(201);
      $httpBackend.whenGET(SESSION_SERVER_URL + '/datacollectiondefinitions/TEST/1').respond(201);

      SkryvSession.loadApp();

      $httpBackend.flush();

      requestDeferredUser.resolve({'userName': 'user', authorities: [ { authority: 'ROLE_USER' } ], metaData: { activeDatacollectionName: 'IQEDFOOT', activeDatacollectionId: 5 }, dataCollectionNames: [ 'TEST', 'IQEDFOOT' ] });
      requestDeferredDC.resolve([ 'IQEDFoot' ]);
      configPromise.resolve([ { value: 'TEST' } ]);
      $rootScope.$digest();
      $rootScope.$digest();

      requestDeferredVersions.resolve([ {id:5, dataCollectionName: 'IQEDFoot', version: { major: 1 }},{ id:12, dataCollectionName: 'IQEDFoot', version: { major: 2 } }]);
      $rootScope.$digest();

      expect($rootScope.SessionActive).to.be.true;
    });

    it('should return original promise', function () {
      mockLocalStorage.set('userInfo', {'userName': 'user'});
      var first = SkryvSession.loadApp();
      var second = SkryvSession.loadApp();
      expect(first).to.deep.equal(second);
    });

    it('should expire when after loading user token is invalid', function () {
      $httpBackend.expectGET('views/popupLoader.html').respond(201);

      SkryvSession.loadApp();
      
      expect($rootScope.SessionActive).to.be.false;
    });

    it('should not start session if no user token', function () {
      mockLocalStorage.remove('userInfo');
      SkryvSession.loadApp();
      expect($rootScope.SessionActive).to.be.false;
    });
  });

  describe('getAuthType', function () {
    it('should get userStorageType from server once', function () {
      SkryvSession.getAuthType();
      configPromise.resolve([ { value: 'DATABASE' } ]);
      $rootScope.$digest();
      SkryvSession.getAuthType();
      $rootScope.$digest();

      expect(SkryvConfig.getConfigByKey).have.been.calledOnce;
    });

    it('should fail getting userStorageType', function () {
      SkryvSession.getAuthType();
      configPromise.reject('FAILED');
      $rootScope.$digest();

      expect(SkryvConfig.getConfigByKey).have.been.calledOnce;
    });

    it('should not get userStorageType after being set', function () {
      SkryvSession.setAuthType('DATABASE');
      SkryvSession.getAuthType();
      $rootScope.$digest();

      expect(SkryvConfig.getConfigByKey).not.have.been.calledOnce;
    });
  });

  describe.skip('setSingleDefinition', function () {
    it('should a new collection to the collection list', function () {
      SkryvSession.setCollections([ { label: 'TEST', versions: [] } ]);
      expect(SkryvSession.getCollections()).to.deep.equal([ { label: 'TEST', versions: [] } ]);

      SkryvSession.setSingleDefinition('TEST', [ 'v1', 'v2' ]);
      expect(SkryvSession.getCollections()).to.deep.equal([ { label: 'TEST', versions: [ 'v1', 'v2' ] } ]);
    });
  });
});
