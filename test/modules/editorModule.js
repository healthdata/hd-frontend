/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

/* global Skryv */

// Do not make things like (function () { ... }).should.throw(...) warn about
// "Wrapping non-IIFE function literals in parens is unnecessary."
/*jshint -W068 */

describe('editor', function () {

  var $compile, $sniffer, $controller, $httpBackend, moment;
  var healthdataDocumentmodel;
  var element;
  var scope;
  var env;

  beforeEach(module('skryv.conf'));
  beforeEach(module('skryv.editor'));
  beforeEach(module('skryv.editor.components'));
  beforeEach(module('skryv.session'));
  beforeEach(module('services.dcd'));
  beforeEach(module('services.navigation'));
  beforeEach(module('views/editor/skrNumberInput.html'));
  beforeEach(module('views/editor/skrBooleanInput.html'));
  beforeEach(module('views/editor/skrDateInput.html'));
  beforeEach(module('views/editor/skrChoiceInput.html'));
  beforeEach(module('views/editor/skrMultiChoiceInput.html'));
  beforeEach(module('views/editor/skrListInput.html'));
  beforeEach(module('views/editor/skrEditorGroup.html'));
  beforeEach(module('views/editor/skrEditorSection.html'));
  beforeEach(module('views/editor/hdReferenceListInput.html'));
  beforeEach(module('ngDialog'));
  beforeEach(module('duScroll'));
  beforeEach(module('LocalStorageModule'));
  beforeEach(module('skryv.configurations'));
  beforeEach(module('services.notification'));
  beforeEach(module('services.workflow'));
  beforeEach(module('services.editor'));
  beforeEach(module('documentmodel'));

  beforeEach(inject(function (_healthdataDocumentmodel_) {
    healthdataDocumentmodel = _healthdataDocumentmodel_;
  }));

  beforeEach(inject(function (_$compile_, _$sniffer_, _$controller_, _$httpBackend_,
                              $rootScope, _moment_) {
    $compile = _$compile_;
    $sniffer = _$sniffer_;
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    scope = $rootScope.$new();
    moment = _moment_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  beforeEach(function () {
    env = {};
  });

  function manipulate(target) {
    if (env.skrDescription) {
      return healthdataDocumentmodel.documentmodelHelpers.openDocument(
        env.skrDescription,
        { content: target },
        { readOnly: false }).root;
    } else {
      return {};
    }
  }

  function compileInput(input) {
    healthdataDocumentmodel.documentmodelHelpers.compile(input, env);
  }

  function renderComponent(input, target) {
    compileInput(input);
    scope.component = env.skrEditorComponent;
    scope.manipulator = manipulate(target);
    element = $compile('<skr-editor-component component="component" manipulator="manipulator"></skr-editor-component>')(scope);
    scope.$digest();
  }

  describe('skrEditorComponent directive', function () {

    it('should require a "component" attribute', function () {
      (function () {
        $compile('<skr-editor-component></skr-editor-component>')(scope);
      }).should.throw('skrEditorComponent directive requires a "component" attribute');
    });

    it('should require a "manipulator" attribute', function () {
      scope.component = {};
      (function () {
        $compile('<skr-editor-component component="component"></skr-editor-component>')(scope);
      }).should.throw('skrEditorComponent directive requires a "manipulator" attribute');
    });

    it('should invoke the component link function', function () {
      $httpBackend.whenGET('path/to/some/template.html').respond('hello');
      var spy = sinon.spy();
      scope.component = {
        link: spy,
        template: 'path/to/some/template.html'
      };
      scope.manipulator = {};
      $compile(
        '<skr-editor-component component="component" manipulator="manipulator">' +
        '</skr-editor-component>')(scope);
      $httpBackend.flush();
      spy.should.have.been.calledOnce;
    });

    it('should check for a valid template URL', function () {
      scope.component = {};
      scope.manipulator = {};
      (function () {
        $compile(
          '<skr-editor-component component="component" manipulator="manipulator">' +
          '</skr-editor-component>')(scope);
      }).should.throw('skrEditorComponent directive requires a valid template URL, got undefined');
    });

    it('should render the template provided by the input', function () {
      $httpBackend.whenGET('path/to/some/template.html').respond('hello');
      scope.component = {
        template: 'path/to/some/template.html'
      };
      scope.manipulator = {};
      element = $compile(
        '<skr-editor-component component="component" manipulator="manipulator">' +
        '</skr-editor-component>')(scope);

      expect(element.text()).to.equal('');
      $httpBackend.flush();
      expect(element.text()).to.equal('hello');
    });

  });

  describe('skrNumberInput', function () {

    var numberInput = {
      type: 'number',
      minimum: 0,
      maximum: 120,
      label: 'Age',
      help: 'We need your age to know how old you are.'
    };

    it('should compile to an editor component', function () {
      compileInput(numberInput);
      env.should.have.property('skrEditorComponent');
      var component = env.skrEditorComponent;
      expect(component.description).to.equal(env.skrDescription);
      expect(component.label).to.equal('Age');
      expect(component.help).to.equal('We need your age to know how old you are.');
    });

    it('should check for matching descriptions during linking', function () {
      scope.component = {
        componentName: 'skrNumberInput'
      };
      scope.manipulator = { description: {} };
      (function () {
      $compile(
        '<skr-editor-component component="component" manipulator="manipulator">' +
        '</skr-editor-component>')(scope);
      }).should.throw('input link: manipulator should have matching description');
    });

    describe('rendering', function () {

      var target;
      var inputElt;

      beforeEach(function () {
        $httpBackend.expectGET('views/editor/repeatables/helpText.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/validationIcons.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/fieldErrors.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/linkedMessages.html').respond(200, '');
        target = scope.target = { value: 1 };
        renderComponent(numberInput, target);
        $httpBackend.flush();
        inputElt = element.find('input');
      });

      function changeInputValueTo(value) {
        inputElt.val(value);
        inputElt.triggerHandler($sniffer.hasEvent('input') ? 'input' : 'change');
      }

      it('should have an editable input field', function () {
        expect(inputElt).to.have.lengthOf(1);
        changeInputValueTo('5');
        expect(inputElt.val()).to.equal('5');
      });

      it('should have the appropriate label', function () {
        var labelElt = element.find('label');
        expect(labelElt).to.have.lengthOf(1);
        expect(labelElt.text().trim()).to.equal('Age');
      });

      it('should have the input controls', function () {
        expect(element.find('skr-editor-input-controls')).to.have.lengthOf(1);
      });

      it('should have a help button', function () {
        expect(element.find('skr-editor-help-button')).to.have.lengthOf(1);
      });

      it('should bind to the target', function () {
        expect(inputElt.val()).to.equal('1');
        changeInputValueTo('10');
        expect(target.value).to.equal(10);

        scope.$apply('target.value = 5');
        expect(inputElt.val()).to.equal('5');
      });

      it.skip('should bind to the manipulator on non-numeric input', function () {
        // FIXME This behavior does not fit well with angular's idea of how
        // validation should work
        changeInputValueTo('!@#$%');
        expect(target.value).to.equal('!@#$%');
      });

      it.skip('should bind to the manipulator on non-numeric data', function () {
        // FIXME This behavior does not fit well with angular's idea of how
        // validation should work
        scope.$apply('target.value = "not a number"');
        expect(inputElt.val()).to.equal('not a number');
      });

      it('should apply CSS class "ng-valid" on valid input', function () {
        changeInputValueTo('4');
        expect(inputElt.hasClass('ng-valid')).to.be.true;
      });

      it('should apply CSS class "ng-invalid" on non-numeric input', function () {
        changeInputValueTo('4');
        expect(inputElt.hasClass('ng-valid')).to.be.true;

        // copy-pasted from angular's number input unit tests
        try {
          // to allow non-number values, we have to change type so that
          // the browser which have number validation will not interfere with
          // this test. IE8 won't allow it hence the catch.
          inputElt[0].setAttribute('type', 'text');
        } catch (e) {}

        changeInputValueTo('!@#$%');
        // verify that the input value retains the last gvalid value
        expect(inputElt.val()).to.equal('4');

        //expect(inputElt.hasClass('ng-valid')).to.be.false;
        //expect(inputElt.hasClass('ng-invalid')).to.be.true;
      });

      it('should apply CSS class "ng-invalid" on input violating the maximum', function () {
        changeInputValueTo('4242');
        expect(inputElt.hasClass('ng-invalid')).to.be.true;
      });

      it('should apply CSS class "ng-invalid" on input violating the minimum', function () {
        changeInputValueTo('-1');
        expect(inputElt.hasClass('ng-invalid')).to.be.true;
      });

    });
  });

  describe('skrBooleanInput', function () {

    var booleanInput = {
      type: 'boolean',
      label: 'Do you like pizza?',
      help: 'Obviously you like pizza, right?'
    };

    it('should compile to an editor component', function () {
      compileInput(booleanInput);
      env.should.have.property('skrEditorComponent');
      var component = env.skrEditorComponent;
      expect(component.description).to.equal(env.skrDescription);
      expect(component.label).to.equal('Do you like pizza?');
      expect(component.help).to.equal('Obviously you like pizza, right?');
    });

    it('should check for matching descriptions during linking', function () {
      scope.component = {
        componentName: 'skrBooleanInput'
      };
      scope.manipulator = { description: {} };
      (function () {
      $compile(
        '<skr-editor-component component="component" manipulator="manipulator">' +
        '</skr-editor-component>')(scope);
      }).should.throw('input link: manipulator should have matching description');
    });

    describe('rendering', function () {

      var target;
      var inputElt;

      beforeEach(function () {
        $httpBackend.expectGET('views/editor/repeatables/helpText.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/validationIcons.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/fieldErrors.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/linkedMessages.html').respond(200, '');
        target = scope.target = { value: null };
        renderComponent(booleanInput, target);
        $httpBackend.flush();
        inputElt = element.find('input');
      });

      function clickRadio(num) {
        inputElt[num].click();
        if(window.navigator.userAgent.indexOf('Firefox') > -1) {
          var targetValue = 'target.value = ' + inputElt[num].value;
          scope.$apply(targetValue);
        }
      }

      it('should have two clickable radio buttons', function () {
        expect(inputElt).to.have.lengthOf(2);
        expect(inputElt[0].checked).to.be.false;
        expect(inputElt[1].checked).to.be.false;
        clickRadio(0);
        expect(inputElt[0].checked).to.be.true;
        expect(inputElt[1].checked).to.be.false;
        clickRadio(1);
        expect(inputElt[0].checked).to.be.false;
        expect(inputElt[1].checked).to.be.true;
      });

      it('should have the appropriate radio button labels', function () {
        var labelElt = element.find('label');
        expect(labelElt).to.have.lengthOf(3);
        expect(labelElt.eq(0).text().trim()).to.equal('Do you like pizza?');
        expect(labelElt.eq(1).text().trim()).to.equal('Yes');
        expect(labelElt.eq(2).text().trim()).to.equal('No');
      });

      it('should have the input controls', function () {
        expect(element.find('skr-editor-input-controls')).to.have.lengthOf(1);
      });

      it('should have a help button', function () {
        expect(element.find('skr-editor-help-button')).to.have.lengthOf(1);
      });

      it('should bind to the target', function () {
        clickRadio(0);
        expect(target.value).to.equal(true);
        clickRadio(1);
        expect(target.value).to.equal(false);

        scope.$apply('target.value = true');
        expect(inputElt[0].checked).to.be.true;
        expect(inputElt[1].checked).to.be.false;

        scope.$apply('target.value = undefined');
        expect(inputElt[0].checked).to.be.false;
        expect(inputElt[1].checked).to.be.false;
      });
    });
  });

  describe('skrPropertyName', function () {

    var skrPropertyManipulator;

    beforeEach(function () {
      skrPropertyManipulator = sinon.stub();
      // stub for skrObject, as needed by skrPropertyName
      env.skrObject = { addProperty: sinon.stub() };
      // stub for skrPropertyManipulator, needed by skrPropertyName
      env.skrPropertyManipulator = skrPropertyManipulator;
      // this is necessary because skrPropertyName requires skrObject on the
      // parent environment
      env = Object.create(env);
    });

    it('should tell components how to find their manipulator', function () {
      compileInput({
        type: 'number',
        label: 'Some number',
        name: 'someNumber'
      });
      var component = env.skrEditorComponent;

      var manipulator = manipulate({ /* empty target */ });
      var numberManipulator = component.nestedManipulator(manipulator);

      expect(skrPropertyManipulator).to.have.been.calledOnce;
      expect(skrPropertyManipulator).to.have.been.calledWithExactly('someNumber', manipulator);
    });

    it('should not get confused by an skrObject', function () {
      compileInput({
        type: 'object',
        name: 'someNumber'
      });
      var component = env.skrEditorComponent;

      var manipulator = manipulate({ /* empty target */ });
      var numberManipulator = component.nestedManipulator(manipulator);

      expect(skrPropertyManipulator).to.have.been.calledOnce;
      expect(skrPropertyManipulator).to.have.been.calledWithExactly('someNumber', manipulator);
    });

  });

  describe('skrObject', function () {

    var pairOfNumbers = {
      type: 'object',
      fields: [
        {
          type: 'number',
          label: 'Value of a',
          name: 'a'
        },
        {
          type: 'number',
          label: 'Value of b',
          name: 'b'
        }
      ]
    };

    it('should compile to an editor group', function () {
      compileInput(pairOfNumbers);
      env.should.have.property('skrEditorComponent');
      var group = env.skrEditorComponent;
      expect(group.components).to.have.lengthOf(2);
    });

    describe('rendering', function () {

      var target;

      beforeEach(function () {
        $httpBackend.expectGET('views/editor/repeatables/helpText.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/validationIcons.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/fieldErrors.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/linkedMessages.html').respond(200, '');
        target = scope.target = { value: { a: 1, b: 2 } };
        renderComponent(pairOfNumbers, target);
        $httpBackend.flush();
      });

      it('should render all nested components', function () {
        expect(element.find('skr-editor-component')).to.have.lengthOf(2);
        expect(element.find('input')).to.have.lengthOf(2);
      });

    });

  });

  describe('skrSection', function () {

    var section = {
      type: 'section',
      label: 'A Cool Section Title',
      help: 'Some useful information'
    };

    it('should compile to an editor component', function () {
      compileInput(section);
      env.should.have.property('skrEditorComponent');
      var component = env.skrEditorComponent;
      expect(component.title).to.equal('A Cool Section Title');
      expect(component.help).to.equal('Some useful information');
      expect(component.depth).to.equal(0);
    });

    var subsections = {
      type: 'object',
      sections: [
        {
          label: 'This is a main section',
          sections: [
            { label: 'This is the subsection' }
          ]
        },
        { label: 'This is another main section' }
      ]
    };

    it('should support subsections', function () {
      compileInput(subsections);
      var group = env.skrEditorComponent;
      expect(group.components).to.have.lengthOf(2);
      expect(group.sections).to.have.lengthOf(2);
      var main1 = group.components[0];
      expect(main1.components).to.have.lengthOf(1);
      var sub = main1.components[0];
      var main2 = group.components[1];
      expect(group.sections[0]).to.equal(main1);
      expect(group.sections[1]).to.equal(main2);
      expect(main1.title).to.equal('This is a main section');
      expect(sub.title).to.equal('This is the subsection');
      expect(main2.title).to.equal('This is another main section');
      expect(main1.depth).to.equal(0);
      expect(sub.depth).to.equal(1);
      expect(main2.depth).to.equal(0);
      expect(main1.subsections).to.have.lengthOf(1);
      expect(main1.subsections[0]).to.equal(sub);
      expect(main1.name).to.equal('section_1_this_is_a_main_section');
      expect(sub.name).to.equal('section_1_1_this_is_a_main_section_this_is_the_subsection');
      expect(main2.name).to.equal('section_2_this_is_another_main_section');
    });

    describe('sectionNumber filter', function () {

      it('should pretty print section numbers', inject(function (sectionNumberFilter) {
        expect(sectionNumberFilter([ 1, 2, 3])).to.equal('1.2.3');
        expect(sectionNumberFilter([])).to.equal('');
      }));

    });

    describe('tableOfContents directive', function () {
      it('should render a table of contents', function () {
        var toc = $compile('<skr-table-of-contents sections="someSections"></skr-table-of-contents>')(scope);
        scope.$digest();
        expect(toc.text()).to.equal('');
        scope.someSections = [
          {
            sectionNumber: [ 1 ],
            title: 'A main sections',
            subsections: [
              {
                sectionNumber: [ 1, 1 ],
                title: 'A subsection'
              }
            ]
          },
          {
            sectionNumber: [ 2 ],
            title: 'Another section'
          }
        ];
        scope.$digest();
        var links = toc.find('a');
        expect(links).to.have.lengthOf(3);
        expect(links[0].text.trim()).to.equal('1 A main sections');
        expect(links[1].text.trim()).to.equal('1.1 A subsection');
        expect(links[2].text.trim()).to.equal('2 Another section');
      });
    });

    describe('rendering', function () {

      var target;
      var inputElt;

      beforeEach(function () {
        target = scope.target = { value: undefined };
      });

      it('should have the titles with section numbers', function () {
        renderComponent(subsections, target);
        var mainElt = element.find('div.level-0 > p.doc-title');
        var subElt = element.find('div.level-1 > p.doc-title');
        expect(mainElt).to.have.lengthOf(2);
        expect(subElt).to.have.lengthOf(1);
        expect(mainElt.eq(0).text().trim()).to.contain('1 - This is a main section');
        expect(mainElt.eq(1).text().trim()).to.contain('2 - This is another main section');
        expect(subElt.text().trim()).to.equal('1.1 - This is the subsection');
      });

    });
  });

  describe('skrDateInput', function () {

    var dateInput = {
      type: 'date',
      label: 'When were your born?',
      help: 'We need this info to send you a birthday present.'
    };

    it('should compile to an editor component', function () {
      compileInput(dateInput);
      env.should.have.property('skrEditorComponent');
      var component = env.skrEditorComponent;
      expect(component.description).to.equal(env.skrDescription);
      expect(component.label).to.equal('When were your born?');
      expect(component.help).to.equal('We need this info to send you a birthday present.');
    });

    it('should check for matching descriptions during linking', function () {
      scope.component = {
        componentName: 'skrDateInput'
      };
      scope.manipulator = { description: {} };
      (function () {
      $compile(
        '<skr-editor-component component="component" manipulator="manipulator">' +
        '</skr-editor-component>')(scope);
      }).should.throw('input link: manipulator should have matching description');
    });

    describe('rendering', function () {

      var target;
      var inputElt;

      beforeEach(function () {
        $httpBackend.expectGET('views/editor/repeatables/helpText.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/validationIcons.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/fieldErrors.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/linkedMessages.html').respond(200, '');
        target = scope.target = { value: null };
        renderComponent(dateInput, target);
        $httpBackend.flush();
        inputElt = element.find('input');
      });

      function changeInputValueTo(value) {
        inputElt.val(value);
        if (window.navigator.userAgent.indexOf('.NET') > -1) {
          inputElt.triggerHandler($sniffer.hasEvent('input') ? 'input' : 'change');
          inputElt.triggerHandler('change');
        } else {
          inputElt.triggerHandler($sniffer.hasEvent('input') ? 'input' : 'change');
        }
      }

      it('should have an editable input field with a mask', function () {
        expect(inputElt).to.have.lengthOf(1);
        expect(inputElt.val()).to.equal('dd/mm/yyyy');
        changeInputValueTo('11');
        expect(inputElt.val()).to.equal('11/mm/yyyy');
      });

      it('should have the appropriate label', function () {
        var labelElt = element.find('label');
        expect(labelElt).to.have.lengthOf(1);
        expect(labelElt.text()).to.contain('When were your born?');
      });

      it('should have the input controls', function () {
        expect(element.find('skr-editor-input-controls')).to.have.lengthOf(1);
      });

      it('should have a help button', function () {
        expect(element.find('skr-editor-help-button')).to.have.lengthOf(1);
      });

      it('should bind to the target', function () {
        // Zomertijd
        changeInputValueTo('22/10/2014');
        expect(target.value).to.equal('2014-10-22');

        // Wintertijd
        changeInputValueTo('22/11/2014');
        expect(target.value).to.equal('2014-11-22');

        target.value = '2013-09-22';
        scope.$digest();
        expect(inputElt.val()).to.equal('22/09/2013');
      });

      it('should support manual editing', function () {
        changeInputValueTo('02/11/2014');
        expect(target.value).to.equal('2014-11-02');

        changeInputValueTo('02/11/201');
        expect(inputElt.val()).to.equal('02/11/201y');
        // TODO: Bug in IE
        if (window.navigator.userAgent.indexOf('.NET') == -1) {
          expect(target.value).to.equal(undefined);
        }
      });

      it('should bind to the manipulator on non-date data', function () {
        // the formatter kicks in and turns the non-date value into a mask
        target.value = 'this is not a date';
        scope.$digest();
        expect(inputElt.val()).to.equal('dd/mm/yyyy');
      });

      it('should apply CSS class "ng-valid" on valid input', function () {
        changeInputValueTo('22/09/2014');
        expect(inputElt.hasClass('ng-valid')).to.be.true;
        expect(inputElt.hasClass('ng-invalid')).to.be.false;
      });

      it('should apply CSS class "ng-invalid" on non-date input', function () {
        var browser = window.navigator.userAgent;
        changeInputValueTo('22/09/2014');
        expect(inputElt.hasClass('ng-valid')).to.be.true;

        changeInputValueTo('44/55/9999');
        // verify that the input value update got through
        // Firefox resets the date
        if (browser.indexOf('Firefox') > -1 || browser.indexOf('.NET') > -1) {
          expect(inputElt.val()).to.equal('10/00/2092');
        } else {
          expect(inputElt.val()).to.equal('44/55/9999');
        }

        expect(inputElt.hasClass('ng-valid')).to.be.false;
        expect(inputElt.hasClass('ng-invalid')).to.be.true;
      });

    });
  });

  describe('skrChoiceInput', function () {

    var choiceInput = {
      type: 'choice',
      label: 'How do you like your eggs?',
      help: 'This is not a joke ...',
      choices: [
        { name: 'scrambled', label: 'Roerei' },
        { name: 'sunnysideup', label: 'Paardenoog' },
        { name: 'boiled', label: 'Gekookt',
          fields: [ { type: 'number', name: 'amount', label: 'Hoeveel?' } ]
        }
      ]
    };

    it('should compile to an editor component', function () {
      compileInput(choiceInput);
      env.should.have.property('skrEditorComponent');
      var component = env.skrEditorComponent;
      expect(component.description).to.equal(env.skrDescription);
      expect(component.label).to.equal('How do you like your eggs?');
      expect(component.help).to.equal('This is not a joke ...');
    });

    it('should check for matching descriptions during linking', function () {
      scope.component = {
        componentName: 'skrChoiceInput'
      };
      scope.manipulator = { description: {} };
      (function () {
      $compile(
        '<skr-editor-component component="component" manipulator="manipulator">' +
        '</skr-editor-component>')(scope);
      }).should.throw('input link: manipulator should have matching description');
    });

    describe('rendering', function () {

      var target;
      var inputElt;

      beforeEach(function () {
        target = scope.target = { value: undefined };
        $httpBackend.expectGET('views/editor/repeatables/helpText.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/validationIcons.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/fieldErrors.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/linkedMessages.html').respond(200, '');
        renderComponent(choiceInput, target);
        $httpBackend.flush();
        inputElt = element.find('input');
      });

      function clickRadio(num) {
        inputElt[num].click();
        if(window.navigator.userAgent.indexOf('Firefox') > -1) {
          scope.manipulator.selectedOption = inputElt[num].value;
          scope.$apply();
        }
      }

      function changeNumberValueTo(value) {
        inputElt.eq(3).val(value);
        inputElt.eq(3).triggerHandler($sniffer.hasEvent('input') ? 'input' : 'change');
      }

      it('should have three clickable radio buttons and an editable number input', function () {
        expect(inputElt).to.have.lengthOf(4);
        expect(inputElt[0].checked).to.be.false;
        expect(inputElt[1].checked).to.be.false;
        expect(inputElt[2].checked).to.be.false;
        clickRadio(0);
        expect(inputElt[0].checked).to.be.true;
        expect(inputElt[1].checked).to.be.false;
        expect(inputElt[2].checked).to.be.false;
        clickRadio(1);
        expect(inputElt[0].checked).to.be.false;
        expect(inputElt[1].checked).to.be.true;
        expect(inputElt[2].checked).to.be.false;
        clickRadio(2);
        expect(inputElt[0].checked).to.be.false;
        expect(inputElt[1].checked).to.be.false;
        expect(inputElt[2].checked).to.be.true;

        expect(inputElt.eq(3).val()).to.equal('');
        changeNumberValueTo('5');
        expect(inputElt.eq(3).val()).to.equal('5');
      });

      it('should show nested fields only when option is selected', function () {
        element.find('input').eq(3).should.have.lengthOf(1);
        element.find('input').eq(3).attr('type').should.equal('number');
        expect(inputElt.eq(3).parents('.ng-hide')).to.have.lengthOf(1);
        clickRadio(2);
        expect(inputElt.eq(3).parents('.ng-hide')).to.have.lengthOf(0);
      });

      it('should have the appropriate radio button labels', function () {
        var labelElt = element.find('label');
        expect(labelElt).to.have.lengthOf(5);
        expect(labelElt.eq(0).text().trim()).to.equal('How do you like your eggs?');
        expect(labelElt.eq(1).text().trim()).to.equal('Roerei');
        expect(labelElt.eq(2).text().trim()).to.equal('Paardenoog');
        expect(labelElt.eq(3).text().trim()).to.equal('Gekookt');
        expect(labelElt.eq(4).text().trim()).to.equal('Hoeveel?');
      });

      it('should have the input controls', function () {
        // one for the choice as a whole
        // another for the number field
        expect(element.find('skr-editor-input-controls')).to.have.lengthOf(2);
      });

      it('should have a help button', function () {
        expect(element.find('skr-editor-help-button')).to.have.lengthOf(2);
      });

      it('should bind to the target', function () {
        clickRadio(0);
        expect(scope.manipulator.selectedOption).to.equal('scrambled');
        clickRadio(1);
        expect(scope.manipulator.selectedOption).to.equal('sunnysideup');
        clickRadio(2);
        expect(scope.manipulator.selectedOption).to.equal('boiled');

        scope.manipulator.selectedOption = 'scrambled';
        scope.$digest();
        expect(inputElt[0].checked).to.be.true;
        expect(inputElt[1].checked).to.be.false;
        expect(inputElt[2].checked).to.be.false;

        scope.manipulator.selectedOption = undefined;
        scope.$digest();
        expect(inputElt[0].checked).to.be.false;
        expect(inputElt[1].checked).to.be.false;
        expect(inputElt[2].checked).to.be.false;
      });

    });
  });

  describe('skrMultiChoiceInput', function () {

    var multiChoiceInput = {
      type: 'multichoice',
      label: 'In welke buurlanden ben je al geweest?',
      help: 'Inderdaad, het Verenigd Koninkrijk staat er niet tussen.',
      choices: [
        { name: 'de', label: 'Duitsland' },
        { name: 'nl', label: 'Nederland' },
        { name: 'fr', label: 'Frankrijk' },
        { name: 'lu', label: 'Luxemburg',
          fields: [ { type: 'date', name: 'when', label: 'Wanneer?' } ] }
      ]
    };

    it('should compile to an editor component', function () {
      compileInput(multiChoiceInput);
      env.should.have.property('skrEditorComponent');
      var component = env.skrEditorComponent;
      expect(component.description).to.equal(env.skrDescription);
      expect(component.label).to.equal('In welke buurlanden ben je al geweest?');
      expect(component.help).to.equal('Inderdaad, het Verenigd Koninkrijk staat er niet tussen.');
    });

    it('should check for matching descriptions during linking', function () {
      scope.component = {
        componentName: 'skrMultiChoiceInput'
      };
      scope.manipulator = { description: {} };
      (function () {
      $compile(
        '<skr-editor-component component="component" manipulator="manipulator">' +
        '</skr-editor-component>')(scope);
      }).should.throw('input link: manipulator should have matching description');
    });

    describe('rendering', function () {

      var target;
      var inputElt;

      beforeEach(function () {
        target = scope.target = { value: undefined };
        $httpBackend.expectGET('views/editor/repeatables/helpText.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/validationIcons.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/fieldErrors.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/linkedMessages.html').respond(200, '');
        renderComponent(multiChoiceInput, target);
        $httpBackend.flush();
        inputElt = element.find('input');
      });

      function clickCheckbox(num) {
        inputElt[num].click();
        if(window.navigator.userAgent.indexOf('Firefox') > -1) {
          var lang;
          switch (num) {
            case 0:
              lang = 'de';
              break;
            case 1:
              lang = 'nl';
              break;
            case 2:
              lang = 'fr';
              break;
            case 3:
              lang = 'lu';
              break;
          }
          scope.manipulator.optionManipulators[lang].isSelected = !scope.manipulator.optionManipulators[lang].isSelected;
          scope.$apply();
        }
      }

      function changeDateValueTo(value) {
        inputElt.eq(4).val(value);
        inputElt.eq(4).triggerHandler($sniffer.hasEvent('input') ? 'input' : 'change');
      }

      it('should have four clickable checkboxes and a date input', function () {
        expect(inputElt).to.have.lengthOf(5);
        expect(inputElt[0].checked).to.be.false;
        expect(inputElt[1].checked).to.be.false;
        expect(inputElt[2].checked).to.be.false;
        expect(inputElt[3].checked).to.be.false;
        clickCheckbox(0);
        expect(inputElt[0].checked).to.be.true;
        expect(inputElt[1].checked).to.be.false;
        expect(inputElt[2].checked).to.be.false;
        expect(inputElt[3].checked).to.be.false;
        clickCheckbox(1);
        expect(inputElt[0].checked).to.be.true;
        expect(inputElt[1].checked).to.be.true;
        expect(inputElt[2].checked).to.be.false;
        expect(inputElt[3].checked).to.be.false;
        clickCheckbox(2);
        expect(inputElt[0].checked).to.be.true;
        expect(inputElt[1].checked).to.be.true;
        expect(inputElt[2].checked).to.be.true;
        expect(inputElt[3].checked).to.be.false;
        clickCheckbox(3);
        expect(inputElt[0].checked).to.be.true;
        expect(inputElt[1].checked).to.be.true;
        expect(inputElt[2].checked).to.be.true;
        expect(inputElt[3].checked).to.be.true;
        clickCheckbox(0);
        expect(inputElt[0].checked).to.be.false;
        expect(inputElt[1].checked).to.be.true;
        expect(inputElt[2].checked).to.be.true;
        expect(inputElt[3].checked).to.be.true;

        expect(inputElt.eq(4).val()).to.equal('dd/mm/yyyy');
        changeDateValueTo('21/04/1987');
        expect(inputElt.eq(4).val()).to.equal('21/04/1987');
      });

      it('should have the appropriate checkbox labels', function () {
        var labelElt = element.find('label');
        expect(labelElt).to.have.lengthOf(6);
        expect(labelElt.eq(0).text().trim()).to.equal('In welke buurlanden ben je al geweest?');
        expect(labelElt.eq(1).text().trim()).to.equal('Duitsland');
        expect(labelElt.eq(2).text().trim()).to.equal('Nederland');
        expect(labelElt.eq(3).text().trim()).to.equal('Frankrijk');
        expect(labelElt.eq(4).text().trim()).to.equal('Luxemburg');
        expect(labelElt.eq(5).text().trim()).to.contain('Wanneer?');
      });

      it('should have the input controls', function () {
        // One for the multichoice as a whole, another for the date 
        expect(element.find('skr-editor-input-controls')).to.have.lengthOf(2);
      });

      it('should have a help button', function () {
        expect(element.find('skr-editor-help-button')).to.have.lengthOf(2);
      });

      it('should bind to the target', function () {
        function om(optionName) {
          return scope.manipulator.optionManipulators[optionName];
        }
        expect(om('de').isSelected).to.equal(false);
        expect(om('nl').isSelected).to.equal(false);
        expect(om('fr').isSelected).to.equal(false);
        expect(om('lu').isSelected).to.equal(false);

        clickCheckbox(0);
        expect(om('de').isSelected).to.equal(true);
        expect(om('nl').isSelected).to.equal(false);
        expect(om('fr').isSelected).to.equal(false);
        expect(om('lu').isSelected).to.equal(false);

        clickCheckbox(0);
        clickCheckbox(2);
        clickCheckbox(3);
        expect(om('de').isSelected).to.equal(false);
        expect(om('nl').isSelected).to.equal(false);
        expect(om('fr').isSelected).to.equal(true);
        expect(om('lu').isSelected).to.equal(true);

        om('de').isSelected = true;
        om('lu').isSelected = false;
        scope.$digest();
        expect(inputElt[0].checked).to.be.true;
        expect(inputElt[1].checked).to.be.false;
        expect(inputElt[2].checked).to.be.true;
        expect(inputElt[3].checked).to.be.false;
      });

    });
  });

  describe('skrListInput', function () {

    var listInput = {
      type: 'list',
      label: 'Postzegels',
      help: 'Je moet er genoeg kleven!',
      element: {
        type: 'number',
        label: 'Waarde',
        minimum: 0
      }
    };

    it('should compile to an editor component', function () {
      compileInput(listInput);
      env.should.have.property('skrEditorComponent');
      var component = env.skrEditorComponent;
      expect(component.description).to.equal(env.skrDescription);
      expect(component.label).to.equal('Postzegels');
      expect(component.help).to.equal('Je moet er genoeg kleven!');
    });

    it('should check for matching descriptions during linking', function () {
      scope.component = {
        componentName: 'skrListInput'
      };
      scope.manipulator = { description: {} };
      (function () {
      $compile(
        '<skr-editor-component component="component" manipulator="manipulator">' +
        '</skr-editor-component>')(scope);
      }).should.throw('input link: manipulator should have matching description');
    });

    describe('rendering', function () {

      var target;
      var buttonAdd;
      var buttonDel;

      beforeEach(function () {
        target = scope.target = { value: [ 1, 2 ] };
        $httpBackend.expectGET('views/editor/repeatables/helpText.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/fieldErrors.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/validationIcons.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/linkedMessages.html').respond(200, '');
        renderComponent(listInput, target);
        $httpBackend.flush();
        buttonAdd = element.find('.btn-list-add');
        buttonDel = element.find('.btn-list-del');
      });

      function clickAdd() {
        buttonAdd[0].click();
        buttonDel = element.find('.btn-list-del');
      }

      function clickDelete(num) {
        buttonDel[num + 1].click();
        buttonDel = element.find('.btn-list-del');
      }

      it('should two clickable add buttons and two delete buttons', function () {
        expect(buttonAdd).to.have.lengthOf(1);
        expect(buttonDel).to.have.lengthOf(2);
        clickAdd();
        expect(buttonDel).to.have.lengthOf(3);
        clickAdd();
        expect(buttonDel).to.have.lengthOf(4);
        clickDelete(0);
        expect(buttonDel).to.have.lengthOf(3);
        clickDelete(1);
        expect(buttonDel).to.have.lengthOf(2);
      });

      it('should render each list element', function () {
        expect(element.find('input')).to.have.lengthOf(2);
        clickAdd();
        expect(element.find('input')).to.have.lengthOf(3);
        clickDelete(1);
        expect(element.find('input')).to.have.lengthOf(2);
      });

      it('should have the input controls', function () {
        // One for the list as a whole, another two for the list elements
        expect(element.find('skr-editor-input-controls')).to.have.lengthOf(3);
      });

      it('should have a help button', function () {
        expect(element.find('skr-editor-help-button')).to.have.lengthOf(3);
      });

      it('should bind to the target', function () {
        clickAdd();
        expect(target.value).to.deep.equal([ 1, 2, undefined ]);
        clickDelete(0);
        expect(target.value).to.deep.equal([ 1, undefined ]);
      });

    });
  });

  describe('ProgressCtrl', function () {
    it.skip("should process a document's progress and put it on the $scope", function () {
      $controller('ProgressCtrl', {
        $scope: scope,
        $attrs: { document: 'someDocument' }
      });

      // when someDocument is not present
      expect(scope.progress).to.be.undefined;
      scope.$digest();
      expect(scope.progress).to.be.undefined;

      // when someDocument is present
      scope.someDocument = {
        progress: {
          valid: 5,
          invalid: 10
        }
      };
      scope.$digest();

      expect(scope.progress).to.deep.equal({
        valid: 5,
        invalid: 10,
        total: 15,
        validRatio: 100 * 5 / 15,
        invalidRatio: 100 * 10 / 15
      });

      // when someDocument.progress changes
      scope.someDocument = {
        progress: {
          valid: 6,
          invalid: 9
        }
      };
      scope.$digest();

      expect(scope.progress).to.deep.equal({
        valid: 6,
        invalid: 9,
        total: 15,
        validRatio: 100 * 6 / 15,
        invalidRatio: 100 * 9 / 15
      });

      // when someDocument disappears
      scope.someDocument = undefined;
      scope.$digest();

      expect(scope.progress).to.be.undefined;
    });
  });

  describe('skrEditorInputControlsDirective', function () {
    it('should wrap its contents with the appropriate id', function () {
      element = $compile('<skr-editor-input-controls>Hello</skr-editor-input-controls>')(scope);
      scope.manipulator = { path: 'pathOfManipulator' };
      scope.$digest();

      var content = element.find('#editorpathOfManipulator');
      expect(content).to.have.lengthOf(1);
      expect(content.text()).to.equal('Hello');
    });

    it('should show a comment button', function () {
      element = $compile('<skr-editor-input-controls></skr-editor-input-controls>')(scope);
      var spy = sinon.spy();
      scope.manipulator = { addComment: spy };
      scope.$digest();

      var commentButton = element.find('.comment-button');
      expect(commentButton).to.have.lengthOf(1);
    });

  });

  describe('commentTitle filter', function () {
    it('should pretty the comment title', inject(function (commentTitleFilter) {
      var title = '$.this_is_my_title';
      expect(commentTitleFilter(title)).to.equal('This is my title');
    }));

  });

  describe('hdReferenceListInput', function () {

    var refListInput = {
      type: 'text',
      referencelist: 'DISTRICT',
      label: 'District'
    };

    it('should compile to an editor component', function () {
      compileInput(refListInput);
      env.should.have.property('skrEditorComponent');
      var component = env.skrEditorComponent;
      expect(component.description).to.equal(env.skrDescription);
      expect(component.label).to.equal('District');
    });

    it('should check for matching descriptions during linking', function () {
      scope.component = {
        componentName: 'hdReferenceListInput'
      };
      scope.manipulator = { description: {} };
      (function () {
      $compile(
        '<skr-editor-component component="component" manipulator="manipulator">' +
        '</skr-editor-component>')(scope);
      }).should.throw('input link: manipulator should have matching description');
    });

    describe('rendering', function () {

      var target;
      var inputElt;

      beforeEach(function () {
        target = scope.target = { value: undefined };
        $httpBackend.expectGET('views/editor/repeatables/helpText.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/validationIcons.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/fieldErrors.html').respond(200, '');
        $httpBackend.expectGET('views/editor/repeatables/linkedMessages.html').respond(200, '');
        renderComponent(refListInput, target);
        $httpBackend.flush();
        inputElt = element.find('input');
      });

      function changeInputValueTo(value) {
        inputElt.val(value);
        inputElt.triggerHandler($sniffer.hasEvent('input') ? 'input' : 'change');
      }

      // function expectRefListRequest(params, response) {
      //   var url = REF_SERVER_URL + '?' + params + '&type=DISTRICT&language=en';
      //   $httpBackend.whenGET(url).respond(response);
      // }

      it('should have a help button', function () {
        expect(element.find('skr-editor-help-button')).to.have.lengthOf(1);
      });

      it('should have the input controls', function () {
        expect(element.find('skr-editor-input-controls')).to.have.lengthOf(1);
      });

      it.skip('should make a request to the reference list when changed', function () {
        // The http calls are not intercepted because we inject $http in a "run" block in
        // the editor module.
        // Is this a bug in angular?
        expectRefListRequest('value=foo&parentCode=', []);
        changeInputValueTo('foo');
        // this will fail if no request was made
        $httpBackend.flush();
      });

    });
  });
});
