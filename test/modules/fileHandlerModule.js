/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('fileHandlerModule', function () {
  var SkryvFile, Notifications;

  beforeEach(module('services.notification'));
  beforeEach(module('skryv.file'));

  beforeEach(inject(function (_SkryvFile_, _Notifications_) {
    SkryvFile = _SkryvFile_;
    Notifications = _Notifications_;
  }));

  describe('upload', function () {
    it('should check browser compatibility', function () {
      window.File = true;
      SkryvFile.upload([]);
      expect(Notifications.notifications).lengthOf(0);

      window.File = false;
      SkryvFile.upload([]);
      expect(Notifications.notifications[0].content).to.equal('File upload not supported. Please update your browser!');
      window.File = true;
    });
  });
});
