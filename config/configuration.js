(function () {
  'use strict';

  function isPresent(constant, constantDefault) {
    if (constant.indexOf('@@') > -1) {
      return constantDefault;
    } else {
      return constant;
    }
  }

  var configuration = angular.module('skryv.conf', []);
  configuration
    .constant('USER_SERVER_URL', '@@ENVIRONMENT_URL' + '/users/')
    .constant('USER_REQUEST_SERVER_URL', '@@ENVIRONMENT_URL' + '/userrequests/')
    .constant('USER_CONFIGURATION_SERVER_URL', '@@ENVIRONMENT_URL' + '/users/configurations/')
    .constant('USER_HD4DP_SERVER_URL', '@@ENVIRONMENT_URL' + '/hd4dpusers/')
    .constant('LDAP_SERVER_URL', '@@ENVIRONMENT_URL' + '/ldap/users/')
    .constant('PARTICIPATION_SERVER_URL', '@@ENVIRONMENT_URL' + '/participations/')
    .constant('ORGANIZATION_SERVER_URL', '@@ENVIRONMENT_URL' + '/organizations/')
    .constant('SESSION_SERVER_URL', '@@ENVIRONMENT_URL')
    .constant('TEMPLATE_SERVER_URL', '/datacollectiondefinitions/')
    .constant('WORKFLOW_SERVER_URL', isPresent('@@WORKFLOW_SERVER_URL', '@@ENVIRONMENT_URL') + '/workflows/')
    .constant('SEARCH_SERVER_URL', '@@ENVIRONMENT_URL' + '/workflows/search/')
    .constant('ELASTIC_REFRESH_URL', '@@ENVIRONMENT_URL' + '/elasticsearch')
    .constant('OAUTH_SERVER_URL', '@@ENVIRONMENT_URL' + '/oauth/token')
    .constant('SSIN_SERVER_URL', '@@ENVIRONMENT_URL' + '/patient/')
    .constant('STABLE_DATA_URL', '@@ENVIRONMENT_URL' + '/stabledata/')
    .constant('ATTACHMENT_URL', '@@ENVIRONMENT_URL' + '/attachments/')
    .constant('WORKFLOW_ENGINE_BASE_URL', '@@ENVIRONMENT_URL')
    .constant('APPLICATION_ID', 'healthdata-client')
    .constant('PLATFORM', '@@PLATFORM')
    .constant('AUTHENTICATION', '@@AUTHENTICATION')
    .constant('SINGLEFORM', isPresent('@@SINGLEFORM', false))
    .constant('SINGLEFORM_SERVER_URL', '@@SINGLEFORM_SERVER_URL')
    .constant('ENVIRONMENT_URL', '@@ENVIRONMENT_URL')
    .constant('CACHE_URL', '@@ENVIRONMENT_URL' + '/cache')
    .constant('CACHE_REFRESH_URL', '@@ENVIRONMENT_URL' + '/cache/evict')
    .constant('REQUEST_ACC_URL', isPresent('@@REQUEST_ACC_URL', false));
})();
