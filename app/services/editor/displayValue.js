/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.editor')
    .factory('documentDisplayValues', documentDisplayValuesFactory)
    .factory('displayValue', displayValueFactory);

  function documentDisplayValuesFactory(_, displayValue) {
    'ngInject';

    return documentDisplayValues;

    function documentDisplayValues(document) {
      // documentPartsDisplayValues() always takes an array, so to start the
      // whole process, we wrap the document in a singleton array.
      // To stay consistent with the earlier version of this function, we return
      // only the 'nested' property; the display value of the entire document is
      // ignored.
      return fieldDisplayValues([ document ]).nested;
    }

    /**
     * To support the list content type in a smart way, this function takes
     * as input an _array_ of fields, to produce a _single_ result. Each
     * time we encounter list fields, the elements are flattened and the array
     * grows (unless the lists are empty).
     */
    function fieldDisplayValues(fields) {

      // Compute the field display value using _all_ the field data collected
      // up to this point.
      var value = fieldDisplayValue(fields);

      // For each field, we get its nested property objects (if any) and flatten
      // them. In the case of list fields, this will result in a flattened array
      // of all the list elements.
      var nestedObjects = _.flatten(_.map(fields, nestedDocumentObjects), true /* shallow */);
      // Collect all the property names occuring in the nested objects.
      var allKeys = _.uniq(_.flatten(_.map(nestedObjects, _.keys), true /* shallow */));
      // Create an object with all the above property names, whose values are
      // the result of recursively invoking fieldDisplayValues().
      var nested = _.object(_.map(allKeys, function (key) {
        return [ key, fieldDisplayValues(_.pluck(nestedObjects, key)) ];
      }));

      return {
        value: value,
        nested: nested
      };
    }

    /**
     * Helper function to obtain the nested objects in a field. For simple
     * content types such as number and text, there are no nested objects. For
     * patientID and choice, there is a single object containing the nested
     * fields. For list, there is an array of nested objects.
     */
    function nestedDocumentObjects(input) {
      var typesWithNested = [ 'object', 'patientID', 'choice', 'multichoice' ];
      if (input.nested || _.contains(typesWithNested, input.contentType)) {
        return [ input.nested ];
      }
      if (input.contentType === 'list') {
        return _.pluck(input.elements, 'nested');
      }
      return [];
    }

    /**
     * Create a single display value for an array of document parts.
     */
    function fieldDisplayValue(field) {
      if (_.all(field, isSimpleContentType)) {
        return _.map(field,
          _.partial(displayValue, _, true)).join('\n');
      }
      return _.map(field,
        _.partial(displayValue, _, false)).join('\n\n');
    }

    function isSimpleContentType(field) {
      return field.contentType !== 'list';
    }

  }

  function displayValueFactory(gettextCatalog, SkryvLanguage, $filter, _) {
    'ngInject';

    return displayValue;

    function byCurrentLanguage(obj, alternative) {
      if (typeof alternative !== 'string') {
        alternative = '';
      }
      var currentLanguage = SkryvLanguage.getLanguage() || 'en';
      if (!obj) { return alternative; }
      return obj[currentLanguage] || alternative;
    }

    function displayValue(data, singleLine) {
      if (data) {
        var type = data.contentType;
        if (type === 'list') {
          if (singleLine) {
            if (data.elements.length > 1) {
              return data.elements.length + ' items';
            } else if (data.elements.length === 1) {
              return '1 item';
            } else {
              return '0 items';
            }
          }
          return _.map(data.elements, function (element) {
            return displayValue(element);
          }).join('\n\n');
        }
        if (type === 'referencelist') {
          return byCurrentLanguage(data.labels, data.value);
        }
        if (type === 'object') {
          if (singleLine) { return ''; }
          var lines = [];
          _.each(data.nested, function (propertyManipulator) {
            var propResult = displayValue(propertyManipulator, true);
            // we skip empty properties
            // this may included nested lists and objects
            if (propResult) {
              lines.push(byCurrentLanguage(propertyManipulator.fieldLabel, '<missing>') + ': ' + propResult);
            }
          });
          return lines.join('\n');
        }
        if (type === 'patientID') {
          return data.value;
        }
        if (type === 'text') {
          return data.value;
        }
        if (type === 'number') {
          return data.value;
        }

        if (type === 'multichoice') {
          var selected = _.map(data.selectedOptionsLabels, byCurrentLanguage);
          return selected.join(singleLine ? ', ' : '\n');
        }
        if (type === 'boolean') {
          if (data.value === true) {
            return gettextCatalog.getString('Yes');
          }
          if (data.value === false) {
            return gettextCatalog.getString('No');
          }
          return '';
        }
        if (type === 'date') {
          var date = $filter('date')(data.value, 'dd-MM-yyyy');
          return date;
        }
        if (type === 'choice') {
          return byCurrentLanguage(data.selectedOptionLabel);
        }
        if (type === 'question') {
          return byCurrentLanguage(data.answerLabel);
        }
        if (!type) {
          if (Object.keys(data).length === 0) { return ''; }
          if (data.value) {
            return data.value;
          }
        }
      }
    }
  }

})();
