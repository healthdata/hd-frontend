/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.editor').factory('DocumentSideMenu', function (_, gettextCatalog, $log) {
    'ngInject';
    var menuItems = [];

    function addMenuItem(item) {
      menuItems.push(item);
    }

    function getMenu(items) {
      var menu = [];
      // Search item
      _.each(items, function (item) {
        var search = _.findWhere(menuItems, { name: item });
        if (search) {
          menu.push(search);
        } else {
          $log.log('Menu item missing for: ' + item);
        }
      });

      return menu;
    }

    //  Items
    addMenuItem({
      name: 'toc',
      label: gettextCatalog.getString('Table of contents'),
      icon: 'fa-list',
      guide: gettextCatalog.getString('Use the table of contents to quickly navigate through the registration.'),
      class: 'doc-content-with-summary',
      elements: [ 'summary', 'toc' ]
    });
    addMenuItem({
      name: 'info',
      label: gettextCatalog.getString('Info'),
      guide: gettextCatalog.getString('Use this tab to see history of all updates on the registration.'),
      icon: 'fa-info-circle',
      elements: [ 'info' ]
    });
    addMenuItem({
      name: 'validation',
      label: gettextCatalog.getString('Validation'),
      icon: 'fa-exclamation-triangle',
      class: 'progress-section',
      guide: gettextCatalog.getString('Use this tab to get a quick overview of the registration progress and the remaining validation errors and warnings.'),
      counter: [ 'documentModel.document.errors.length' ],
      elements: [ 'validation' ]
    });
    addMenuItem({
      name: 'comments',
      label: gettextCatalog.getString('Comments'),
      icon: 'fa-comment',
      guide: gettextCatalog.getString('Use this tab to see the comments that have been added to the registration.'),
      counter: [ 'documentModel.document.comments.length', 'workflow.notes.length' ],
      elements: [ 'notes' ]
    });

    return {
      menuItems: getMenu
    };
  });
})();
