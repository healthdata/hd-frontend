/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.editor')
    .factory('openDocument', openDocumentFactory);

  function openDocumentFactory($http, SkryvSession, healthdataDocumentmodel, _, documentEquals, SSIN_SERVER_URL, STABLE_DATA_URL) {
    'ngInject';

    function loadStableData(dataCollectionName, id, cb) {
      var url = STABLE_DATA_URL + encodeURIComponent(id) + '/' + dataCollectionName;
      $http.get(url).then(
        function (response) {
          cb(null, response.data);
        },
        function () {
          cb({ /* error */ });
        });
    }

    function enrichPatientID(id, cb) {
      var url = SSIN_SERVER_URL + id;
      $http.get(url).then(
        function (response) {
          cb(null, response.data);
        },
        function () {
          cb({ /* error */ });
        });
    }

    return function openDocument(documentDefinition, documentData, context, options) {

      var dataCollectionName = documentDefinition.dataCollectionName;
      var description = documentDefinition.description;

      context = context || {
        installationId: SkryvSession.getInstallationId()
      };

      var defaultOptions = {
        loadStableData: _.partial(loadStableData, dataCollectionName),
        enrichPatientID: enrichPatientID
      };

      // Interpret the document contents by means of the data description.
      // This yields a 'document' object, with a root 'manipulator'.
      // These objects are used by the editor to interact with the document contents.
      var document = healthdataDocumentmodel.documentmodelHelpers.openDocument(
        description,
        _.extend({ context: context }, documentData),
        _.extend(defaultOptions, options));

      var rootManipulator = document.root;

      // FIXME Stijn: find a solution for this
      // Give each manipulator a little push, to make sure computed fields are properly initialized
      _.each(document.registeredManipulators, function (m) {
        m.isActive();
        m.invalidate();
      });

      return {
        document: document,
        component: documentDefinition.component,
        manipulator: rootManipulator,
        dataForSave: dataForSave,
        isValid: isValid,
        makeChangeTracker: makeChangeTracker
      };

      /**
       * Return all the document data that should be sent to the backend when
       * saving.
       */
      function dataForSave() {
        var computedExpressions = rootManipulator.computedExpressions;
        var uniqueID = computedExpressions.uniqueID;
        if (!_.isString(uniqueID) || uniqueID.length === 0) {
          uniqueID = undefined;
        }
        var progress = healthdataDocumentmodel.documentmodelHelpers.progressInfo(document);
        var toBeCoded = document.toBeCoded();

        // documentData contains the actual raw data. When the document has been
        // opened with pseudonymised data, we only update the content. When
        // comments have been separated from document data, this case will be
        // obsolete, and we can simply return all document data all the time.
        var documentData = document.options.isPseudonymised ?
          { content: document.documentData.content } :
          document.documentData;

        var affectedFields = healthdataDocumentmodel.documentmodelHelpers
          .affectedFields(document);

        return {
          // documentContent is kept for now, but is obsolete, and will be removed
          // after the required changes in the backend are done
          documentContent: document.documentData.content,
          // metaData is kept for now, but is also obsolete
          metaData: document.documentData.patientID,

          documentData: documentData,
          toBeCoded: toBeCoded,
          progress: progress,
          computedExpressions: computedExpressions,
          uniqueID: uniqueID,
          affectedFields: affectedFields
        };
      }

      function isValid() {
        return document.errors && document.errors.length === 0;
      }

      function makeChangeTracker() {

        var savedCopy;

        // make sure the current document data is stored for future comparison
        reset();

        return {
          reset: reset,
          hasChanged: hasChanged
        };

        /*
         * Reset the change tracker, such that the current version is used as
         * the basis for future comparisons.
         */
        function reset() {
          savedCopy = angular.copy(document.documentData);
        }

        /*
         * Check whether the current document data is different from the last
         * time the change tracker was reset (or created).
         */
        function hasChanged() {
          return !documentEquals(savedCopy, document.documentData);
        }
      }
    };
  }

})();
