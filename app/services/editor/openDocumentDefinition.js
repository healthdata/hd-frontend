/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.editor')
    .factory('openDocumentDefinition', openDocumentDefinitionFactory);

  function openDocumentDefinitionFactory(healthdataDocumentmodel) {
    'ngInject';
    return function (documentDefinition) {
      // only accept objects
      if (documentDefinition !== Object(documentDefinition)) {
        throw new Error('openDocumentDefinition: expected a JSON document definition, got ' + documentDefinition);
      }

      // Compile the document definition with all known directives.
      // As a result, the environment env is populated with
      // - skrDescription: the data description
      // - skrEditorComponent: the view description
      var env = {};
      healthdataDocumentmodel.documentmodelHelpers.compile(documentDefinition, env);
      var description = env.skrDescription;
      var component = env.skrEditorComponent;
      return {
        dataCollectionName: documentDefinition.label,
        description: description,
        component: component
      };
    };
  }

})();
