/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.editor')
    .factory('normalizeDate', normalizeDateFactory)
    .factory('documentEquals', documentEqualsFactory);

  function documentEqualsFactory(normalizeDate) {
    'ngInject';

    return documentEquals;

    function documentEquals(leftDocumentData, rightDocumentData) {

      // make copies of everything before we start modifying stuff
      var leftMD = angular.copy(leftDocumentData.patientID);
      var rightMD = angular.copy(rightDocumentData.patientID);

      var leftDoc = angular.copy(leftDocumentData.content);
      var rightDoc = angular.copy(rightDocumentData.content);

      var leftPrivate = angular.copy(leftDocumentData.private);
      var rightPrivate = angular.copy(rightDocumentData.private);

      // In earlier versions, the meta data property UPDATED_ON was used to
      // check for concurrent modification of documents. This is annoying here,
      // because we do not want to use that property to compare document data,
      // but the meta data also contains patient ID info that should be
      // compared. To do the comparison, we first remove the UPDATED_ON
      // property. See WDC-1003
      if (leftMD) { delete leftMD.UPDATED_ON; }
      if (rightMD) { delete rightMD.UPDATED_ON; }

      // Sometimes the timezones of dates in the meta data get messed up, so we fix them here.
      function normalizeMetaDataDate(propertyName, metaData) {
        try {
          if (metaData[propertyName]) {
            metaData[propertyName] = JSON.stringify(normalizeDate(JSON.parse(metaData[propertyName])));
          }
        } catch (err) {
          // we ignore any errors, becuase if this fails it simply means the date
          // values are actually different
        }
      }
      if (leftMD) { normalizeMetaDataDate('date_of_birth', leftMD); }
      if (rightMD) { normalizeMetaDataDate('date_of_birth', rightMD); }

      // PatientID sec value is not always set correctly
      // Add patientID sex if the value has been set in the document
      // See WDC-1831 && WDC-1787
      if (leftMD) { addPatientSex(leftMD, leftDoc); }
      if (rightMD) { addPatientSex(rightMD, rightDoc); }

      // always make sure there is a PATIENT_IDENTIFIER
      // The backend does not seem to return the property when it is the empty string.
      if (leftMD) { addEmptyPatientID(leftMD); }
      if (rightMD) { addEmptyPatientID(rightMD); }

      // in BNMDR the questionsOrder property is sometimes initialized by the frontend
      // we simply ignore this property because it is not actually part of the data
      delete leftDoc.questionsOrder;
      delete rightDoc.questionsOrder;

      var metaDataEquals = angular.equals(leftMD, rightMD);
      var documentContentEquals = angular.equals(leftDoc, rightDoc);
      var privateEquals = angular.equals(leftPrivate, rightPrivate);

      return metaDataEquals && documentContentEquals && privateEquals;
    }
  }

  function normalizeDateFactory(moment) {
    'ngInject';
    return normalizeDate;
    function normalizeDate(dateString) {
      if (typeof dateString === 'string' && dateString.length) {
        return moment(dateString, moment.ISO_8601).format();
      }
      return dateString;
    }
  }

  function addPatientSex(metaData, document) {
    if (!metaData.sex) {
      if (document.value.patient_id && document.value.patient_id.sex) {
        metaData.sex = JSON.stringify(document.value.patient_id.sex);
      }
    }
  }

  function addEmptyPatientID(metaData) {
    if (!metaData.PATIENT_IDENTIFIER) {
      metaData.PATIENT_IDENTIFIER = '';
    }
  }

})();
