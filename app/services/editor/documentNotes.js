/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.editor').factory('DocumentNotes',
    function ($rootScope, _, PLATFORM, SkryvSession, moment, CurrentDocument, Statuses, WorkflowNotes, $location) {
      'ngInject';

      var notes;
      var user = SkryvSession.getLoggedUser();
      var newFieldNotes;
      var apiError = false;

      // Reset and set the notes
      function resetInternalState(notes) {
        newFieldNotes = [];
        // Show in interface that an error occurred
        if (notes === 'error') {
          apiError = true;
          resetNotes([]);
        } else {
          apiError = false;
          resetNotes(notes);
        }
        emitChange();
      }

      function resetNotes(notesArray) {
        notes = notesArray;
      }

      // Add general note to workflow
      function addNote(title, content) {
        var note = {
          title: title,
          comments: [ { content: content } ]
        };

        if (!CurrentDocument.currentWorkflow() || !CurrentDocument.currentWorkflow().id) {
          return CurrentDocument.saveItAnyway().then(function (workflow) {
            return WorkflowNotes.save(workflow.id, note).then(function () {
              $location.path('/document/' + workflow.id);
            });
          });
        } else {
          return WorkflowNotes.save(CurrentDocument.currentWorkflow().id, note).then(function (newNote) {
            notes.push(newNote);
            emitChange();
          });
        }
      }

      // Add field note to workflow
      function addFieldNote(fieldNote) {
        var noteCopy = angular.copy(fieldNote);
        noteCopy.comments = [ { content: fieldNote.content } ];
        delete noteCopy.content;

        if (!CurrentDocument.currentWorkflow() || !CurrentDocument.currentWorkflow().id) {
          return CurrentDocument.saveItAnyway().then(function (workflow) {
            return WorkflowNotes.save(workflow.id, noteCopy).then(function (newNote) {
              clearFieldNote(fieldNote);
              notes.push(newNote);
              emitChange();
            });
          });
        } else {
          return WorkflowNotes.save(CurrentDocument.currentWorkflow().id, noteCopy).then(function (newNote) {
            clearFieldNote(fieldNote);
            notes.push(newNote);
            emitChange();
          });
        }
      }

      // Add comment to the note
      function addComment(content, note) {
        var noteCopy = angular.copy(note);
        noteCopy.comments.push({
          content: content
        });
        return WorkflowNotes.update(noteCopy).then(function (updatedNote) {
          notes[notes.indexOf(note)] = updatedNote;
          emitChange();
        });
      }

      // Edit comment of the note
      function editComment(content, comment, note) {
        var noteCopy = angular.copy(note);
        noteCopy.comments[note.comments.indexOf(comment)].content = content;
        return WorkflowNotes.update(noteCopy).then(function (updatedNote) {
          notes[notes.indexOf(note)] = updatedNote;
          emitChange();
        });
      }

      // Toggle solved boolean
      function toggleSolved(note) {
        var noteCopy = angular.copy(note);
        noteCopy.resolved = !noteCopy.resolved;
        return WorkflowNotes.update(noteCopy).then(function (updatedNote) {
          notes[notes.indexOf(note)] = updatedNote;
          emitChange();
        });
      }

      // Delete note
      function deleteComment(comment, note) {
        if (note.comments.length === 1) {
          return WorkflowNotes.delete(note).then(function () {
            notes.splice(notes.indexOf(note), 1);
            emitChange();
          });
        } else {
          var noteCopy = angular.copy(note);
          noteCopy.comments.splice(note.comments.indexOf(comment), 1);
          return WorkflowNotes.update(noteCopy).then(function (updatedNote) {
            notes[notes.indexOf(note)] = updatedNote;
            emitChange();
          });
        }
      }

      // Preparation for adding a new note
      function setNewFieldNote(manipulator) {
        var search = _.find(newFieldNotes, function (note) {
          return note.title === manipulator.description.label;
        });
        // Avoid duplicate field comments
        if (!search) {
          newFieldNotes.push({
            title: manipulator.description.label,
            fieldPath: manipulator.path.join('.')
        });
        }
        emitChange();
      }

      function clearFieldNote(fieldNote) {
        newFieldNotes.splice(newFieldNotes.indexOf(fieldNote), 1);
        emitChange();
      }

      // If Actions available
      function isEditable(note, parent, validDate) {
        var workflow = CurrentDocument.currentWorkflow();
        // TODO: Refactor validDate
        // If workflow status allows comments to be edited
        // If user is the original poster
        // Data collection date hasn't been overdue
        return (workflow && Statuses.hasProperty(workflow.status, 'commentable') && !parent.resolved &&
        validDate && PLATFORM.toLowerCase() === note.platform.toLowerCase() && user.username === note.username);
      }

      function linkedManipulator(fieldPath) {
        if (fieldPath) {
          return CurrentDocument.currentDocumentModel().document.registeredManipulators[ '%.' + fieldPath.replace('|', '.') ];
        }
      }

      // Read-only functions made available to change listeners
      var getters = {
        notes: function () { return notes; },
        newFieldNotes: function () { return newFieldNotes; },
        apiError: function () { return apiError; }
      };

      var emitter = $rootScope.$new();

      function emitChange() {
        emitter.$emit('change');
      }

      function listen(callback) {
        // immediately invoke the callback, to give it the current state
        callback(getters);
        // register the callback to notify it of any updates
        // the return value is an unregister function
        return emitter.$on('change', function () {
          callback(getters);
        });
      }

      return _.extend({
        listen: listen,
        // Notes
        addNote: addNote,
        addFieldNote: addFieldNote,
        toggleSolved: toggleSolved,
        setNewFieldNote: setNewFieldNote,
        clearFieldNote: clearFieldNote,
        // Comments
        editComment: editComment,
        addComment: addComment,
        deleteComment: deleteComment,
        // General
        isEditable: isEditable,
        linkedManipulator: linkedManipulator,
        resetInternalState: resetInternalState
      }, getters);
    });
})();
