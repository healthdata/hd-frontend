/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.editor')
    .filter('actionAllowForDefinition', actionAllowForDefinition)
    .factory('DocumentMenuActions', DocumentMenuActions)
    .factory('DocumentMenu', DocumentMenuFactory);

  function DocumentMenuActions($rootScope, $route, $location, gettextCatalog, WorkflowActions,
                               CurrentDocument) {

    'ngInject';

    function performWorkflowAction(params) {
      function theAction() {
        return CurrentDocument.performWorkflowAction(params.action);
      }

      return WorkflowActions.applyAction(theAction, params.notifications)
        .then(function () {
          if (params.returnHome) {
            $location.path('/data collection');
          } else if (!params.skipReload) {
            $route.reload();
          }
        });
    }

    function saveItAnyway(notifications) {
      var workflow = CurrentDocument.currentWorkflow();
      if (!notifications && workflow.status === 'ANNOTATIONS_NEEDED') {
        notifications = {
          infoMessage: gettextCatalog.getString('Saving, please wait ...'),
          successMessage: gettextCatalog.getString('Comments saved'),
          errorMessage: gettextCatalog.getString('Could not save registration')
        };
      }

      notifications = notifications || {
          infoMessage: gettextCatalog.getString('Saving the registration content, please wait ...'),
          successMessage: gettextCatalog.getString('Registration saved'),
          errorMessage: gettextCatalog.getString('Could not save the registration')
        };

      function theAction() {
        return CurrentDocument.saveItAnyway();
      }

      return WorkflowActions.applyAction(theAction, notifications);
    }

    return {
      performWorkflowAction: performWorkflowAction,
      saveItAnyway: saveItAnyway
    };
  }

  function DocumentMenuFactory(CurrentDocument, gettextCatalog, ngDialog, DocumentMenuActions, $location, _, $filter, PLATFORM) {
    'ngInject';

    var actionMenuItems = [];

    function actionMenuItem(item) {
      actionMenuItems.push(item);
    }

    // ===========================//
    // ==== Workflow actions ==== //
    // ===========================//

    function performDELETEAction() {
      ngDialog.open({
        template: '<p>' + gettextCatalog.getString('Are you sure you want to delete this registration?') +
        '<div class="row">' +
        '<button class="btn btn-secundary" ng-click="doDeleteAction()" translate>Yes</button>' +
        '<button class="btn btn-primary" ng-click="closeThisDialog()"><a translate>No</a></button>' +
        '</div>',
        plain: true,
        showClose: false,
        controller: [ '$scope', function ($scope) {
          $scope.doDeleteAction = function () {
            ngDialog.close();
            DocumentMenuActions.performWorkflowAction({
              notifications: {
                infoMessage: gettextCatalog.getString('Deleting registration, please wait ...'),
                successMessage: gettextCatalog.getString('Registration deleted'),
                errorMessage: gettextCatalog.getString('Could not delete registration')
              },
              returnHome: true,
              action: 'DELETE'
            });
          };
        } ]
      });
    }

    function performSUBMITAction() {
      ngDialog.open({
        template: '<p>' + gettextCatalog.getString('Are you sure you want to submit this registration?') +
        '<div class="row">' +
        '<button class="btn btn-secundary" ng-click="doDeleteAction()" translate>Yes</button>' +
        '<button class="btn btn-primary" ng-click="closeThisDialog()"><a translate>No</a></button>' +
        '</div>',
        plain: true,
        showClose: false,
        controller: [ '$scope', function ($scope) {
          $scope.doDeleteAction = function () {
            ngDialog.close();
            DocumentMenuActions.performWorkflowAction({
              notifications: {
                infoMessage: gettextCatalog.getString('Submitting for review, please wait ...'),
                successMessage: gettextCatalog.getString('Submitted for review'),
                errorMessage: gettextCatalog.getString('Could not be submitted for review')
              },
              returnHome: true,
              action: 'SUBMIT'
            });
          };
        } ]
      });
    }

    function performREOPENAction() {
      ngDialog.open({
        template: '<p>' + gettextCatalog.getString('Are you sure you want to reopen this submitted registration?') +
        '<div class="row">' +
        '<button class="btn btn-secundary" ng-click="doRequestAction()" translate>Yes</button>' +
        '<button class="btn btn-primary" ng-click="closeThisDialog()"><a translate>No</a></button>' +
        '</div>',
        plain: true,
        showClose: false,
        controller: [ '$scope', function ($scope) {
          $scope.doRequestAction = function () {
            ngDialog.close();
            DocumentMenuActions.performWorkflowAction({
              notifications: {
                infoMessage: gettextCatalog.getString('Reopening, please wait ...'),
                successMessage: gettextCatalog.getString('Registration reopened'),
                errorMessage: gettextCatalog.getString('Could not reopen registration')
              },
              returnHome: false,
              action: 'REOPEN'
            });
          };
        } ]
      });
    }

    function performREQUESTCORRECTIONSAction() {
      ngDialog.open({
        template: '<p>' + gettextCatalog.getString('Are you sure you want to request corrections?') +
        '<div class="row">' +
        '<button class="btn btn-secundary" ng-click="doRequestAction()" translate>Yes</button>' +
        '<button class="btn btn-primary" ng-click="closeThisDialog()"><a translate>No</a></button>' +
        '</div>',
        plain: true,
        showClose: false,
        controller: [ '$scope', function ($scope) {
          $scope.doRequestAction = function () {
            ngDialog.close();
            DocumentMenuActions.performWorkflowAction({
              notifications: {
                infoMessage: gettextCatalog.getString('Requesting corrections, please wait ...'),
                successMessage: gettextCatalog.getString('Corrections requested'),
                errorMessage: gettextCatalog.getString('Could not request corrections')
              },
              returnHome: true,
              action: 'SUBMIT_ANNOTATIONS'
            });
          };
        } ]
      });
    }

    // ===========================//
    // ==== New workflow action buttons ==== //
    // ===========================//

    actionMenuItem({
      name: 'Save',
      newWorkflow: true,
      'date': 'comments',
      onlyWhen: [ 'IN_PROGRESS' ],
      click: function () {
        DocumentMenuActions.saveItAnyway().then(function (document) {
          $location.path('/document/' + document.id);
        });
      }
    });
    actionMenuItem({
      'name': 'Submit',
      'onlyWhen': [ 'IN_PROGRESS' ],
      'date': 'submission',
      newWorkflow: true,
      'class': workflowSubmissionClass,
      'disabledHelptext': getDisabledHelptext,
      'click': function () {
        DocumentMenuActions.saveItAnyway()
          .then(function (workflow) {
            performSUBMITAction();
            $location.path('/document/' + workflow.id);
          });
      }
    });

    // ===========================//
    // ==== Workflow action buttons ==== //
    // ===========================//

    actionMenuItem({
      'name': 'Delete',
      'date': 'comments',
      'onlyWhen': [ 'IN_PROGRESS', 'ACTION_NEEDED' ],
      'click': performDELETEAction,
      'class': 'btn-new-second'
    });

    actionMenuItem({
      'name': 'Print',
      'date': 'always',
      'icon': 'fa-print',
      'click': function () {
        $location.path('/pdf/' + CurrentDocument.currentWorkflow().id);
      }
    });

    actionMenuItem({
      name: 'Save',
      date: 'comments',
      onlyWhen: [ 'IN_PROGRESS', 'ACTION_NEEDED' ],
      click: function () {
        DocumentMenuActions.saveItAnyway();
      }
    });
    actionMenuItem({
      'name': 'Submit',
      'onlyWhen': [ 'IN_PROGRESS' ],
      'date': 'submission',
      'class': workflowSubmissionClass,
      'disabledHelptext': getDisabledHelptext,
      'click': function () {
        DocumentMenuActions.saveItAnyway()
          .then(performSUBMITAction);
      }
    });
    actionMenuItem({
      'name': 'Reopen',
      'onlyWhen': [ 'SUBMITTED' ],
      'date': 'comments',
      'click': performREOPENAction
    });
    actionMenuItem({
      'name': 'Re-submit',
      'onlyWhen': [ 'ACTION_NEEDED' ],
      'date': 'comments',
      'class': workflowValidCommentClass,
      'disabledHelptext': getDisabledHelptext,
      'click': function () {
        DocumentMenuActions.saveItAnyway()
          .then(performSUBMITAction);
      }
    });
    actionMenuItem({
      'name': 'Re-submit',
      'onlyWhen': [ 'SUBMISSION_FAILED' ],
      'date': 'comments',
      'class': workflowSubmissionClass,
      'click': performSUBMITAction
    });
    actionMenuItem({
      'name': 'Request corrections',
      'date': 'comments',
      'onlyWhen': [ 'ANNOTATIONS_NEEDED' ],
      'click': performREQUESTCORRECTIONSAction
    });

    if (PLATFORM === 'hd4res') {
      actionMenuItem({
        'name': 'Re-submit to STG',
        'click': function () {
          DocumentMenuActions.performWorkflowAction({
            notifications: {
              infoMessage: gettextCatalog.getString('Resubmitting registration to STG, please wait ...'),
              successMessage: gettextCatalog.getString('Registration resubmitted to STG'),
              errorMessage: gettextCatalog.getString('Could not be resubmitted to STG')
            },
            returnHome: true,
            action: 'RESUBMIT'
          });
        }
      });
    }

    function checkValidSubmissionDate() {
      var definition = CurrentDocument.currentDefinition().dataCollectionGroup;

      if (!definition.endDateSubmission) {
        return true;
      }

      var today = new Date();
      var endDateSubmission = new Date(definition.endDateSubmission);

      if (endDateSubmission < today) {
        return false;
      }

      return true;
    }

    function checkValidCommentDate() {
      var definition = CurrentDocument.currentDefinition().dataCollectionGroup;

      if (!definition.endDateComments) {
        return true;
      }

      var today = new Date();
      var endDateComments = new Date(definition.endDateComments);

      if (endDateComments < today) {
        return false;
      }

      return true;
    }

    // Decide which helptexts to display
    function getDisabledHelptext() {
      if (!CurrentDocument.isValidDocument()) {
        return 'It is not possible to submit registrations when there are validation errors.\n' +
          'The validation errors are shown in the validation tab on the right hand side.';
      }

      if (!checkValidSubmissionDate()) {
        return 'It is not possible to submit registrations after the deadline.';
      }
    }

    function workflowValidCommentClass() {
      return {
        disabled: (!CurrentDocument.isValidDocument() || !checkValidCommentDate())
      };
    }

    function workflowSubmissionClass() {
      return {
        disabled: (!CurrentDocument.isValidDocument() || !checkValidSubmissionDate())
      };
    }

    return {
      makeMenu: function () {
        var workflow = CurrentDocument.currentWorkflow();

        // Workflow needs to be present
        if (!workflow) {
          return [];
        }

        var definition = CurrentDocument.currentDefinition().dataCollectionGroup;
        return _.filter(actionMenuItems, function (action) {
          // check the requirements
          // in case of new (non-existing) workflow
          // in case of status
          if ((action.newWorkflow === workflow.newWorkflow) && (!action.onlyWhen || action.onlyWhen.indexOf(workflow.status) > -1)) {
            // Data collection definition dates validity check
            // Actions that are always shown
            if (action.date === 'always') {
              return action;
            }
            // If followup is submitted once, you can't delete the workflow
            if (action.name === 'Delete' && workflow && !workflow.deletable) {
              return;
            }
            // When the workflow has already be submitted once, it goes trough the corrections flow
            if (workflow && workflow.submittedOnce) {
              if (action.date === 'submission') {
                action.date = 'comments';
              }
            }
            // Definition date allows action
            if (definition && $filter('actionAllowForDefinition')(definition, action)) {
              return action;
            }
          }
        });
      }
    };

  }

})();

function actionAllowForDefinition($filter) {
  'ngInject';
  'use strict';
  return function (dataDefinition, item) {
    var currentDate = new Date();
    currentDate = $filter('date')(currentDate, 'yyyy-MM-dd HH:mm:ss.sss');

    var copareDateStart;

    if (dataDefinition.startDate) {
      copareDateStart = $filter('filterDate')(dataDefinition.startDate, 'YYYY-MM-DD HH:mm:ss.sss');
    }

    if (copareDateStart < currentDate && dataDefinition.startDate !== null) {
      if (item.date) {

        var copareDateCreation;
        var copareDateSubmission;
        var copareDateComments;

        if (dataDefinition.endDateCreation) {
          copareDateCreation = $filter('filterDate')(dataDefinition.endDateCreation, 'YYYY-MM-DD HH:mm:ss.sss');
        }
        if (dataDefinition.endDateSubmission) {
          copareDateSubmission = $filter('filterDate')(dataDefinition.endDateSubmission, 'YYYY-MM-DD HH:mm:ss.sss');
        }
        if (dataDefinition.endDateComments) {
          copareDateComments = $filter('filterDate')(dataDefinition.endDateComments, 'YYYY-MM-DD HH:mm:ss.sss');
        }

        if (item.date === 'creation') {
          if (dataDefinition.endDateCreation) {
            return (copareDateCreation > currentDate);
          } else {
            return true;
          }
        }
        if (item.date === 'submission') {
          if (dataDefinition.endDateSubmission) {
            return (copareDateSubmission > currentDate);
          } else {
            return true;
          }
        }
        if (item.date === 'comments') {
          if (dataDefinition.endDateComments) {
            return (copareDateComments > currentDate);
          } else {
            return true;
          }
        }
      } else {
        return true;
      }
    } else {
      return false;
    }
  };
}
