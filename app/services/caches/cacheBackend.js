(function () {
  'use strict';

  angular.module('services.cache', [])
    .factory('cacheBackend', cacheBackend);

  function cacheBackend($http, CACHE_REFRESH_URL, CACHE_URL) {
    'ngInject';

    function getCaches() {
      return $http.get(CACHE_URL);
    }

    function getCatalogueCaches() {
      return $http.get(CACHE_URL + '?catalogue=true');
    }

    function clearCache(cacheName, isCatalogueCache) {
      var query = '?cacheName=' + cacheName;

      if (isCatalogueCache) {
        query += '?catalogue=true';
      }

      return $http.post(CACHE_REFRESH_URL + query);
    }

    return {
      getCaches: getCaches,
      getCatalogueCaches: getCatalogueCaches,
      clearCache: clearCache
    };
  }

})();
