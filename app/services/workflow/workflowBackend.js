/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.workflow')
    .constant('stripTrailingSlash', stripTrailingSlash)
    .factory('WorkflowRequest', WorkflowBackendFactory);

  // Remove a trailing slash, if any, from given URL.
  function stripTrailingSlash(str) {
    if (str.substr(-1) === '/') {
      return str.substr(0, str.length - 1);
    }
    return str;
  }

  function WorkflowBackendFactory($http, $q, _, csv, SEARCH_SERVER_URL, WORKFLOW_SERVER_URL, gettextCatalog, SINGLEFORM_SERVER_URL, stripTrailingSlash, $timeout, ngDialog) {
    'ngInject';

    return {
      get: function (id) {
        return $http.get(WORKFLOW_SERVER_URL + id)
          .then(
            function (response) {
              return response.data;
            },
            function () {
              return $q.reject(gettextCatalog.getString('Registration not found'));
            });
      },
      getSingleForm: function (id) {
        return $http.get(SINGLEFORM_SERVER_URL + id)
          .then(
            function (response) {
              return response.data;
            },
            function () {
              return $q.reject(gettextCatalog.getString('Registration not found'));
            });
      },
      save: function (body) {
        return $http.post(stripTrailingSlash(WORKFLOW_SERVER_URL), body)
          // delay a successful response to allow elasticsearch to be updated
          .then(function (response) {
            return $timeout(function () {
              return response;
            }, 1500);
          })
          .then(function (response) {
            return response.data;
          },
          function (response) {
            var message;
            if (response && response.data && response.data.error === 'INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION') {
              var info = response.data.error_description.split('open for ');
              message = 'Data collection is not open for ' + info[1];
            } else if (response && response.data && response.data.error === 'CREATE_DUPLICATE_KEY') {
              message = 'Item cannot be created since the key already exists in the database.';
            } else {
              message = 'Could not save the registration';
            }
            return $q.reject(gettextCatalog.getString(message));
          });
      },
      getByDatacollection: function (id, query, timeout) {
        return $http({
          method: 'POST',
          url: SEARCH_SERVER_URL + id,
          timeout: timeout,
          data: query
        }).then(
          function (response) {
            return response.data;
          },
          function () {
            return $q.reject(gettextCatalog.getString('Could not get registrations'));
          });
      },
      applyAction: function (action) {
        var url = WORKFLOW_SERVER_URL;
        if (action && action.action === 'SINGLEFORM_SAVE') {
          url = SINGLEFORM_SERVER_URL;
        }
        return $http.post(url + 'actions', action).then(
          function (response) {
            if (response.status === 202) {
              $timeout(function () {
                ngDialog.open({
                  template: 'views/dialog/action/notIndexed.html',
                  showClose: false
                });
              }, 2000);
            }
            return response.data;
          },
          function (response) {
            return $q.reject(response && response.data.error);
          });
      },
      downloadAll: function (dataDefinition, options) {
        options = options || {};
        var params = {};
        if (options.stable) {
          params.stable = 'true';
        }
        return $http.get(WORKFLOW_SERVER_URL + 'download/' + dataDefinition.id, { params: params })
          .then(
          function (response) {
            return response.data;
          },
          function () {
            return $q.reject(gettextCatalog.getString('Failed downloading registrations'));
          });
      },
      bulkDelete: function (array) {
        var deleteArray = [];
        _.each(array, function (workflow) {
          deleteArray.push(workflow.id);
        });

        return $http(
          {
            method: 'DELETE',
            url: WORKFLOW_SERVER_URL,
            data: deleteArray
          }).then(
          function (response) {
            return response.data;
          },
          function () {
            return $q.reject(gettextCatalog.getString('Failed deleting registrations'));
          }
        );
      },
      getDocument: function (id) {
        return $http.get(WORKFLOW_SERVER_URL + id + '/documents')
          .then(
            function (response) {
              return response.data;
            },
            function () {
              return $q.reject(gettextCatalog.getString('Registration not found'));
            });
      }
    };

  }

})();
