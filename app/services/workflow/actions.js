/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.workflow')
    .factory('WorkflowActions', WorkflowActionsFactory);

  function WorkflowActionsFactory($q, Notifications, ngDialog, $timeout) {
    'ngInject';

    function openDialog(message) {
      ngDialog.open({
        template: 'views/dialog/simpleMessage.html',
        showClose: false,
        controller: [ '$scope', 'PLATFORM', function ($scope, PLATFORM) {
          $scope.message = message;
          $scope.platform = PLATFORM;
        } ]
      });
    }

    return {
      applyAction: function (action, notifications) {
        if (notifications.infoMessage) {
          openDialog(notifications.infoMessage);
        }
        return action()
          .then(function (response) {
            return $timeout(function () {
              return response;
            }, 1500);
          })
          .then(function (response) {
            if (notifications.successMessage) {
              ngDialog.close();
              Notifications.success(notifications.successMessage);
            }
            return response;
          },
          function (error) {
            ngDialog.close();
            Notifications.error(notifications.errorMessage);
            if (error) {
              if (error === 'CREATE_DUPLICATE_KEY') {
                ngDialog.open({
                  template: 'views/dialog/action/uniqueID.html',
                  showClose: false,
                  controller: [ '$scope', 'SkryvDefinition', 'CurrentDocument', function ($scope, SkryvDefinition, CurrentDocument) {
                    SkryvDefinition.get(CurrentDocument.currentWorkflow().dataCollectionDefinitionId).then(function (dcd) {
                      $scope.explanation = dcd.content.uniqueIDExplanation;
                    }, function (err) {
                      Notifications.error(err);
                    });
                  } ]
                });
              }
              if (error === 'PARALLEL_MODIFICATION_EXCEPTION') {
                ngDialog.open({
                  template: 'views/dialog/action/parallelModification.html',
                  showClose: false
                });
              }
            }
            return $q.reject(notifications.errorMessage);
          });
      }
    };
  }
})();
