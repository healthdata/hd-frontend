/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.workflow')
    .factory('WorkflowNotes', WorkflowNotes);

  function WorkflowNotes($http, $q, gettextCatalog, SESSION_SERVER_URL) {
    'ngInject';

    var url = SESSION_SERVER_URL + '/notes?workflowType=registration&workflowId=';

    return {
      get: function (noteId) {
        return $http.get(SESSION_SERVER_URL + '/notes/' + noteId)
          .then(
            function (response) {
              return response.data;
            },
            function () {
              return $q.reject(gettextCatalog.getString('Comment not found'));
            });
      },
      getAll: function (workflowId) {
        return $http.get(url + workflowId)
          .then(
            function (response) {
              return response.data;
            },
            function () {
              return $q.reject(gettextCatalog.getString('Comments not found'));
            });
      },
      save: function (workflowId, note) {
        return $http.post(url + workflowId, note)
          .then(
            function (response) {
            return response.data;
          },
          function () {
            return $q.reject(gettextCatalog.getString('Failed saving comment'));
          });
      },
      update: function (note) {
        return $http.put(SESSION_SERVER_URL + '/notes/' + note.id, note)
          .then(
            function (response) {
              return response.data;
            },
            function () {
              return $q.reject(gettextCatalog.getString('Failed saving comment'));
            });
      },
      delete: function (note) {
        return $http.delete(SESSION_SERVER_URL + '/notes/' + note.id)
          .then(
            function (response) {
              return response.data;
            },
            function () {
              return $q.reject(gettextCatalog.getString('Failed deleting comment'));
            });
      }
    };
  }

})();
