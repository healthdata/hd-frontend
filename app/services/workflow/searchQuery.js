/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.workflow')
    .factory('makeSearchQuery', SearchQueryFactory);

  /*ngInject*/
  function SearchQueryFactory(_, PLATFORM, SkryvSession) {
    return makeSearchQuery;
    function makeSearchQuery(searchParams, page, pageSize) {

      var filters = [];
      var negativeFilters = [];
      var queries = [];

      if (searchParams.statuses && searchParams.statuses.length) {
        filters.push({
          terms: {
            status: _.pluck(searchParams.statuses, 'status')
          }
        });
        var flagsFilters = [];
        _.each(searchParams.statuses, function (status) {
          var statusName = status.status;
          if (status.options && status.options.flag) {
            var propertyName = 'flags.' + status.options.flag;
            var termFilter = { term: {} };
            termFilter.term[propertyName] = true;
            flagsFilters.push({ not: { term: { status: statusName } }});
            flagsFilters.push(termFilter);
          } else {
            if (statusName === 'NEW') {
              // For the HD4RES status, we need to take special care and require
              // that both flags for corrections and follow ups are false.
              flagsFilters.push({ not: { term: { status: statusName } }});
              flagsFilters.push({
                bool: {
                  must: [
                    { term: { 'flags.corrections': false } },
                    { term: { 'flags.followUp': false } }
                  ]
                }
              });
            }
          }
        });
        if (flagsFilters.length) {
          filters.push({ bool: { should: flagsFilters } });
        }
      }

      if (searchParams.followUps && searchParams.followUps.length) {
        angular.forEach(searchParams.followUps, function (followUp) {

          if (!followUp.active && !followUp.planned && !followUp.submitted) {
            // no filter to apply for this follow-up
            return;
          }

          // The desired result is obtained by combining three ES filters, one on the value of
          // the "active" property (which is true for both active and submitted follow-ups), one
          // on the presence of and activation date (which is the case for both active and
          // planned follow-ups), and on the the presence of a submission date.
          var activeFilter = { term: {} };
          activeFilter.term['context.followUps.active'] = true;

          var activationDateFilter = { exists: { field: 'context.followUps.activationDate' } };

          var submittedOnFilter = { exists: { field: 'context.followUps.submittedOn' } };

          var followUpFilterMust = [];
          var followUpFilterShould = [];
          var followUpFilterMustNot = [];

          if (followUp.active && followUp.planned) {
            // Whether active, planned, or submitted, a follow-up has an activation date.
            followUpFilterMust.push(activationDateFilter);
            // therefore we need to exclude submitted if not selected
            if (!followUp.submitted) {
              followUpFilterMustNot.push(submittedOnFilter);
            }
          } else if (followUp.active && followUp.submitted) {
            followUpFilterShould.push(activeFilter);
            followUpFilterShould.push(submittedOnFilter);
          } else if (followUp.planned && followUp.submitted) {
            followUpFilterMust.push(activationDateFilter);
            followUpFilterShould.push({ not: activeFilter });
            followUpFilterShould.push(submittedOnFilter);
          } else {
            if (followUp.submitted) {
              followUpFilterMust.push(submittedOnFilter);
            } else {
              followUpFilterMustNot.push(submittedOnFilter);
            }
            if (followUp.active) {
              followUpFilterMust.push(activeFilter);
            }
            if (followUp.planned) {
              followUpFilterMust.push(activationDateFilter);
              followUpFilterMustNot.push(activeFilter);
            }
          }

          // Follow ups are mapped as nested objects, so we need to first find the correct follow up
          // using a 'nested' filter and apply the filter on it.
          var byNameFilter = { term: {} };
          byNameFilter.term['context.followUps.name'] = followUp.name;
          followUpFilterMust.push(byNameFilter);
          filters.push({
            nested: {
              path: 'context.followUps',
              filter: {
                bool: {
                  must: followUpFilterMust,
                  must_not: followUpFilterMustNot,
                  should: followUpFilterShould
                }
              }
            }
          });
        });
      }

      if (PLATFORM === 'hd4dp') {
        negativeFilters.push({
          term: {
            status: 'DELETED'
          }
        });
      }

      if (searchParams.lastUpdatedAfter || searchParams.lastUpdatedBefore) {
        filters.push({
          range: {
            updatedOn: {
              gte: searchParams.lastUpdatedAfter,
              lte: searchParams.lastUpdatedBefore
            }
          }
        });
      }

      if (searchParams.createdAfter || searchParams.createdBefore) {
        filters.push({
          range: {
            createdOn: {
              gte: searchParams.createdAfter,
              lte: searchParams.createdBefore
            }
          }
        });
      }

      if (searchParams.onlyMine) {
        var user = SkryvSession.getLoggedUser();
        filters.push({
          nested: {
            path: 'history',
            filter: {
              term: {
                'history.executedBy': user.username
              }
            }
          }
        });
      }

      if (searchParams.patientID) {
        queries.push({
          'match_phrase_prefix': {
            'document.documentContent.patientID.patientID': searchParams.patientID
          }
        });
      }

      if (searchParams.internalPatientID) {
        queries.push({
          'match_phrase_prefix': {
            'document.documentContent.patientID.internalPatientID': searchParams.internalPatientID
          }
        });
      }

      if (searchParams.dateOfBirthBefore || searchParams.dateOfBirthAfter) {
        filters.push({
          range: {
            'document.documentContent.patientID.birthdate': {
              gte: searchParams.dateOfBirthAfter,
              lte: searchParams.dateOfBirthBefore
            }
          }
        });
      }

      if (searchParams.patientName) {
        queries.push({
          'match_phrase_prefix': {
            'document.documentContent.patientID.lastName.searchable': searchParams.patientName
          }
        });
      }

      if (searchParams.patientFirstName) {
        queries.push({
          'match_phrase_prefix': {
            'document.documentContent.patientID.firstName.searchable': searchParams.patientFirstName
          }
        });
      }

      if (searchParams.queryText) {
        queries.push({
          'match_phrase_prefix': {
            _all: searchParams.queryText
          }
        });
      }

      if (searchParams.dataProvider) {
        if (isNaN(searchParams.dataProvider)) {
          queries.push({
            'match_phrase_prefix': {
              'identificationName': searchParams.dataProvider
            }
          });
        } else {
          queries.push({
            'match_phrase_prefix': {
              'identificationValue': searchParams.dataProvider
            }
          });
        }
      }

      var hasFilters = filters.length !== 0;
      var hasNegativeFilters = negativeFilters.length !== 0;
      var hasQueries = queries.length !== 0;
      var multipleFilters = filters.length > 1;
      var multipleQueries = queries.length > 1;

      var filter;
      var query;

      if (hasFilters || hasNegativeFilters) {
        if (multipleFilters || hasNegativeFilters) {
          filter = { bool: { must: filters, 'must_not': negativeFilters }};
        } else {
          filter = filters[0];
        }
      }

      if (hasQueries) {
        if (multipleQueries) {
          query = { bool: { must: queries }};
        } else {
          query = queries[0];
        }
      }

      // combine query and filter
      if (filter) {
        var origQuery = query;
        query = {
          filtered: {
            filter: filter
          }
        };
        if (origQuery) {
          query.filtered.query = origQuery;
        }
      }

      var esQuery = {
        size: pageSize,
        from: (page - 1) * pageSize
      };

      if (query) {
        esQuery.query = query;
      }

      if (searchParams.sort) {
        var sort = searchParams.sort;

        var fieldName;

        var fieldNameMappings = {
          followUp: 'context.followUps.activationDate',
          warnings: 'document.documentContent.warnings',
          errors: 'document.documentContent.errors',
          progress: 'document.documentContent.errors',
          comments: 'document.documentContent.comments'
        };

        if (typeof sort.sortColumn === 'string') {
          fieldName = fieldNameMappings[sort.sortColumn] || sort.sortColumn;
        } else if (sort.sortColumn && sort.sortColumn.name) {
          fieldName = 'document.documentContent.display.nested.';
          if (sort.sortColumn.parent && sort.sortColumn.parent !== 'undefined' /* see WDC-1900 */) {
            fieldName += sort.sortColumn.parent;
            fieldName += '.nested.';
          }
          fieldName += sort.sortColumn.name + '.value';
        }

        if (fieldName) {
          esQuery.sort = {};
          esQuery.sort[fieldName] = {
            unmapped_type: 'string', // avoid error when sorting on unknown types
            order: sort.reverse ? 'desc' : 'asc',
            missing: '_last'
          };
        }
      }

      return esQuery;
    }
  }

})();
