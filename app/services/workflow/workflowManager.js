/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.workflow')
    .factory('CurrentDocument', CurrentDocumentFactory);

  function CurrentDocumentFactory($rootScope, WorkflowRequest, openDocumentDefinition, openDocument, _, Statuses, $q, $timeout) {
    'ngInject';

    var documentModel, workflow, definition,
      changeTracker;

    function closeCurrentDocument() {
      documentModel = undefined;
      workflow = undefined;
      definition = undefined;
      changeTracker = undefined;
    }

    function deepCopy() {
      changeTracker.reset();
    }

    function loadDocument(dc, wf, dcd, readOnly, cb) {
      closeCurrentDocument();

      // Parse and create a model for the document definition
      var documentDefinition = openDocumentDefinition(dcd.content);

      documentModel = openDocument(
        documentDefinition,
        dc.documentData,
        wf.context,
        { readOnly: readOnly });

      changeTracker = documentModel.makeChangeTracker();
      workflow = wf;
      definition = dcd;
      // Give metaData to new workflows to equal original
      // FIXME is this still needed?
      if (!workflow.metaData) {
        workflow.metaData = documentModel.document.documentData.patientID;
      }
      deepCopy();
      cb();
    }

    function saveItAnyway() {
      if (Statuses.hasProperty(workflow.status, 'editable')) {
        return saveCurrentContent();
      } else if (Statuses.hasProperty(workflow.status, 'commentable')) {
        return saveCurrentContent('SAVE_ANNOTATIONS');
      }
    }

    var promise;
    var autoSaveObj = {
      saving: false,
      success: false,
      error: false
    };

    function autoSave() {
      // Return promise if not resolved yet
      if (promise) {
        return promise;
      }

      // Document needs to be changed to trigger an autosave
      if (hasChanged()) {
        autoSaveObj.saving = true;
        emitChange();
       promise = saveCurrentContent('SINGLEFORM_SAVE').then(function () {
         autoSaveObj.success = true;
          deepCopy();
          promise = null;
          return autoSave(); // Check if data has been changed again
        }).catch(function () {
         autoSaveObj.error = true;
       }).finally(function () {
         autoSaveObj.saving = false;
         emitChange();
         $timeout(function () {
           autoSaveObj.success = false;
           autoSaveObj.error = false;
           emitChange();
         }, 3000);
       });

        return promise;
      } else {
        return $q.resolve(false);
      }
    }

    function saveCurrentContent(action) {
      deepCopy();

      var documentParams = _.pick(
        documentModel.dataForSave(),
        'documentData',
        'toBeCoded',
        'progress',
        'computedExpressions',
        'uniqueID',
        'affectedFields');

      var actionsParams = _.extend(documentParams, {
        action: action || 'SAVE',
        workflowId: workflow.id,
        updatedOn: workflow.updatedOn, // for concurrent modification check
        dataCollectionDefinitionId: workflow.dataCollectionDefinitionId
      });
      return WorkflowRequest.applyAction(actionsParams)
        .then(function (result) {
          workflow = angular.merge(workflow, result);
          return result;
        });
    }

    function performWorkflowAction(actionName) {
      return WorkflowRequest.applyAction({
        action: actionName,
        workflowId: workflow.id
      }).then(function (result) {
        workflow = result;
      });
    }

    function hasChanged() {
      return changeTracker.hasChanged();
    }

    function isValidDocument() {
      return documentModel.isValid();
    }

    var emitter = $rootScope.$new();

    function emitChange() {
      emitter.$emit('change');
    }

    function listen(callback) {
      // immediately invoke the callback, to give it the current state
      callback(getters);
      // register the callback to notify it of any updates
      // the return value is an unregister function
      return emitter.$on('change', function () {
        callback(getters);
      });
    }

    var getters = {
      autoSaveObj: function () {
        return autoSaveObj;
      }
    };

    return {
      currentDocumentModel: function () {
        return documentModel;
      },
      currentWorkflow: function () {
        return workflow;
      },
      currentDefinition: function () {
        return definition;
      },
      hasChanged: hasChanged,
      isValidDocument: isValidDocument,
      closeCurrentDocument: closeCurrentDocument,
      loadDocument: loadDocument,
      saveCurrentContent: saveCurrentContent,
      saveItAnyway: saveItAnyway,
      performWorkflowAction: performWorkflowAction,
      autoSave: autoSave,
      listen: listen
    };
  }

})();
