/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.workflow')
    .factory('ElasticsearchRebuild', ElasticsearchRebuild);

  function ElasticsearchRebuild($http, $q, ELASTIC_REFRESH_URL) {
    'ngInject';
    return {
      rebuildAll: function () {
        return $http.post(ELASTIC_REFRESH_URL + '/rebuild')
          .then(
            function (response) {
              return response.data;
            },
            function (err) {
              return $q.reject(err);
            });
      },
      rebuildOrganization: function (id) {
        return $http.post(ELASTIC_REFRESH_URL + '/rebuild?organizationId=' + id)
          .then(
            function (response) {
              return response.data;
            },
            function (err) {
              return $q.reject(err);
            });
      },
      rebuildCollection: function (id, name) {
        return $http.post(ELASTIC_REFRESH_URL + '/rebuild?organizationId=' + id + '&dataCollectionGroup=' + name)
          .then(
            function (response) {
              return response.data;
            },
            function (err) {
              if (err && err.data && err.data.error_description) {
                return $q.reject(err.data.error_description);
              } else {
                return $q.reject('Could not rebuild Elasticsearch for "' + name + '"');
              }
            });
      },
      status: function () {
        return $http.get(ELASTIC_REFRESH_URL + '/status/advanced')
          .then(
            function (response) {
              return response.data;
            },
            function () {
              return $q.reject('Could not retrieve Elasticsearch rebuilding status');
            });
      }
    };
  }

})();
