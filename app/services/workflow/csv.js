/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.workflow')
    .factory('SkryvCsv', CsvFactory);

  function CsvFactory($resource, SESSION_SERVER_URL, Notifications, _, healthdataDocumentmodel, $q) {
    'ngInject';

    var resource = $resource(SESSION_SERVER_URL + '/preloading',
      {}, {
        'get': { method: 'GET' },
        'post': { method: 'POST' }
      });

    function uploadCsv(csv, id, csvFileName, mode, master) {

      var delay = $q.defer();

      var upload = {
        dataCollectionDefinitionId: id,
        fileName: csvFileName,
        csv: csv,
        mode: mode,
        master: master
      };

      try {
        var newline = csv.substr(csv.length - 1);

        if (newline === '\n') {
          csv = csv.substr(0, csv.length - 1);
        }

        if (csv.match(/(?:"(?:[^"]|"")*"|[^,\n]*)(?:,(?:"(?:[^"]|"")*"|[^,\n]*))*\n/g).length >= 1) {
          resource.post(upload, function (uploadedData) {
            var count = 0;
            angular.forEach(uploadedData.status, function (workflow) {
              if (workflow.status !== 'error') {
                count = count + 1;
              }
            });
            delay.resolve(uploadedData);
          }, function () {
            Notifications.error('CSV upload error');
            delay.reject('CSV upload error');
          });
        } else {
          Notifications.error('CSV contains no data');
          delay.reject('CSV contains no data');
        }
      } catch (err) {
        Notifications.error('CSV contains no data');
        delay.reject('CSV contains no data');

      }
      return delay.promise;
    }

    function downloadCsv(workflows, options) {
      options = options || {};
      var delay = $q.defer();

      if (workflows.length > 0) {
        var documents = _.map(workflows, function (workflow) {
          return function () {
            return workflow.openDocument().document;
          };
        });

        try {
          healthdataDocumentmodel.csvMapping.exportDocumentsToCSV(documents, options, function (err, csv) {
            if (err) {
              Notifications.error('CSV download failed');
              delay.reject('CSV download failed');
            } else {
              delay.resolve(csv);
            }
          });
        } catch (err) {
          Notifications.error('CSV download failed');
          delay.reject('CSV download failed');
        }
      }
      return delay.promise;
    }

    function getStatus() {
      var deferred = $q.defer();

      try {
        resource.get(function (res) {
          deferred.resolve(res);
        });
      } catch (error) {
        deferred.reject(error);
      }

      return deferred.promise;
    }

    return {
      downloadCsv: downloadCsv,
      uploadCsv: uploadCsv,
      getStatus: getStatus
    };
  }

})();
