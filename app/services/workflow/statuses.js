/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.workflow')
    .factory('Statuses', Statuses)
    .filter('statusName', statusNameFilter)
    .filter('findWorkflowStatus', findWorkflowStatusFilter)
    .factory('Authorities', Authorities);

  function Statuses(_, PLATFORM) {
    'ngInject';

    var statusList = [];

    function statusItem(status) {
      statusList.push(status);
    }

    /*
     action: Needs to be highlighted as to do action for user
     editable: Doc needs to be editable and not read-only?
     statusMessaging: Show in status messaging table
     commentable: User can add comments
      */

    // HD4DP
    statusItem({
      name: 'open',
      status: 'IN_PROGRESS',
      label: 'Open',
      platform: 'hd4dp',
      options: {
        statusMessaging: true,
        editable: true,
        commentable: true,
        action: true
      }
    });

    statusItem({
      name: 'corrections_needed',
      status: 'ACTION_NEEDED',
      label: 'Corrections needed',
      platform: 'hd4dp',
      options: {
        flag: 'corrections',
        statusMessaging: true,
        editable: true,
        commentable: true,
        action: true
      }
    });

    statusItem({
      name: 'follow_up_needed',
      status: 'ACTION_NEEDED',
      label: 'Follow-up needed',
      platform: 'hd4dp',
      options: {
        flag: 'followUp',
        statusMessaging: true,
        editable: true,
        commentable: true,
        action: true
      }
    });

    statusItem({
      name: 'sending',
      status: 'OUTBOX',
      label: 'Sending',
      platform: 'hd4dp',
      options: {
        statusMessaging: true
      }
    });

    statusItem({
      name: 'submitted',
      status: 'SUBMITTED',
      label: 'Submitted',
      platform: 'hd4dp',
      options: {
        statusMessaging: true
      }
    });

    statusItem({
      name: 'approved',
      status: 'APPROVED',
      label: 'Approved',
      platform: 'hd4dp',
      options: {
        statusMessaging: true
      }
    });

    statusItem({
      name: 'unauthorized',
      status: 'UNAUTHORIZED',
      label: 'Error',
      platform: 'hd4dp',
      options: {
        statusMessaging: true
      }
    });

    // HD4RES
    statusItem({
      name: 'new',
      status: 'NEW',
      label: 'New',
      platform: 'hd4res'
    });
    statusItem({
      name: 'new_corrections',
      status: 'NEW',
      label: 'New correction',
      platform: 'hd4res',
      options: {
        flag: 'corrections'
      }
    });

    statusItem({
      name: 'new_follow_up',
      status: 'NEW',
      label: 'New follow-up',
      platform: 'hd4res',
      options: {
        flag: 'followUp'
      }
    });

    statusItem({
      name: 'approved',
      status: 'APPROVED',
      label: 'Approved',
      platform: 'hd4res'
    });

    statusItem({
      name: 'comments_needed',
      status: 'ANNOTATIONS_NEEDED',
      label: 'Comments needed',
      platform: 'hd4res',
      options: {
        action: true,
        commentable: true
      }
    });
    statusItem({
      name: 'corrections_requested',
      status: 'CORRECTIONS_REQUESTED',
      label: 'Corrections requested',
      platform: 'hd4res'
    });
    statusItem({
      name: 'sending',
      status: 'OUTBOX',
      label: 'Sending',
      platform: 'hd4res'
    });
    statusItem({
      name: 'deleted',
      status: 'DELETED',
      label: 'Deleted',
      platform: 'hd4res'
    });
    statusItem({
      name: 'unauthorized',
      status: 'AUTHORIZATION_FAILED',
      label: 'Error',
      platform: 'hd4res'
    });
    statusItem({
      name: 'loaded_in_stg',
      status: 'LOADED_IN_STG',
      label: 'Error',
      platform: 'hd4res'
    });
    statusItem({
      name: 'technical_error',
      status: 'TECHNICAL_ERROR',
      label: 'Error',
      platform: 'hd4res'
    });
    statusItem({
      name: 'validation_failed',
      status: 'VALIDATION_FAILED',
      label: 'Error',
      platform: 'hd4res'
    });

    return {
      // Platform filtered
      statuses: function () {
        return _.where(statusList, { platform: PLATFORM });
      },
      // Not platform filtered
      allStatuses: function () {
        return _.where(statusList, { platform: PLATFORM });
      },
      hasProperty: function (name, property) {
        var search = _.find(statusList, function (status) {
          return status.status === name;
        });
        return (search && search.options ? search.options[property] : false);
      },
      withProperty: function (property) {
        return _.filter(statusList, function (status) {
          return status.options && status.options[property];
        });
      }
    };
  }

  function Authorities(PLATFORM) {
    'ngInject';

    var authList = [];

    function authItem(role) {
      if (!role.platform || role.platform === PLATFORM) {
        authList.push(role);
      }
    }

    authItem({
      name: 'ROLE_ADMIN',
      label: 'Administrator'
    });

    authItem({
      name: 'ROLE_USER',
      label: 'Researcher',
      platform: 'hd4res'
    });

    authItem({
      name: 'ROLE_USER',
      label: 'Data provider',
      platform: 'hd4dp'
    });

    return {
      Authorities: function () {
        return authList;
      }
    };
  }

  function statusNameFilter(Statuses, _) {
    'ngInject';

    function nameFilter(status) {
      if (status) {
        var name = _.findWhere(Statuses.statuses(), { name: status.toLowerCase() });
        if (name && name.label) {
          return name.label;
        } else if (name) {
          return name;
        } else {
          return status.toLowerCase().replace(/_/g, ' ');
        }
      }
    }

    return nameFilter;
  }

  function findWorkflowStatusFilter(Statuses, _) {
    'ngInject';

    function workflowStatus(workflow) {

      var arr = _.where(Statuses.allStatuses(), { status: workflow.status });

      if (arr.length === 1) {
        return arr[0].label;
      } else if (arr.length > 1) {
        var label = '';

        _.each(arr, function (status) {
          if (workflow.flags && status.options && status.options.flag && workflow.flags[status.options.flag]) {
            if (label.length > 0) { label += '\n'; }
            label += status.label;
          }
        });

        if (label.length === 0) {
          return arr[0].label;
        }

        return label;
      } else {
        return workflow.status;
      }

    }
    return workflowStatus;
  }

})();
