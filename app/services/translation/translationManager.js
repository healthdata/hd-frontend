/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.translation',
  [ 'skryv.conf', 'ngRoute', 'ngResource', 'skryv.language' ])

  .factory('SkryvTranslation',
    function (gettextCatalog, SESSION_SERVER_URL, $http, $q) {
      'use strict';

      var url = SESSION_SERVER_URL + '/translations';

      return {
        getTranslations: function (language, skipSet) {
          var searchLang = (language) ? '?language=' + language : '';

          return $http.get(url + searchLang)
            .then(function (translations) {
              if (language && !skipSet) {
                gettextCatalog.setStrings(language, translations.data);
              }
              return translations.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Translations not found'));
            });
        },
        storeTranslations: function (body) {
          return $http.post(url + '/manager', body)
            .then(function (translation) {
              return translation.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not save translations'));
            });
        },
        updateTranslations: function (body) {
          return $http.put(url + '/manager', body)
            .then(function (translation) {
              return translation.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not update translations'));
            });
        }
      };
    });
