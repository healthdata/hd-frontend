/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('services.participation')
  .factory('SkryvParticipation',
    function ($q, PARTICIPATION_SERVER_URL, $http, gettextCatalog) {
      'use strict';
      return {
        get: function (id) {
          return $http.get(PARTICIPATION_SERVER_URL + id)
            .then(function (response) {
                return response.data;
              }, function () {
                return $q.reject(gettextCatalog.getString('Participation not found'));
              }
            );
        },
        getAll: function (params) {
          return $http.get(PARTICIPATION_SERVER_URL, { params: params })
            .then(function (response) {
                return response.data;
              }, function () {
                return $q.reject(gettextCatalog.getString('Could not load participations'));
              }
            );
        },
        getNonParticipants: function (params) {
          return $http.get(PARTICIPATION_SERVER_URL + 'nonparticipating', { params: params })
            .then(function (response) {
                return response.data;
              }, function () {
                return $q.reject(gettextCatalog.getString('Could not load non-participants'));
              }
            );
        },
        getDocument: function (id) {
          return $http.get(PARTICIPATION_SERVER_URL + id + '/documents')
            .then(function (response) {
                return response.data;
              }, function (err) {
                return $q.reject(err);
              }
            );
        },
        action: function (action) {
          return $http.post(PARTICIPATION_SERVER_URL + 'actions', action)
            .then(function (response) {
                return response.data;
              }, function (err) {
                return $q.reject(err);
              }
            );
        }
      };
    });
