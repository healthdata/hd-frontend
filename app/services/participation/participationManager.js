/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.participation')
    .factory('CurrentParticipationModel', CurrentParticipationModel);

  function CurrentParticipationModel(_, openDocument, SkryvParticipation, openDocumentDefinition) {
    'ngInject';

    // workflowId = participationId
    // due to backend reuse of code the workflowId property name needs to be kept
    var document, participation, collectionDefinition, participationModel;
    function closeCurrentDocument() {
      document = undefined;
      participation = undefined;
      collectionDefinition = undefined;
      participationModel = undefined;
    }

    function loadDocument(doc, pcp, dcd) {
      closeCurrentDocument();
      document = doc;
      participation = pcp;
      collectionDefinition = dcd;

      participationModel = openDocument(
        openDocumentDefinition(dcd),
        doc.documentData,
        null, // no context
        { readOnly: pcp.status !== 'IN_PROGRESS' });
    }

    function save() {
      var documentParams = _.pick(
        participationModel.dataForSave(),
        // For participations, the backend still relies on the documentContent
        // property, rather than the documentData property. Once the backend has
        // been updated here as well, the documentContent property may be
        // removed.
        'documentContent',
        'documentData',
        'progress');
      return doAction(_.extend(documentParams, {
        action: 'SAVE',
        workflowType: 'PARTICIPATION',
        workflowId: participation.id,
        dataCollectionDefinitionId: collectionDefinition.id
      }));
    }

    function submit() {
      return doAction({
        'action': 'SUBMIT',
        'workflowType': 'PARTICIPATION',
        'workflowId': participation.id
      });
    }

    function doAction(action) {
      return SkryvParticipation.action(action);
    }

    function isValidDocument() {
      return participationModel.isValid();
    }

    return {
      currentParticipationModel: function () { return participationModel; },
      isValidDocument: isValidDocument,
      closeCurrentDocument: closeCurrentDocument,
      loadDocument: loadDocument,
      save: save,
      submit: submit
    };
  }

}) ();
