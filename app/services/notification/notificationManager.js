/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.notification').service('Notifications', function (Notification, $timeout) {
    'ngInject';

    var notifications = [];

    return {

      notifications: notifications,

      clear: function () {
        this.notifications.splice(0,this.notifications.length); // Make array empty without setting a new array
      },

      info: function (content, duration) {
        this.addNotification('info', 'info', content, duration);
      },

      error: function (content, duration) {
        this.addNotification('error', 'times-circle-o', content, duration);
      },

      success: function (content, duration) {
        this.addNotification('success', 'check-circle-o', content, duration);
      },

      warning: function (content, duration) {
        this.addNotification('warning', 'exclamation-triangle', content, duration);
      },

      // TODO: Split helperfunctions in seperate service
      addNotification: function (type, icon, content, duration) {
        var notification = new Notification(type, icon, content, duration);
        this.notifications.push(notification);

        // Sticky notification
        var self = this;
        if (notification.duration !== 'sticky') {
          $timeout(function () {
            self.removeNotification(notification);
          }, notification.duration);
        }
      },

      removeNotification: function (notification) {
        for (var i = 0; i < this.notifications.length; i = i + 1) {
          if (this.notifications[i] === notification) {
            this.notifications.splice(i, 1);
            return;
          }
        }
      }
    };
  });
})();
