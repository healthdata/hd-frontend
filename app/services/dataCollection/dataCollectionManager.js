/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function () {
  'use strict';
  angular.module('services.dataCollection')
    .service('dataCollectionManager', dataCollectionManager);

  function dataCollectionManager($http, ENVIRONMENT_URL) {
    'ngInject';

    function postDataCollectionRequest(orgId, collections) {
      var data = { dataCollections: collections };

      return $http.post(ENVIRONMENT_URL + '/organizations/' + orgId + '/dataCollectionAccessRequests', data)
        .then(function (response) {
          return response.data;
        });
    }

    function getDataCollectionRequests(orgId, processed, userId) {
      var query = '?';

      if (processed !== undefined) {
        query += 'processed=' + processed;
      }
      if (userId !== undefined) {
        query += '&userId=' + userId;
      }

      return $http.get(ENVIRONMENT_URL + '/organizations/' + orgId + '/dataCollectionAccessRequests' + query)
        .then(function (response) {
          return response.data;
        });
    }

    function decide(orgId, request) {
      var data = {};

      request.dataCollections.forEach(function (entry) {
        data[entry.key] = entry.value || false;
      });

      return $http.post(ENVIRONMENT_URL + '/organizations/' + orgId + '/dataCollectionAccessRequests/' + request.id + '/decision', data)
        .then(function (response) {
          return response.data;
        });
    }

    function getDataProvidersForDataCollection(collectionName) {
      return $http.get(ENVIRONMENT_URL + '/datacollections/' + collectionName + '/dataproviders')
        .then(function (response) {
          return response.data;
        });
    }

    return {
      decide: decide,
      getDataCollectionRequests: getDataCollectionRequests,
      postDataCollectionRequest: postDataCollectionRequest,
      getDataProvidersForDataCollection: getDataProvidersForDataCollection
    };
  }
})();
