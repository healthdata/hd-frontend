(function () {

  'use strict';
  angular.module('services.dataCollection', [
    'skryv.conf',
    'skryv.session'
  ]);

})();
