/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  var SkryvView = angular.module('skryv.view',
    []);

  SkryvView.factory('SkryvView',
    function ($q, Notifications, _, SkryvSession, SkryvUser, SkryvDefinition, $rootScope, SkryvUserView, gettextCatalog) {
      'ngInject';

      var dataDefinition;

      var views;
      var activeView;
      var initiated = false;

      function resetInternalState(definition) {
        dataDefinition = definition;
        removeOldViews();
      }

      function getViews() {
        return SkryvUserView.get(dataDefinition.id).then(function (userViews) {
          views = userViews;
          emitChange();
        });
      }

      function removeOldViews() {
        var userViews = [];
        var userCopy = SkryvSession.getLoggedUser();

        // If user has old metadata views stored
        if (userCopy.metaData.views) {
          delete userCopy.metaData.views;
          SkryvUser.updateMetadata(userCopy);
        }

        // If user has views stored
        if (userCopy.metaData['VIEWS-' + dataDefinition.dataCollectionName]) {
          // If views are array
          // Bug was that ""[]"" could not been set
          var json;
          try {
            json = JSON.parse(userCopy.metaData['VIEWS-' + dataDefinition.dataCollectionName]);
          }
          catch (e) {
            delete userCopy.metaData['VIEWS-' + dataDefinition.dataCollectionName];
          }

          if (json) {
            userViews = json;
            if (_.compact(userViews).length === 0) { // compact removes all empty elements
              delete userCopy.metaData['VIEWS-' + dataDefinition.dataCollectionName];
              SkryvUser.updateMetadata(userCopy);
            } else {
              // Set data collection name and id
              SkryvUserView.get(dataDefinition.id).then(function (newViews) {
                transformOldViews(userViews, newViews);
              });
            }
          } else {
            getViews();
          }
        } else {
          getViews();
        }

        return views;
      }

      function transformOldViews(views, newViews) {
        views = JSON.parse(views);
        var userViews;
        var promises = [];
        var userCopy = SkryvSession.getLoggedUser();

        views.forEach(function (view) {
          var delay = $q.defer();

          // Each 'dataCollection' property should be replaced with 'dataCollectionName' and 'dataCollectionId'
          if (!view.dataCollectionName || !view.dataCollectionDefinitionId) {
            // If old collections is still set
            if (view.dataCollection) {
              delete view.dataCollection;
            }

            // Add new properties
            view.dataCollectionName = dataDefinition.dataCollectionName;
            view.dataCollectionDefinitionId = dataDefinition.id;
          }

          // If columns replace with docColumn
          if (!view.docColumns) {
            view.docColumns = transformOldDocColumns(view.columns);
            delete view.columns;
          }

          // If no filters grouped, group filters
          if (!view.filters) {
            view.filters = transformOldFilters(view);
          }

          if (!view.extraColumns) {
            view.extraColumns = transformOldExtraColumns(view);
          }

          // Duplicate -> ignore
          var duplicate = _.findWhere(newViews, { name: view.name });
          if (!duplicate) {
            SkryvUserView.save(dataDefinition.id, view.name, view).then(function () {
              // Success -> view can be deleted from metaData
              delay.resolve();
            }, function () {
              // Views that not could be saved, try again next time
              userViews.push(view);
              delay.resolve();
            });
          } else {
            delay.resolve();
          }

          promises.push(delay.promise);
        });

        $q.all(promises).then(function () {
          if (_.compact(userViews).length === 0) {
            delete userCopy.metaData['VIEWS-' + dataDefinition.dataCollectionName];
          } else {
            userCopy.metaData['VIEWS-' + dataDefinition.dataCollectionName] = JSON.stringify(userViews);
          }

          SkryvUser.updateMetadata(userCopy);
        });

        getViews(); // Get updated views
      }

      function transformOldDocColumns(oldColumns) {
        var newColumns = [];
        _.each(oldColumns, function (column) {
          var name = '';
          if (column.parent) {
            name = column.parent + '.nested.';
          }
          name += column.name + '.value';
          newColumns.push(name);
        });
        return newColumns;
      }

      function transformOldFilters(view) {
        var oldFilter = [ 'createdAfter', 'createdBefore', 'dataProviderID', 'dateOfBirthAfter', 'dateOfBirthBefore',
          'internalPatientID', 'lastUpdatedAfter', 'lastUpdatedBefore', 'onlyMyWorkflows', 'patientFirstName', 'patientID',
          'patientName' ];
        var newFilters = {};

        // Filters
        _.each(oldFilter, function (filter) {
          // If view has property
          if (view.hasOwnProperty(filter)) {
            // If filter has value
            if (view[filter]) {
              newFilters[filter] = view[filter];
            }

            delete view[filter];
          }
        });

        // statuses
        _.each(view.statuses, function (status, name) {
          newFilters[name] = true;
        });
        delete view.statuses;

        return newFilters;
      }

      function transformOldExtraColumns(view) {
        var oldExtraColumns = [ 'identifications', 'qualities', 'sourceOfData', 'timings' ];
        var newExtraColumns = [];

        // Extra columns
        _.each(oldExtraColumns, function (column) {
          // If view has property
          if (view.hasOwnProperty(column)) {
            _.each(view[column], function (prop, name) {
              newExtraColumns.push(name);
            });

            delete view[column];
          }
        });

        // sendStatus
        if (view.hasOwnProperty('sendStatus')) {
          newExtraColumns.push('sendStatus');
          delete view.sendStatus;
        }

        return newExtraColumns;
      }

      function getLastView() {
        var userCopy = SkryvSession.getLoggedUser();

        if (userCopy.metaData.lastView) {
          activeView = JSON.parse(userCopy.metaData.lastView);
          return JSON.parse(userCopy.metaData.lastView);
        } else {
          return {};
        }
      }

      function setLastView(view, skipSet) {
        var userCopy = SkryvSession.getLoggedUser();
        if (!view.content) {
          view.content = angular.copy(view);
        }

        if (!skipSet) {
          activeView = view;
        }

        userCopy.metaData.lastView = JSON.stringify(view);
        SkryvUser.updateMetadata(userCopy).then(function (newUser) {
          userCopy = newUser;
          emitChange();
        });
      }

      function addView(view) {
        var delay = $q.defer();

        SkryvUserView.save(dataDefinition.id, view.name, view).then(function (viewResponse) {
          Notifications.success('Your view has been saved');
          views.push(viewResponse);
          emitChange();
          delay.resolve(viewResponse);
        }, function (err) {
          if (err.status === 409) {
            Notifications.error(gettextCatalog.getString('View with that name already exists'));
          } else {
            Notifications.error(gettextCatalog.getString('Failed to save your view'));
          }
          delay.reject(view);
        });

        return delay.promise;
      }

      function updateView(view) {
        var delay = $q.defer();
        SkryvUserView.update(dataDefinition.id, view.name, view).then(function () {
          Notifications.success('Your view has been updated');
          getViews();
          delay.resolve(view);
        }, function (err) {
          Notifications.error(err);
          delay.reject(view);
        });

        return delay.promise;
      }

      function deleteView(view) {
        var delay = $q.defer();
        var userCopy = SkryvSession.getLoggedUser();
        delay.resolve(views);

        SkryvUserView.delete(dataDefinition.id, view.name, view).then(function () {
          Notifications.success('Your view has been deleted');

          views = _.without(views, _.findWhere(views, {
            name: view.name
          }));

          // Remove active view
          if (activeView) {
            delete activeView.name;
            userCopy.metaData.lastView = JSON.stringify(activeView);
            SkryvUser.updateMetadata(userCopy);
          }

          emitChange();

          delay.resolve(view);
        }, function (err) {
          Notifications.error(err);
          delay.reject(view);
        });

        return delay.promise;
      }

      // Make sure we don't always set active view
      // Only do when switching DC or reloading
      function setInitiated() {
        initiated = true;
      }

      var getters = {
        initiated: function () {
          return initiated;
        },
        views: function () {
          return views;
        },
        activeView: function () {
          return activeView;
        }
      };

      var emitter = $rootScope.$new();

      function emitChange() {
        emitter.$emit('change');
      }

      function listen(callback) {
        // immediately invoke the callback, to give it the current state
        callback(getters);
        // register the callback to notify it of any updates
        // the return value is an unregister function
        return emitter.$on('change', function () {
          callback(getters);
        });
      }

      function definitionChange() {
        resetInternalState();
      }

      return _.extend({
        listen: listen,
        setLastView: setLastView,
        addView: addView,
        getLastView: getLastView,
        updateView: updateView,
        deleteView: deleteView,
        setInitiated: setInitiated,
        definitionChange: definitionChange,
        resetInternalState: resetInternalState
      }, getters);

    });
})();
