/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.listview').factory('ListViewExtraColumns',
    function ($rootScope, _, PLATFORM, gettextCatalog) {
      'ngInject';

      var extraColumns;
      var dataDefinition;

      // Reset the extra columns
      function resetInternalState(definition) {
        dataDefinition = definition;
        resetExtraColumns(true);
        return extraColumns;
      }

      resetInternalState();

      /**
       * Add extra columns to display in the listview
       * @param {string} name: Name of the column
       * @param {string} label: Label to display in the front
       * @param {string} valueLocation: Location of the data in the registration
       * @param {array} array: Columns with:  active state,
       *                                      label
       *                                      display type
       * @param {string} platform: Display for specific platform only
       */
      // Define dropdown
      function addExtraColumnDropdown(name, label, valueLocation, array, platform) {
        extraColumns[name] = {
          platform: platform,
          label: label,
          type: 'dropdown',
          valueLocation: valueLocation,
          columns: {}
        };

        angular.forEach(array, function (field) {
          if (!field.platform || field.platform === PLATFORM) {
            extraColumns[name].columns[field.key] = addExtraColumnField(field);
          }
        });
      }

      // Define field
      function addExtraColumnField(field) {
        return {
          active: field.default || false,
          key: field.key,
          type: field.type,
          order: field.order,
          fieldType: field.fieldType,
          label: field.label,
          toggleActive: function () {
            this.active = !this.active;
            emitChange();
          }
        };
      }

      // List of extra columns
      function resetExtraColumns(showDefault) {
        extraColumns = {};

        if (PLATFORM === 'hd4res') {
          extraColumns.id = addExtraColumnField({
            key: 'readableId', label: gettextCatalog.getString('Registration ID'), type: 'checkbox', fieldType: 'int', default: true, order: 3
          });
        } else {
          extraColumns.id = addExtraColumnField({
            key: 'id', label: gettextCatalog.getString('Registration ID'), type: 'checkbox', fieldType: 'int', default: (showDefault ? true : false), order: 3
          });
        }

        addExtraColumnDropdown('identifications', gettextCatalog.getString('Data provider'), '', [
          { key: 'identificationValue', label: gettextCatalog.getString('Data provider id'), fieldType: 'string' },
          { key: 'identificationType', label: gettextCatalog.getString('Data provider id type'), fieldType: 'string' },
          { key: 'identificationName', label: gettextCatalog.getString('Data provider name'), fieldType: 'string' }
        ], 'hd4res');
        addExtraColumnDropdown('sourceOfData', gettextCatalog.getString('Source of data'), 'firstSaveAction', [
          { key: 'source', label: gettextCatalog.getString('Source of data'), fieldType: 'string' },
          { key: 'executedBy', label: gettextCatalog.getString('User'), fieldType: 'string', platform: 'hd4dp' },
          { key: 'file', label: gettextCatalog.getString('File name'), fieldType: 'string' },
          { key: 'messageType', label: gettextCatalog.getString('Message type'), fieldType: 'string' }
        ], '');
        addExtraColumnDropdown('timings', gettextCatalog.getString('Timings'), '', [
          { key: 'createdOn', label: gettextCatalog.getString('Creation date'), fieldType: 'date' },
          { key: 'updatedOn', label: gettextCatalog.getString('Last modification date'), fieldType: 'date' }
        ], '');
        if (dataDefinition && dataDefinition.followUpDefinitions && dataDefinition.followUpDefinitions.length > 0) {
          extraColumns.followUp = addExtraColumnField({ key: 'followUp', label: gettextCatalog.getString('Follow-up'), type: 'checkbox', fieldType: 'followup', order: 2 });
        }

        addExtraColumnDropdown('qualities', gettextCatalog.getString('Data quality items'), '', [
          { key: 'totalNotes', label: gettextCatalog.getString('Comments'), fieldType: 'int' },
          { key: 'openNotes', label: gettextCatalog.getString('Open comments'), fieldType: 'int' },
          { key: 'resolvedNotes', label: gettextCatalog.getString('Resolved comments'), fieldType: 'int' },
          { key: 'errors', label: gettextCatalog.getString('Errors'), fieldType: 'int' },
          { key: 'progress', label: gettextCatalog.getString('Progress'), fieldType: 'progress' },
          { key: 'warnings', label: gettextCatalog.getString('Warnings'), fieldType: 'int' }
        ], '');

        extraColumns.sendStatus = addExtraColumnField({ key: 'sendStatus',
          label: gettextCatalog.getString('Status confirmation'), type: 'checkbox', fieldType: 'string', order: 1 });
      }

      // Get all active keys
      function getActiveKeys() {
        var keys = [];
        _.each(extraColumns, function (column) {
          if (column.type === 'checkbox' && column.active) {
            keys.push(column.key);
          }
          if (column.columns) {
            _.each(column.columns, function (nestedColumn) {
              if (nestedColumn.active) {
                keys.push(nestedColumn.key);
              }

            });
          }
        });

        return keys;
      }

      // Set active keys
      function setActiveKeys(keyArray) {
        // Set all columns to false
        resetExtraColumns();

        if (keyArray) {
          _.each(extraColumns, function (column) {
            if (column.type === 'checkbox' && keyArray.indexOf(column.key) > -1) {
              column.active = true;
            }
            if (column.columns) {
              _.each(column.columns, function (nestedColumn) {
                if (keyArray.indexOf(nestedColumn.key) > -1) {
                  nestedColumn.toggleActive();
                }
              });
            }
          });
        }
        emitChange();
      }
      // Read-only functions made available to change listeners
      var getters = {
        extraColumns: function () {
          return extraColumns;
        },
        extraActives: getActiveKeys
      };

      var emitter = $rootScope.$new();

      function emitChange() {
        emitter.$emit('change');
      }

      function listen(callback) {
        // immediately invoke the callback, to give it the current state
        callback(getters);
        // register the callback to notify it of any updates
        // the return value is an unregister function
        return emitter.$on('change', function () {
          callback(getters);
        });
      }

      return _.extend({
        listen: listen,
        resetInternalState: resetInternalState,
        clearExtraColumns: function () {
          resetExtraColumns(true);
          emitChange();
        },
        getActiveKeys: getActiveKeys,
        setActiveKeys: setActiveKeys
      }, getters);
    });
})();
