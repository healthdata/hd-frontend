/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.listview').factory('ListViewFilters',
    function ($http, $rootScope, _, Statuses, $filter, gettextCatalog, Organization) {
      'ngInject';

      var filters;
      var dataDefinition;
      var emitter = $rootScope.$new();

      // Reset the extra columns
      function resetInternalState(definition) {
        dataDefinition = definition;
        resetFilters();
        return filters;
      }

      resetInternalState();

      // List of filters
      function resetFilters() {
        filters = {};

        filters.onlyMyWorkflows = { label: 'Only my registrations', type: 'checkbox', value: false, platform: 'hd4dp' };
        addFilterDropdown('creationDates', gettextCatalog.getString('Creation date'),
          gettextCatalog.getString('Use the datepicker to select the dates.'), {
            createdAfter: addFilterField(gettextCatalog.getString('After'), 'date'),
            createdBefore: addFilterField(gettextCatalog.getString('Before'), 'date')
          });
        addFilterDropdown('modificationDates', gettextCatalog.getString('Last modification date'),
          gettextCatalog.getString('Use the datepicker to select the dates.'), {
            lastUpdatedAfter: addFilterField(gettextCatalog.getString('After'), 'date'),
            lastUpdatedBefore: addFilterField(gettextCatalog.getString('Before'), 'date')
          });

        addFilterDropdown('statuses', gettextCatalog.getString('Status'), '', getStatusFilterFields());
        var followUps = {};

        if (dataDefinition && dataDefinition.followUpDefinitions && dataDefinition.followUpDefinitions.length > 0) {
          _.each($filter('orderBy')(dataDefinition.followUpDefinitions, 'label'), function (followUp) {
            followUps[followUp.name] =  addFilterNestedDropdown(followUp.label, gettextCatalog.getString('Activation'), {
              active: addFilterField(gettextCatalog.getString('Active'), 'checkbox'),
              planned: addFilterField(gettextCatalog.getString('Planned'), 'checkbox'),
              submitted: addFilterField(gettextCatalog.getString('Submitted'), 'checkbox')
            });
          });

          addFilterDropdown('followUp', gettextCatalog.getString('Follow-up'), gettextCatalog.getString('Follow-up names'), followUps);
        }

        addFilterDropdown('patient', gettextCatalog.getString('Patient'), gettextCatalog.getString('Use the field below to filter on patient identification:'), {
          patientID: addFilterField(gettextCatalog.getString('Patient Id'), 'string'),
          internalPatientID: addFilterField(gettextCatalog.getString('Internal Patient Id'), 'string', 'hd4dp'),
          patientName: addFilterField(gettextCatalog.getString('Last name'), 'string', 'hd4dp'),
          patientFirstName: addFilterField(gettextCatalog.getString('First name'), 'string', 'hd4dp'),
          dateOfBirthAfter: addFilterField(gettextCatalog.getString('Date of birth after'), 'date', 'hd4dp'),
          dateOfBirthBefore: addFilterField(gettextCatalog.getString('Date of birth before'), 'date', 'hd4dp')
        });

        addFilterDropdown('provider', gettextCatalog.getString('Data provider identification'),
          gettextCatalog.getString('Use the field below to filter on data provider:'), {
            dataProvider: addAutocompleteFilterField(gettextCatalog.getString('Data provider'), 'autocomplete', undefined, undefined, {
              promise: function (viewValue) {
                return Organization.getDataProviders().then(function (response) {
                  return response.data.filter(function (entry) {
                    if (entry.identificationValue.toString().indexOf(viewValue.toLowerCase()) > -1) {
                      return entry;
                    }

                    if (entry.name.toLowerCase().indexOf(viewValue.toLowerCase()) > -1) {
                      return entry;
                    }
                  });
                });
              },
              renderMatch: function (match) {
                if (match) {
                  return match.name;
                }
              },
              inputFormatter: function () {
                return this.value.name;
              }
            })
          }, 'hd4res');

      }

      // Add filter dropdown
      function addFilterDropdown(name, label, help, array, platform) {
        filters[name] = {
          label: label,
          platform: platform,
          help: help,
          fields: array,
          type: 'dropdown',
          isActive: function () {
            var active = false;
            angular.forEach(this.fields, function (field) {
              if (field && field.value) {
                active = true;
              }
              if (field.fields) {
                angular.forEach(field.fields, function (field) {
                  if (field && field.value) {
                    active = true;
                  }
                });
              }
            });
            return active;
          }
        };
      }

      // Add filter nested dropdown
      function addFilterNestedDropdown(label, help, array, platform) {
        return {
          label: label,
          platform: platform,
          help: help,
          fields: array,
          type: 'nested',
          isActive: function () {
            var active = false;
            angular.forEach(this.fields, function (field) {
              if (field && field.value) {
                active = true;
              }
            });
            return active;
          }
        };
      }

      // Add filter fields for dropdown
      function addFilterField(label, type, platform, trueValue) {
        var field = {
          label: label,
          type: type,
          value: '',
          platform: platform
        };

        if (type === 'checkbox') {
          field.value = false;
          field.toggleValue = function () {
            field.value = !field.value;
          };

          field.trueValue = trueValue; // Giving extra true value information
        }

        return field;
      }

      function addAutocompleteFilterField(label, type, platform, trueValue, autoCompleteOptions) {
        // autoCompleteOptions should take the form of:
        // {
        //  model: 'string',
        //  typeahead-loading: 'isLoadingFilter',
        //  typeahead-min-length: 2,
        //  typeahead: Promise
        // }
      // <input
      //   type="textarea"
      //   autocomplete="false"
      //   ng-trim="false"
      // class="form-control"
      //   ng-readonly="::readOnly"
      //   ng-model="referenceListQuery"
      //   skr-validation="manipulator"
      //   typeahead-min-length="typeaheadMinLength"
      //   uib-typeahead="match as (match) for match in autocompleteFor($viewValue)"
      //   typeahead-loading="isLoading"
      //   typeahead-on-select="selectMatch(referenceListQuery)"
      //   size="{{ ::size }}">
        var baseField = addFilterField(label, type, platform, trueValue);
        return _.extend(baseField, autoCompleteOptions);
      }

      // Status filter fields
      function getStatusFilterFields() {
        var statuses = _.uniq(Statuses.statuses(), function (status) {
          return status.label;
        });

        var filterStatuses = {};

        angular.forEach(statuses, function (status) {
          filterStatuses[status.name] = addFilterField(status.label, 'checkbox', '', status);
        });

        return filterStatuses;
      }

      // Get active filters
      function getActiveFilters() {
        return loopGetActives(filters);
      }
      function loopGetActives(loopFilters) {
        var activeFilters = {};
        _.each(loopFilters, function (filter, name) {
          if (filter.value) {
            activeFilters[name] = filter.value;
          }

          if (filter.fields) {
            var looped = loopGetActives(filter.fields);

            // Only add fields that have a value
            if (!_.isEmpty(looped)) {
              activeFilters[name] = looped;
            }
          }
        });
        return activeFilters;
      }

      function loopSetActives(setFilters, loopFilters) {
        _.each(loopFilters, function (filter, name) {
          if (!filter.fields && setFilters && setFilters[name]) {
            filter.value = setFilters[name];
          }

          if (filter.fields && setFilters && setFilters[name]) {
            loopSetActives(setFilters[name], filter.fields);
          }
        });
      }

      // Set active keys
      function setActiveFilters(valueObj) {
        // Set all filters empty
        resetFilters();

        loopSetActives(valueObj, filters);

        emitChange();
      }

      // Read-only functions made available to change listeners
      var getters = {
        filters: function () {
          return filters;
        }
      };

      function emitChange() {
        emitter.$emit('change');
      }

      function listen(callback) {
        // immediately invoke the callback, to give it the current state
        callback(getters);
        // register the callback to notify it of any updates
        // the return value is an unregister function
        return emitter.$on('change', function () {
          callback(getters);
        });
      }

      return _.extend({
        listen: listen,
        clearFilters: function () {
          resetFilters();
          emitChange();
        },
        getActiveFilters: getActiveFilters,
        resetInternalState: resetInternalState,
        setActiveFilters: setActiveFilters
      }, getters);
    });
})();
