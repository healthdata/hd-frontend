/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.listview').factory('ListViewDocColumns',
    function ($rootScope, _, openDocumentDefinition, PLATFORM, Notifications, $log, healthdataDocumentmodel) {
      'ngInject';

      var docColumns;
      var dataDefinition;
      var documentDefinition;
      var activeDocColumns;

      // Reset the extra columns
      function resetInternalState(definition) {
        dataDefinition = angular.copy(definition); // copy prevents overwriting the listview value

        try {
          documentDefinition = openDocumentDefinition(dataDefinition.content);
        } catch (e) {
          Notifications.error('Document definition cannot be processed');
          $log.log(e);
          documentDefinition = {};
        }

        resetDocColumns(true);
        emitChange();
        return docColumns;
      }

      // List of default columns
      // Set sticky fields active
      function resetDocColumns(sticky) {
        docColumns = {
          fields: {},
          sections: {},
          label: dataDefinition.content.label
        };

        docColumns = addFieldsAndSections(docColumns, dataDefinition.content, sticky);
        activeDocColumns = getActiveColumns(docColumns);
      }

      // Generate dropdown
      function addFieldsAndSections(parent, content, sticky) {
        // Add fields
        _.each(content.fields, function (field) {
          // We need to skip patient ID fields on HD4RES that do not have the
          // "keep" property (either true or "coded").
          if (PLATFORM === 'hd4res' && parent.type === 'patientID' && !field.keep) {
            return;
          }
          if (field.type === 'fieldset') {
            parent.sections[field.name] = addSection(field, parent, false, sticky);
          } else if (field.type === 'list') {

            // Add separate column to toggle on for list field itself.
            var listAsField = {
              name: field.name,
              label: field.label,
              type: 'text',
              sticky: (field.sticky && sticky ? true : false),
              // we set showsParentValue to indicate that when this field is
              // selected, we should should in fact display the value of the
              // parent field. This is to visualize fields that both have a
              // value of their own and nested fields.
              showsParentValue: true
            };
            field.fields = field.fields || [];
            field.fields.unshift(listAsField);

            parent.sections[field.name] = addSection(field, parent, false, sticky);
          } else if (field.type === 'patientID') {
            // First we extend the patient ID fields using a documentmodel
            // helper function, such that we have all the fields correctly
            // available (they are not required to be fully specified in the DCD
            // itself).
            field.fields = healthdataDocumentmodel
              .normalizeNestedPatientIDFields(field.fields);

            // Add separate column to toggle on for patient ID.
            var separateField = {
              name: 'patient_id',
              label: field.label,
              type: 'text',
              sticky: (field.sticky && sticky ? true : false),
              // we set keep to true to make sure that it is always shown in
              // HD4RES
              keep: true,
              // we set showsParentValue to indicate that when this field is
              // selected, we should should in fact display the value of the
              // parent field. This is to visualize fields that both have a
              // value of their own and nested fields.
              showsParentValue: true
            };
            field.fields.unshift(separateField);

            parent.sections[field.name] = addSection(field, parent, false, sticky);
          } else {
            parent.fields[field.name] = addField(field, parent.key, sticky);
          }
        });

        // Add sections
        _.each(content.sections, function (section) {
          parent.sections[section.name] = addSection(section, parent, true, sticky);
        });

        return parent;
      }

      // Field obj
      function addField(field, parentKey, sticky) {
        // Based on the field and parent info, we generate a field key that
        // should match the document structure as generated by displayValue().
        // This structure is stored in workflow.trueData.
        var keyBase = parentKey ? parentKey + '.nested.' : '';
        // With the showsParentValue option, we allow for "special" fields that
        // display the parent's value.
        var fieldKey = field.showsParentValue ? parentKey : keyBase + field.name;
        return {
          type: field.type,
          name: field.name,
          label: field.label,
          key: fieldKey + '.value',
          active: (field.sticky && sticky ? true : false),
          toggleActive: function () {
            this.active = !this.active;
            emitChange();
          }
        };
      }

      // Section obj
      function addSection(section, parent, skipPath, sticky) {
        // console.log(section, parent, skipPath, sticky);
        var newKey = extendPath(parent.key, section.name, skipPath);
        return addFieldsAndSections({
          // We need to pass along the type, in case the section is in fact
          // a patient ID field. This is then used in addFieldsAndSections()
          // to hide nested patient ID fields that are not available in HD4RES.
          type: section.type,
          label: section.label,
          fields: {},
          key: newKey,
          sections: {}
        }, section, sticky);
      }

      function extendPath(base, propertyName, skip) {
        base = base || '';
        if (skip) { return base; }
        if (base === '') { return propertyName; }
        return base + '.nested.' + propertyName;
      }

      // Get all columns with active: true
      function getActiveColumns(obj) {
        var activeArray = [];

        if (obj) {
          _.each(obj.fields, function (field) {
            if (field.active) {
              activeArray.push(field);
            }
          });

          _.each(obj.sections, function (section) {
            activeArray = activeArray.concat(getActiveColumns(section));
          });
        }

        activeDocColumns = activeArray;

        return activeArray;
      }

      // Used for storing keys
      function getActiveKeys() {
        return _.map(activeDocColumns, function (column) {
          return column.key;
        });
      }

      // Set active columns with key
      function setActiveKeys(obj, keyArray) {
        if (obj && keyArray) {
          _.each(obj.fields, function (field) {
            if (keyArray.indexOf(field.key) > -1) {
              field.active = true;
            }
          });

          _.each(obj.sections, function (section) {
            setActiveKeys(section, keyArray);
          });
        }
      }

      // Toggle all active columns
      function clearActiveColumns() {
        _.each(activeDocColumns, function (column) {
          column.toggleActive();
        });
      }

      // Read-only functions made available to change listeners
      var getters = {
        docColumns: function () {
          return docColumns;
        },
        docActiveColumns: function () {
          return activeDocColumns;
        },
        documentDefinition: function () {
          return documentDefinition;
        }
      };

      var emitter = $rootScope.$new();

      function emitChange() {
        getActiveColumns(docColumns);
        emitter.$emit('change');
      }

      function listen(callback) {
        // immediately invoke the callback, to give it the current state
        callback(getters);
        // register the callback to notify it of any updates
        // the return value is an unregister function
        return emitter.$on('change', function () {
          callback(getters);
        });
      }

      return _.extend({
        listen: listen,
        resetInternalState: resetInternalState,
        clearActiveColumns: clearActiveColumns,
        getActiveKeys: getActiveKeys,
        setDefaultColumns: function () {
          resetDocColumns(true);
          emitChange();
        },
        setActiveKeys: function (array) {
          resetDocColumns();
          setActiveKeys(docColumns, array);
          emitChange();
        }
      }, getters);
    });
})();
