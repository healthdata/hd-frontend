/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('services.listview')

  .factory('ListViewWorkflowsStore', function ($rootScope, _) {
      'use strict';

      // Internal state
      var isLoading;            // are we loading more workflows
      var workflows;            // all workflows loaded so far
      var error;                // when in an error state
      var searchParams;         // which workflows to show
      var total;                // the total number of workflows matching the search
      var workflowCache;        // a cache (by workflow ID) of processed workflows
      var cancelDeferred;       // a promise used to cancel the current request

      function resetInternalState() {
        isLoading = false;
        workflows = [];
        error = undefined;
        searchParams = undefined;
        total = undefined;
        cancelDeferred = undefined;
        if (!workflowCache) {
          // the workflowCache is not cleared
          workflowCache = {};
        }
      }

      resetInternalState();

      // Read-only functions made available to change listeners
      var getters = {
        workflows: function () { return workflows; },
        hasError: function () { return typeof error !== 'undefined'; },
        error: function () { return error; },
        isLoading: function () { return isLoading; },
        searchParams: function () { return searchParams; },
        cancelDeferred: function () { return cancelDeferred; },
        total: function () { return total; }
      };

      function processAndCache(workflow, process) {
        var cacheKey = 'workflow:' + workflow.id;
        var cachedWorkflow = workflowCache[cacheKey];
        if (cachedWorkflow && workflow.originalUpdatedOn === cachedWorkflow.originalUpdatedOn) {
          return cachedWorkflow;
        }
        process(workflow);
        workflowCache[cacheKey] = workflow;
        return workflow;
      }

      // State updating function available to action creators

      function onLoadWorkflows(_searchParams_, _cancel_) {
        resetInternalState();
        searchParams = _searchParams_;
        isLoading = true;
        cancelDeferred = _cancel_;
        emitChange();
      }

      function onLoadWorkflowsSuccess(_workflows_, _total_) {
        isLoading = false;
        total = _total_;
        workflows = _workflows_;
        cancelDeferred = undefined;
        emitChange();
      }

      function onLoadWorkflowsFailure(err) {
        isLoading = false;
        error = err;
        cancelDeferred = undefined;
        emitChange();
      }

      var emitter = $rootScope.$new();
      function emitChange() {
        emitter.$emit('change');
      }
      function listen(callback) {
        // immediately invoke the callback, to give it the current state
        callback(getters);
        // register the callback to notify it of any updates
        // the return value is an unregister function
        return emitter.$on('change', function () {
          callback(getters);
        });
      }

      return _.extend({
        listen: listen,
        onLoadWorkflows: onLoadWorkflows,
        onLoadWorkflowsSuccess: onLoadWorkflowsSuccess,
        onLoadWorkflowsFailure: onLoadWorkflowsFailure,
        processAndCache: processAndCache
      }, getters);

    })
  .factory('ListViewWorkflowsActions',
    function ($q, WorkflowRequest, ListViewWorkflowsStore, _, openDocument,
              documentDisplayValues, SkryvLanguage, makeSearchQuery) {
      'use strict';

      return {
        loadWorkflows: loadWorkflows
      };

      function loadWorkflows(searchParams, page, pageSize) {

        var isLoading = ListViewWorkflowsStore.isLoading();
        var cancelOld;
        if (isLoading) {
          cancelOld = ListViewWorkflowsStore.cancelDeferred();
        }

        var dataCollectionId = searchParams.dataCollectionId;
        var esQuery = makeSearchQuery(searchParams, page, pageSize);

        var cancel = $q.defer();
        var promise = WorkflowRequest.getByDatacollection(dataCollectionId, esQuery, cancel.promise)
          .then(successCallback, failureCallback);

        ListViewWorkflowsStore.onLoadWorkflows(searchParams, cancel);
        if (cancelOld) {
          cancelOld.resolve();
        }

        function successCallback(data) {
          if (ListViewWorkflowsStore.searchParams() !== searchParams) { return; }
          var total = data.hits.total;
          var workflows = _.map(data.hits.hits, function (hit) {
            return hit._source;
          });
          var processedWorkflows = _.map(workflows, processReceivedWorkflow);
          ListViewWorkflowsStore.onLoadWorkflowsSuccess(processedWorkflows, total);
          return processedWorkflows;
        }

        function failureCallback(err) {
          if (ListViewWorkflowsStore.searchParams() !== searchParams) { return; }
          ListViewWorkflowsStore.onLoadWorkflowsFailure(err);
          return err;
        }

        return promise;
      }

      function activateDisplayByLanguage(workflow) {
        var currentLanguage = SkryvLanguage.getLanguage() || 'en';
        if (!workflow.trueDataByLanguage) {
          workflow.trueDataByLanguage = {};
        }
        if (!workflow.trueDataByLanguage[currentLanguage]) {
          var elasticsearchDocument = workflow.document.documentContent;
          workflow.trueDataByLanguage[currentLanguage] = documentDisplayValues(elasticsearchDocument.display);
        }
        workflow.trueData = workflow.trueDataByLanguage[currentLanguage];
        return workflow;
      }

      function processReceivedWorkflow(workflow) {
        workflow.originalUpdatedOn = workflow.updatedOn;
        var processedWorkflow = ListViewWorkflowsStore.processAndCache(workflow, processNewWorkflow);
        return activateDisplayByLanguage(processedWorkflow);
      }

      function processNewWorkflow(workflow) {
        var elasticsearchDocument = workflow.document.documentContent;
        workflow.errors = elasticsearchDocument.errors;
        workflow.warnings = elasticsearchDocument.warnings;
        workflow.progress = elasticsearchDocument.progress;
        workflow.comments = elasticsearchDocument.comments;

        var searchParams = ListViewWorkflowsStore.searchParams();
        var documentDefinition = searchParams.documentDefinition;
        var originalDocumentData = workflow.document.documentContent.originalDocumentData;
        var readOnly = true;
        workflow.openDocument = function (loadReferenceLists) {
          return openDocument(
            documentDefinition,
            originalDocumentData,
            workflow.context,
            { readOnly: Boolean(readOnly), doNotLoadRefLists: !loadReferenceLists });
        };
      }

  });
