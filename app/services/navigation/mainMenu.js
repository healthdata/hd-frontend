/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.navigation')
    .factory('MainMenu', MainMenuFactory);

  function MainMenuFactory(gettextCatalog, SkryvSession, PLATFORM, ENVIRONMENT_URL) {
    var mainMenu = [];

    var pageGuide = introJs(); // jshint ignore:line

    function menuItem(item) {
      mainMenu.push(item);
    }

    // =============================//
    // ==== Menu specification ==== //
    // =============================//
    function makeMenu() {
      mainMenu = [];
      var user = SkryvSession.getLoggedUser();

      menuItem({
        name: 'participation',
        url: '#/participation',
        position: 'left',
        auth: 'user',
        label: 'participation'
      });
      menuItem({
        name: 'data collection',
        url: '#/data%20collection',
        position: 'left',
        auth: 'user',
        label: 'data collection'
      });
      menuItem({
        name: 'dashboard',
        url: '#/settings',
        position: 'left',
        auth: 'admin',
        label: gettextCatalog.getString('dashboard')
      });
      menuItem({
        name: 'reporting',
        url: 'http://www.healthstat.be/',
        blank: true,
        position: 'left',
        label: 'reporting'
      });
      menuItem({
        name: 'guide',
        position: 'right',
        label: 'Guide',
        click: function () {
          pageGuide.start();
        }
      });
      menuItem({
        name: 'help',
        position: 'right',
        label: 'help',
        dropdownPosition: 'right',
        dropdownItems: (function () {
          var items = [ {
            url: 'http://support.healthdata.be/customer/portal/topics/764536-use-hd4dp/articles',
            name: 'support',
            blank: true,
            label: 'Support',
            order: 2
          } ];

          if (PLATFORM === 'hd4res') {
            items.push({
              url: ENVIRONMENT_URL + '/pages/stable-data-report',
              name: 'stabledata',
              blank: true,
              label: 'Stable Data',
              order: 1
            });

            items.push({
              url: 'http://support.healthdata.be/customer/portal/emails/new',
              name: 'contact',
              blank: true,
              label: 'Contact',
              order: 3
            });
          }

          if (PLATFORM === 'hd4dp') {
            items.push({
              url: 'mailto:support.healthdata@wiv-isp.be?subject=HD4DP support enquiry',
              name: 'contact',
              blank: false,
              label: 'Contact',
              order: 3
            });
          }

          return items.sort(function (a, b) {
            return a.order - b.order;
          });

        }())
      });
      menuItem({
        name: 'settings',
        url: '#/settings',
        position: 'right',
        auth: 'user',
        label: 'settings'
      });
      menuItem({
        name: 'user',
        position: 'right',
        icon: (function () {
          if (PLATFORM === 'hd4res') {
            return 'fi flaticon-medical';
          } else {
            return 'fa fa-stethoscope';
          }
        }()),
        label: (function () {
          if (user && (user.firstName || user.lastName)) {
            return user.firstName + ' ' + user.lastName;
          } else if (user) {
            return user.username;
          }
        }()),
        dropdownPosition: 'right',
        dropdownItems: [ {
          name: 'logout',
          url: '#/logout',
          icon: 'fa fa-sign-out',
          label: 'Logout'
        } ]
      });

      return mainMenu;
    }

    return {
      mainMenu: makeMenu
    };
  }
})();
