/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.attachment')
    .factory('AttachmentBackend', AttachmentBackendFactory);

  function AttachmentBackendFactory(_, $q, $http, ATTACHMENT_URL) {
    'ngInject';
    return {
      getAttachmentInfo: function (attachmentID) {
        if (!_.isString(attachmentID) || _.isEmpty(attachmentID)) {
          return $q.reject(new Error('getAttachmentInfo: attachmentID is required'));
        }
        return $http.get(ATTACHMENT_URL + 'info/' + attachmentID)
          .then(function (response) {
            return response.data;
          });
      },
      getAttachmentData: function (attachmentID) {
        if (!_.isString(attachmentID) || _.isEmpty(attachmentID)) {
          return $q.reject(new Error('getAttachmentData: attachmentID is required'));
        }
        return $http.get(ATTACHMENT_URL + 'download/' + attachmentID, { responseType: 'arraybuffer' })
          .then(function (response) {
            return response.data;
          }, function (response) {
            return response.data;
          });
      },
      uploadAttachment: function (workflowID, attachmentID, file) {
        if (!workflowID) {
          return $q.reject(new Error('uploadAttachment: workflowID is required'));
        }

        if (!_.isString(attachmentID) || _.isEmpty(attachmentID)) {
          return $q.reject(new Error('uploadAttachment: attachmentID is required'));
        }

        if (!file) {
          return $q.reject(new Error('uploadAttachment: empty file'));
        }

        /*global FormData */
        var formData = new FormData();
        formData.append('workflowId', workflowID);
        formData.append('file', file);

        return $http.post(ATTACHMENT_URL + 'upload/' + attachmentID, formData, {
          headers: {
            'Content-Type': undefined
          }
        }).then(function (response) {
          return response.data;
        }).catch(function (error) {
          return error.data;
        });
      },
      deleteAttachment: function (attachmentID) {
        if (!_.isString(attachmentID) || _.isEmpty(attachmentID)) {
          return $q.reject(new Error('getAttachmentData'));
        }

        return $http.delete(ATTACHMENT_URL + 'delete/' + attachmentID)
          .then(function (response) {
            return response.data;
          }, function (response) {
            return response.data;
          });
      }
    };
  }

})();
