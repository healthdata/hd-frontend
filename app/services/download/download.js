/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  /* global saveAs, Blob */

  angular.module('services.download')
    .factory('Download', DownloadFactory);

  function DownloadFactory() {
    'ngInject';
    return {
      /* Supported options:
       *   - type: the MIME type of the data
       *   - name: the filename to be used
       *   - endings: "native" of "transparant" (See Blob documentation)
       *   - disableAutoBOM: when true, do not automatically add byte order mark
       *                     to uft-8 encoded data
       */
      download: function (data, options) {
        options = options || {};
        var blobOptions = {};
        // By explicitly transferring options only when they are provided, we
        // avoid issues when browsers (e.g., Safari) do not like options with a
        // value of undefined.
        if (options.type) { blobOptions.type = options.type; }
        if (options.endings) { blobOptions.endings = options.endings; }
        var blob = new Blob([ data ], blobOptions);
        return saveAs(blob, options.name, options.disableAutoBOM);
      }
    };
  }

})();
