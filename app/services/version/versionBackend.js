/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.version',
  [ 'skryv.conf', 'ngRoute', 'ngResource' ])

  .factory('SkryvVersion', function ($http, SESSION_SERVER_URL, PLATFORM) {
    'use strict';
    return {
      get: function () {
        return $http.get(SESSION_SERVER_URL + '/about/' + PLATFORM.toUpperCase() + '_VERSION').then(function (response) {
          return response.data;
        });
      }
    };
  });
