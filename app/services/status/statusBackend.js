/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.status')
    .factory('SkryvStatus', function ($q, $http, $log, SESSION_SERVER_URL, Notifications, gettextCatalog) {

      return {
        getStatusMessagesByDate: function (date) {
          var params = (date) ? { params: { createdOn: date } } : {};

          return $http.get(SESSION_SERVER_URL + '/statusmessages', params)
            .then(function (response) {
              return response.data;
            }, function () {
              Notifications.error ('Could not retrieve status messages');
              return $q.reject(gettextCatalog.getString('Could not retrieve status messages'));
            });
        },

        getStatusMessages: function () {
          return $http.get(SESSION_SERVER_URL + '/statistics/registrations').then(function (response) {
            return response.data;
          }, function () {
            Notifications.error ('Could not retrieve status messages');
            return $q.reject(gettextCatalog.getString('Could not retrieve status messages'));
          });
        },
        getStatusAPI: function () {
          return $http.get(SESSION_SERVER_URL + '/status').then(function (response) {
            return response.data;
          }, function () {
            $log.log('Could not get status of API\'s');
            return $q.reject('Could not get status of API\'s');
          });
        }
      };
    });

})();
