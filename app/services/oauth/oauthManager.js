/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.oauth')
    .factory('SkryvOauth', function ($http, OAUTH_SERVER_URL, ORGANIZATION_SERVER_URL, APPLICATION_ID, $q, SkryvSession) {

      function getAccessToken(username, password, clientId, organizationId) {
        return $http.post(OAUTH_SERVER_URL +
          '?username=' + encodeURIComponent(username) +
          '&password=' + encodeURIComponent(password) +
          '&client_id=' + encodeURIComponent(clientId) +
          '&organization_id=' + encodeURIComponent(organizationId) +
          '&grant_type=' + 'organization_password')
          .then(
            function (tokens) {
              // Store user in session
              SkryvSession.storeUserTokens(tokens.data.access_token, tokens.data.refresh_token);
              return $q.resolve();
            },
            function (err) {
              return $q.reject(err);
            });
      }

      function refreshAccessToken() {
        var userInfo = SkryvSession.getUserTokens();

        if (userInfo && userInfo.refreshToken) {
          return $http.post(OAUTH_SERVER_URL +
            '?client_id=' + encodeURIComponent(APPLICATION_ID) +
            '&refresh_token=' + userInfo.refreshToken +
            '&grant_type=' + 'refresh_token')
            .then(
              function (tokens) {
                SkryvSession.storeUserTokens(tokens.data.access_token, tokens.data.refresh_token);
                return $q.resolve();
              },
              function () {
                SkryvSession.destroyUserSession();
                return $q.reject('Unable to refresh token');
              });
        } else {
          SkryvSession.destroyUserSession();
          return $q.reject('No refresh token present');
        }
      }

      function sendResetMail(organizationId, email) {
        return $http.post(ORGANIZATION_SERVER_URL + organizationId + '/users/forgotPassword?email=' + email);
      }

      function resetPassword(organizationId, token, newPassword) {
        return $http.post(ORGANIZATION_SERVER_URL + organizationId + '/users/resetPassword?token=' + token + '&newPassword=' + newPassword);
      }

      return {
        getAccessToken: getAccessToken,
        refreshAccessToken: refreshAccessToken,
        sendResetMail: sendResetMail,
        resetPassword: resetPassword
      };
    });

})();
