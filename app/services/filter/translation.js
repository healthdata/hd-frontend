/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.filter')
    .factory('transFilter', transFilter)
    .filter('sortArray', sortArray);

  function transFilter($filter) {
    'ngInject';
    return {
      sortArray: function (arr) {
        return $filter('sortArray')(arr);
      }
    };
  }

  function sortArray(gettextCatalog) {
    'ngInject';
    return function (array) {
      var translatedArray = [];

      function compare(a, b) {
        if (a.originalName < b.originalName) {
          return -1;
        }
        if (a.originalName > b.originalName) {
          return 1;
        }
        return 0;
      }

      angular.forEach(array, function (item) {
        translatedArray.push({ originalName: item, name: gettextCatalog.getString(item) });
      });

      return translatedArray.sort(compare);
    };
  }
}) ();
