/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

angular.module('services.user')
  .factory('SkryvUserView', function ($q, USER_SERVER_URL, $http, $injector, gettextCatalog) {
    return {
      get: function (id) {
        return $http.get(USER_SERVER_URL + 'views/' + id)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not get views'));
            }
          );
      },
      save: function (id, name, content) {
        return $http.post(USER_SERVER_URL + 'views/' + id + '/' + name, content)
          .then(function (response) {
              return response.data;
            }, function (err) {
              return $q.reject(err);
            }
          );
      },
      update: function (id, name, content) {
        return $http.put(USER_SERVER_URL + 'views/' + id + '/' + name, content)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not update view'));
            }
          );
      },
      delete: function (id, name) {
        return $http.delete(USER_SERVER_URL + 'views/' + id + '/' + name)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not update view'));
            }
          );
      }
    };
  });

})();
