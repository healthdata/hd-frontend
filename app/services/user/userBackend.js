/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.user')
    .factory('SkryvUser', function ($q, USER_SERVER_URL, LDAP_SERVER_URL, $http, $injector, gettextCatalog) {
      return {
        get: function (id) {
          return $http.get(USER_SERVER_URL + id)
            .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('User not found'));
            }
          );
        },
        delete: function (id) {
          return $http.delete(USER_SERVER_URL + id)
            .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not delete user'));
            }
          );
        },
        update: function (user) {
            return $http.put(USER_SERVER_URL + user.id, user)
            .then(function (response) {
              return response.data;
            }, function (err) {
              if (err.data && err.data.error && err.data.error === 'USERNAME_ALREADY_EXISTS') {
                return $q.reject(gettextCatalog.getString('Username already exists'));
              } else if (err.data && err.data.error && err.data.error === 'EMAIL_ALREADY_EXISTS') {
                return $q.reject(gettextCatalog.getString('User email already taken'));
              } else if (err.data && err.data.error && err.data.error === 'LDAP_USER_REQUEST_EXCEPTION') {
                return $q.reject(gettextCatalog.getString('LDAP not configured correctly to update this user'));
              } else if (err.data && err.data.error && err.data.error === 'NON_UNIQUE_SEARCH_RESULT') {
                return $q.reject(gettextCatalog.getString('Username already exists'));
              } else {
                return $q.reject(gettextCatalog.getString('Could not edit user'));
              }
            }
          );
        },
        save: function (user) {
          return $http.post(USER_SERVER_URL, user)
            .then(function (response) {
              return response.data;
            }, function (err) {
              if (err.data && err.data.error && err.data.error === 'USERNAME_ALREADY_EXISTS') {
                return $q.reject(gettextCatalog.getString('Username already exists'));
              } else if (err.data && err.data.error && err.data.error === 'EMAIL_ALREADY_EXISTS') {
                return $q.reject(gettextCatalog.getString('User email already taken'));
              } else if (err.data && err.data.error && err.data.error === 'LDAP_USER_REQUEST_EXCEPTION') {
                return $q.reject(gettextCatalog.getString('LDAP not configured correctly to save this user'));
              } else {
                return $q.reject(gettextCatalog.getString('Could not create user'));
              }
            }
          );
        },
        updatePassword: function (user) {
          return $http.put(USER_SERVER_URL + 'password/' + user.id + '?oldPassword=' + user.oldPassword + '&newPassword=' + user.newPassword)
            .then(function (response) {
              return response.data;
            }, function (error) {
              return $q.reject(error.data);
            });
        },
        updateMetadata: function (user) {
          return $http.put(USER_SERVER_URL + 'metadata/' + user.id, user.metaData)
            .then(function (response) {
              var SkryvSession = $injector.get('SkryvSession');
              SkryvSession.setLoggedUser(response.data);
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not edit user metadata'));
            }
          );
        },
        getLdapUser: function (username) {
          return $http.get(LDAP_SERVER_URL + username)
            .then(function (response) {
                return response.data;
              }, function () {
                return $q.reject(gettextCatalog.getString('User not found'));
              }
            );
        },
        getAllLdapUsers: function (search) {
          return $http.get(LDAP_SERVER_URL + '?search=' + search)
            .then(function (response) {
                return response.data;
              }, function () {
                return $q.reject(gettextCatalog.getString('Users not found'));
              }
            );
        }
      };
    });

})();
