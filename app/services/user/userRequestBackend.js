/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

angular.module('services.user')
  .factory('SkryvUserRequest', function ($q, USER_REQUEST_SERVER_URL, $http, gettextCatalog) {
    return {
      get: function (id) {
        return $http.get(USER_REQUEST_SERVER_URL + id)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('User request not found'));
            }
          );
      },
      save: function (request) {
          return $http.post(USER_REQUEST_SERVER_URL, request)
          .then(function (response) {
              return response.data;
            }, handleError);
      },
      update: function (request) {
          return $http.put(USER_REQUEST_SERVER_URL + request.id, request)
          .then(function (response) {
              return response.data;
            }, handleError);
      },
      accept: function (id) {
          return $http.post(USER_REQUEST_SERVER_URL + id + '/accept')
          .then(function (response) {
              return response.data;
            }, handleError);
      },
      reject: function (id) {
        return $http.post(USER_REQUEST_SERVER_URL + id + '/reject')
          .then(function (response) {
              return response.data;
            }, function () {
            return $q.reject(gettextCatalog.getString('Could not reject user request'));
            }
          );
      },
      getAll: function (params) {
        return $http.get(USER_REQUEST_SERVER_URL, { params: params })
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not load user requests'));
            }
          );
      }
    };

    function handleError(err) {
      if (err.data && err.data.error_description) {
        return $q.reject(gettextCatalog.getString(err.data.error_description));
      } else {
        return $q.reject(gettextCatalog.getString('Could not edit user'));
      }
    }
  });

})();
