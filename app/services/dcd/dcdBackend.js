/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.dcd')
    .factory('SkryvDefinition', SkryvDefinition)
    .factory('DataCollectionGroupManager', DataCollectionGroupManager)
    .factory('SkryvDefinitionManager', SkryvDefinitionManager);

  // Data collection definition calls for HD4DP
  function SkryvDefinition($http, SESSION_SERVER_URL, $q, gettextCatalog) {
    'ngInject';

    var url = SESSION_SERVER_URL + '/datacollectiondefinitions';

    return {
      // Returns all published versions with the highest minor version for each major version
      getVersions: function (name) {
        return $http.get(url + '/latestmajors/' + name)
          .then(function (response) {
            return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not get data collections'));
            }
          );
      },
      get: function (id) {
        return $http.get(url + '/' + id)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not get data collection'));
            }
          );
      },
      // Returns the version with the highest major and minor version for dataCollectionName
      getLatest: function (name) {
        return $http.get(url + '/latest/' + name)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('No data collection found with name: ') + name);
            }
          );
      }
    };
  }

  function DataCollectionGroupManager($http, moment, _, ENVIRONMENT_URL) {
    var url = ENVIRONMENT_URL + '/datacollectiongroups/manager';

    return {
      getById: function (id) {
        return $http.get(url + '/' + id)
          .then(function (response) {
            var group = response.data;
            group.active = collectionGroupActive(group);
            return group;
          });
      },

      getByName: function (name) {
        return $http.get(url + '/name/' + name)
          .then(function (response) {
            return response.data;
          });
      },

      getAll: function () {
        return $http.get(url)
          .then(function (response) {
            var collectionGroups = response.data;
            _.each(collectionGroups, function (collectionGroup) {
              collectionGroup.active = collectionGroupActive(collectionGroup);
            });
            return collectionGroups;
          });
      },

      createGroup: function (group) {
        return $http.post(url, group)
          .then(function (response) {
            return response.data;
          });
      },

      updateGroup: function (group) {
        return $http.put(url + '/' + group.dataCollectionGroupId, group)
          .then(function (response) {
            return response.data;
          });
      },

      deleteGroup: function (id) {
        return $http.delete(url + '/' + id)
          .then(function (response) {
            return response.data;
          });
      },

      publishGroup: function (id) {
        return $http.post(url + '/' + id + '/publish')
          .then(function (response) {
            return response.data;
          });
      },

      republishGroup: function (id) {
        return $http.post(url + '/' + id + '/republish')
          .then(function (response) {
            return response.data;
          });
      }
    };

    function collectionGroupActive(collectionGroup) {
      // Set all hours to 0, because we only need the date
      var today = new Date().setHours(0,0,0,0);
      var start = (collectionGroup.startDate ? new Date(collectionGroup.startDate).setHours(0,0,0,0) : undefined);
      var creation = (collectionGroup.endDateCreation ? new Date(collectionGroup.endDateCreation).setHours(0,0,0,0) : undefined);
      var submission = (collectionGroup.endDateSubmission ? new Date(collectionGroup.endDateSubmission).setHours(0,0,0,0) : undefined);
      var comments = (collectionGroup.endDateComments ? new Date(collectionGroup.endDateComments).setHours(0,0,0,0) : undefined);

      if (!start) {
        return 'inactive';
      }

      if (start && today < start) {
        return 'not started';
      }

      if (comments && comments <= today) {
        return 'finished';
      }

      if (submission && submission <= today) {
        return 'active, no new submissions';
      }

      if (creation && creation <= today) {
        return 'active, no new creations';
      }

      return 'active';
    }

  }

  // Data collection definition calls for HD4RES
  function SkryvDefinitionManager($http, SESSION_SERVER_URL, $q, gettextCatalog) {
    'ngInject';

    var url = SESSION_SERVER_URL + '/datacollectiondefinitions/manager/';

    return {
      getAll: function (params) {
        return $http.get(url, { params: params })
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not get data collections'));
            }
          );
      },
      getMajors: function () {
        return $http.get(url + 'latestmajors')
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not get data collections'));
            }
          );
      },
      get: function (id) {
        return $http.get(url + id)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not get data collection'));
            }
          );
      },
      getByName: function (name) {
        return $http.get(url + 'name/' + name)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not get data collections'));
            }
          );
      },

      getHistoryById: function (id) {
        return $http.get(url + id + '/history')
          .then(function (response) {
            return response.data;
          });
      },

      delete: function (id) {
        return $http.delete(url + id)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Data collection could not be deleted'));
            }
          );
      },

      save: function (collection) {
        return $http.post(url, collection)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('It was not possible to save the data collection'));
            }
          );
      },

      publish: function (id) {
        return $http.post(url + id + '/publish')
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('It was not possible to publish the data collection'));
            }
          );
      },

      republish: function (id) {
        return $http.post(url + id + '/republish')
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('It was not possible to republish the data collection'));
            }
          );
      },

      update: function (collection) {
        return $http.put(url + collection.dataCollectionDefinitionId, collection)
          .then(function (response) {
              return response.data;
            }, function (err) {
              if (err && err.data && err.data.error && err.data.error === 'INVALID_TIMING_CONDITION') {
                return $q.reject(gettextCatalog.getString('Timing condition must contain a number followed by a timing unit'));
              }
              return $q.reject(gettextCatalog.getString('It was not possible to update the data collection'));
            }
          );
      },
      createWip: function (collectionId, wip) {
        return $http.post(url + collectionId + '/history', wip)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('It was not possible to save your changes'));
            }
          );
      }
    };
  }

})();
