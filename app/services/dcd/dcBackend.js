/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.dcd')
    .factory('SkryvCollection', SkryvCollection);

  function SkryvCollection($q, SESSION_SERVER_URL, gettextCatalog, $http, _) {
    'ngInject';

    var url = SESSION_SERVER_URL;
    return {
      getDataCollectionGroups: function () {
        return $http.get(url + '/datacollectiongroups/list').then(function (response) {
          return response.data;
        });
      },

      getDataCollectionGroup: function (id) {
        return $http.get(url + '/datacollectiongroups/' + id).then(function (response) {
          return response.data;
        });
      },

      getCollections: function () {
        return $http.get (url + '/datacollections')
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject('No data collections found');
            }
          );
      },

      getByOrganization: function (organization) {
        return $http.post(url + '/datacollections/organization', organization)
          .then(function (response) {
              var keys = _.keys(response.data);
              var collections = [];
              keys.forEach(function (key) {
                var collection = {
                  name: key,
                  children: response.data[key]
                };

                collections.push(collection);
              });

              return _.sortBy(collections, function (collection) {
                return collection.name;
              });
            }, function () {
              return $q.reject(gettextCatalog.getString('No data collections found for organization: ') + organization.name);
            }
          );
      }
    };
  }

})();
