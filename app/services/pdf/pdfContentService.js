/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('services.pdf')
.factory('SkryvPdf', function ($q, $http, gettextCatalog, WorkflowRequest, Handlebars, PdfTemplateBackend) {
  'use strict';

  function getWorkflowPdf(workflow) {
    var defer = $q.defer();

    // ES query to get a single workflow
    var query = {
      query: {
        filtered: {
          filter: {
            bool: {
              must: [
                { term: {
                    id: parseInt(workflow.id)
                  }
                }
              ]
            }
          }
        }
      }
    };

    // Get template for data collection
    PdfTemplateBackend.get(workflow.dataCollectionDefinitionId).then(function (result) {
      var source = result.template || result;
      // Get workflow ES document
      WorkflowRequest.getByDatacollection(workflow.dataCollectionDefinitionId, query).then(function (workflows) {
        try {
          var template = Handlebars.compile(source);
          var hbrs = template({ document: workflows.hits.hits[0]._source.document.documentContent.display });
          defer.resolve(hbrs);
        } catch (e) {
          defer.reject('Template content could not be parsed');
          throw e; // rethrow to not marked as handled
        }
      }, function (err) {
        defer.reject(err);
      });
    }, function (err) {
      defer.reject(err);
    });

    return defer.promise;
  }

  return {
    getWorkflowPdf: getWorkflowPdf
  };
});
