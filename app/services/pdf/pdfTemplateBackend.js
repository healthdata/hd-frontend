/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('services.pdf')
  .factory('PdfTemplateBackend', function ($q, $http, gettextCatalog, SESSION_SERVER_URL) {
    'use strict';

    function get(id) {
      return $http.get(SESSION_SERVER_URL + '/pdf/templates/' + id)
        .then(function (response) {
            return response.data;
          }, function () {
            return $q.reject(gettextCatalog.getString('Could not get pdf template'));
          }
        );
    }

    return {
      get: get
    };
  });
