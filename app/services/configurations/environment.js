/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('services.configurations')
    .factory('EnvironmentConfig', EnvironmentConfig);

  function EnvironmentConfig($q, SkryvConfig) {
    'ngInject';

    var ddeEnabled;
    var publishEnabled;

    function isDdeEnabled() {
      var delay = $q.defer();
      if (ddeEnabled) {
        delay.resolve(ddeEnabled === 'true');
      } else {
        SkryvConfig.getConfigByKey('USE_DATA_DEFINITION_EDITOR').then(function (config) {
          if (config[0] && config[0].value) {
            ddeEnabled = config[0].value.toLowerCase();
            delay.resolve(ddeEnabled === 'true');
          } else {
            delay.resolve(false);
          }
        }, function (err) {
          delay.reject(err);
        });
      }
      return delay.promise;
    }

    function isPublishEnabled() {
      var delay = $q.defer();
      if (publishEnabled) {
        delay.resolve(publishEnabled === 'true');
      } else {
        SkryvConfig.getConfigByKey('USE_DATA_DEFINITION_PUBLISH').then(function (config) {
          if (config[0] && config[0].value) {
            publishEnabled = config[0].value.toLowerCase();
            delay.resolve(publishEnabled === 'true');
          } else {
            delay.resolve(false);
          }
        }, function (err) {
          delay.reject(err);
        });
      }
      return delay.promise;
    }
    return {
      isDdeEnabled: isDdeEnabled,
      isPublishEnabled: isPublishEnabled
    };
  }
})();
