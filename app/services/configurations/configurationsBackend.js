/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

angular.module('services.configurations')
  .factory('SkryvConfig', function ($q, SESSION_SERVER_URL, $http, gettextCatalog) {

    'use strict';

    var url = SESSION_SERVER_URL + '/configurations';

    return {
      deleteConfig: function (id) {
        return $http.delete(url + '/' + id)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Configuration could not be deleted'));
            }
          );
      },
      getConfig: function () {
        return $http.get(url)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('No configurations found'));
            }
          );
      },
      getConfigByKey: function (key) {
        return $http.get(url + '?key=' + key)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('No configurations found'));
            }
          );
      },
      editConfig: function (config) {
        return $http.put(url + '/' + config.id, config)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not update configuration'));
            }
          );
      },
      storeConfig: function (config) {
        return $http.post(url, config)
          .then(function (response) {
              return response.data;
            }, function () {
              return $q.reject(gettextCatalog.getString('Could not create configuration'));
            }
          );
      }
    };
  });
