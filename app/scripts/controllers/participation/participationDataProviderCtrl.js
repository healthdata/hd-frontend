/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('skryv.participation')
    .controller('DataProviderParticipationCtrl', Controller);

  function Controller(_, gettextCatalog, ngDialog, $location, $q, $routeParams, $scope,
                      CurrentParticipationModel, Notifications, SkryvParticipation,
                      SkryvSession, SkryvCollection, SkryvView, WorkflowRequest) {

    $scope.collections = SkryvSession.getCollections();
    $scope.hiddenSections = [];

    $scope.hide = function (id) {
      if ($scope.hiddenSections.indexOf(id) > -1) {
        $scope.hiddenSections = _.without($scope.hiddenSections, id);
      } else {
        $scope.hiddenSections.push(id);
      }
    };

    $scope.participate = function () {
      SkryvParticipation.action({
        'action': 'START',
        'workflowType': 'PARTICIPATION',
        'dataCollectionDefinitionId': $scope.selectedVersion.dataCollectionGroupId
      }).then(function (participation) {
        updateCollections().then(function () {
          Notifications.success(gettextCatalog.getString('Participation started'));
        });
      }, function (err) {
        if (err && err.data && err.data.error === 'CREATE_DUPLICATE_KEY') {
          Notifications.error(gettextCatalog.getString('Participation already started'));
        } else if (err && err.data && err.data.error) {
          Notifications.error(err.data.error_description);
        } else {
          Notifications.error(gettextCatalog.getString('Failed starting participation'));
        }
      });
    };

    $scope.stop = function () {
      SkryvParticipation.action({
        'action': 'STOP',
        'workflowType': 'PARTICIPATION',
        'dataCollectionDefinitionId': $scope.selectedVersion.dataCollectionGroupId,
        'workflowId': $scope.participationDetails.id
      }).then(function () {
        updateCollections().then(function () {
          Notifications.success(gettextCatalog.getString('Participation finalized'));
        });
      }, function () {
        Notifications.error(gettextCatalog.getString('Failed finalizing participation'));
      });
    };

    $scope.finalize = function () {
      if ($scope.participationDetails.status === 'IN_PROGRESS' && $scope.hasContent) {
        showFinalizeSubmitWarningDialog();
        return;
      }

      checkForOpenRegistrations($scope.selectedVersion).then(function (res) {
        // continue with usual flow
        console.log("derp");
        showFinalizeDialog();
      }, function () {
        showFinalizeWarningDialog();
      });
    };

    $scope.reopen = function () {
      SkryvParticipation.action({
        'action': 'START',
        'workflowType': 'PARTICIPATION',
        'dataCollectionDefinitionId': $scope.selectedVersion.dataCollectionGroupId,
        'workflowId': $scope.participationDetails.id
      }).then(function (participation) {
        updateCollections().then(function () {
          Notifications.success(gettextCatalog.getString('Participation reopened'));
        });
      }, function () {
        Notifications.error(gettextCatalog.getString('Failed reopening participation'));
      });
    };

    $scope.save = function () {
      CurrentParticipationModel.save().then(function (participation) {
        // Update participation on scope
        $scope.participationDetails = participation;
        Notifications.success(gettextCatalog.getString('Participation registration saved'));
      }, function () {
        Notifications.error(gettextCatalog.getString('Failed saving participation registration'));
      });
    };

    $scope.submit = function () {
      if (!CurrentParticipationModel.isValidDocument()) {
        return Notifications.error(gettextCatalog.getString('Please fill in the registration correctly'));
      } else {
        CurrentParticipationModel.save().then(function () {
          CurrentParticipationModel.submit().then(function (participation) {
            // Update participation on scope
            $scope.participationDetails = participation;
            Notifications.success(gettextCatalog.getString('Participation registration submitted'));
          }, function () {
            Notifications.error(gettextCatalog.getString('Failed submitting participation registration'));
          });
        }, function () {
          Notifications.error(gettextCatalog.getString('Failed saving participation registration'));
        });
      }
    };

    $scope.goToDataCollection = function () {
      $location.path('/data collection').search({});
    };

    $scope.goToSubDataCollection = function (id) {
      $location.path('/data collection').search({collectionId: id});
    };
    /* ###################
     *   helper functions
     ################### */

    function updateCollections() {
      $scope.isUpdatingCollection = true;
      return SkryvSession.updateCollections().then(function () {
        $scope.collections = SkryvSession.getCollections();

        var result = {};
        $scope.collections.forEach(function (collection) {
          collection.versions.forEach(function (version) {
            if (version.dataCollectionGroupId === $scope.selectedVersion.dataCollectionGroupId) {
              result.version = version;
              result.group = collection;
              return;
            }
          });
        });

        $scope.selectVersion(result.version, result.group);
        $scope.isUpdatingCollection = false;
      });
    }

    function initParticipation(version) {
      $scope.isLoading = true;
      $scope.participationModel = undefined;
      $scope.participationDetails = undefined;
      $scope.participationInfo = undefined;
      $scope.validStartDate = !checkCollectionDate(version, 'startDate');
      $scope.validEndDate = checkCollectionDate(version, 'endDateCreation');

      if (version.participationId) {
        SkryvParticipation.get(version.participationId)
          .then(function (details) {
            $scope.isLoading = false;

            $scope.participationDetails = details;

            return details;
          })
          .then(function (details) {
            // grab definition from participationContent
            SkryvCollection.getDataCollectionGroup(version.dataCollectionGroupId)
              .then(function (group) {
                if (group.participationContent) {
                  $scope.hasContent = true;

                  // Select participation info
                  $scope.participationInfo = group.participationContent['participation-info'];

                  SkryvParticipation.getDocument(details.id)
                    .then(function (document) {
                      $scope.isLoading = false;
                      // Checks if the user has switched collections in the meantime
                      if (version.dataCollectionGroupId === $scope.selectedVersion.dataCollectionGroupId) {
                        setParticipationModel(document, details, group.participationContent);
                      }
                    }, function (error) {
                      $scope.isLoading = false;
                      // no document
                      if (error && error.data && error.data.error === 'GET_NON_EXISTING') {
                        setParticipationModel({}, details, group.participationContent);
                      }
                    });
                } else {
                  $scope.hasContent = false;
                }
              }, function (err) {
                $scope.isLoading = false;
              });
          });
      } else {
        $scope.isLoading = false;
        $scope.participationModel = undefined;
      }
    }

    function checkCollectionDate(version, date) {
      if (version && version[date]) {
        var check = new Date(version[date]);
        var today = new Date();

        return check > today;
      }

      return true;
    }

    function checkForOpenRegistrations(version) {
      var promises = [];

      var deferred = $q.defer();

      version.dataCollectionDefinitions.forEach(function (definition) {
        WorkflowRequest.getByDatacollection(definition.dataCollectionDefinitionId).then(function (data) {
          var hits = data.hits.hits;

          var found = _.some(hits, function (entry) {
            var source = entry._source;
            return source.status === 'IN_PROGRESS';
          });
          if (found) {
            deferred.reject(definition);
          }

          deferred.resolve(definition);
        });

        promises.push(deferred.promise);
      });

      return $q.all(promises);
    }

    function showFinalizeDialog() {
      ngDialog.open({
        template: 'views/dialog/participation/finalize.html',
        showClose: false,
        scope: $scope,
        controller: [ '$scope', function (scope) {
          scope.confirm = function () {
            $scope.stop();
            ngDialog.close();
          };
        } ]
      });
    }

    function showFinalizeWarningDialog() {
      ngDialog.open({
        template: 'views/dialog/participation/finalizeWarning.html',
        showClose: false,
        scope: $scope,
        controller: ['$scope', function (scope) {
          scope.confirm = function () {
            $scope.stop();
            ngDialog.close();
          };
        }]
      });
    }

    function showFinalizeSubmitWarningDialog() {
      ngDialog.open({
        template: 'views/dialog/participation/info.html',
        showClose: true
      });
    }

    function setParticipationModel(document, participationDetails, version) {
      CurrentParticipationModel.closeCurrentDocument();
      CurrentParticipationModel.loadDocument(document, participationDetails, version);
      $scope.participationModel = CurrentParticipationModel.currentParticipationModel();
    }

    // TODO(arno): This should be in a util service.
    function locateDefinition(groups, id) {
      var result = undefined;
      // We iterate over the collections, then over the versions and finally over the definitions,
      // if the definitionId of a definition is equal to given id, we have found what we're looking for and
      // the lookup will end and return result.
      _.find(groups, function (group) {
        return _.find(group.versions, function (version) {
          return _.find(version.dataCollectionDefinitions, function (collection) {
            if (collection.dataCollectionDefinitionId !== id) {
              return false
            }

            result = {
              group: group,
              version: version,
              collection: collection
            };

            return true;
          });
        });
      });

      return result;
    }

    function updateLastView() {
      if ($scope.selectedVersion.dataCollectionDefinitions.length > 0) {
        SkryvView.setLastView({dataCollectionDefinitionId: $scope.selectedVersion.dataCollectionDefinitions[0].dataCollectionDefinitionId});
      }
    }

    // FIXME(arno): this should be in a util service.
    // Duplicated in participationCtrl & participationDataProviderCtrl...
    $scope.selectLatestVersion = function (group) {
      var latestVersion = _.max(group.versions, function (version) {
        return version.majorVersion;
      });
      $scope.selectVersion(latestVersion, group);
    };

    $scope.selectGroup = function (group) {
      $scope.selectedGroup = group;
      if (group.versions.length < 1) {
        Notifications.error(gettextCatalog.getString('No versions available'));
      } else {
        // select the last version
        $scope.selectLatestVersion(group);
      }
    };

    $scope.selectVersion = function (version, group) {
      $scope.selectedGroup = group;
      $scope.selectedVersion = version;

      initParticipation(version);

      var view = SkryvView.getLastView();
      // We only want to update the view if the user select a different group, this way we preserve
      // the previously selected child collection.
      var result = locateDefinition([$scope.selectedGroup], view.dataCollectionDefinitionId);
      if (!result) {
        updateLastView();
      }
    };

    /* ######################
     initalize view
     ##################### */

    // TODO(arno): refactor
    // This whole function can go into a util service.
    // The $routeParams cases can be made generic, since they work all the same on different screens.
    // Duplicated in listViewCtrl & participationCtrl
    if ($routeParams.id) {
      try {
        var id = JSON.parse($routeParams.id);
        var found = locateDefinition($scope.collections, id);
        $scope.selectLatestVersion(found.group);
      } catch (exception) {
      }

    } else {
      /* Initialize view! */
      // Get last view from user
      var lastView = SkryvView.getLastView();

      if (lastView && lastView.dataCollectionDefinitionId) {
        var found = locateDefinition($scope.collections, lastView.dataCollectionDefinitionId);
        // If we found something, set the group, set the latest version & just set the first collection of the version.
        if (found) {
          if (found.version) {
            $scope.selectVersion(found.version, found.group);
          } else {
            $scope.selectLatestVersion(found);
          }

        }
      }
    }

    if (!$scope.selectedGroup) {
      // Just select first group by default
      $scope.selectGroup(_.sortBy($scope.collections, function (collection) {
        return collection.name.toLowerCase();
      })[0]);
    }
  }
})();
