/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('skryv.participation')
    .filter('collectionsKeyword', function (_) {
      return function (input, text) {
        if (!text) {
          return input;
        }

        text = text.toLowerCase();

        return input.filter(function (entry) {
          return entry.name.toLowerCase().indexOf(text) > -1 ? entry : undefined;
        });
      };
    }).filter('organizationsKeyword', function () {
    return function (input, text) {
      if (!text) {
        return input;
      }

      text = text.toLowerCase();

      return input.filter(function (entry) {
        var name = entry.name || entry.identificationName;
        var type = entry.type || entry.identificationType;
        var value = entry.value.toString() || entry.entry.healthDataIdentification.value.toString();

        name = name.toLowerCase();
        type = type.toLowerCase();

        if (name.indexOf(text) > -1 || type.indexOf(text) > -1 || value.indexOf(text) > -1) {
          return entry;
        }
      });
    };
  }).filter('activeCollections', function () {
    return function (input) {
      return input.filter(function (entry) {
        if (entry.status.toLowerCase().indexOf('inactive') === -1) {
          return entry;
        }
      });
    };
  }).filter('userKeyword', function () {
    return function (input, text) {
      if (!text) {
        return input;
      }

      text = text.toLowerCase();

      return input.filter(function (entry) {
        var fullName = (entry.firstName + ' ' + entry.lastName).toLowerCase();

        if (fullName.indexOf(text) > -1) {
          return entry;
        }

        if (entry.email && entry.email.toLowerCase().indexOf(text) > -1) {
          return entry;
        }
      });
    };
  }).filter('dataProviderFilter', function (_) {
    return function (input, selectedDataProviders) {
      if (selectedDataProviders.length === 0) {
        return input;
      }

      return input.filter(function (entry) {
        var value = entry.value || entry.healthDataIdentification.value;
        var found = _.where(selectedDataProviders, { value: value }).length > 0;

        if (found) {
          return entry;
        }
      });
    };
  }).filter('participationActive', function () {
    return function (input) {
      if (input.startedOn && !input.completedOn) {
        return 'participating';
      } else if (input.completedOn) {
        return 'finalised';
      }
    };
  }).filter('participationStatus', function () {
    return function (input, statuses) {
      if (!input) {
        return input;
      }

      return input.filter(function (entry) {
        if (statuses[entry.status]) {
          return entry;
        }
      });
    };
  });

})();
