/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('skryv.participation')
    .controller('ParticipationCtrl', Controller);

  function Controller(_, $q, $scope, $filter, $location, $routeParams, SkryvSession, SkryvDefinition, HD4DPUsers, SkryvParticipation,
                      CurrentParticipationModel, SkryvView, SkryvCollection, dataCollectionManager, ngDialog, Notifications, gettextCatalog) {

    /* Initialize scope-variables */
    $scope.collections = SkryvSession.getCollections().map(function (collection) {
      collection.versions.map(function (version) {
        version.status = $filter('collectionActive')(version);
        return version;
      });
      return collection;
    });
    $scope.activeFilters = {
      participating: true,
      nonParticipating: true
    };
    $scope.dataCollectionUsers = [];
    $scope.dataProviders = [];
    $scope.dataProvidersLimit = 5;
    $scope.hiddenSections = [];
    $scope.selectedDataProviders = [];
    $scope.selectedUsers = [];

    $scope.selectGroup = function (group) {
      $scope.selectedGroup = group;
      // Clear the selectedVersion since we only want to display the group
      $scope.selectedVersion = undefined;
      if (group && group.versions && group.versions.length > 0 && group.versions[0].dataCollectionDefinitions && group.versions[0].dataCollectionDefinitions.length > 0) {
        updateLastView($scope.selectedGroup.versions[0].dataCollectionDefinitions[0].dataCollectionDefinitionId);
      }

      return initGroup(group);
    };

    $scope.selectVersion = function (version) {
      $scope.selectedVersion = version;
      if (version.dataCollectionDefinitions && version.dataCollectionDefinitions.length > 0) {
        updateLastView(version.dataCollectionDefinitions[0].dataCollectionDefinitionId);
      }
      initVersion(version);
    };

    $scope.selectDataProvider = function (dataProvider) {
      if ($scope.selectedDataProviders.indexOf(dataProvider) > -1) {
        $scope.selectedDataProviders = _.without($scope.selectedDataProviders, dataProvider);
      } else {
        $scope.selectedDataProviders.push(dataProvider);
      }
    };

    $scope.increaseDataProvidersLimit = function () {
      $scope.dataProvidersLimit += 5;
    };

    $scope.isDataProviderSelected = function(dataProvider) {
      return $scope.selectedDataProviders.indexOf(dataProvider) > -1;
    };

    $scope.selectUser = function (user) {
      if ($scope.selectedUsers.indexOf(user) > -1) {
        $scope.selectedUsers = _.without($scope.selectedUsers, user);
      } else {
        $scope.selectedUsers.push(user);
      }
    };

    $scope.isUserSelected = function(user) {
      return $scope.selectedUsers.indexOf(user) > -1;
    };

    $scope.hide = function (id) {
      if ($scope.hiddenSections.indexOf(id) > -1) {
        $scope.hiddenSections = _.without($scope.hiddenSections, id);
      } else {
        $scope.hiddenSections.push(id);
      }
    };

    // Use this function to clear any array referenced on $scope,
    // e.g. clearArray($scope.dataProviders)
    $scope.clearArray = function (array) {
      array.length = 0;
    };

    $scope.goToList = function () {
      if ($scope.selectedVersion) {
        $location.path('/data collection').search({ id: $scope.selectedVersion.id });
      } else {
        $location.path('/data collection').search({ id: $scope.selectedGroup.dataCollectionGroupId });
      }
    };

    $scope.goToStatus = function () {
      $location.path('/status/report').search({ name: $scope.selectedGroup.name });
    };

    $scope.notifyUsers = function () {
      var selectedUsers;
      var addresses = '';

      if ($scope.selectedDataProviders.length !== 0 && $scope.selectedUsers.length === 0) {
        selectedUsers = $filter('dataProviderFilter')($scope.dataCollectionUsers, $scope.selectedDataProviders);
      } else if ($scope.selectedUsers.length > 0) {
        selectedUsers = $scope.selectedUsers;
      } else {
        selectedUsers = $scope.dataCollectionUsers;
      }

      selectedUsers.forEach(function (entry) {
        if (entry.email) {
          addresses += entry.email + '\n';
        }
      });

      // Open up a dialog that shows all the emails
      ngDialog.open({
        template: 'views/dialog/participation/notify.html',
        controller: [ '$scope', 'gettextCatalog', function ($scope, gettextCatalog) {
          $scope.selectedUsers = selectedUsers;
          $scope.addresses = addresses;

          $scope.copyToClipboard = function () {
            angular.element('#email-box').select();
            document.execCommand('copy');

            Notifications.success(gettextCatalog.getString('Copied!'));
          };
        } ]
      });
    };

    /* Helper functions */
    function initGroup(group) {
      _resetInternalState();

      var promises = [];

      $scope.isLoadingDataProviders = true;
      var dataProvidersPromise = dataCollectionManager.getDataProvidersForDataCollection(group.name)
        .then(function (response) {
          response = response.map(formatDataProvider);
          $scope.dataProviders = response;
          $scope.isLoadingDataProviders = false;
        }, function (err) {
          Notifications.error(gettextCatalog.getString('Could not load dataproviders'));
          $scope.isLoadingDataProviders = false;
        });
      promises.push(dataProvidersPromise);

      $scope.isLoadingUsers = true;
     var hd4dpPromise = HD4DPUsers.getHD4DP({ dataCollectionName: group.name })
        .then(function (res) {
          $scope.dataCollectionUsers = res;
          $scope.isLoadingUsers = false;
        }, function (err) {
          Notifications.error(gettextCatalog.getString('Could not load users'));
          $scope.isLoadingUsers = false;
        });
     promises.push(hd4dpPromise);

     return $q.all(promises);
    }

    function initVersion(version) {
      _resetInternalState();

      $scope.isLoadingDataProviders = true;
      var dataProviders = [];
      var promises = [];
      var nonParticipating = SkryvParticipation.getNonParticipants({ dataCollectionDefinitionId: version.dataCollectionGroupId })
        .then(function (response) {
          response = response.map(function (entry) {
            entry = formatDataProvider(entry);
            entry.status = 'nonParticipating';

            return entry;
          });

          dataProviders = dataProviders.concat(response);
          return response;
        });

      var participating = SkryvParticipation.getAll({ dataCollectionDefinitionId: version.dataCollectionGroupId })
        .then(function (response) {
          response = response.map(function (entry) {
            entry = formatDataProvider(entry);
            entry.status = $filter('participationActive')(entry);

            SkryvCollection.getDataCollectionGroup(version.dataCollectionGroupId)
              .then(function (group) {
                return SkryvParticipation.getDocument(entry.id).then(function (res) {
                  CurrentParticipationModel.loadDocument(res, entry, group.participationContent);
                  entry.participationDocument = CurrentParticipationModel.currentParticipationModel();
                });
              });

            return entry;
          });
          dataProviders = dataProviders.concat(response);
          return response;
        });

      promises.push(nonParticipating);
      promises.push(participating);

      $q.all(promises).then(function () {
        $scope.isLoadingDataProviders = false;
        $scope.isLoadingUsers = true;
        $scope.selectedVersion.relevantDataProviders = dataProviders;

        // grab all the users
        var userPromises = [];
        var users = [];

        dataProviders.forEach(function (provider) {
          var promise = HD4DPUsers.getHD4DP({
            identificationType: provider.identificationType,
            identificationValue: provider.value,
            dataCollectionName: $scope.selectedGroup.name
          }).then(function (response) {
            users = _.uniq(users.concat(response), function (entry) {
              return entry.id;
            });
          });

          userPromises.push(promise);
        });

        $q.all(userPromises).then(function () {
          $scope.isLoadingUsers = false;
          $scope.dataCollectionUsers = users;
        }, function () {
          Notifications(gettextCatalog.getString('Couldn\'t fetch users'));
        });
      });
    }

    function _resetInternalState() {
      $scope.dataCollectionUsers = [];
      $scope.dataProviders = [];
      $scope.dataProvidersLimit = 5;
      $scope.selectedDataProviders = [];
    }

    function formatDataProvider(dataProvider) {
      dataProvider.label = dataProvider.name || dataProvider.identificationName;
      dataProvider.value = dataProvider.value || dataProvider.identificationValue;
      delete dataProvider.identificationName;
      delete dataProvider.identificationValue;

      return dataProvider;
    }

    function updateLastView(id) {
      SkryvView.setLastView({ dataCollectionDefinitionId: id });
    }

    // TODO(arno): refactor
    // This whole function can go into a util service.
    // The $routeParams cases can be made generic, since they work all the same on different screens.
    // Duplicated in listViewCtrl & participationDataProviderCtrl
    function locateDefinition(groups, id) {
      var result = undefined;
      // We iterate over the collections, then over the versions and finally over the definitions,
      // if the definitionId of a definition is equal to given id, we have found what we're looking for and
      // the lookup will end and return result.
      _.find(groups, function (group) {
        if (group.dataCollectionGroupId === id) {
          result = {
            group: group
          };

          return true
        }

        return _.find(group.versions, function (version) {
          return _.find(version.dataCollectionDefinitions, function (collection) {
            if (collection.dataCollectionDefinitionId !== id) {
              return false
            }

            result = {
              group: group,
              version: version,
              collection: collection
            };

            return true;
          });
        })
      });

      return result;
    }

    // FIXME(arno): this should be in a util service.
    // Duplicated in participationCtrl & participationDataProviderCtrl...
    $scope.selectLatestVersion = function (group) {
      var latestVersion = _.max(group.versions, function (version) {
        return version.majorVersion;
      });
      $scope.selectVersion(latestVersion);
    };

    if ($routeParams.id) {
      try {
        var id = JSON.parse($routeParams.id);
        var found = locateDefinition($scope.collections, id);
        $scope.selectGroup(found.group);

        if (found.version) {
          $scope.selectVersion(found.version);
        } else {
          $scope.selectLatestVersion(found.group);
        }
      } catch (exception) {
      }

    } else {
      /* Initialize view! */
      // Get last view from user
      var lastView = SkryvView.getLastView();

      if (lastView && lastView.dataCollectionDefinitionId) {
        var found = locateDefinition($scope.collections, lastView.dataCollectionDefinitionId);
        // If we found something, set the group, set the latest version & just set the first collection of the version.
        if (found) {
          $scope.selectGroup(found.group);
          if (found.version) {
            $scope.selectVersion(found.version);
          } else {
            $scope.selectLatestVersion(found.group);
          }
        }
      }
    }

    if (!$scope.selectedGroup) {
      $scope.selectGroup(_.sortBy($scope.collections, function (collection) {
        return collection.name.toLowerCase();
      })[0]);
      $scope.selectLatestVersion($scope.selectedGroup);
    }

  }

})();
