/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.collection')
  .controller('collectionGroupListCtrl', function ($scope, $location, _, DataCollectionGroupManager, Notifications, PLATFORM, EnvironmentConfig) {
    'use strict';

    $scope.loading = true;
    $scope.platform = PLATFORM;
    $scope.showLatestVersionOnly = true;
    $scope.allCollectionGroups = [];
    $scope.latestCollectionGroups = [];
    $scope.columnSort = {
      reverse: false,
      sortColumn: 'dataCollectionGroupName'
    };
    $scope.inputFilter = {};

    EnvironmentConfig.isDdeEnabled().then(function (result) {
      $scope.ddeEnabled = result;
    });

    DataCollectionGroupManager.getAll().then(function (data) {
      $scope.loading = false;
      $scope.allCollectionGroups = data;
      $scope.latestCollectionGroups = latestVersions(data);
    }, function (err) {
      $scope.loading = false;
      Notifications.error(err);
    });

    // Redirect to view
    $scope.viewCollectionGroup = function (selected) {
      $location.path('/collectiongroup/edit/' + selected.dataCollectionGroupId);
    };

    // Redirect to new definition
    $scope.newGroup = function () {
      $location.path('/collectiongroup/new');
    };

    $scope.sortByColumn = function (column) {
      if (column !== $scope.columnSort.sortColumn) {
        $scope.columnSort.reverse = false;
      } else {
        $scope.columnSort.reverse = !$scope.columnSort.reverse;
      }
      $scope.columnSort.sortColumn = column;
    };

    // TODO(stijn): move this helper function to the service layer, and write a
    // test for it.
    function latestVersions(collectionGroups) {
      var groupedByName = _.groupBy(collectionGroups, 'dataCollectionGroupName');
      var latestPerName = _.mapObject(groupedByName, function (collectionGroups) {
        return _.max(collectionGroups, _.property('majorVersion'));
      });
      return _.values(latestPerName);
    }

  });
