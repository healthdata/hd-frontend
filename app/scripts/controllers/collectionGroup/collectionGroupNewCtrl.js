/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.collection')
  .controller('collectionGroupNewCtrl', function (_, $location, $scope, $window, $q, DataCollectionGroupManager,
                                                  moment, Notifications, SkryvCollection) {
    'use strict';

    var groups = [];
    /* Initialize */
    DataCollectionGroupManager.getAll()
      .then(function (response) {
        // we should calculate the latest version + 1 and set that as version
        groups = response;
      }, function (error) {
        // if there aren't any results, set v1
        $scope.group.majorVersion = 0;
      });

    // Grab all the collections to fill autocomplete
    SkryvCollection.getCollections().then(function (collections) {
      $scope.salesForceCollections = _.sortBy(_.keys(collections), function (key) {
        return key;
      });
    });

    $scope.group = {
      majorVersion: 1
    };

    $scope.datepickerConfig = {
      format: 'dd/MM/yyyy'
    };

    $scope.save = function () {
      if ($scope.skryv_collection_form.$valid) {
        // Validate if dates are in chronological order
        var start = ($scope.group.startDate ? moment($scope.group.startDate).format() : null);
        var creation = ($scope.group.endDateCreation ? moment($scope.group.endDateCreation).format() : null);
        var submission = ($scope.group.endDateSubmission ? moment($scope.group.endDateSubmission).format() : null);
        var comments = ($scope.group.endDateComments ? moment($scope.group.endDateComments).format() : null);

        if ((creation >= start || !creation)
          && (submission >= creation || !submission)
          && (comments >= submission || !comments)) {

          // Transform to date that backend accepts
          var copy = angular.copy($scope.group);

          copy.startDate = start;
          copy.endDateCreation = creation;
          copy.endDateSubmission = submission;
          copy.endDateComments = comments;

          // Copy prevents number from incrementing in view
          return DataCollectionGroupManager.createGroup(copy).then(function (response) {
            Notifications.success('The data collection was successfully saved!');
            $location.path('/collectiongroup/edit/' + response.dataCollectionGroupId);
            return response;
          }, function (err) {
            Notifications.error(err.data.error_description);
            return $q.reject(err);
          });

        } else {
          Notifications.info('Dates are not in a chronological order');
          return $q.reject();
        }
      } else {
        Notifications.info('Please fill in all fields');
        return $q.reject();
      }
    };

    $scope.calculateVersion = function () {
      var name = $scope.group.dataCollectionGroupName;
      if (name) {
        $scope.group.majorVersion = calculateVersion(groups, name);
      } else {
        $scope.group.majorVersion = 1;
      }
    };

    $scope.open = function (event, type) {
      event.preventDefault();
      $scope.datepickerConfig[type] = !$scope.datepickerConfig[type];
    };

    /* Helper functions */
    function calculateVersion(groups, name) {
      var latest = 0;
      groups.forEach(function (group) {
        if (group.dataCollectionGroupName === name && group.majorVersion > latest) {
          latest = group.majorVersion;
        }
      });

      return latest + 1;
    }

    $scope.inputFormatter = function (model) {
      return model;
    };

    $scope.renderMatch = function (match) {
      if (match) {
        return match;
      }
    };

    $scope.getCollections = function ($viewValue) {
      return $scope.salesForceCollections.filter(function (group) {
        if (group.toLowerCase().indexOf($viewValue.toLowerCase()) > -1) {
          return group;
        }
      });
    };
  });
