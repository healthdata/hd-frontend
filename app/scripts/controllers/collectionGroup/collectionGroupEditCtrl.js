/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.collection')
  .controller('collectionGroupEditCtrl', function (_, $scope, $routeParams, $q, $location,
    DataCollectionGroupManager, SkryvDefinitionManager, EnvironmentConfig, gettextCatalog, moment, Notifications, ngDialog, SkryvSession) {
    'use strict';

    $scope.latestVersion = {};
    $scope.isCreatingNewVersion = false;
    // initialize datepickers
    $scope.datepickerConfig = {
      format: 'dd/MM/yyyy'
    };

    $scope.isLoading = true;
    $scope.showLatestVersions = true;
    $scope.isAdmin = SkryvSession.isAdmin();
    EnvironmentConfig.isPublishEnabled().then(function (result) {
      $scope.isPublishEnabled = result;
    });

    DataCollectionGroupManager.getById($routeParams.id)
      .then(function (group) {
        // Check if this group is the latest version
        DataCollectionGroupManager.getAll().then(function (res) {
          var grouped = _.groupBy(res, 'dataCollectionGroupName');
          var currentGroupVersions = grouped[group.dataCollectionGroupName];
          if (currentGroupVersions) {
            $scope.latestVersion = _.max(currentGroupVersions, function (collection) {
              return collection.majorVersion;
            });
          }
        });

        group.period = group.period ? group.period : {};

        $scope.latestChildCollections = latestVersions(group);
        if (group.startDate) {
          group.startDate = new Date(group.startDate);
        }
        if (group.endDateCreation) {
          group.endDateCreation = new Date(group.endDateCreation);
        }
        if (group.endDateSubmission) {
          group.endDateSubmission = new Date(group.endDateSubmission);
        }
        if (group.endDateComments) {
          group.endDateComments = new Date(group.endDateComments);
        }
        if (group.period.start) {
          group.period.start = new Date(group.period.start);
        }
        if (group.period.end) {
          group.period.end = new Date(group.period.end);
        }

        $scope.group = group;
        $scope.isLoading = false;
      }, function (error) {
        Notifications.error(gettextCatalog.getString('No datacollection group found with that ID'));
        $location.path('/collectiongroup');
      });

    $scope.open = function (event, type) {
      event.preventDefault();

      $scope.datepickerConfig[type] = !$scope.datepickerConfig[type];
    };

    $scope.addCollection = function () {
      $location.path('collection/new').search({ id: $scope.group.dataCollectionGroupId });
    };

    $scope.editChildCollection = function (child) {
      $location.path('collection/edit/' + child.dataCollectionDefinitionId);
    };

    $scope.save = function () {
      if ($scope.skryv_collection_form.$valid) {
        // Validate if dates are in chronological order
        var start = ($scope.group.startDate ? moment($scope.group.startDate).format() : null);
        var creation = ($scope.group.endDateCreation ? moment($scope.group.endDateCreation).format() : null);
        var submission = ($scope.group.endDateSubmission ? moment($scope.group.endDateSubmission).format() : null);
        var comments = ($scope.group.endDateComments ? moment($scope.group.endDateComments).format() : null);

        var periodStart = ($scope.group.period.start ? moment($scope.group.period.start).format('DD/MM/YYYY') : undefined);
        var periodEnd = ($scope.group.period.end ? moment($scope.group.period.end).format('DD/MM/YYYY') : undefined);

        if ((creation >= start || !creation) &&
          (submission >= creation || !submission) &&
          (comments >= submission || !comments)) {

          // Transform to date that backend accepts
          var copy = angular.copy($scope.group);

          copy.startDate = start;
          copy.endDateCreation = creation;
          copy.endDateSubmission = submission;
          copy.endDateComments = comments;

          copy.period = {
            start: periodStart,
            end: periodEnd
          };

          // Copy prevents number from incrementing in view
          return DataCollectionGroupManager.updateGroup(copy).then(function (st) {
            Notifications.success('The data collection was successfully saved!');
            return st;
          }, function (err) {
            Notifications.error(err);
            return $q.reject(err);
          });

        } else {
          Notifications.info('Dates are not in a chronological order');
          return $q.reject();
        }
      } else {
        Notifications.info('Please fill in all fields');
        return $q.reject();
      }
    };

    // TODO(arno): WDC-3032
    // $scope.remove = function () {
    //   DataCollectionGroupManager.deleteGroup($scope.group.dataCollectionGroupId)
    //     .then(function (response) {
    //       Notifications.success(gettextCatalog.getString('Collection successfully deleted'));
    //       $location.path("#/collectiongroup");
    //     }, function (error) {
    //       Notifications.error(gettextCatalog.getString('Collection could not be deleted'));
    //     });
    // };

    $scope.createNewVersion = function () {
      var group = angular.copy($scope.group);
      group.published = false;

      group.majorVersion = $scope.latestVersion.majorVersion + 1;
      group.startDate =  ($scope.group.startDate ? moment($scope.group.startDate).format() : null);
      group.endDateCreation = ($scope.group.endDateCreation ? moment($scope.group.endDateCreation).format() : null);
      group.endDateSubmission = ($scope.group.endDateSubmission ? moment($scope.group.endDateSubmission).format() : null);
      group.endDateComments = ($scope.group.endDateComments ? moment($scope.group.endDateComments).format() : null);

      var validCreation = (group.endDateCreation >= group.startDate || !group.endDateCreation);
      var validSubmission = (group.endDateSubmission >= group.endDateCreation || !group.endDateSubmission);
      var validComments = (group.endDateComments >= group.endDateSubmission || !group.endDateComments);

      if (!validCreation || !validSubmission || ! validComments) {
        Notifications.warning(gettextCatalog.getString('Dates are not in a chronological order'));
        return;
      }

      $scope.isCreatingNewVersion = true;
      // Create a group, we need the new ID from the backend...
      DataCollectionGroupManager.createGroup(group)
        .then(function (newGroup) {
          // We group the current dataCollectionDefinitions by name,
          // calculate the latest version and add it to definitionsToTransition.
          var groupedDefinitions = _.groupBy(group.dataCollectionDefinitions, 'dataCollectionDefinitionName');
          var definitionsToTransition = [];
          var keys = _.keys(groupedDefinitions);
          keys.forEach(function (key) {
            // Grab the latest from that definition
            var latest = _.max(groupedDefinitions[key], function (definition) {
              return definition.minorVersion;
            });

            definitionsToTransition.push(latest);
          });

          // Iterate over the definitions to grab the actual content.
          // We keep track of the requests in the definitionPromises array to chain onto $q.all when they resolve.
          var definitionPromises = [];
          definitionsToTransition.forEach(function (definition) {
            var promise = SkryvDefinitionManager.get(definition.dataCollectionDefinitionId).then(function (res) {
              return res;
            });

            definitionPromises.push(promise);
          });

          $q.all(definitionPromises).then(function (definitionsWithContent) {
            // We grabbed the content from all the definitions...
            // Now we iterate over them and send requests to push them to the new group.
            // We store the requests in an array to resolve $q.all.
            var definitionsToTransition = [];
            definitionsWithContent.forEach(function (definitionWithContent) {
              definitionWithContent.published = false;
              definitionWithContent.minorVersion = 0;
              definitionWithContent.dataCollectionGroupId = newGroup.dataCollectionGroupId;
              var promise = SkryvDefinitionManager.save(definitionWithContent);
              definitionsToTransition.push(promise);
            });

            $q.all(definitionsToTransition).then(function (response) {
              $scope.isCreatingNewVersion = false;
              $location.path('/collectiongroup/edit/' + newGroup.dataCollectionGroupId);
              Notifications.success(gettextCatalog.getString('Successfully saved new group'));
            }, function (error) {
              $scope.isCreatingNewVersion = false;
              Notifications.error(gettextCatalog.getString('Could not transition all definitions to new collection'));
            });
          }, function (error) {
            $scope.isCreatingNewVersion = false;
            Notifications.error(gettextCatalog.getString('Could not fetch definition content'));
          });
        }, function (error) {
          $scope.isCreatingNewVersion = false;
          Notifications.error(gettextCatalog.getString('Could not create new collection'));
        });
    };

    // TODO(arno): actually open publish dialog and call publish() somewhere
    $scope.openPublishDialog = function () {
      ngDialog.open({
        template: 'views/dialog/collection/publish.html',
        controller: [ '$scope', function (scope) {
          scope.publish = function () {
            publish();
            scope.closeThisDialog();
          };

          scope.saveAndPublish = function () {
            $scope.save().then(function () {
              publish();
              scope.closeThisDialog();
            }, function () {
              scope.closeThisDialog();
            });
          };
        }]
      });
    };

    // TODO(arno): allow publishing when backend is fixed
    function publish() {
      DataCollectionGroupManager.publishGroup($scope.group.dataCollectionGroupId)
        .then(function () {
          $scope.group.published = true;
          Notifications.success(gettextCatalog.getString('Collection successfully published'));
        }, function () {
          Notifications.error(gettextCatalog.getString('Could not publish collection'));
        });
    }

    // TODO(arno): actually open republish dialog and call republish() somewhere
    $scope.openRepublishDialog = function () {
      // Republish dialog
      ngDialog.open({
        template: 'views/dialog/collection/rePublish.html',
        controller: ['$scope', function (scope) {
          scope.rePublish = function (item) {
            republish();
            scope.closeThisDialog(item);
          };

          scope.saveAndRePublish = function () {
            $scope.save().then(function () {
              republish();
              scope.closeThisDialog();
            }, function () {
              scope.closeThisDialog();
            });
          };
        }]
      });
    };

    // TODO(arno): allow republishing when backend is fixed
    function republish() {
      DataCollectionGroupManager.republishGroup($scope.group.dataCollectionGroupId)
        .then(function () {
          Notifications.success(gettextCatalog.getString('Collection successfully republished'));
        }, function () {
          Notifications.error(gettextCatalog.getString('Could not republish collection'));
        });
    }

    function latestVersions(group) {
      var groupedByName = _.groupBy(group.dataCollectionDefinitions, 'dataCollectionDefinitionName');
      var latestPerName = _.mapObject(groupedByName, function (collections) {
        return _.max(collections, function (collection) {
          return collection.minorVersion;
        });
      });
      return _.values(latestPerName);
    }
  });
