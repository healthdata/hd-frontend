/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('frontendApp')
  .controller('SettingsCtrl', function ($scope, $location, $q, _, SkryvLanguage, Notifications, ElasticsearchRebuild, EnvironmentConfig,
              SkryvUser, PLATFORM, Authorities, SkryvSession, ngDialog, gettextCatalog, SkryvTranslation, $timeout,
              SkryvUserRequest, cacheBackend, dataCollectionManager) {

      // If definitions can be added
      EnvironmentConfig.isDdeEnabled().then(function (result) {
        $scope.ddeEnabled = result;
      });

      // Variables
      $scope.platform = false;
      $scope.password = false;
      $scope.admin = SkryvSession.isAdmin();
      $scope.authorities = Authorities.Authorities();
      $scope.loggedUser = SkryvSession.getLoggedUser();

      // LDAP or DB
      SkryvSession.getAuthType().then(function (type) {
        $scope.authType = type;
      });

      // Set admin-specific behaviour
      if ($scope.admin) {

        // Clearing of caches
        cacheBackend.getCaches().then(function (response) {

          var mapped = response.data.map(function (entry) {
            var label = entry.split(/(?=[A-Z])/).join(' ').replace('Cache', '');

            return {
              label: label,
              name: entry
            };
          });

          $scope.caches = mapped;
        });

        $scope.clearCache = function (name) {
          cacheBackend.clearCache(name).then(function (response) {
            Notifications.success('Cache successfully cleared');
          }).catch(function (error) {
            Notifications.error(error);
          });
        };

        $scope.clearCatalogueCaches = function () {
          return cacheBackend.getCatalogueCaches().then(function (response) {
            var promises = [];
            response.data.forEach(function (entry) {
              promises.push(cacheBackend.clearCache(entry + '&catalogue=true'));
            });
            $q.all(promises).then(function () {
              Notifications.success(gettextCatalog.getString('Caches successfully cleared'));
            });
          }, function (err) {
            Notifications.error(gettextCatalog.getString('Could not clear caches'));
          });
        };

        $scope.clearAllCaches = function () {
          var caches = angular.copy($scope.caches);

          var promises = [];
          caches.forEach(function (cache) {
            promises.push(cacheBackend.clearCache(cache.name));
          });

          if ($scope.platform) {
            promises.push($scope.clearCatalogueCaches());
          }

          $q.all(promises).then(function (response) {
            Notifications.success(gettextCatalog.getString('Caches successfully cleared'));
          }, function (error) {
            Notifications.error(gettextCatalog.getString('Could not clear caches'));
          });
        };

        // User requests
        if (PLATFORM === 'hd4dp') {
          SkryvUserRequest.getAll().then(function (data) {
            $scope.requests = data;
          }, function () {
            $scope.requests = [];
            console.log('Could not load requests')
          });
        }

        // Data collection requests
        if (PLATFORM === 'hd4dp') {
          dataCollectionManager.getDataCollectionRequests($scope.loggedUser.organization.id).then(function (response) {
            $scope.dcRequests = _.where(response, { treated: false });
          });
        }
      }

      $scope.loggedUser = SkryvSession.getLoggedUser();

      // Get language
      $scope.selectedLang = SkryvLanguage.getLanguage();
      if (!$scope.selectedLang) {
        $scope.selectedLang = 'en';
      }

      $scope.selectedLangChanged = function () {
        SkryvLanguage.setLanguage($scope.selectedLang);
        if ($scope.selectedLang !== 'en') {
          SkryvTranslation.getTranslations($scope.selectedLang);
        }
      };

      // Enable platform specific components
      if (PLATFORM === 'hd4res') {
        $scope.platform = true;
      }

      // Elastic search
      $scope.elasticsearchRebuildStatus = {};

      $scope.rebuildDialog = function () {
        ngDialog.open({
          template: 'views/dialog/settings/esRebuild.html',
          showClose: false,
          controller: [ '$scope', 'Organization', 'SkryvSession', 'SkryvCollection', function (scope, Organization, SkryvSession, SkryvCollection) {

            scope.userOrganization = SkryvSession.getLoggedUser().organization; // Logged user
            scope.collectionLoading = true;
            scope.colError = false;

            SkryvCollection.getByOrganization(scope.userOrganization).then(function (defs) {
              scope.collections = defs;
              scope.collectionLoading = false;
            }, function () {
              scope.collectionLoading = false;
              scope.colError = true;
              scope.collections = [];
            });

            scope.esRebuildOrganization = esRebuildOrganization;
            scope.esRebuildCollection = esRebuildCollection;
          } ]
        });
      };

      function esRebuildAll() {
        ElasticsearchRebuild.rebuildAll().then(function () {
          Notifications.success('Rebuild started');
          $scope.refreshElasticsearchStatus(true);
          ngDialog.close();
        }, function (err) {
          ngDialog.close();
          Notifications.error(err);
        });
      }

      function esRebuildOrganization(id) {
        ElasticsearchRebuild.rebuildOrganization(id).then(function () {
          Notifications.success('Rebuild started');
          ngDialog.close();
          $scope.refreshElasticsearchStatus(true);
        }, function (err) {
          ngDialog.close();
          Notifications.error(err);
        });
      }

      function esRebuildCollection(id, name) {
        ElasticsearchRebuild.rebuildCollection(id, name).then(function () {
          Notifications.success('Rebuild started');
          ngDialog.close();
          $scope.refreshElasticsearchStatus(true);
        }, function (err) {
          ngDialog.close();
          console.log(err);
          Notifications.error(err);
        });
      }

      $scope.refreshElasticsearchStatus = function (pass) {
        ElasticsearchRebuild.status()
          .then(function (status) {
            $scope.progress = status.documents_in_database.documents_indexed + '/' + status.documents_in_database.total;
            $scope.percentage = Math.floor((status.documents_in_database.documents_indexed / status.documents_in_database.total) * 100);
            $scope.elasticsearchRebuildStatus = status;
            if (status.indexing_task.status == 'RUNNING') {
              $scope.elasticsearchRebuildStatus.status = 'RUNNING';
              // Prevent multiple clicks on refresh
              if ($scope.percentage != 100 && (!$scope.skipClick || pass)) {
                $scope.skipClick = true;
                $timeout(function () {
                  $scope.refreshElasticsearchStatus(true)
                }, 2000);
              } else if ($scope.percentage == 100) {
                $scope.skipClick = false;
              }
            }
          }, function (err) {
            $scope.elasticsearchRebuildStatus.status = 'FAILED';
            Notifications.error(err);
          });
      };

    })

  .directive('sameAs', function () {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$parsers.unshift(function (viewValue) {
          if (viewValue === scope[attrs.sameAs]) {
            ctrl.$setValidity('sameAs', true);
            return viewValue;
          } else {
            ctrl.$setValidity('sameAs', false);
            return undefined;
          }
        });
      }
    };
  });
