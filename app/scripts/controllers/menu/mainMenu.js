/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
(function () {

angular.module('frontendApp')
  .controller('MainMenuCtrl', MainMenuCtrl);

function MainMenuCtrl($scope, MainMenu, SkryvSession, $location, PLATFORM, _) {
  $scope.mainMenu = MainMenu.mainMenu();

  // Define which pages shouldn't have the main menu
  var hideNavigation = [ '/login', '/logout', '/document/hd4pc' ];

  function hideNav(path) {
    var pathInList = _.find(hideNavigation, function(hiddenPath){
      return path.indexOf(hiddenPath) > -1;
    });

    $scope.showNavigation = (typeof pathInList == "undefined");
  }

  hideNav($location.path());

  $scope.$on('$locationChangeStart', function (event, next, current) {
    hideNav(next);
  });

  $scope.today = new Date().setHours(0,0,0,0);

  $scope.admin = SkryvSession.isAdmin();
  $scope.platform = PLATFORM;
  $scope.authType = ($scope.admin ? 'admin' : 'user');

  $scope.isActive = function (item) {
    var url = item.url || '';
    return url.replace(/%20/g, " ").indexOf($location.path().replace(/%20/g, " ")) > -1;
  };

  $scope.itemClass = function (item) {
    if (typeof item.class === 'function') {
      return item.class();
    }
    return item.class;
  };

  $(document).keyup(function (e) {
    if (e.keyCode == 27) {
      introJs().exit();
    }
  });
}
})();
