/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
angular.module('skryv.configurations',
  [ 'skryv.conf', 'ngRoute', 'ngResource', 'services.configurations' ])

  .config(function ($routeProvider) {
    $routeProvider
      .when('/configurations', {
        templateUrl: 'views/configuration/configurations.html',
        controller: 'ConfigurationCtrl',
        resolve: {
          authenticate: function ($q, authFilter) {
            authFilter('admin');
          }
        }
      });
  })

// CONFIG LIST
.controller('ConfigurationCtrl', function ($scope, $location, SkryvConfig, Authorities, Notifications, SkryvUser, PLATFORM, SkryvSession) {

    // Advanced options
    $scope.inputFilter = { advanced: false, key: '' };
    $scope.skip = false;

    $scope.$watch('inputFilter.key', function () {
      if ($scope.skip) { return $scope.skip = false; }

      if($scope.inputFilter.key) {
        delete $scope.inputFilter.advanced;
      } else {
        $scope.inputFilter.advanced = false;
      }
    });

    $scope.changeAdvanced = function(bool) {
      $scope.skip = true;
      $scope.inputFilter.key = '';
      $scope.inputFilter.advanced = bool;
    };

    // Loader
    $scope.platform = PLATFORM;
    $scope.loading = true;

    // Date picker
    $scope.datepicker = { format: 'dd-MM-yyyy' };

    $scope.open = function ($event, config) {
      $event.preventDefault();
      $event.stopPropagation();
      config.isOpen = true;
    };

    // Configurations
    SkryvConfig.getConfig().then(function (data) {
      $scope.loading = false;
      $scope.configs = data;
      angular.forEach(data, function (config) {
        config = parseConfig(config);
      });
    });

    function parseConfig(config) {
      if (config.type == 'INTEGER' && config.value) {
        config.value = parseInt(config.value, 10);
      }
      if (config.type == 'BOOLEAN' && config.value) {
        config.value = config.value.toLowerCase();
      }

      return config;
    }

    $scope.editConfig = function (config) {
      $scope.editable = angular.copy(config);
    };

    $scope.defaultConfig = function () {
      if ($scope.editable.type == 'INTEGER' && $scope.editable.value) {
        $scope.editable.value = parseInt($scope.editable.defaultValue, 10);
      } else {
        $scope.editable.value = $scope.editable.defaultValue;
      }
    };

    $scope.cancelConfig = function () {
      delete $scope.editable;
    };

    $scope.saveConfig = function (config) {
      SkryvConfig.editConfig($scope.editable).then(function (response) {
        response = parseConfig(response);

        config.value = response.value;
        config.useFromMain = response.useFromMain;

        if (config.key === 'USER_MANAGEMENT_TYPE') {
          SkryvSession.setAuthType(config.value);
        }

        delete $scope.editable;
        Notifications.success('Configuration has been edited');
      }, function () {
        Notifications.error('Configuration could not be edited');
      });
    };

    // Table
    $scope.columnSort = { reverse: false, sortColumn: 'key' };
    $scope.itemsPerPage = 10;
    $scope.currentPage = 1;

    $scope.sortByColumn = function (column) {
      if (column != $scope.columnSort.sortColumn) {
        $scope.columnSort.reverse = false;
      } else {
        $scope.columnSort.reverse = !$scope.columnSort.reverse;
      }
      $scope.columnSort.sortColumn = column;
      $scope.currentPage = 1;
    };

    $scope.reload = function () {
      $scope.currentPage = 1;
    };

    // Is current user from main organisation
    $scope.isMainOrg = SkryvSession.getLoggedUser().organization.main;
  })

  .filter('num', function () {
    return function (input) {
      return parseInt(input, 10) || input;
    };
  });