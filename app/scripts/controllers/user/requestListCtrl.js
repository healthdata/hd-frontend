/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.user')
  .controller('requestListCtrl', function ($scope, $location, SkryvUserRequest, Notifications, _, ngDialog, $route, gettextCatalog, PLATFORM) {

      $scope.platform = PLATFORM;
      $scope.loading = true;

      $scope.redirectRequest = function (id) {
        $location.path('/user/' + id + '/request');
      };

      // Get users
      $scope.loading = true;
      SkryvUserRequest.getAll().then(function (data) {
        $scope.loading = false;
        // $scope.requests = data;

        $scope.requests = data;

      }, function () {
        $scope.loading = false;
        $scope.users = [];
        Notifications.error('Requests not found');
      });

      // Remove user
      $scope.reject = function (confirmed, id) {
        if (confirmed) {
          ngDialog.close();
          SkryvUserRequest.reject(id).then(function () {
            Notifications.success('Request has been rejected!');
            $route.reload();
          }, function (err) {
            Notifications.error(err);
          });
        } else {
          ngDialog.open({
            template: '<p>' + gettextCatalog.getString('Are you sure you want to reject this request?') +
            '<div class="row">' +
            '<button class="btn btn-secundary" ng-click="reject(true,' + id + ')" translate>Yes</button>' +
            "<button class='btn btn-primary' ng-click='closeThisDialog()'><a translate>No</a></button>" +
            '</div>',
            plain: true,
            showClose: false,
            scope: $scope
          });
        }
      };

      $scope.accept = function (confirmed, id) {
        if (confirmed) {
          ngDialog.close();
          SkryvUserRequest.accept(id).then(function () {
            Notifications.success('Request has been accepted!');
            $route.reload();
          }, function (err) {
            Notifications.error(err);
          });
        } else {
          ngDialog.open({
            template: '<p>' + gettextCatalog.getString('Are you sure you want to accept this request?') +
            '<div class="row">' +
            '<button class="btn btn-secundary" ng-click="accept(true,' + id + ')" translate>Yes</button>' +
            "<button class='btn btn-primary' ng-click='closeThisDialog()'><a translate>No</a></button>" +
            '</div>',
            plain: true,
            showClose: false,
            scope: $scope
          });
        }
      };

      // Table
      $scope.columnSort = { reverse: false, sortColumn: 'username' };

      $scope.sortByColumn = function (column) {
        if (column !== $scope.columnSort.sortColumn) {
          $scope.columnSort.reverse = false;
        } else {
          $scope.columnSort.reverse = !$scope.columnSort.reverse;
        }
        $scope.columnSort.sortColumn = column;
      };

    });
