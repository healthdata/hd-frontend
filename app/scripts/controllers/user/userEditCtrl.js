/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.user')
  .controller('userEditCtrl', function ($scope, $location, SkryvUser, SkryvCollection, $routeParams,
              Notifications, ngDialog, gettextCatalog, _, SkryvSession, Authorities) {

      $scope.userDataCollections = {};
      $scope.dataCollections = [];
      $scope.multDataCollections = [];
      $scope.userEdit = {};
      $scope.editPassword = false;
      $scope.ldap = false;
      $scope.authorities = Authorities.Authorities();

      SkryvUser.get($routeParams.id).then(function (user) {
        // Set user on scope
        $scope.userEdit = user;
        delete $scope.userEdit.password;
        // Get organisations for user organisation
        getCollections(user);
      }, function (err) {
        Notifications.error(err);
      });

      // Get authentication type
      SkryvSession.getAuthType().then(function (type) {
        if (type === 'DATABASE_LDAP') {
          $scope.ldap = true;
        }
      });

      function getCollections(user) {
        // Get definitions
        SkryvCollection.getByOrganization(user.organization).then(function (defs) {

          $scope.dataCollections = _.sortBy(defs, function (def) {
            return def.name.toLowerCase();
          });

          // In case of multiselect -> Tick correct dc
          // In case of checkboxes  -> Check correct dc
          if ($scope.dataCollections.length > 5) {
            $scope.dataCollections = $scope.dataCollections.map(function (dataCollection) {
              if (user.dataCollectionNames.indexOf(dataCollection.name) > -1) {
                dataCollection.ticked = true;
              }

              return dataCollection;
            });
          } else {
            angular.forEach(user.dataCollectionNames, function (dataCollection) {
              $scope.userDataCollections[dataCollection] = dataCollection;
            });
          }
        }, function (err) {
          Notifications.error(err);
        });
      }

      // Edit password
      $scope.togglePassword = function () {
        $scope.editPassword = !$scope.editPassword;
      };

      // Update user
      $scope.update = function (user) {

        var updatedUser = angular.copy(user);
        if (updatedUser.passwordRepeat) {
          delete updatedUser.passwordRepeat;
        }

        SkryvUser.update(updatedUser).then(function (data) {
          Notifications.success('The user was successfully edited!');
          $location.path('/user/list');
        }, function (err) {
          Notifications.error(err);
        });
      };

      // Save action
      $scope.save = function () {
        $scope.userEdit.dataCollectionNames = [];

        // Checkboxes
        if ($scope.dataCollections.length <= 5) {
          angular.forEach($scope.dataCollections, function (dataCollection) {
            if ($scope.userDataCollections[dataCollection.name]) {
              $scope.userEdit.dataCollectionNames.push(dataCollection.name);
            }
          });
        }

        // Multiselect
        if ($scope.dataCollections.length > 5) {
          angular.forEach($scope.userDataCollections, function (dataCollection) {
            $scope.userEdit.dataCollectionNames.push(dataCollection.name);
          });
        }

        // check form
        if (!$scope.user_form.$valid) {
          return;
        } else if ($scope.editPassword && $scope.userEdit.password != $scope.userEdit.passwordRepeat) {
          Notifications.info('Repeat of new password is not the same');
          return;
        } else if (!$scope.userEdit.authorities || $scope.userEdit.authorities.length === 0) {
          Notifications.info('Select a role for the user');
          return;
        } else if ($scope.userEdit.authorities[0].authority === 'ROLE_USER' && ($scope.userEdit.dataCollectionNames.length === 0 || !$scope.userEdit.dataCollectionNames)) {
          Notifications.info('Select a data collection for the user');
          return;
        } else {
          $scope.update($scope.userEdit);
        }
      };

      $scope.remove = function (confirmed) {
        if (confirmed) {
          ngDialog.close();
          SkryvUser.delete($scope.userEdit.id).then(function () {
            Notifications.success('User has been deleted!');
            $location.path('/user/list');
          }, function (err) {
            Notifications.error(err);
          });
        } else {
          ngDialog.open({
            template: '<p>' + gettextCatalog.getString('Are you sure you want to delete this user?') +
            '<div class="row">' +
            '<button class="btn btn-secundary" ng-click="remove(true)" translate>Yes</button>' +
            "<button class='btn btn-primary' ng-click='closeThisDialog()'><a translate>No</a></button>" +
            '</div>',
            plain: true,
            showClose: false,
            scope: $scope
          });
        }
      };

      // Multi-select button translations
      $scope.multiLang = {
        selectAll: gettextCatalog.getString('Select all'),
        selectNone: gettextCatalog.getString('Select none'),
        reset: gettextCatalog.getString('Reset'),
        search: gettextCatalog.getString('Search...'),
        nothingSelected: gettextCatalog.getString('None selected')
      };
    })
