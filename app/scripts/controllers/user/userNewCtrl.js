/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.user')
  .controller('userNewCtrl', function ($scope, $location, $window, SkryvUser, SkryvCollection, $routeParams,
              Authorities, Notifications, SkryvSession, prefilledUser, gettextCatalog, _) {
      'use strict';

      $scope.userEdit = {};

      if (prefilledUser) {
        $scope.isPrefilled = true;
        $scope.userEdit = prefilledUser;
      }

      $scope.userEdit.organization = SkryvSession.getLoggedUser().organization;

      $scope.authorities = Authorities.Authorities();
      $scope.userDataCollections = {};
      $scope.multDataCollections = [];
      $scope.dataCollections = [];

      // Get definitions for organisation
      $scope.getDefinitions = function () {
        SkryvCollection.getByOrganization($scope.userEdit.organization).then(function (defs) {
          $scope.dataCollections = _.sortBy(defs, function (def) {
            return def.name.toLowerCase();
          });
        }, function (err) {
          Notifications.error(err);
        });
      };

      $scope.getDefinitions();

      $scope.store = function (newUser) {
        var user = angular.copy(newUser);

        if (user.passwordRepeat) {
          delete user.passwordRepeat;
        }

        SkryvUser.save(user).then(function () {
          Notifications.success('The user was successfully created!');
          $location.path('/user/list');
        }, function (err) {
          Notifications.error(err);
        });
      };

      $scope.save = function () {
        $scope.userEdit.dataCollectionNames = [];
        // Checkboxes
        if ($scope.dataCollections.length <= 5) {
          angular.forEach($scope.dataCollections, function (dataCollection) {
            if ($scope.userDataCollections[dataCollection.name]) {
              $scope.userEdit.dataCollectionNames.push(dataCollection.name);
            }
          });
        }

        // Multiselect
        if ($scope.dataCollections.length > 5) {
          angular.forEach($scope.userDataCollections, function (dataCollection) {
            $scope.userEdit.dataCollectionNames.push(dataCollection.name);
          });
        }

        // form valid
        if (!$scope.user_form.$valid) {
          return;
        } else if ($scope.userEdit.password !== $scope.userEdit.passwordRepeat) {
          Notifications.info('Repeat of new password is not the same');
          return;
        } else if (!$scope.userEdit.authorities || $scope.userEdit.authorities.length === 0) {
          Notifications.info('Select a role for the user');
          return;
        } else if ($scope.userEdit.authorities[0].authority === 'ROLE_USER' && ($scope.userEdit.dataCollectionNames.length === 0 || !$scope.userEdit.dataCollectionNames)) {
          Notifications.info('Select a data collection for the user');
          return;
        } else {
          $scope.store($scope.userEdit);
        }
      };

      // Multi-select button translations
      $scope.multiLang = {
        selectAll: gettextCatalog.getString('Select all'),
        selectNone: gettextCatalog.getString('Select none'),
        reset: gettextCatalog.getString('Reset'),
        search: gettextCatalog.getString('Search...'),
        nothingSelected: gettextCatalog.getString('None selected')
      };

      $scope.back = function () {
        $window.history.back();
      };
    });
