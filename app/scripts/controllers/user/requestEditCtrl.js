/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.user')
  .controller('requestEditCtrl', function ($scope, $location, SkryvUserRequest, SkryvCollection, $routeParams, Authorities,
              Notifications, ngDialog, gettextCatalog, _, transFilter, SkryvSession) {

      $scope.userDataCollections = {};
      $scope.dataCollections = [];
      $scope.multDataCollections = [];
      $scope.userEdit = {};
      $scope.editPassword = false;
      $scope.authorities = Authorities.Authorities();
      $scope.ldap = false;
      $scope.organization = SkryvSession.getLoggedUser().organization;

      SkryvUserRequest.get($routeParams.id).then(function (user) {
        // Set user on scope
        $scope.userEdit = user;
        delete $scope.userEdit.password;
        // Get organisations for user organisation
        getCollections(user);
      }, function (err) {
        $location.path('/user/requests');
        Notifications.error(err);
      });

      // Get authentication type
      SkryvSession.getAuthType().then(function (type) {
        if (type === 'DATABASE_LDAP') {
          $scope.ldap = true;
        }
      });

      function getCollections(user) {
        // Get definitions
        SkryvCollection.getByOrganization($scope.organization).then(function(defs) {
          // Filter definitions
          $scope.dataCollections = _.sortBy(defs, function(def) {
            return def.name.toLowerCase();
          });

          // In case of multiselect -> Tick correct dc
          // In case of checkboxes  -> Check correct dc
          if ($scope.dataCollections.length > 5) {
            angular.forEach(user.dataCollectionNames, function (dataCollection) {
              var col = _.findWhere($scope.dataCollections, { name: dataCollection });
              if (col) {
                col.ticked = true;
              }
            });
          } else {
            angular.forEach(user.dataCollectionNames, function (dataCollection) {
              $scope.userDataCollections[dataCollection] = dataCollection;
            });
          }
        }, function (err) {
          Notifications.error(err);
        });
      }

      function setCollections() {
        $scope.userEdit.dataCollectionNames = [];

        // Checkboxes
        if ($scope.dataCollections.length <= 5) {
          angular.forEach($scope.dataCollections, function (dataCollection) {
            if ($scope.userDataCollections[dataCollection.name]) {
              $scope.userEdit.dataCollectionNames.push(dataCollection.name);
            }
          });
        }

        // Multiselect
        if ($scope.dataCollections.length > 5) {
          angular.forEach($scope.userDataCollections, function (dataCollection) {
            $scope.userEdit.dataCollectionNames.push(dataCollection.name);
          });
        }
      }

      // Update user
      function update() {

        setCollections();

        var updatedUser = angular.copy($scope.userEdit);
        if (updatedUser.passwordRepeat) {
          delete updatedUser.passwordRepeat;
        }

        return SkryvUserRequest.update(updatedUser);
      }

      function accept(user) {
        return SkryvUserRequest.accept($scope.userEdit.id);
      }

      // Save action
      $scope.save = function () {

        // check form
        if ($scope.user_form.$valid) {
          // form valid
          if ($scope.editPassword) {
            if ($scope.userEdit.password != $scope.userEdit.passwordRepeat) {
              Notifications.info('Repeat of new password is not the same');
              return;
            }
          }

          if ($scope.userEdit.passwordRepeat) {
            delete $scope.userEdit.passwordRepeat;
          }

          update($scope.userEdit).then(function (data) {
            Notifications.success('The request was successfully edited!');
          }, function (err) {
            Notifications.error(err);
          });
        }
      };

      $scope.reject = function (confirmed) {
        if (confirmed) {
          ngDialog.close();
          SkryvUserRequest.reject($scope.userEdit.id).then(function () {
            Notifications.success('Request has been rejected!');
            $location.path('/user/requests');
          }, function (err) {
            Notifications.error(err);
          });
        } else {
          ngDialog.open({
            template: '<p>' + gettextCatalog.getString('Are you sure you want to reject this request?') +
            '<div class="row">' +
            '<button class="btn btn-secundary" ng-click="reject(true)" translate>Yes</button>' +
            "<button class='btn btn-primary' ng-click='closeThisDialog()'><a translate>No</a></button>" +
            '</div>',
            plain: true,
            showClose: false,
            scope: $scope
          });
        }
      };

      $scope.approve = function (confirmed) {
        if (confirmed) {
          ngDialog.close();
          update($scope.userEdit).then(function (data) {
            accept($scope.userEdit).then(function () {
              Notifications.success('Request has been accepted!');
              $location.path('/user/requests');
            }, function (err) {
              Notifications.error(err);
            });
          }, function (err) {
            Notifications.error(err);
          });
        } else {
          ngDialog.open({
            template: '<p>' + gettextCatalog.getString('Are you sure you want to accept this request?') +
            '<div class="row">' +
            '<button class="btn btn-secundary" ng-click="approve(true)" translate>Yes</button>' +
            "<button class='btn btn-primary' ng-click='closeThisDialog()'><a translate>No</a></button>" +
            '</div>',
            plain: true,
            showClose: false,
            scope: $scope
          });
        }
      };

      // Multi-select button translations
      $scope.multiLang = {
        selectAll: gettextCatalog.getString('Select all'),
        selectNone: gettextCatalog.getString('Select none'),
        reset: gettextCatalog.getString('Reset'),
        search: gettextCatalog.getString('Search...'),
        nothingSelected: gettextCatalog.getString('None selected')
      };

    })
