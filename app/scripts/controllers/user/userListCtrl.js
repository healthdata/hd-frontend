/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.user')
  .controller('userListCtrl', function ($scope, $location, SkryvUser, Notifications, _, ngDialog, $route, gettextCatalog, PLATFORM) {

      $scope.platform = PLATFORM;
      $scope.loading = true;

      $scope.redirectUser = function (id) {
        $location.path('/user/' + id + '/edit');
      };

      // Get users
      $scope.loading = true;
      SkryvUser.get('').then(function (data) {
        $scope.loading = false;
        $scope.users = _.map(data, function (user) {
          if (user.authorities && user.authorities[0] && user.authorities[0].authority) {
            user.authority = user.authorities[0].authority.split('ROLE_')[1].toLowerCase();
          }
          return user;
        });

        $scope.loading = false;
      }, function () {
        $scope.loading = false;
        $scope.users = [];
        Notifications.error('Users not found');
      });

      // Remove user
      $scope.remove = function (confirmed, id) {
        if (confirmed) {
          ngDialog.close();
          SkryvUser.delete(id).then(function () {
            Notifications.success('User has been deleted!');
            $route.reload();
          }, function (err) {
            Notifications.error(err);
          });
        } else {
          ngDialog.open({
            template: '<p>' + gettextCatalog.getString('Are you sure you want to delete this user?') +
            '<div class="row">' +
            '<button class="btn btn-secundary" ng-click="remove(true,' + id + ')" translate>Yes</button>' +
            "<button class='btn btn-primary' ng-click='closeThisDialog()'><a translate>No</a></button>" +
            '</div>',
            plain: true,
            showClose: false,
            scope: $scope
          });
        }
      };

      $scope.notifyUsers = function () {
        ngDialog.open({
          template: 'views/dialog/settings/notifyUsers.html',
          showClose: false,
          controller: [ '$scope', 'supportManager', 'Notifications', 'gettextCatalog', function ($scope, supportManager, Notifications) {
            $scope.confirm = function() {
              supportManager.notifyUsers($scope.subject, $scope.body).then(
                function(response) {
                  Notifications.success(gettextCatalog.getString('Message sent!'));
                }, function (error){
                  Notifications.error(gettextCatalog.getString('Something went wrong'));
                });
            };
          } ]
        });
      };

      // Table
      $scope.columnSort = { reverse: false, sortColumn: 'username' };

      $scope.sortByColumn = function (column) {
        if (column !== $scope.columnSort.sortColumn) {
          $scope.columnSort.reverse = false;
        } else {
          $scope.columnSort.reverse = !$scope.columnSort.reverse;
        }
        $scope.columnSort.sortColumn = column;
      };

    });
