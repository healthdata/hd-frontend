/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.user')
  .controller('requestNewCtrl', function (_, $scope, SkryvUserRequest, Notifications, ngDialog, gettextCatalog, $q, Organization, SkryvCollection, $location) {

      $scope.user = {};
      $scope.loading = false;
      $scope.dataCollections = [];
      $scope.userDataCollections = {};

      Organization.get().then(function (data) {
        $scope.organizations = data;

        if (data.length === 1) {
          $scope.user.organization = data[0];
        } else {
          angular.forEach(data, function (org) {
            if (org.main) {
              $scope.user.organization = org;
            }
          });
        }

        $scope.getDefinitions();
      }, function (err) {
        Notifications.error(err);
      });

      // Get definitions for organisation
      $scope.getDefinitions = function () {
        if ($scope.user.organization) {
          $scope.searching = true;
          SkryvCollection.getByOrganization($scope.user.organization).then(function (defs) {
            $scope.dataCollections = _.sortBy(defs, function(def) {
              return def.name.toLowerCase();
            });
            $scope.searching = false;
          }, function (err) {
            Notifications.error(err);
            $scope.searching = false;
          });
        }
      };

      $scope.store = function (newUser) {
        var user = angular.copy(newUser);

        if ($scope.user.organization) {
          user.organizationId = $scope.user.organization.id;
        }
        SkryvUserRequest.save(user).then(function () {
          Notifications.success('User account was successfully requested!');
          $location.path('/login');
        }, function (err) {
          Notifications.error(err);
        });
      };

      $scope.request = function () {
        $scope.user.dataCollectionNames = [];

        // Checkboxes
        if ($scope.dataCollections.length <= 5) {
          angular.forEach($scope.dataCollections, function (dataCollection) {
            if ($scope.userDataCollections[dataCollection.name]) {
              $scope.user.dataCollectionNames.push(dataCollection.name);
            }
          });
        }

        // Multiselect
        if ($scope.dataCollections.length > 5) {
          angular.forEach($scope.multiCollections, function (dataCollection) {
            $scope.user.dataCollectionNames.push(dataCollection.name);
          });
        }

        if (!$scope.request_form.$valid && $scope.organization) {
          Notifications.info('Please fill in all fields');
        } else if ($scope.user.dataCollectionNames.length === 0) {
          Notifications.info('Select a data collection for the user');
        } else {
          $scope.store($scope.user);
        }
      };

      // Multi-select button translations
      $scope.multiLang = {
        selectAll: gettextCatalog.getString('Select all'),
        selectNone: gettextCatalog.getString('Select none'),
        reset: gettextCatalog.getString('Reset'),
        search: gettextCatalog.getString('Search...'),
        nothingSelected: gettextCatalog.getString('None selected')
      };

    });
