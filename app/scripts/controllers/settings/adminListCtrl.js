/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
  'use strict';

  angular.module('skryv.settings')
    .filter('adminFilter', Filter)
    .controller('adminListCtrl', Controller);


  function Controller (gettextCatalog, ngDialog, $scope, Notifications, supportManager) {
    supportManager.getAdmins().then(
      function (response) {
        $scope.admins = response.data;
      },
      function (error) {
        console.error(error);
        Notifications.error(gettextCatalog.getString('Could not load admin list'));
      });

    $scope.show = function(admin) {
      ngDialog.open({
        template: 'views/dialog/settings/adminDetails.html',
        controller: [ '$scope', function ($scope) {
          $scope.admin = admin;
        } ]
      });
    };
  }

  function Filter() {
    return function(input, text) {
      if (!text) {
        return input;
      }

      var text = text.toLowerCase();
      return input.filter(function (entry) {
        if (entry.firstName && entry.firstName.toLowerCase().indexOf(text) > -1 ||
          entry.lastName && entry.lastName.toLowerCase().indexOf(text) > -1 ||
          entry.username && entry.username.toLowerCase().indexOf(text) > -1 ||
          entry.email && entry.email.toLowerCase().indexOf(text) > -1) {
          return entry;
        }
      });
    };
  }
})();