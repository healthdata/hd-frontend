/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {

  angular.module('skryv.settings')
    .controller('dcRequestCtrl', dcRequestCtrl);


  function dcRequestCtrl($scope, ngDialog, _, dataCollectionManager, SkryvSession, Notifications, gettextCatalog) {
    var user = SkryvSession.getLoggedUser();
    var allRequests = [];
    var filters = {
      treated: false
    };

    function showWarningDialog() {
      ngDialog.open({
        template: 'views/dialog/settings/dcRequestWarning.html',
        showClose: true,
        scope: $scope
      });
    }

    function showRequestDialog() {
      ngDialog.open({
        template: 'views/dialog/settings/dcRequest.html',
        showClose: true,
        scope: $scope,
        showClose: true
      });
    }

    // Since request.dataCollections can either be a String array or an object,
    // we choose to map to an array of objects instead...
    function mapDataCollectionsToObjectsArray(request) {
      if (_.isArray(request.dataCollections)) {
        request.dataCollections = request.dataCollections.map(function (entry) {
          return {
            key: entry
          };
        });
      } else if (_.isObject(request.dataCollections)) {
        var keys = _.keys(request.dataCollections);
        var mappedArray = [];
        keys.forEach(function (key) {
          mappedArray.push({
            key: key,
            value: request.dataCollections[key]
          });
        });

        request.dataCollections = mappedArray;
      }

      return request;
    }

    function initializeRequests() {
      return dataCollectionManager.getDataCollectionRequests(user.organization.id).then(function (response) {
        allRequests = response.map(mapDataCollectionsToObjectsArray);
      });
    }

    // Initialize requests
    initializeRequests().then(function () {
      $scope.filterChanged();
    });

    $scope.filterChanged = function () {
      if ($scope.showTreated) {
        $scope.requests = _.where(allRequests, {});
      } else {
        $scope.requests = _.where(allRequests, { treated: false });
      }
    }

    $scope.edit = function (request) {
      $scope.request = request;
      showWarningDialog();
    };

    $scope.show = function (request) {
      $scope.request = request;
      $scope.allowedCollections = _.where(request.dataCollections, { value: true })
      $scope.deniedCollections = _.difference(request.dataCollections, $scope.allowedCollections);
      showRequestDialog();
    };

    $scope.save = function () {
      dataCollectionManager.decide(user.organization.id, $scope.request).then(function (response) {
        Notifications.success(gettextCatalog.getString('Succesfully executed'));
        ngDialog.close();
        initializeRequests().then(function (response) {
          $scope.filterChanged();
        });
      }, function (error) {
        Notifications.error(gettextCatalog.getString(error));
        ngDialog.close();
      });
    };

    $scope.cancel = function () {
      // Clear request selection when the dialog is closed
      ngDialog.close();
    };
  }

})();
