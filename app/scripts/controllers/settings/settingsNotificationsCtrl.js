/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.settings')
  .controller('settingsNotificationsCtrl', function ($scope, SkryvUserConfiguration, _, Notifications) {
      $scope.isLoading = true;

      SkryvUserConfiguration.get().then(function (data) {
        $scope.userConfig = data;
        $scope.isLoading = false;
      }, function () {
        $scope.isLoading = false;
      });

      $scope.findConfig = function(key) {
        return _.findWhere($scope.userConfig, { key: key });
      };

      $scope.update = function (config) {
        if (/^\d*(s|m|h|d|w|M|y)?$/.test(config.value) && config.value) {
          updateConfig(config);
        } else {
          Notifications.info('Please enter a valid value');
        }
      };

      function updateConfig(config) {
        $scope.isLoading = true;
        SkryvUserConfiguration.update(config).then(function () {
          $scope.isLoading = false;
          Notifications.success('Setting changed');
        }, function (err) {
          $scope.isLoading = false;
          Notifications.error(err);
        });
      }

    });
