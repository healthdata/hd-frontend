/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function (){
  'use strict';
  angular.module('skryv.settings')
    .controller('profileCtrl', ProfileCtrl);


  function ProfileCtrl($scope, SkryvSession, SkryvUser, SkryvCollection,
                       Notifications, dataCollectionManager, gettextCatalog, PLATFORM) {
    var user = SkryvSession.getLoggedUser();

    // Requesting access to additional data collections is only available on hd4dp...
    $scope.isHD4DP = PLATFORM === 'hd4dp';
    if ($scope.isHD4DP) {
      $scope.activeCollections = SkryvSession.getCollectionNames();

      SkryvCollection.getByOrganization(user.organization).then(function (response) {
        $scope.orgCollections = response.filter(function (collection) {
          if ($scope.activeCollections.indexOf(collection.name) === -1) {
            return collection;
          }
        });
      });

      $scope.sendRequest = function () {
        var selectedCollections = $scope.selectedCollections.map(function (entry) {
          return entry.name;
        });

        dataCollectionManager.postDataCollectionRequest(user.organization.id, selectedCollections).then(function (response) {
          Notifications.success('Request submitted!');
          $scope.selectedCollections = [];
        }, function (error) {
          Notifications.error('Could not submit request');
        });
      };
    }

    $scope.editPassword = function () {
      if ($scope.user_form.$valid) {
        var editedUser =
          {
            id: user.id,
            name: user.username,
            oldPassword: $scope.user_form.oldPassword.$viewValue,
            newPassword: $scope.user_form.password.$viewValue
          };
        SkryvUser.updatePassword(editedUser).then(function () {
          Notifications.success('Password changed');
          Notifications.success('Please login with your new password');
          SkryvSession.destroyUserSession();
        }, function (err) {
          if (err.error) {
            switch (err.error) {
              case 'INVALID_USER_PASSWORD': {
                $scope.error = gettextCatalog.getString('Your old password is incorrect');
                break;
              }
              case 'UNAUTHORIZED': {
                Notifications.error(gettextCatalog.getString('Not allowed'));
                break;
              }
              default: {
                Notifications.error(gettextCatalog.getString('Password could not be changed'));
              }
            }
          }
        });
      } else {
        if ($scope.user_form.oldPassword.$viewValue) {
          $scope.error = gettextCatalog.getString('Repeat of new password is not the same');
        } else {
          $scope.error = gettextCatalog.getString('Please fill in all fields');
        }
      }
    };
  }
})();
