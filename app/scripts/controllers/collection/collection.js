/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.collection',
  [ 'skryv.conf', 'ngRoute', 'services.dcd', 'documentmodel', 'services.configurations', 'ngTagsInput' ])

  // Add active state to collection
  .filter('collectionPeriod', function (moment) {
    return function (collection) {
      var content = collection.content;
      var period = {};

      if (content && content.period) {
        collection.period = content.period;

        period.end = content.period.end;
        period.start = content.period.start;
        period.active = moment(new Date()).isBetween(content.period.start, content.period.end);
      } else {
        period.active = false;
      }
      return period;
    };
  })

  .filter('collectionActive', function () {
    return function (collection) {
      // Set all hours to 0, because we only need the date
      var today = new Date().setHours(0,0,0,0);
      var start = (collection.startDate ? new Date(collection.startDate).setHours(0,0,0,0) : undefined);
      var creation = (collection.endDateCreation ? new Date(collection.endDateCreation).setHours(0,0,0,0) : undefined);
      var submission = (collection.endDateSubmission ? new Date(collection.endDateSubmission).setHours(0,0,0,0) : undefined);
      var comments = (collection.endDateComments ? new Date(collection.endDateComments).setHours(0,0,0,0) : undefined);

      if (!start) {
        return 'inactive';
      }

      if (start && today < start) {
        return 'not started';
      }

      if (comments && comments <= today) {
        return 'finished';
      }

      if (submission && submission <= today) {
        return 'active, no new submissions';
      }

      if (creation && creation <= today) {
        return 'active, no new creations';
      }

      return 'active';
    };
  });
