/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.collection')
  .controller('collectionVersionCtrl', function ($scope, SkryvDefinitionManager, Notifications, PLATFORM, $location, $routeParams, $filter, $route, ngDialog, _, EnvironmentConfig) {

      $scope.loading = true;
      $scope.platform = PLATFORM;
      $scope.collectionName = $routeParams.name;

      var collections;
      var eligibleMajor;
      var eligibleVersions;
      var eligibleMinors;
      
      // Disable version creation
      EnvironmentConfig.isDdeEnabled().then(function(result) {
        $scope.ddeEnabled = result;
      });

      // Get definitions
      SkryvDefinitionManager.getByName($scope.collectionName).then(function (data) {
        data = $filter('orderBy')(data, [ 'version.major', 'version.minor' ], true);
        collections = angular.copy(data);
        var allVersions = [];

        // Set active property for collection
        angular.forEach(data, function (collection)
        {
          allVersions.push(collection);
          collection.active = $filter('collectionActive')(collection);
          var latest = _.max(collection.history, function(history){ return Date.parse(history.createdOn); });
          collection.period = $filter('collectionPeriod')(latest);

          angular.forEach(collection.history, function (wip, $index2) {
            // Latest Collection
            if (latest && latest.historyId === wip.historyId ) {
              wip.latest = true;
              wip.name = $index2;
            }

            wip.period = $filter('collectionPeriod')(wip);
            wip.parent = collection;
            wip.name = $index2 + 1;
            wip.wip = true;
            allVersions.push(wip);
          });
        });
        $scope.collections = allVersions;
        $scope.loading = false;
        calculateNewVersions();
      }, function (err) {
        $scope.collections = [];
        Notifications.error(err);
      });

      function calculateNewVersions () {
        eligibleVersions = _.map(angular.copy(collections), function (collection) {
          var version = collection.version;
          version.published = collection.published;
          version.collectionId = collection.id;
          return version;
        });

        eligibleMajor = getEligibleMajor(eligibleVersions);
        eligibleMinors = getEligibleMinors(eligibleVersions);

        $scope.toVersions = [];

        if (eligibleMajor) { $scope.toVersions.push(eligibleMajor); }
        if (eligibleMinors) { $scope.toVersions = _.union(eligibleMinors, $scope.toVersions); }
      }

      // Get versions to make search easier

      // Get eligible new major version: - Latest major is published
      function getEligibleMajor(versions) {
        var majorVersion = _.max(angular.copy(versions), function (version) { return version.major; });
        if (majorVersion.minor > 0 || (majorVersion.minor == 0 && majorVersion.published )) {
          majorVersion.major = majorVersion.major + 1;
          majorVersion.minor = 0;
          return majorVersion;
        }
      }

      // Get eligible new minor version: - Latest minor is published
      function getEligibleMinors(versions) {
        var majors = _.where(angular.copy(versions), { minor: 0 });
        var latestMinors = [];
        _.each(majors, function (major) {
          var minors = _.where(angular.copy(versions), { major: major.major });
          if (minors.length > 0) {
            var latest = _.max(minors, function (minor) { return minor.minor; });
            if (latest.published) {
              latest.minor = latest.minor + 1;
              latestMinors.push(latest);
            }
          }
        });
        return latestMinors;
      }


      $scope.newVersion = function () {
        ngDialog.open({
          template: 'views/dialog/collection/newVersion.html',
          showClose: false,
          controller: [ '$scope', function (scope) {
            scope.versions = eligibleVersions;
            scope.toVersions = $scope.toVersions;
            scope.create = function () {
              var from =  JSON.parse(scope.fromVersion);
              var to =  JSON.parse(scope.toVersion);
              var fromDc = _.find(angular.copy(collections), function (collection) { return collection.id === from.collectionId; });

              var newVersion = angular.copy(fromDc);
              delete newVersion.createdOn;
              delete newVersion.updatedOn;
              delete newVersion.id;
              newVersion.content = newVersion.history[newVersion.history.length - 1].content;
              delete newVersion.history;

              newVersion.published = false;
              newVersion.version.minor = to.minor;
              newVersion.version.major = to.major;

              SkryvDefinitionManager.save(newVersion).then(function () {
                Notifications.success('The data collection was successfully created!');
                ngDialog.close();
                $route.reload();
              }, function (err) {
                Notifications.error(err);
              });
            };
          } ]
        });
      };



      // Redirect to view
      $scope.viewCollection = function (collection) {
        if (collection.parent && collection.parent.id) {
          $location.path('/dde/' + collection.parent.id).search({version: collection.historyId });
        } else {
          $location.path('/collection/edit/' + collection.id);
        }
      };
    });
