/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.collection')
  .controller('collectionRawCtrl', function ($scope, SkryvDefinitionManager, Notifications, $route, _, skryvcollection, ngDialog) {
    $scope.collection = skryvcollection;
    $scope.latestContent = '{}';
    $scope.isDirty = false;
    $scope.isValid = true;

    SkryvDefinitionManager.getHistoryById(skryvcollection.dataCollectionDefinitionId).then(function (history) {
      $scope.latestWip = _.max(history, function (history) { return Date.parse(history.createdOn); });
      $scope.latestContent = JSON.stringify($scope.latestWip.content, undefined, 4);
      $scope.isValid = validateJson($scope.latestContent);
    });

    // Open save dialog
      $scope.save = function () {
        if ($scope.isValid) {
          ngDialog.open({
            template: 'views/dialog/collection/saveRaw.html',
            controller: [ '$scope', function (scope) {
              scope.createWip = function (item) {
                $scope.userDescription = scope.description;
                $scope.createFromRaw();
                scope.closeThisDialog(item);
              };
            } ]
          });
        } else {
          Notifications.warning('The JSON is not valid. Please edit the content.');
        }

      };

      // See if json is valid
      function validateJson() {
        try {
          JSON.parse($scope.latestContent);
        } catch (e) {
          return false;
        }
        return true;
      }

      $scope.changeRaw = function () {
        $scope.isDirty = true;
        $scope.isValid = validateJson($scope.latestContent);
      };

      $scope.createFromRaw = function () {
        $scope.collection.content = JSON.parse($scope.latestContent);
        $scope.collection.userDescription = $scope.userDescription;

        SkryvDefinitionManager.update($scope.collection).then(function (res) {
          Notifications.success('The data collection was succesfully saved!');
        }, function (err) {
          Notifications.error('Data collection could not be created');
        });
      }

    });
