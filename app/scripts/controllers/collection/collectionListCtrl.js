/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.collection')
  .controller('collectionListCtrl', function ($scope, SkryvDefinitionManager, Notifications, PLATFORM, $location, $filter, EnvironmentConfig) {

      $scope.loading = true;
      $scope.platform = PLATFORM;

      EnvironmentConfig.isDdeEnabled().then(function(result) {
        $scope.ddeEnabled = result;
      });

      // Get definitions
      SkryvDefinitionManager.getMajors().then(function (data) {

        // Set active property for collection
        angular.forEach(data, function (collection) {
          collection.period = $filter('collectionPeriod')(collection);
          collection.active = $filter('collectionActive')(collection);
        });
        $scope.collections = data;
        $scope.loading = false;
      }, function (err) {
        $scope.collections = [];
        Notifications.error(err);
      });

      // Redirect to view
      $scope.viewCollection = function (name) {
        $location.path('/collection/version/' + name);
      };

      // Redirect to new definition
      $scope.new = function () {
        $location.path('/collection/new');
      };


      // Table
      $scope.columnSort = {reverse: false, sortColumn: 'dataCollectionName'};

      $scope.sortByColumn = function (column) {
        if (column !== $scope.columnSort.sortColumn) {
          $scope.columnSort.reverse = false;
        } else {
          $scope.columnSort.reverse = !$scope.columnSort.reverse;
        }
        $scope.columnSort.sortColumn = column;
      };
    });
