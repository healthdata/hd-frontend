/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.collection')
  .controller('collectionEditCtrl', function ($scope, skryvcollection, $location, $filter, $routeParams, EnvironmentConfig,
                                              Notifications, SkryvDefinitionManager, DataCollectionGroupManager, ngDialog, moment, $window, $route, $q, _, SkryvTranslation, SkryvSession) {
    'use strict';

    $scope.collection = skryvcollection;
    $scope.isPublished = skryvcollection.published;
    $scope.showRepublish = false;
    $scope.isAdmin = SkryvSession.isAdmin();

    // Disable dde usage
    EnvironmentConfig.isDdeEnabled().then(function (result) {
      $scope.ddeEnabled = result;
    });

    // Disable publish action
    EnvironmentConfig.isPublishEnabled().then(function (result) {
      $scope.publishEnabled = result;
      getCollections();
    });

    function getCollections() {
      SkryvDefinitionManager.getHistoryById(skryvcollection.dataCollectionDefinitionId)
        .then(function (data) {
          $scope.history = data;
        });

      // Get info about the group
      DataCollectionGroupManager.getById(skryvcollection.dataCollectionGroupId).then(function (data) {
        $scope.group = data;

        var upperCollection = _.find(data.dataCollectionDefinitions, function (collection) {
          return (collection.minorVersion === skryvcollection.minorVersion + 1);
        });

        // Show republish if:
        // 1) new minor version exists that not is published
        // 2) Collection is latest minor and published
        if ((upperCollection && !upperCollection.published && skryvcollection.published) || (!upperCollection && skryvcollection.published)) {
          $scope.showRepublish = true;
        }
      });
    }

    // Save collection
    $scope.save = function () {
      if ($scope.skryv_collection_form.$valid) {

        // Copy prevents number from incrementing in view
        return SkryvDefinitionManager.update(angular.copy($scope.collection)).then(function (st) {
          Notifications.success('The data collection was successfully saved!');
          return st;
        }, function (err) {
          Notifications.error(err);
          return $q.reject(err);
        });

      } else {
        Notifications.info('Please fill in all fields');
        return $q.reject();
      }
    };

    $scope.viewWIP = function (id) {
      $location.path('/dde/' + skryvcollection.dataCollectionDefinitionId).search({ wipId: id });
    };

    // Make the delete function available in the scope
    $scope.remove = function (confirm) {
      if (confirm) {
        ngDialog.close();
        SkryvDefinitionManager.delete($scope.collection.dataCollectionDefinitionId).then(function () {
          Notifications.success('Data collection has been deleted');
          $location.path('/collection');
        }, function (err) {
          Notifications.error(err);
        });
      } else {
        ngDialog.open({
          template: 'views/dialog/collection/editRemove.html',
          showClose: false,
          scope: $scope
        });
      }
    };

    // Back
    $scope.back = function () {
      $location.path('#/collectiongroup');
    };

    // Publish dialog
    $scope.openPublishDialog = function () {
      ngDialog.open({
        template: 'views/dialog/collection/publish.html',
        controller: [ '$scope', function (scope) {
          scope.publish = function () {
            $scope.publishCollection();
            scope.closeThisDialog();
          };

          scope.saveAndPublish = function () {
            $scope.save().then(function () {
              $scope.publishCollection();
              scope.closeThisDialog();
            }, function () {
              scope.closeThisDialog();
            });
          };
        }]
      });
    };

    $scope.publishCollection = function () {
      SkryvDefinitionManager.publish(skryvcollection.dataCollectionDefinitionId).then(function () {
        skryvcollection.published = true;
        Notifications.success('The data collection was successfully published!');
        $route.reload();
      }, function (err) {
        Notifications.error(err);
      });
    };

    // Republish dialog
    $scope.openRePublishDialog = function () {
      ngDialog.open({
        template: 'views/dialog/collection/rePublish.html',
        controller: [ '$scope', function (scope) {
          scope.rePublish = function (item) {
            $scope.rePublishCollection();
            scope.closeThisDialog(item);
          };

          scope.saveAndRePublish = function () {
            $scope.save().then(function () {
              $scope.rePublishCollection();
              scope.closeThisDialog();
            }, function () {
              scope.closeThisDialog();
            });
          };
        }]
      });
    };

    $scope.rePublishCollection = function () {
      SkryvDefinitionManager.republish(skryvcollection.id).then(function () {
        Notifications.success('The data collection was successfully republished!');
        $route.reload();
      }, function (err) {
        Notifications.error(err);
      });
    };

    $scope.openFollowUp = function (followUp) {
      ngDialog.open({
        template: 'views/dialog/collection/followUp.html',
        controller: [ '$scope', function (scope) {

          var original = angular.copy(followUp);
          var collection = angular.copy($scope.collection);

          scope.conditions = angular.copy(followUp.conditions);
          scope.followUp = angular.copy(followUp);
          scope.action = 'Edit follow-up';
          scope.deleteEnabled = true;
          scope.showTranslations = false;

          // Search translations once for follow-up
          var allNL = [];
          var allFR = [];

          SkryvTranslation.getTranslations('nl', true).then(function (langs) {
            allNL = langs;
          });

          SkryvTranslation.getTranslations('fr', true).then(function (langs) {
            allFR = langs;
          });

          scope.nl = allNL[scope.followUp.label];
          scope.fr = allFR[scope.followUp.label];

          scope.toggleTranslations = function () {
            scope.showTranslations = !scope.showTranslations;
            scope.searchTranslation();
          };

          scope.searchTranslation = function () {
            scope.nl = allNL[scope.followUp.label];
            scope.fr = allFR[scope.followUp.label];
          };

          scope.change = function () {
            scope.searchTranslation();
          };

          scope.deleteFollowUp = function () {
            var search = _.findWhere(collection.followUpDefinitions, {name: original.name});
            collection.followUpDefinitions.splice(collection.followUpDefinitions.indexOf(search), 1);

            SkryvDefinitionManager.update(collection).then(function (st) {
              $scope.collection = st;
              scope.closeThisDialog();
              Notifications.success('The follow-up was successfully deleted!');
            }, function (err) {
              Notifications.error(err);
            });
          };

          scope.addFollowUp = function () {
            if (scope.followUpForm.$valid) {
              var search = _.findWhere(collection.followUpDefinitions, {name: original.name});

              var followUp = angular.copy(scope.followUp);
              followUp.conditions = [];

              _.each(scope.conditions, function (condition) {
                followUp.conditions.push(condition.text);
              });

              collection.followUpDefinitions[collection.followUpDefinitions.indexOf(search)] = followUp;

              SkryvDefinitionManager.update(collection).then(function (st) {
                $scope.collection = st;
                addTranslations();
                scope.closeThisDialog();
                Notifications.success('The follow-up was successfully updated!');
              }, function (err) {
                Notifications.error(err);
              });

            } else {
              Notifications.info('Please fill in all fields.');
            }
          };

          function addTranslations() {
            if (scope.nl && scope.fr) {
              SkryvTranslation.storeTranslations([{key: collection.label, nl: scope.nl, fr: scope.fr}]);
            }
          }

        }]
      });
    };

    $scope.createNewVersion = function () {
      var latestVersion = _.max($scope.group.dataCollectionDefinitions, function (def) {
        return def.minorVersion;
      });
      $scope.collection.minorVersion = latestVersion.minorVersion + 1;
      SkryvDefinitionManager.save($scope.collection).then(function (res) {
        $location.path('/collection/edit/' + res.dataCollectionDefinitionId);
      });
    };

    $scope.addFollowUp = function () {
      ngDialog.open({
        template: 'views/dialog/collection/followUp.html',
        controller: [ '$scope', function (scope) {
          var collection = angular.copy($scope.collection);
          var index = collection.followUpDefinitions.length;

          scope.followUp = {};
          scope.action = 'Add follow-up';
          scope.showTranslations = false;
          scope.nl = '';
          scope.fr = '';
          scope.conditions = angular.copy(collection.conditions);
          // Search translations once for follow-up
          var allNL = [];
          var allFR = [];

          SkryvTranslation.getTranslations('nl', true).then(function (langs) {
            allNL = langs;
          });

          SkryvTranslation.getTranslations('fr', true).then(function (langs) {
            allFR = langs;
          });

          scope.addFollowUp = function () {
            if (scope.followUpForm.$valid) {

              // Add follow-up array if non-existing
              if (!collection.followUpDefinitions || collection.followUpDefinitions.length < 0) {
                collection.followUpDefinitions = [];
              }

              // Convert objects to array with strings
              scope.followUp.conditions = [];

              _.each(scope.conditions, function (condition) {
                scope.followUp.conditions.push(condition.text);
              });

              // Add follow-up to collection
              collection.followUpDefinitions[index] = scope.followUp;

              SkryvDefinitionManager.update(collection).then(function (st) {
                $scope.collection = st;
                addTranslations();
                scope.closeThisDialog();
                Notifications.success('The follow-up was successfully added!');
              }, function (err) {
                Notifications.error(err);
              });

            } else {
              Notifications.info('Please fill in all fields.');
            }
          };

          scope.toggleTranslations = function () {
            scope.showTranslations = !scope.showTranslations;
            scope.searchTranslation();
          };

          scope.searchTranslation = function () {
            scope.nl = allNL[scope.followUp.label];
            scope.fr = allFR[scope.followUp.label];
          };

          scope.change = function () {
            scope.searchTranslation();
          };

          function addTranslations() {
            if (scope.nl && scope.fr) {
              SkryvTranslation.storeTranslations([ { key: collection.label, nl: scope.nl, fr: scope.fr } ]);
            }
          }

        }]
      });
    };

  });
