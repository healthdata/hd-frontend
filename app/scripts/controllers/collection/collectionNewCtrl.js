/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.collection')
  .controller('collectionNewCtrl', function ($scope, $location, $routeParams, $window, _, DataCollectionGroupManager, Notifications, SkryvDefinitionManager, SkryvCollection) {
    'use strict';

    if (!$routeParams.id) {
      $location.path('collectiongroup/list');
    }

    DataCollectionGroupManager.getById($routeParams.id)
      .then(function (group) {
        $scope.group = group;

        SkryvCollection.getCollections().then(function (salesForceCollections) {
          $scope.salesForceCollections = salesForceCollections[group.dataCollectionGroupName];
        });
      });

    $scope.group = {
      dataCollectionDefinitions: []
    };
    $scope.collection = {
      dataCollectionGroupId: $routeParams.id,
      content: {},
      published: false,
      minorVersion: 0
    };

    $scope.save = function () {
      if ($scope.skryv_collection_form.$valid) {

        SkryvDefinitionManager.save($scope.collection).then(function (st) {
          Notifications.success('The data collection was successfully created!');
          $location.path('/collection/edit/' + st.dataCollectionDefinitionId);
        }, function (err) {
          Notifications.error(err);
        });

      } else {
        Notifications.info('Please fill in all fields');
      }
    };

    $scope.renderMatch = function (match) {
      if (match) {
        return match;
      }
    };

    $scope.inputFormatter = function ($model) {
      return $model;
    };

    $scope.getCollections = function ($viewModel) {
      if ($scope.salesForceCollections) {
        return $scope.salesForceCollections.filter(function (collection) {
          if (collection.toLowerCase().indexOf($viewModel.toLowerCase()) > -1) {
            return collection;
          }
        });
      }

      return [];
    };

    $scope.calculateMinorVersion = function () {
      $scope.previousNotPublished = false;

      if ($scope.group && $scope.group.dataCollectionDefinitions.length > 0) {
        var found;

        // Grabbing the latest version...
        $scope.group.dataCollectionDefinitions.filter(function (definition) {
          if (definition.dataCollectionDefinitionName === $scope.collection.dataCollectionDefinitionName) {
            if (!found) {
              found = definition;
            }

            if (definition.minorVersion >= found.minorVersion) {
              found = definition;
            }
          }
        });

        if (!found) {
          $scope.collection.minorVersion = 0;
        } else {
          if (!found.published) {
            $scope.previousNotPublished = true;
          } else {
            $scope.collection.minorVersion = found.minorVersion + 1;
          }
        }
      }
    }
  });
