/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
angular.module('skryv.listView')
  .directive('stopEvent', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attr) {
        element.bind('click', function (e) {
          e.stopPropagation();
        });
      }
    };
  })

  .factory('columnState', function ($q) {
      var openDropdowns = [];

      function setOpenDropdown (label, index) {
        openDropdowns[index] = label;
      }

      function clearOpenDropdowns () {
        openDropdowns.splice(0,openDropdowns.length);
      }

      return {
        openDropdowns: openDropdowns,
        clearOpenDropdowns: clearOpenDropdowns,
        setOpenDropdown: setOpenDropdown
      }
    })

  .directive('columnObject', function ($compile, columnState) {
    return {
      restrict: 'E',
      scope: {
        object: '=',
        level: '=',
        updateLastView: '&'
      },
      link: function (scope, element) {
        var html = '<li class="dropdown-header">{{ object.label | translate }}</li>';
        html += '<column-field ng-repeat="field in object.fields" update-last-view="updateLastView()" field="field"></column-field>';
        html += '<column-section ng-repeat="objectSection in object.sections" section="objectSection" columns="columns" update-last-view="updateLastView()" level="level"></column-section>';

        var e = $compile(html)(scope);
        element.replaceWith(e);
      }
    };
  })

  .directive('columnField', function () {
    return {
      restrict: 'E',
      scope: {
        field: '=',
        updateLastView: '&'
      },
      replace: true,
      templateUrl: 'views/listview/columns/field.html'
    };
  })

  .directive('columnSection', function (columnState) {
    return {
      restrict: 'E',
      scope: {
        section: '=',
        level: '=',
        updateLastView: '&'
      },
      replace: true,
      templateUrl: 'views/listview/columns/section.html',
      link: function (scope) {
        scope.openNested = function (label) {
          columnState.setOpenDropdown(label, scope.level);
        };

        scope.openDropdowns = columnState.openDropdowns;
      }
    };
  });