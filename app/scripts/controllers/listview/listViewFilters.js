/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
angular.module('skryv.listView')
  // Set dates to correct format
  .filter('filterDate', function (moment) {
    function dateFilter(date, format) {
      if (date) {
        date = moment(new Date(date)).format(format);
      }
      return date;
    }

    return dateFilter;
  })

  // Order column //TODO: can be removed and use angular orderBy?
  .filter('orderObjectBy', function () {
    return function (items, field, reverse) {
      var filtered = [];
      angular.forEach(items, function (item) {
        filtered.push(item);
      });
      filtered.sort(function (a, b) {
        return (a[field] > b[field] ? 1 : -1);
      });
      if (reverse) filtered.reverse();
      return filtered;
    };
  })

.filter('getNestedColumns', function () {
    return function (items) {
      var columns = {};
      angular.forEach(items, function (item, name) {
        if (item.type == 'dropdown') {
          angular.forEach(item.columns, function (field, fieldName) {
            field.parent = name;
            field.valueLocation = item.valueLocation;
            columns[fieldName] = field;
          });
        } else if (item.type == 'checkbox') {
          columns[name] = item;
        }

      });
      return columns;
    };
  });
