/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
angular.module('skryv.listView')
  .controller('ListViewCtrl', function ($scope, $rootScope, $q, $route, $location, $filter, $interval, WorkflowRequest, ListViewDocColumns, $routeParams,
                                        Statuses, SkryvCsv, PLATFORM, gettextCatalog, openDocumentDefinition, openDocument, $timeout,
                                        SkryvSession, Notifications, SkryvUser, WorkflowActions, _, ngDialog, SkryvIntro, SkryvFile, moment,
                                        ListViewWorkflowsStore, ListViewWorkflowsActions, SkryvView, SkryvDefinition, ListViewExtraColumns, ListViewFilters, LoadingBar,
                                        localStorageService) {

    /* ##########################
     - Columns & Filters Services
     ------------------------- */
    var documentDefinition;
    var dataDefinition;
    $scope.columnSort = {};

    // Extra columns
    var unsubscribeListViewExtraColumns = ListViewExtraColumns.listen(function (getters) {
      // Set extra columns on scope
      $scope.extraColumns = _.sortBy(getters.extraColumns(), 'label');
      $scope.extraActives = getters.extraActives();
    });

    // Filters
    var unsubscribeListViewFilters = ListViewFilters.listen(function (getters) {
      // Set filters on scope
      $scope.filters = getters.filters();
    });

    // Document columns
    var unsubscribeListViewDocColumns = ListViewDocColumns.listen(function (getters) {
      // Set document columns on scope
      $scope.docColumns = getters.docColumns();
      $scope.docActiveColumns = getters.docActiveColumns();
      documentDefinition = getters.documentDefinition();
    });

    // Views
    var unsubscribeViews = SkryvView.listen(function (getters) {
      $scope.views = getters.views();
      $scope.activeView = getters.activeView();
      $scope.itemsPerPage = ($scope.activeView && $scope.activeView.itemsPerPage ? $scope.activeView.itemsPerPage.toString() : '25'); //TODO: can be improved WDC-1913
    });

    $scope.updateLastView = function () {
      if (dataDefinition) {
        SkryvView.setLastView(viewModel());
      }
    };

    /* ##########################
     - Initialise listView
     ------------------------- */
    // Get collection list
    $scope.collections = SkryvSession.getCollections();

    var uploadInProgressHandler = function (status, collection) {
      try {

        LoadingBar.start({
          id: status.startedOn + status.dataCollectionDefinitionId,
          autoProgress: false,
          label: 'Uploading CSV to ' + collection.name + ': '
        });

      } catch (error) {
        // If promise was already queued...
        //console.error(error);
      } finally {
        // We don't really care if promise was already queued or not, just set progress
        LoadingBar.edit({
          id: status.startedOn + status.dataCollectionDefinitionId,
          label: 'Uploading CSV to ' + collection.name + ': ' + (status.successCount + status.errorCount) + '/' + status.totalCount,
          progress: (status.successCount + status.errorCount) / status.totalCount * 100 // Set the percentage of the bar
        });
      }
    };

    var uploadSuccessHandler = function (status, collection, periodic) {
      try {
        LoadingBar.start({
          id: status.startedOn + status.dataCollectionDefinitionId,
          autoProgress: false,
          label: 'Uploading CSV to ' + collection.name + ': '
        });
      } catch (error) {
        // Promise was already queued, don't show the report when catching this error
        //console.error(error);
      } finally {

        LoadingBar.edit({
          id: status.startedOn + status.dataCollectionDefinitionId,
          label: 'CSV upload to ' + collection.name + ' was successful. (' + status.successCount + '/' + status.totalCount + ')'
        });

        LoadingBar.complete({
          id: status.startedOn + status.dataCollectionDefinitionId
        });

        // Promise was executed, show the report
        ngDialog.open({
          template: 'views/dialog/listview/upload.html',
          showClose: true,
          className: 'ngdialog-theme-default ngdialog-wide',
          controller: ['$scope', 'PLATFORM', function ($scope) {
            $scope.loading = false;
            $scope.hasResult = true;

            $scope.csvValid = _.where(status.records, {status: 'SUCCESS'});
            $scope.csvInValid = _.where(status.records, {status: 'ERROR'});
          }]
        });

        $interval.cancel(periodic);
        updateLoadedWorkflows();
      }

    };

    var uploadFailedHandler = function (status, collection, periodic) {

      try {
        LoadingBar.start({
          id: status.startedOn + status.dataCollectionDefinitionId,
          autoProgress: false,
          label: 'CSV upload to ' + collection.name + ' failed. (' + status.errorCount + '/' + status.totalCount + ')'
        });

        LoadingBar.fail({
          id: status.startedOn + status.dataCollectionDefinitionId
        });

        // Promise was queued, show the report
        ngDialog.open({
          template: 'views/dialog/listview/upload.html',
          showClose: true,
          className: 'ngdialog-theme-default ngdialog-wide',
          controller: ['$scope', 'PLATFORM', function ($scope) {
            $scope.loading = false;
            $scope.hasResult = true;

            $scope.csvValid = _.where(status.records, {status: 'SUCCESS'});
            $scope.csvInValid = _.where(status.records, {status: 'ERROR'});
          }]
        });

        updateLoadedWorkflows();
      } catch (error) {
        // Promise wasn't queued, don't show the report
      } finally {
        $interval.cancel(periodic);
      }
    };

    // Should be started any time the user navigates to the listview, since uploads can still be going on in the background or queue'd
    var initializePeriodicCheck = function (currentStatus) {
      var deferred = $q.defer();
      // We can't use the selected group here since it might already have changed since starting the upload.
      var collection = locateDefinition($scope.collections, currentStatus.dataCollectionDefinitionId).group;

      if (currentStatus.stoppedOn) {
        uploadSuccessHandler(currentStatus, collection);
        deferred.resolve(currentStatus);
        return deferred.promise;
      }

      // Use $interval to set up a periodic call every 10 seconds to check the status
      var periodic = $interval(function () {
        SkryvCsv.getStatus().then(function (response) {

          deferred.resolve(response);

          switch (response.status) {
            case 'UPLOAD_FAILED': {
              uploadFailedHandler(response, collection, periodic);
              break;
            }
            case 'UPLOAD_SUCCESS': {
              uploadSuccessHandler(response, collection, periodic);
              break;
            }
            default: {
              uploadInProgressHandler(response, collection);
              break;
            }
          }
        });
      }, 1000);

      return deferred.promise;
    };

    // Clear columns; check the sticky ones and clear filters
    $scope.defaultView = function () {
      ListViewExtraColumns.clearExtraColumns();
      ListViewDocColumns.setDefaultColumns();
      ListViewFilters.clearFilters();

      $scope.inputFilter = undefined;

      var defaultView = viewModel();
      defaultView.name = undefined;

      SkryvView.setLastView(defaultView);
      $scope.changeView(defaultView);

      defaultTableValues();
      searchFiltersChanged();
    };

    // Set view on scope
    $scope.changeView = function (view) {
      if (view && view.content) {
        ListViewFilters.setActiveFilters(view.content.filters);
        ListViewDocColumns.setActiveKeys(view.content.docColumns);
        ListViewExtraColumns.setActiveKeys(view.content.extraColumns);
        ListViewFilters.getActiveFilters();

        SkryvView.setLastView(view);

        if (view.content.columnSort) {
          $scope.columnSort = view.content.columnSort;
        }

        if (view.content.inputFilter && typeof view.content.inputFilter === 'string') {
          $scope.inputFilter = view.content.inputFilter;
        }

        $scope.activeView = view;
        $scope.itemsPerPage = view.content.itemsPerPage || '25';
      }

      searchFiltersChanged();
    };

    $scope.openCollection = function (collection) {
      $scope.openedCollection = collection;
    };

    // FIXME(arno): this should be in a util service.
    // Duplicated in participationCtrl & participationDataProviderCtrl...
    $scope.selectLatestVersion = function (group) {
      var latestVersion = _.max(group.versions, function (version) {
        return version.majorVersion;
      });
      $scope.selectVersionAndFirstCollection(latestVersion);
    };

    $scope.selectGroup = function (group) {
      // Just keep it in $scope, we don't really do anything w/ the group except show children.
      $scope.selectedGroup = group;
    };

    $scope.selectCollection = function (collection, view) {
      $scope.selectedRegistrations = [];
      $scope.selectedCollection = collection;
      initCollection(collection, view);
    };

    $scope.selectVersionAndFirstCollection = function (version) {
      $scope.selectedVersion = version;

      if (version.dataCollectionDefinitions.length > 0) {
        $scope.selectCollection(version.dataCollectionDefinitions[0]);
      }
    };

    $scope.selectVersion = function (version) {
      $scope.selectedVersion = version;
    };

    // Set active collection
    function initCollection(collection, view) {
      $scope.loading = true;

      return SkryvDefinition.get(collection.dataCollectionDefinitionId).then(function (response) {
        $scope.loading = false;
        $scope.selectedCollection = response;
        dataDefinition = response;

        SkryvView.resetInternalState(dataDefinition);
        ListViewDocColumns.resetInternalState(dataDefinition);
        ListViewExtraColumns.resetInternalState(dataDefinition);
        ListViewFilters.resetInternalState(dataDefinition);
        setCollectionDates();

        if (view) {
          $scope.changeView(view);
        } else {
          $scope.defaultView();
        }
      });
    }

    $scope.toggleStatusColumn = function () {
      $scope.activeView.statusCollapsed = !$scope.activeView.statusCollapsed;
    };

    // ##########################
    // - Columns
    // -------------------------

    // Get workflow property by stringified path
    $scope.getPropByString = function (obj, propString) {
      if (!propString)
        return obj;

      var prop, props = propString.split('.');

      for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
        prop = props[i];

        var candidate = obj[prop];
        if (candidate !== undefined) {
          obj = candidate;
        } else {
          break;
        }
      }

      return obj[props[i]];
    };

    // Status needs to have different styling
    $scope.statusNeedsAction = function (name) {
      var status = _.findWhere(Statuses.statuses(), {status: name});

      if (status && status.options) {
        return status.options.action;
      }
    };

    // Force dropdown to open on click -> because we don't use hover functionality
    $scope.openColumnDropdown = function (item) {
      $scope.openMenu = item;
    };

    // Default table
    function defaultTableValues() {
      // Set table
      $scope.columnSort = {'reverse': true, 'sortColumn': 'updatedOn'};
      $scope.timings = {};
      $scope.itemsPerPage = $scope.activeView.content.itemsPerPage || '25';
      $scope.currentPage = 1;
      $scope.updateLastView();
    }

    // Clear extra and doc columns
    $scope.clearColumns = function () {
      ListViewExtraColumns.clearExtraColumns();
      ListViewDocColumns.clearActiveColumns();
      defaultTableValues();
    };

    // initialize watcher for saving the current page
    $scope.$watch('currentPage', function (newValue, oldValue) {
      if (oldValue !== newValue) {
        localStorageService.set('listview.activePage', newValue);
      }
    });

    /* ##########################
     - Other functions
     ------------------------- */

    // Open datepicker dialog
    $scope.open = function ($event, opened) {
      $scope.openDatePicker = {};
      $event.preventDefault();
      $event.stopPropagation();

      // Prevents multiple datepickers to be opened at once
      $scope.openDatePicker[opened] = true;
    };

    // Navigate to document
    $scope.goToDoc = function (id) {
      $location.path('/document/' + id);
    };

    // Get label for technical status name
    $scope.getStatusLabel = function (workflow) {
      return gettextCatalog.getString(($filter('findWorkflowStatus')(workflow)));
    };

    /* ##########################
     - Other scope variables
     ------------------------- */

    // Platform
    $scope.platform = PLATFORM;

    // Datepicker format
    $scope.datepicker = {format: 'dd-MM-yyyy'};

    /* ##########################
     - Views
     ----------------------- */
    function viewModel() {
      var model = {
        dataCollectionDefinitionId: dataDefinition.id,
        dataCollectionName: dataDefinition.dataCollectionName,
        filters: ListViewFilters.getActiveFilters(),
        docColumns: ListViewDocColumns.getActiveKeys(),
        extraColumns: ListViewExtraColumns.getActiveKeys(),
        itemsPerPage: $scope.itemsPerPage,
        columnSort: $scope.columnSort,
        inputFilter: $scope.inputFilter
      };

      if ($scope.activeView) {
        model.statusCollapsed = $scope.activeView.statusCollapsed;
        model.name = $scope.activeView.name;
        model.inputFilter = $scope.inputFilter;
      } else {
        model.statusCollapsed = false;
        model.name = '';
        model.inputFilter = '';
      }

      return model;
    }

    // Add view to user metaData
    $scope.addView = function (duplicate) {
      var view = viewModel();

      ngDialog.open({
        template: 'views/dialog/listview/addView.html',
        showClose: false,
        controller: ['$scope', function (scope) {
          scope.duplicate = duplicate;

          scope.submitView = function () {
            if (scope.newViewName) {
              view.name = scope.newViewName;
              ngDialog.close();
              SkryvView.addView(view).then(function (view) {
                $scope.changeView(view);
              });
            }
          }
        }]
      });
    };

    // Update view from user metaData
    $scope.updateView = function () {
      ngDialog.open({
        template: 'views/dialog/listview/updateView.html',
        showClose: false,
        controller: ['$scope', function (scope) {
          scope.name = $scope.activeView.name;
          scope.updateViewConfirm = function () {
            ngDialog.close();
            SkryvView.updateView(viewModel()).then(function (view) {
              $scope.changeView(view);
            });
          };
        }]
      });
    };

    // Delete view from user metaData
    $scope.deleteView = function () {
      ngDialog.open({
        template: 'views/dialog/listview/deleteView.html',
        showClose: false,
        controller: ['$scope', function (scope) {
          scope.name = $scope.activeView.name;
          scope.deleteViewConfirm = function () {
            ngDialog.close();
            SkryvView.deleteView($scope.activeView);
          };
        }]
      });
    };

    // Save last view on route change
    $scope.$on('$locationChangeStart', function (event, next, current) {
      $scope.clearRegistrationSelection();
    });

    $scope.$on('skryv.user.logout', function () {
      $interval.cancel(periodic);
    });

    /* ##################################
     - ListViewWorkflowsStore listener
     ---------------------------------- */

    $scope.$on('$destroy', function () {
      unsubscribeListViewWorkflowsStore();
      unsubscribeListViewExtraColumns();
      unsubscribeListViewDocColumns();
      unsubscribeListViewFilters();
      unsubscribeViews();
    });

    var errorShown = false;
    var unsubscribeListViewWorkflowsStore = ListViewWorkflowsStore.listen(function (getters) {
      // In this function we should align the state of the list view controller with
      // the state of the ListViewWorkflows store. The data store is the source of truth!
      // Any state kept in the controller should only be for displaying stuff.
      // Note: there are no guarantees about how many times this function will execute.
      // Therefore, it should be idempotent, meaning that consecutive calls produce the same
      // result.

      // If there was an error, show it, but keep track of whether it was already shown.
      if (getters.hasError()) {
        if (!errorShown) {
          errorShown = true;
          Notifications.error(getters.error());
        }
      } else {
        errorShown = false;
      }

      var isLoading = getters.isLoading();
      $scope.loading = isLoading;
      if (isLoading) {
        // while we are loading the workflows, we do not change any other property
        // on the scope, to avoid confusing angular. For example, changing totalNumberOfWorkflows
        // to zero may cause problems with the pagination widget.
        return;
      }

      // Get all loaded workflows and put them on the $scope.
      $scope.shownWorkflows = getters.workflows();

      $scope.totalNumberOfWorkflows = getters.total();

      // Derive some information about the current search parameters
      var searchParams = getters.searchParams();

      if (searchParams) {
        // determine wether all statuses are selected, or none
        $scope.noStatuses = !_.some($scope.selectedStatuses);
        // Are there any search filters activated?
      } else {
        $scope.noStatuses = false;
        $scope.filtersActive = false;
      }

      $scope.filtersActive = _.size(ListViewFilters.getActiveFilters()) > 0;
    });

    /* ##################################
     - Search filter management
     ---------------------------------- */

    // This function changes a date input to a full ISO8601 string.
    function dateToISO(date) {
      if (!date) {
        return undefined;
      }
      return moment(date).format();
    }

    /**
     * With this function, all relevant search filter and pagination info is
     * collected and used to fetch the correct workflows.
     */
    function updateLoadedWorkflows() {
      // Don't search if no data collection
      if (!dataDefinition || !dataDefinition.id) {
        return
      }
      ;

      // collect the selected statuses
      var statuses = [];
      var followUps = [];

      _.each($scope.filters.statuses.fields, function (status) {
        if (status.value) {
          // Grouping of error label
          if (status.label === 'Error') {
            var errorStatuses = _.filter(Statuses.statuses(), function (status) {
              return status.platform === PLATFORM && status.label === 'Error';
            });

            _.each(errorStatuses, function (error) {
              statuses.push(error);
            });
          } else {
            statuses.push(status.trueValue);
          }
        }
      });

      statuses = _.uniq(statuses); // Duplicate ACTIONS_NEEDED

      // collect filters for active and/or planned follow-ups
      if ($scope.filters.followUp) {
        _.each($scope.filters.followUp.fields, function (props, name) {
          followUps.push({
            name: name,
            active: props.fields.active.value,
            planned: props.fields.planned.value,
            submitted: props.fields.submitted.value
          });
        });
      }

      var searchParams = {
        documentDefinition: documentDefinition, // TODO: ADD
        dataCollectionId: dataDefinition.id,
        queryText: $scope.inputFilter,
        sort: $scope.columnSort,

        // Filters
        onlyMine: $scope.filters.onlyMyWorkflows.value,

        dateOfBirthBefore: dateToISO($scope.filters.patient.fields.dateOfBirthBefore.value),
        dateOfBirthAfter: dateToISO($scope.filters.patient.fields.dateOfBirthAfter.value),
        patientName: $scope.filters.patient.fields.patientName.value,
        patientFirstName: $scope.filters.patient.fields.patientFirstName.value,
        patientID: $scope.filters.patient.fields.patientID.value,
        internalPatientID: $scope.filters.patient.fields.internalPatientID.value,

        createdBefore: dateToISO($scope.filters.creationDates.fields.createdBefore.value),
        createdAfter: dateToISO($scope.filters.creationDates.fields.createdAfter.value),

        lastUpdatedBefore: dateToISO($scope.filters.modificationDates.fields.lastUpdatedBefore.value),
        lastUpdatedAfter: dateToISO($scope.filters.modificationDates.fields.lastUpdatedAfter.value),

        dataProvider: $scope.filters.provider.fields.dataProvider.value,

        statuses: statuses,
        followUps: followUps
      };

      ListViewWorkflowsActions.loadWorkflows(searchParams, $scope.currentPage, $scope.itemsPerPage).then(function (data) {
        checkCheckboxes();
      });
    }

    /**
     * This function must be invoked whenever the user modifies any of the search filters.
     * Pagination is reset to page one.
     * This function is called once upon initialization so it can be used to initialize the active page.
     */
    function searchFiltersChanged() {
      if (!$scope.currentPage) {
        $scope.currentPage = localStorageService.get('listview.activePage') || 1;
      } else {
        $scope.currentPage = 1;
      }

      $scope.updateLastView();
      updateLoadedWorkflows();
      $scope.clearRegistrationSelection();
    }

    /**
     * The searchFiltersChanged function is put on the $scope twice, once for direct access
     * and one debounced version; the latter is to be used on text fields to avoid too frequent
     * invocations.
     */
    $scope.searchFiltersChanged = searchFiltersChanged;
    $scope.searchFiltersChangedDebounced = _.debounce(function () {
      $scope.$apply(function () {
        searchFiltersChanged();
      });
    }, 400);

    $scope.clearFilters = function () {
      ListViewFilters.clearFilters();
      searchFiltersChanged();
    };

// ##########################
// - Table settings
// -------------------------

    $scope.sortByColumn = function (column) {
      $scope.columnSort.reverse = ($scope.columnSort && column != $scope.columnSort.sortColumn) ? false : !$scope.columnSort.reverse;
      $scope.columnSort.sortColumn = column;
      searchFiltersChanged();
    };

    $scope.getSortColumn = function (column, value) {
      if (value) {
        column = value + '.' + column;
      }

      return column;
    };

    /**
     * This function should be called when the user modified the current page. It will
     * automatically load the correct workflows.
     */
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
      updateLoadedWorkflows();
    };

    $scope.itemsPerPageChanged = function () {
      // We go back to the first page, which will trigger a reload as well.
      // For WDC-1483, we replaced a $watch with an ng-change to avoid running
      // this code on controller initialization.
      $scope.setPage(1);
      $scope.updateLastView();
    };

// ##########################
// - Data Collection Action Dates
// -------------------------
    function setCollectionDates() {
      $scope.validCommentsDate = checkCollectionDate('endDateComments');
      $scope.validSubmitDate = checkCollectionDate('endDateSubmission');
      $scope.validCreationDate = checkCollectionDate('endDateCreation');
      $scope.validStartDate = !checkCollectionDate('startDate');
    }

    function checkCollectionDate(date) {
      if ($scope.selectedVersion && $scope.selectedVersion[date]) {
        var today = new Date();
        var checkDate = new Date($scope.selectedVersion[date]);
        return (checkDate > today);
      } else {
        return true;
      }
    }

// ##########################
// - CSV management
// -------------------------
    var csvFileName;

    $scope.redirect = function (path) {
      $location.path(path);
    };

    $scope.upload = function () {

      var definition = dataDefinition;
      var uniqueID = definition.content.uniqueIDExplanation;

      if ($scope.validSubmitDate) {
        // Open dialog to download from
        ngDialog.open({
          template: 'views/dialog/listview/upload.html',
          showClose: true,
          className: 'ngdialog-theme-default ngdialog-wide',
          controller: ['$scope', 'PLATFORM', function ($scope, PLATFORM) {
            $scope.platform = PLATFORM;
            $scope.loading = false;
            $scope.hasResult = false;

            var previousPendingUploads = [];

            LoadingBar.queue.forEach(function (entry) {
              if (entry.id.indexOf(definition.id) > -1) {
                previousPendingUploads.push(entry);
              }
            });

            var query = {
              query: {
                filtered: {
                  filter: {
                    bool: {
                      must: [],
                      must_not: [{
                        term: {
                          status: 'DELETED'
                        }
                      }]
                    }
                  }
                }
              }
            };
            var count = 0;

            // Get workflow count
            WorkflowRequest.getByDatacollection(definition.id, query).then(function (workflows) {
              count = workflows.hits.total;
            });

            $scope.uniqueID = uniqueID;

            $scope.$watch('files', function () {
              if ($scope.files && $scope.files[0]) {
                csvFileName = $scope.files[0].name;
                if (csvFileName.split('.').pop() === 'csv') {
                  // If unique id is not defined or no records shown -> simple upload
                  if (uniqueID && count !== 0) {
                    $scope.showActions = true;
                  } else {
                    $scope.upload('overwrite', 'csv');
                  }
                } else {
                  $scope.showActions = false;
                  Notifications.info('Please select a CSV file.');
                }
              }
            });

            $scope.upload = function (mode, master) {
              $scope.loading = true;

              var now = moment().subtract(30, 'second');

              SkryvFile.upload($scope.files).then(function (csv) {

                SkryvCsv.uploadCsv(csv, definition.id, csvFileName, mode, master).then(function (result) {

                  previousPendingUploads.forEach(function (entry) {
                    LoadingBar.complete({
                      id: entry.id
                    });
                  });

                  // Start a placeholder bar...
                  LoadingBar.start({
                    id: 'start_load_' + now + definition.id,
                    label: 'Starting upload'
                  });

                  LoadingBar.edit({
                    id: 'start_load_' + now + definition.id,
                    progress: 12
                  });

                  var promise = $interval(function () {
                    SkryvCsv.getStatus().then(function (status) {
                      var before = moment().subtract(40, 'seconds');
                      var after = moment().add(40, 'seconds');

                      var started = moment(status.startedOn);

                      // Check if the response returns the call you made
                      if (started.isBetween(before, after, 'seconds')) {

                        $interval.cancel(promise);

                        initializePeriodicCheck(status).then(function () {
                          // Only dismiss the bar when the new bar is displayed
                          ngDialog.close();
                          LoadingBar.dismiss({
                            id: 'start_load_' + now + definition.id
                          });
                        });
                      }
                    });
                  }, 4000);

                }).catch(function (error) {
                  ngDialog.close();
                });
              });
            };
          }]
        });
      }
    };

    function downloadCSV(workflows, options) {
      var fileName = dataDefinition.dataCollectionName + '.csv';
      SkryvCsv.downloadCsv(workflows, options)
        .then(downloadFileInBrowser)
        .then(
          successNotification,
          errorNotification);

      function downloadFileInBrowser(csv) {
        return SkryvFile.download(csv, fileName);
      }

      function successNotification() {
        Notifications.success('Download success');
        ngDialog.close();
      }

      function errorNotification() {
        Notifications.error('CSV download failed');
      }
    }

    $scope.downloadCSV = function (workflows) {
      downloadCSV(workflows);
    };

    $scope.downloadStableCSV = function (workflows) {
      downloadCSV(workflows, {stable: true});
    };

    function downloadAllCSV(options) {
      ngDialog.open({
        template: 'views/popupLoader.html',
        showClose: false,
        controller: ['PLATFORM', '$scope', function (PLATFORM, $scope) {
          $scope.platform = PLATFORM;
          $scope.message = gettextCatalog.getString('Downloading...');
        }]
      });

      WorkflowRequest.downloadAll(dataDefinition, options).then(function (data) {
        SkryvFile.download(data, dataDefinition.dataCollectionName + '.csv').then(function () {
          Notifications.success('Download success');
          ngDialog.close();
        }, function () {
          ngDialog.close();
        });
      }, function () {
        ngDialog.close();
      });
    }

    $scope.downloadAllCSV = function () {
      downloadAllCSV();
    };

    $scope.downloadAllStableCSV = function () {
      downloadAllCSV({stable: true});
    };

// ##########################
// - Actions
// -------------------------

    $scope.bulkWorkflows = 0;

    $scope.submitSelectedDialog = function () {
      $scope.bulkWorkflows = 0;
      if ($scope.selectedValidSubmit.length > 0) {
        // Open dialog
        var message = '<p>' + gettextCatalog.getString('Are you sure you want to submit the selected registrations? After submission, it will no longer be possible to modify the data.') + '</p>';
        ngDialog.open({
          template: message +
          '<div class="row">' +
          '<button class="btn btn-secundary" ng-click="submitSelected();closeThisDialog()" translate>Yes</button>' +
          "<button class='btn btn-primary' ng-click='closeThisDialog()'><a translate>No</a></button>" +
          '</div>',
          plain: true,
          showClose: false,
          scope: $scope
        });
      }
    };

    $scope.submitSelected = function () {
      // Confirmed submit
      ngDialog.close();

      if (!$scope.validSubmitDate && !$scope.validCommentsDate) {
        Notifications.error('Data collection not available for submit.');
      }

      // All selected workflows contain errors
      if ($scope.selectedRegistrations.length > 0 && ($scope.validSubmitDate || $scope.validCommentsDate)) {
        ngDialog.open({
          template: 'views/dialog/listview/submit.html',
          showClose: false,
          scope: $scope,
          controller: ['PLATFORM', '$scope', function (PLATFORM, $scope) {
            $scope.platform = PLATFORM;
            $scope.loadingSubmission = true;

            $scope.errors = [];

            // Call for submitting workflows
            angular.forEach($scope.selectedRegistrations, function (workflow) {
              if (workflow.status != 'IN_PROGRESS' && workflow.status != 'SUBMISSION_FAILED' && workflow.status != 'ACTION_NEEDED') {
                $scope.errors.push({
                  id: workflow.id,
                  message: 'Status' + " '" + workflow.status + "' " + gettextCatalog.getString('is not valid for submit')
                });
                $scope.bulkWorkflows++;
                // Mathias: temporary fix to enable submission of workflows with status "corrections needed" with no more validSubmitDate
              } else if (!$scope.validSubmitDate && !($scope.validCommentsDate && workflow.submittedOnce)) {
                $scope.errors.push({
                  id: workflow.id,
                  message: 'Status' + " '" + workflow.status + "' " + gettextCatalog.getString('is not valid for submit')
                });
                $scope.bulkWorkflows++;
              } else if (workflow.errors > 0) {
                $scope.errors.push({
                  id: workflow.id,
                  message: workflow.errors + ' ' + gettextCatalog.getString('errors in registration')
                });
                $scope.bulkWorkflows++;
              } else {
                WorkflowRequest.applyAction({workflowId: workflow.id, action: 'SUBMIT'}).then(function () {
                  $scope.bulkWorkflows++;
                }, function () {
                  $scope.errors.push({id: workflow.id, message: 'Could not submit'});
                  $scope.bulkWorkflows++;
                });
              }

              $scope.$watch('bulkWorkflows', function () {
                if ($scope.bulkWorkflows == $scope.selectedRegistrations.length && $scope.bulkWorkflows > 0) {
                  $scope.loadingSubmission = false;
                  $scope.success = $scope.selectedRegistrations.length - $scope.errors.length;

                  $timeout(function () {
                    updateLoadedWorkflows();
                  }, 2000);
                }
              }, true);
            });
          }]
        });
      }
    };

    $scope.deleteSelectedDialog = function () {
      if ($scope.selectedValidDelete.length > 0) {
        $scope.bulkWorkflows = 0;

        var message = '<p>' + gettextCatalog.getString('Are you sure you want to delete the selected registrations?');
        if ($scope.selectedValidDelete.length < $scope.selectedRegistrations.length) {
          message += '<br />' + gettextCatalog.getString('Only registrations with status "Open" can be deleted. Delete eligible registrations?');
        }
        message += '</p>';

        ngDialog.open({
          template: '<p>' + message +
          '<div class="row">' +
          '<button class="btn btn-secundary" ng-click="deleteSelected()" translate>Yes</button>' +
          "<button class='btn btn-primary' ng-click='closeThisDialog()'><a translate>No</a></button>" +
          '</div>',
          plain: true,
          showClose: false,
          scope: $scope
        });
      }
    };

    $scope.deleteSelected = function () {
      ngDialog.close();
      if ($scope.selectedValidDelete.length > 0) {
        ngDialog.open({
          template: 'views/popupLoader.html',
          showClose: false,
          scope: $scope,
          controller: ['PLATFORM', '$scope', function (PLATFORM, $scope) {
            $scope.platform = PLATFORM;
            $scope.message = gettextCatalog.getString('Deleting registrations, please wait ...');
          }]
        });

        // Delete action
        var length = $scope.selectedValidDelete.length;
        WorkflowRequest.bulkDelete($scope.selectedValidDelete).then(function () {
          $timeout(function () {
            Notifications.success(length + ' ' + gettextCatalog.getString('registration(s) deleted'));
            ngDialog.close();
            $scope.clearRegistrationSelection();
            updateLoadedWorkflows();
          }, 2000);
        }, function () {
          ngDialog.close();
          Notifications.error('Could not be deleted');
        });

      } else if ($scope.selectedValidDelete.length === 0) {
        Notifications.error('There are no registrations that can be deleted.');
      }
    };

// ##########################
// - Checkboxes
// -------------------------

    $scope.selectedRegistrations = [];
    $scope.selectedValidDelete = [];
    $scope.selectedValidSubmit = [];

    $scope.clickMainCheckbox = function () {
      mainCheckbox(false, true);
      selectBulkRegistrations($scope.shownWorkflows);
    };

    function mainCheckbox(value, bulk, indeterminate) {
      // display for main checkbox
      if (indeterminate) {
        $('.select-all').prop('indeterminate', indeterminate);
      } else {
        $('.select-all').prop('indeterminate', false);
      }
      
      $scope.checked = value;

      // If you also want to update all registrations
      if (bulk) {
        angular.forEach($scope.shownWorkflows, function (item) {
          item.checked = value;
        });
      }
    }

    function checkCheckboxes() {
      if ($scope.selectedRegistrations.length > 0) {
        $scope.selectedRegistrations.forEach(function (selected) {
          var found = _.findWhere($scope.shownWorkflows, {id: selected.id});
          if (found) {
            found.checked = true;
          }
        });
        if (_.intersection($scope.selectedRegistrations, $scope.shownWorkflows).length === $scope.shownWorkflows.length) {
          $scope.checked = true;
          mainCheckbox(true, true, true);
        } else {
          mainCheckbox(false, false, true);
        }
      } else {
        $scope.checked = false;
        mainCheckbox(false, true);
      }
    }

    $scope.clearRegistrationSelection = function () {
      $scope.checked = false;
      $scope.selectedRegistrations = [];
      $scope.selectedValidDelete = [];
      $scope.selectedValidSubmit = [];
      mainCheckbox(false, true);
    };

    function selectBulkRegistrations(registrations) {

      if ($scope.selectedRegistrations.length > 0) {

        if (_.intersection($scope.selectedRegistrations, registrations).length === registrations.length) {
          $scope.selectedRegistrations = _.difference($scope.selectedRegistrations, registrations);
        } else {
          var toSelect = _.without(registrations, $scope.selectedRegistrations);
          $scope.selectedRegistrations = $scope.selectedRegistrations.concat(toSelect);
        }

      } else {
        $scope.selectedRegistrations = $scope.selectedRegistrations.concat(registrations);
      }

      refreshFilters();
    }

    $scope.selectRegistration = function (registration) {
      if (_.findWhere($scope.selectedRegistrations, {id: registration.id})) {
        var index = _.indexOf($scope.selectedRegistrations, registration);
        $scope.selectedRegistrations.splice(index, 1);
      } else {
        $scope.selectedRegistrations.push(registration);
      }

      refreshFilters();
    };

    $scope.goToParticipation = function () {
      var collection = $scope.selectedCollection;

      $location.path('/participation').search({id: collection.id});
    };

    $scope.getDisabledHelpText = function () {
      if (!$scope.validCreationDate) {
        return gettextCatalog.getString('The creation period has ended.');
      }
      if (!$scope.validStartDate) {
        return gettextCatalog.getString('The data collection period hasn\'t started yet.');
      }
      if (!$scope.selectedVersion.participationId) {
        return gettextCatalog.getString('You are not yet participating in this data collection period.');
      }
    };

    function refreshFilters() {
      //$scope.selectedRegistrations = $filter('filter')($scope.shownWorkflows, { checked: true });
      var inProgress = $filter('filter')($scope.selectedRegistrations, {status: 'IN_PROGRESS'}, true);
      var actionNeeded = $filter('filter')($scope.selectedRegistrations, {status: 'ACTION_NEEDED'}, true);
      var submissionFailed = $filter('filter')($scope.selectedRegistrations, {status: 'SUBMISSION_FAILED'}, true);
      var submissionOnceFailed = $filter('filter')($scope.selectedRegistrations, {
        status: 'SUBMISSION_FAILED',
        submittedOnce: true
      }, true);
      var deleteableFollowUpsActions = $filter('filter')($scope.selectedRegistrations, {
        status: 'ACTION_NEEDED',
        deletable: true
      }, true);
      var inProgressDeleteable = $filter('filter')($scope.selectedRegistrations, {
        status: 'IN_PROGRESS',
        deletable: true
      }, true);

      /* Submittable when submissionDate passed
       Registrations with status ACTION_NEEDED
       Registrations with status SUBMISSION_FAILED which already submitted once before
       */

      /* Submittable when submissionDate is open
       Registrations with status IN_PROGRESS
       Registrations with status ACTION_NEEDED
       Registrations with status SUBMISSION_FAILED
       */

      // Feedback flow
      if (!$scope.validSubmitDate && $scope.validCommentsDate) {
        $scope.selectedValidSubmit = _.union(actionNeeded, submissionOnceFailed);
      }
      // Normal flow
      else if ($scope.validSubmitDate && $scope.validCommentsDate && $scope.validStartDate) {
        $scope.selectedValidSubmit = _.union(inProgress, actionNeeded, submissionFailed);
      } else {
        $scope.selectedValidSubmit = [];
      }

      /* Deletable when passed
       Registrations with status IN_PROGRESS
       Registrations with status ACTION_NEEDED, with no submitted followups
       */

      $scope.selectedValidDelete = _.union(deleteableFollowUpsActions, inProgressDeleteable);

      checkCheckboxes();
    }

    // TODO(arno): This should be in a util service.
    // Duplicated in participationCtrl & participationDataProviderCtrl
    function locateDefinition(groups, id) {
      var result = undefined;
      // We iterate over the collections, then over the versions and finally over the definitions,
      // if the definitionId of a definition is equal to given id, we have found what we're looking for and
      // the lookup will end and return result.
      _.find(groups, function (group) {
        return _.find(group.versions, function (version) {
          return _.find(version.dataCollectionDefinitions, function (collection) {
            if (collection.dataCollectionDefinitionId !== id) {
              return false;
            }

            result = {
              group: group,
              version: version,
              collection: collection
            };

            return true;
          });
        });
      });

      return result;
    }

    /* Initialize view! */
    // Get last view from user
    var lastView = SkryvView.getLastView();

    // TODO(arno): refactor
    // This whole function can go into a util service.
    // The $routeParams cases can be made generic, since they work all the same on different screens.
    // Duplicated in participationCtrl & participationDataProviderCtrl
    if ($routeParams.collectionId) {
      try {
        var id = JSON.parse($routeParams.collectionId);
        var found = locateDefinition($scope.collections, id);

        if (found) {
          $scope.selectGroup(found.group);
          $scope.selectVersion(found.version);
          $scope.selectCollection(found.collection, lastView);
        }
      } catch (exception) {

      }

    } else if (lastView && lastView.dataCollectionDefinitionId) {
      var found = locateDefinition($scope.collections, lastView.dataCollectionDefinitionId);

      if (found) {
        $scope.selectGroup(found.group);
        if (found.version) {
          $scope.selectedVersion = found.version;
          $scope.selectCollection(found.collection, lastView);
        } else {
          $scope.selectLatestVersion(found.group);
        }
      }
    }

    // If selectedGroup isn't selected at this point...
    if (!$scope.selectedGroup) {
      // Just select first group by default
      $scope.selectGroup(_.sortBy($scope.collections, function (collection) {
        return collection.name.toLowerCase();
      })[0]);
      if ($scope.selectedGroup.versions) {
        $scope.selectLatestVersion($scope.selectedGroup);
      }

      if ($scope.selectedVersion.dataCollectionDefinitions.length > 0) {
        $scope.selectCollection($scope.selectedVersion.dataCollectionDefinitions[0], lastView);
      }
    }
  });
