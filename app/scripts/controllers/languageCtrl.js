/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
angular.module('frontendApp')
.controller('LanguageCtrl', function ($scope, $location, SkryvLanguage, $window) {
    $scope.lang = '';

    // Get Language for session
    var browserLang = $window.navigator.language || $window.navigator.userLanguage;

    if (SkryvLanguage.getLanguage()) {
      $scope.lang = SkryvLanguage.getLanguage();
    } else if (browserLang === 'nl' || browserLang === 'en' || browserLang === 'fr') {
      $scope.lang = browserLang;
    } else {
      $scope.lang = 'en';
    }

    // Set Language
    SkryvLanguage.setLanguage($scope.lang);
});