/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.oauth')
  .controller('oauthLoginCtrl', function ($rootScope, $scope, $location, $http, OAUTH_SERVER_URL, APPLICATION_ID, REQUEST_ACC_URL, SkryvIntro,
            SkryvOauth, SkryvSession, Notifications, Organization, PLATFORM, gettextCatalog) {

    $scope.user = {};
    $scope.platform = PLATFORM;
    $scope.loading = false;
    $scope.reqAccUrl = REQUEST_ACC_URL;

    Organization.get().then(function (data) {
      $scope.organizations = data;

      if (data.length === 1) {
        $scope.user.organization = data[0];
      } else {
        angular.forEach(data, function (org) {
          if (org.main) {
            $scope.user.organization = org;
          }
        });
      }
    });

    $scope.doSignin = function () {
      SkryvSession.destroyUserSession();

      // Enable introduction video
      $rootScope.dialogPopped = true;
      $rootScope.skipLastView = true;

      $scope.loading = true;

      // Get Tokens for user and store in session
      SkryvOauth.getAccessToken($scope.user.email, $scope.user.password, APPLICATION_ID, $scope.user.organization.id).then(function () {
        SkryvSession.loadApp().then(function (previousPath) {
          if (previousPath) {
            $location.path(previousPath);
          } else if (SkryvSession.isAdmin()) {
            $location.path('/settings');
          } else {
            if (PLATFORM === 'hd4dp') { SkryvIntro.popDialogFeatures(); }
            $location.path('/data collection');
          }
        });
      }, function (error) {
        $scope.loading = false;

        if (error.status === 400) {
          Notifications.error(error.data.error_description);
        } else if (error.status === 404) {
          Notifications.error(gettextCatalog.getString(PLATFORM.toUpperCase() + ' is not available at the moment'));
        } else {
          Notifications.error(gettextCatalog.getString('An internal error occured while verifying your credentials.\nPlease contact healthdata support or your IT service.'));
        }
      });
    };
  });