/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('skryv.oauth')
    .controller('ResetPasswordCtrl', Controller);


  function Controller($location, $routeParams, $scope, gettextCatalog, Notifications, SkryvOauth) {
    // Don't show this page if there's no organizationId or token in the route...
    if (!$routeParams.organizationId || !$routeParams.token) {
      $location.path('/login');
    }

    // Reset password...
    $scope.resetPassword = function() {
      var organizationId = $routeParams.organizationId;
      var token = $routeParams.token;
      SkryvOauth.resetPassword(organizationId, token, $scope.password).then(function () {
        Notifications.success('Password successfully changed!');
        $location.path('/login');
      }, function (error) {
        if (error.status === 404) {
          Notifications.error(gettextCatalog.getString('No user was found with that email'));
        } else {
          Notifications.error(gettextCatalog.getString("Could not reset password"));
        }
      });
    }
  }
})();