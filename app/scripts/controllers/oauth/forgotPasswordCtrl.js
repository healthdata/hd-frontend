/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('skryv.oauth')
    .controller('ForgotPasswordCtrl', Controller);


  function Controller($scope, gettextCatalog, Notifications, Organization, SkryvOauth) {

    $scope.sendResetMail = function () {
      SkryvOauth.sendResetMail($scope.selectedOrganization.id, $scope.email)
        .then(function (response) {
          Notifications.success(gettextCatalog.getString('Email was sent!'));
        }, function (error) {
          if (error.status === 404) {
            Notifications.error(gettextCatalog.getString('No user found with that email'));
          } else {
            Notifications.error(gettextCatalog.getString('Could not reset mail'));
          }
        });
    };

    Organization.get().then(function (response) {
      response.forEach(function (entry) {
        if (entry.main) {
          $scope.selectedOrganization = entry;
        }
      });

      $scope.organizations = response;
    });
  }
})();