/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
angular.module('frontendApp')
  .controller('FooterCtrl', function ($scope, $location, SkryvVersion) {

      $scope.visible = true;

      // Hide footer when in documentView
      $scope.$on('$routeChangeSuccess', function (event) {
        $scope.visible = !(($location.path().indexOf('document/') > -1) || ($location.path().indexOf('documentDefinition/') > -1) || ($location.path().indexOf('workbench') > -1) || ($location.path().indexOf('dde/') > -1) || $location.path().indexOf('participation') > -1 || $location.path().indexOf('data collection') > -1) ;
        introJs().exit();
      });

      SkryvVersion.get().then(function (data) {
        $scope.version = data.value;
      });

    });
