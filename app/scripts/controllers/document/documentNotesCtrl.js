/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.document')
  .controller('documentNotesCtrl',
    function ($scope, PLATFORM, SkryvSession, Statuses, _, DocumentNotes, $timeout) {
      var openNote = '';
      $scope.sure = false;
      $scope.commentMode = false;
      $scope.replyError = false;
      $scope.newNoteError = false;

      // Filters
      var unsubscribeDocumentNotes = DocumentNotes.listen(function (getters) {
        // Set notes on scope
        $scope.generalNotes = _.filter(getters.notes(), function (note) {
          return !note.fieldPath && !note.resolved;
        });
        $scope.fieldNotes = _.filter(getters.notes(), function (note) {
          return note.fieldPath  && !note.resolved;
        });
        $scope.resolvedNotes = _.filter(getters.notes(), function (note) {
          return note.resolved;
        });
        $scope.newFieldNotes = getters.newFieldNotes();
        $scope.apiError = getters.apiError();
      });

      $scope.$on('$destroy', function () {
        unsubscribeDocumentNotes();
      });

      $scope.linkedManipulator = DocumentNotes.linkedManipulator;

      // Show reply dialàg
      $scope.toggleCommentMode = function (bool) {
        $scope.replyError = false;
        if (bool) {
          $scope.commentMode = bool;
        } else {
          $scope.commentMode = !$scope.commentMode;
        }
      };

      $scope.clearFieldNote = function (fieldNote) {
        DocumentNotes.clearFieldNote(fieldNote);
      };

      // Open input for new note
      $scope.toggleNote = function () {
        $scope.newNoteError = false;
        $scope.newNote = !$scope.newNote;
      };

      // Add new note to workflow notes
      $scope.addNote = function (title, content) {
        $scope.newNoteError = false;

        if (title && content) {
          DocumentNotes.addNote(title, content).then(function () {
            $scope.openNote($scope.generalNotes[$scope.generalNotes.length - 1]);
            $scope.toggleNote();
          });
        } else {
          $scope.newNoteError = true;
        }
      };

      $scope.addFieldNote = function (fieldNote) {
        if (fieldNote.content) {
          DocumentNotes.addFieldNote(fieldNote).then(function () {
            $scope.openNote($scope.fieldNotes[$scope.fieldNotes.length - 1]);
          });
        }
      };

      // Make comments visible
      $scope.openNote = function (note, $event) {
        if ($scope.isNoteOpen(note)) {
          return openNote = null;
        }

        openNote = note.id;

        if ($event) {
          $timeout(function () {
            var content = document.querySelector('.doc-container-right .doc-content');
            $(content).animate({scrollTop: $event.currentTarget.offsetTop - 5}, "slow");
          }, 600);
        }
      };

      // Check if comment is open
      $scope.isNoteOpen = function (note) {
        return note && (openNote === note.id);
      };

      // Add comment to note
      $scope.addComment = function (note, message) {
        $scope.replyError = false;

        if (!message) {
          $scope.replyError = true;
          return;
        }

        DocumentNotes.addComment(message, note).then(function () {
          $scope.toggleCommentMode(false);

          var content = document.querySelector('.comment-reply-wrapper');
          $(content).animate({scrollTop: '0px' }, "slow");
        });
      }

      $scope.toggleNoteSolved = function (note) {
        // close node box
        if (!note.resolved) {
          openNote = null;
        }

        DocumentNotes.toggleSolved(note);
      };

      $scope.canComment = function (workflow) {
        var isCommentable = Statuses.hasProperty(workflow.status, 'commentable');

        if (isCommentable && $scope.dcd.endDateComments) {
          var currentDate = new Date();
          var endDateComments = new Date($scope.dcd.endDateComments);

          return currentDate < endDateComments;
        }

        return isCommentable;
      };

      // Check if notes can be edited depending on the workflow status
      $scope.isNoteEditableStatus = function (workflow) {
        return (workflow && Statuses.hasProperty(workflow.status, 'commentable'));
      };
    })
