/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.document')
  .controller('documentHd4pcCtrl', function ($scope, $location, $routeParams, SkryvDefinition, WorkflowRequest, $route, CurrentDocument, Notifications, _) {
      $scope.readOnly = false;
      $scope.validDate = true;
      $scope.activeTabLeft = 'toc';
      $scope.activeTabResopnsive = 'toc';

      // Load workflow
      if ($route.current.params.uuid) {
        WorkflowRequest.getSingleForm($route.current.params.uuid).then(function (data) {
          var doc = data.document;
          var workflow = data;

          SkryvDefinition.get(workflow.dataCollectionDefinitionId).then(function (dcd) {
            $scope.definition = dcd;
            linkDocument(doc, workflow, dcd);
          }, function (err) {
            Notifications.error(err);
          });
        }, function () {
          Notifications.error('Workflow not found');
        });
      }

      var save = {};

      // Call the save action
      save._makeSearchRequest = function () {
        CurrentDocument.autoSave();
      };

      // Wait x time to trigger function (avoid multiple calls)
      save.autoSave = _.debounce(save._makeSearchRequest, 2000);

      var unsubscriAutoSaveObj = CurrentDocument.listen(function (getters) {
        $scope.autoSaveObj = getters.autoSaveObj();
      });

      $scope.$on('$destroy', function () {
        unsubscriAutoSaveObj();
      });

      function linkDocument(doc, workflow, dcd) {
        CurrentDocument.loadDocument(doc, workflow, dcd, false, function () {
          $scope.dcd = dcd;
          $scope.registration = workflow;
          $scope.documentLoaded = true;
          $scope.documentModel = CurrentDocument.currentDocumentModel();
          $scope.displayDebugInfo = $routeParams.debug === 'true';

          Notifications.success('The registration was loaded and is ready to work on');

          // Reload documentModel when currentDocument model content has been saved
          var unsubscribe = CurrentDocument.currentDocumentModel().document.listen(function () {
            $scope.documentModel = CurrentDocument.currentDocumentModel();
            save.autoSave();
          });

          $scope.$on('$destroy', function () {
            unsubscribe();
          });
        });
      }
    });
