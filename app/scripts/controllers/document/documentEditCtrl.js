/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.document')
  .controller('documentEditCtrl',
    function ($scope, $location, $routeParams, Statuses, skryvdcd, $q, WorkflowNotes,
              skryvdoc, $anchorScroll, WorkflowRequest, DocumentMenuActions, SkryvDefinition,
              CurrentDocument, Notifications, PLATFORM, $window, gettextCatalog, ngDialog, SkryvSession, $filter, skryvnotes, DocumentNotes) {
      'ngInject';

      var workflowID = $routeParams.workflowId;

      $scope.documentLoaded = false;
      $scope.platform = PLATFORM;

      // Reset comments if previous were active
      DocumentNotes.resetInternalState([]);

      // Get User
      $scope.loggedUser = SkryvSession.getLoggedUser();

      // Check submission, start and end comment date
      function checkDate(status, dcd) {
        // Use group for calculating validDate...
        var def = angular.copy(dcd).dataCollectionGroup;
        var currentDate = $filter('filterDate') (new Date(), 'YYYY-MM-DD HH:mm:ss.sss');
        if (def) {
          if (def.startDate) {
            var copareDateStart = $filter('filterDate') (def.startDate, 'YYYY-MM-DD HH:mm:ss.sss');
          } else {
            var copareDateStart = new Date();
            currentDate = $filter('date') (copareDateStart, 'YYYY-MM-DD HH:mm:ss.sss');
          }

          if (copareDateStart < currentDate && def.startDate != null) {
            if (def.endDateComments) {
              var copareDateComments = $filter('filterDate') (def.endDateComments, 'YYYY-MM-DD HH:mm:ss.sss');
            }
            if (def.endDateSubmission) {
              var copareDateSubmit = $filter('filterDate') (def.endDateSubmission, 'YYYY-MM-DD HH:mm:ss.sss');
            }
            if (def.endDateSubmission) {
              if (Statuses.hasProperty(status, 'editable')) {
                return (copareDateSubmit > currentDate || $scope.reSubmit);
              } else {
                return false;
              }
            } else {
              return true;
            }
          } else {
            return false;
          }
        }
      };

      function canComment() {
        var isCommentable = Statuses.hasProperty($scope.registration.status, 'commentable');

        if (isCommentable && $scope.dcd.endDateComments) {
          var currentDate = new Date();
          var endDateComments = new Date($scope.dcd.endDateComments);

          return currentDate < endDateComments;
        }

        return isCommentable;
      };

      // Load workflow
      if (skryvdoc) {
        // Get workflow
        WorkflowRequest.get(workflowID).then(function (workflow) {
          if ((workflow.flags && workflow.flags.corrections && (workflow.status === 'NEW' || workflow.status === 'ACTION_NEEDED')) || workflow.status === 'ANNOTATIONS_NEEDED') {
            $scope.defaultTab = 'comments';
          }

          // Use workflow current version
          SkryvDefinition.get(workflow.dataCollectionDefinitionId).then(function (dcd) {
            $scope.definition = dcd;

            if (PLATFORM === 'hd4dp') {
              var searchCollection = $filter('filter')(SkryvSession.getCollections(), { label: dcd.dataCollectionName });

              if (searchCollection.length == 1) {
                var searchVersion = $filter('filter')(searchCollection[0].versions, { id: dcd.id });

                if (searchVersion.length == 1) {
                  if (!searchVersion[0].participationId) {
                    Notifications.error(gettextCatalog.getString('Not participating in') + ' ' + dcd.dataCollectionName + ' v' + dcd.version.major + '.' + dcd.version.minor);
                    $location.path('/participation').search({ name: dcd.dataCollectionName, id: dcd.id });
                    return;
                  }
                }
              }
            }

            linkDocument(skryvdoc, workflow, dcd);
          });

        }, function (err) {
          Notifications.error(err);
        });
      } else if (skryvdcd) {
        $scope.definition = skryvdcd;

        if (PLATFORM === 'hd4dp') {
          // TODO: Remove when SkryvDefinition.get also returns participationId
          var searchCollection = $filter('filter')(SkryvSession.getCollections(), { label: skryvdcd.dataCollectionName });

          if (searchCollection.length == 1) {
            var searchVersion = $filter('filter')(searchCollection[0].versions, { id: skryvdcd.id });

            if (searchVersion.length == 1) {
              if (!searchVersion[0].participationId) {
                Notifications.error(gettextCatalog.getString('Not participating in') + ' ' + skryvdcd.dataCollectionName + ' v' + skryvdcd.version.major + '.' + skryvdcd.version.minor);
                $location.path('/participation').search({ name: skryvdcd.dataCollectionName, id: skryvdcd.id });
                return;
              }
            }
          }
        }

        linkDocument({}, {
          status: 'IN_PROGRESS',
          reSubmit: false,
          newWorkflow: true,
          dataCollectionName: skryvdcd.dataCollectionName,
          dataCollectionDefinitionId: skryvdcd.id
        }, skryvdcd);
      }

      function linkDocument(doc, workflow, dcd) {
        var readOnly = !Statuses.hasProperty(workflow.status, 'editable');

        // Init comments
        DocumentNotes.resetInternalState(skryvnotes);

        // Init document
        CurrentDocument.loadDocument(doc, workflow, dcd, readOnly, function () {
          $scope.dcd = dcd;
          $scope.registration = workflow;
          $scope.readOnly = readOnly;
          $scope.needComments = Statuses.hasProperty(workflow.status, 'commentable');
          $scope.canComment = canComment();
          $scope.documentLoaded = true;
          $scope.uniqueID = dcd.content.uniqueIDExplanation;
          $scope.validDate = checkDate(workflow.status, dcd);
          $scope.reSubmit = workflow.submittedOnce;
          $scope.documentModel = CurrentDocument.currentDocumentModel();
          $scope.displayDebugInfo = $routeParams.debug === 'true';

          if (!readOnly) {
            Notifications.success('The registration was loaded and is ready to work on');
          } else {
            Notifications.success('The registration was loaded and is ready to be read');
          }

          // Reload documentModel when currentDocument model content has been saved
          var unsubscribe = CurrentDocument.currentDocumentModel().document.listen(function () {
            $scope.documentModel = CurrentDocument.currentDocumentModel();
          });

          $scope.$on('$destroy', function () {
            unsubscribe();
          });
        });
      }

      // Allow to bypass leaving dialog
      $scope.allowRedirect = false;

      // Pop alert if trying to close browser with unsaved changes
      $window.onbeforeunload = function (event) {
        var s = $location.url();
        var dirty = CurrentDocument.hasChanged();
        if (dirty && s.indexOf('document') > -1 && $scope.validDate) {
          var message =  gettextCatalog.getString('You have unsaved changes.');
          if (typeof event == 'undefined') {
            event = window.event;
          }
          if (event) {
            event.returnValue = message;
          }
          return message;
        }
      };

      // Open dialog when trying to change route or redirected
      $scope.$on('$locationChangeStart', function (event, next, current) {
        // Make sure document is loaded (bugfix-WDC-1690)
        if ($scope.documentLoaded) {
          var workflow = CurrentDocument.currentWorkflow();
          var dirty = CurrentDocument.hasChanged();

          /* Show when: - Redirect is requested
           - Status is IN_PROGRESS or ANNOTATIONS_IN_PROGRESS
           - Date allows SAVE actions
           - Document has been changed
           */
          if ((Statuses.hasProperty(workflow.status, 'commentable') || Statuses.hasProperty(workflow.status, 'editable')) && dirty && $scope.validDate && !$scope.allowRedirect) {

            // Scope for dialog
            $scope.next = next;

            $scope.locationChange = function (next, save) {
              ngDialog.close();
              var arr = next.split('/#');
              var path = arr[arr.length - 1];

              if (save) {
                DocumentMenuActions.saveItAnyway().then(function () {
                  $location.path(path);
                });
              } else {
                $scope.allowRedirect = true;
                $location.path(path);
              }
            };

            // Open dialog
            ngDialog.open({
              template: 'views/dialog/document/saveOnLeave.html',
              showClose: false,
              scope: $scope
            });
            event.preventDefault();
          }
        }
      });
    })
