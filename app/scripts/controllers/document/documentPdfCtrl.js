/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an \"AS IS\" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.document')
  .controller('documentPdfCtrl',
    function ($scope, $location, $routeParams, WorkflowRequest, $sce, SkryvPdf, gettextCatalog, Notifications, $window) {
      var workflowID = $routeParams.workflowId;
      $scope.loading = true;

      // Trust html injection
      $scope.sanitizeMe = function (text) {
        return $sce.trustAsHtml(text);
      };

      // Get workflow
      WorkflowRequest.get(workflowID).then(function (workflow) {
        // ES workflow, get html content and set to scope
        SkryvPdf.getWorkflowPdf(workflow).then(function (html) {
          $scope.result = html;
        }, function (err) {
          Notifications.error(gettextCatalog.getString(err));
        })
          .finally(function () {
            $scope.loading = false;
          });
      }, function (err) {
        $scope.loading = false;
        Notifications.error(gettextCatalog.getString(err));
      });

      // Open browser print dialog
      $scope.print = function () {
        $window.print();
      };

      // Redirect to document editor
      $scope.redirect = function () {
        $location.path('/document/' + workflowID);
      }
    });
