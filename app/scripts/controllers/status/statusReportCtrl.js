/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {

  'use strict';
  angular.module('skryv.status')
    .filter('StatusMsgFilter', function () {
      return function (messages, searchTerm) {
        if (!searchTerm) {
          return messages;
        }

        searchTerm = searchTerm.toLowerCase();

        return messages.filter(function (message) {
          if (message.identification.value.toLowerCase().indexOf(searchTerm) > -1) {
            return message;
          }

          if (message.identification.name.toLowerCase().indexOf(searchTerm) > -1) {
            return message;
          }
        });
      };
    })
    .controller('StatusReportCtrl', function ($scope, $routeParams, SkryvStatus, $filter, Statuses, _, SkryvCollection) {

      $scope.total = {};
      $scope.collections = [];
      $scope.columnSort = {
        sortColumn: 'value',
        reverse: false
      };

      $scope.statuses = getStatuses();

      $scope.statuses.forEach(function (status) {
        $scope.total[status] = 0;
      });

      SkryvCollection.getCollections().then(function (collections) {
        var keys = _.keys(collections);
        keys.forEach(function (key) {
          $scope.collections = _.uniq($scope.collections.concat(collections[key]));
        });
        $scope.selectedCollection = $routeParams.name || $scope.collections[0];
      });

      SkryvStatus.getStatusMessages().then(function (messages) {
        $scope.messages = messages;

        // initialize version by URL
        if ($routeParams.version) {
          var version = $routeParams.version;
          var found = _.find($scope.messages[$scope.selectedCollection], function (entry) {
            if (entry.version.major == version) {
              return entry;
            }
          });

          $scope.selectedVersion = found;
        } else {
          var latest = getLatestVersion($scope.messages[$scope.selectedCollection]);
          $scope.selectedVersion = latest;
        }

        $scope.change();
      });

      $scope.changeCollection = function () {
        $scope.selectedVersion = getLatestVersion($scope.messages[$scope.selectedCollection]);

        $scope.change();
      };

      $scope.change = function () {
        $scope.total = {};

        if (!$scope.hasNoVersions) {
          calculateTotal();
        }
      };

      $scope.sortByColumn = function (value) {
        if ($scope.columnSort.sortColumn === value) {
          $scope.columnSort.reverse = !$scope.columnSort.reverse;
        } else {
          $scope.columnSort.sortColumn = value;
          $scope.columnSort.reverse = false;
        }
      };

      $scope.columnSorter = function (column) {
        switch ($scope.columnSort.sortColumn) {
          case 'value': {
            return column.identification.value;
          }
          case 'name': {
            return column.identification.name;
          }
          case 'createdOn': {
            return column.createdOn;
          }
          default: {
            if (!column.registrations[$scope.columnSort.sortColumn]) {
              return 0;
            }
            return column.registrations[$scope.columnSort.sortColumn];
          }
        }
      };

      function calculateTotal() {
        $scope.total = {};

        $scope.selectedVersion.dataProviders.forEach(function (dataProvider) {
          var keys = _.keys(dataProvider.registrations);
          keys.forEach(function (key) {
            if ($scope.total[key] === undefined) {
              $scope.total[key] = dataProvider.registrations[key];
            } else {
              $scope.total[key] += dataProvider.registrations[key];
            }
          });
        });
      }

      // Get all the statusses with unique status property
      // Temporarily workaround flags - WDC-2267
      function getStatuses() {
        var array = Statuses.withProperty('statusMessaging');
        var flags = [], output = [], l = array.length, i;
        for (i = 0; i < l; i++) {
          if (flags[array[i].status]) continue;
          flags[array[i].status] = true;
          output.push(array[i]);
        }
        return output;
      }

      function getLatestVersion(versions) {
        if (!versions) {
          $scope.hasNoVersions = true;
          return;
        } else {
          $scope.hasNoVersions = false;
        }

        if (versions.length === 1) {
          return versions[0];
        }

        var latest = versions[0];
        // dataProviders & version
        versions.forEach(function (entry) {
          if (entry.version.major > latest.version.major) {
            latest = entry;
          }
        });

        return latest;
      }
    });
})();
