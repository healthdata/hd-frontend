/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
angular.module('skryv.status')

  .controller('StatusListCtrl', function ($scope, SkryvStatus, moment, ngDialog, $filter) {

      function processMessages(messages) {
        // Set popover messages
        angular.forEach(messages, function (message) {
          angular.forEach(message.content.validation, function (validation) {
            if (validation.error) {
              validation.errorMessage = validation.error.join('\n\n');
            }
          });
        });

        $scope.messages = messages;
      }

      // Get Messages
      SkryvStatus.getStatusMessagesByDate().then(function (result) {
        processMessages(result);
      });

      // Datepicker
      $scope.datepicker = { format: 'dd-MM-yyyy' };

      $scope.open = function ($event, opened) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope[opened] = true;
      };

      // Search on date
      $scope.searchFiltersChanged = function () {
        var date = moment($scope.dateOfMessages).add(12, 'hours').format();
        date = (moment(date).isValid()) ? date : '';
        $scope.for = $filter('filterDate') ($scope.dateOfMessages, 'DD-MM-YYYY');

        SkryvStatus.getStatusMessagesByDate(date).then(function (result) {
          processMessages(result)
        });
      };

      $scope.showDetails = function (message) {
        var template = '<pre>' + JSON.stringify(message, null, 2) + '</pre>';

        ngDialog.open({
          template: template,
          showClose: false,
          plain: true
        });
      };

      // Table sorting
      $scope.columnSort = {
        reverse: false,
        sortColumn: 'healthDataIdentification.name'
      };

      $scope.sortByColumn = function (column) {
        $scope.columnSort.reverse = (column != $scope.columnSort.sortColumn) ? false : !$scope.columnSort.reverse;
        $scope.columnSort.sortColumn = column;
      };

      function getDescendantProp(obj, desc) {
        var arr = desc.split('.');
        while (arr.length && (obj = obj[arr.shift()]));
        return obj;
      }

      // If item is date, return valid date object
      $scope.orderByDate = function (message) {
        var item = getDescendantProp(message, $scope.columnSort.sortColumn);
        if (moment(item, [ 'DD-MM-YYY HH:mm:ss' ], true).isValid()) {
          return moment(item, 'DD-MM-YYY HH:mm:ss');
        } else {
          return item;
        }
      };
    })

  .filter('nullToBottomStatus', function () {
    function getDescendantProp(obj, desc) {
      var arr = desc.split('.');
      while (arr.length && (obj = obj[arr.shift()]));
      return obj;
    }

    return function (messages, column) {
      if (!angular.isArray(messages)) { return; }
      var present = messages.filter(function (item) {
        var val = getDescendantProp(item, column);
        return (val || val == 0);
      });
      var empty = messages.filter(function (item) {
        var val = getDescendantProp(item, column);
        return (!val && val != 0);
      });
      return present.concat(empty);
    }
  });
