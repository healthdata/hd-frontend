/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.ui.loadingbarService', [
  'ngProgress'
]).service('LoadingBar', function(_, ngProgressFactory){
  var queue = [];
  var observers = [];

  /** Observer pattern **/
  var addObserver = function (object) {
    observers.push(object);
  };

  var removeObserver = function (object) {
    var index = _.indexOf(observers, object);
    observers.splice(index, 1);
  };

  var notifyListeners = function () {
    observers.forEach(function (entry) {
      entry.queueChanged();
    });
  };

  /** Helper functions **/
  var startLoading = function (args) {
    var instance = ngProgressFactory.createInstance();

    instance.setColor('grey');
    instance.setHeight(35);
    instance.start();

    // if autoProgress isn't true, set the progress to 0
    if (!args.autoProgress) {
      instance.set(0);
    }

    var item = {
      id: args.id,
      label: args.label,
      completed: false,
      instance: instance
    };

    queue.push(item);
  };

  var startLoadingWithPromise = function (args) {
    if (!args.id) {
      throw new Error('no id specified');
    }

    startLoading(args);

    args.then(function (response) {
      complete(args);
      return response;
    }).catch(function (error) {
      fail(args);
      return error;
    });
  };

  /** Exposed functions for the service **/
  var start = function (args) {
    if (!args.id) {
      throw new Error('no id specified');
    }

    var found = _.where(queue, { id: args.id });

    // Don't queue if the ID is the same as one that is already queue'd
    if (found.length > 0) {
      throw new Error('already queued');
    }

    if (args.promise) {
      startLoadingWithPromise(args);
    } else {
      startLoading(args);
    }

    notifyListeners();
  };

  var edit = function (args) {
    if (!args.id) {
      throw new Error('no id specified');
    }

    var found = _.where(queue, { id: args.id });
    if (found.length > 0) {
      found = found[0];
    } else {
      throw new Error('promise not queued');
    }

    if (args.label) {
      found.label = args.label;
    }

    if (args.progress) {
      found.instance.set(args.progress);
    }
  }

  var stop = function(id) {
    if (!args.id) {
      throw new Error('no id specified');
    }

    var found = _.where(queue, { id: args.id });
    if(found.length > 0) {
      found = found[0];
    } else {
      throw new Error('promise not queued');
    }

    found.instance.stop();
  };

  var clear = function() {
    queue.length = 0;
  };

  var complete = function(args) {
    if (!args.id) {
      throw new Error('no id specified');
    }

    var found = _.where(queue, { id: args.id });
    if (found.length > 0) {
      found = found[0];
    } else {
      throw new Error('promise not queued');
    }

    found.instance.set(99.999);
    found.instance.setColor('#90c488');
    found.completed = true;
    found.failed = false;
  };

  var fail = function(args) {
    if (!args.id) {
      throw new Error('no id specified');
    }

    var found = _.where(queue, { id: args.id });
    if (found.length > 0) {
      found = found[0];
    } else {
      throw new Error('promise not queued');
    }

    found.instance.set(99.999);
    found.instance.setColor('#de7568');
    found.completed = true;
    found.failed = true;
  }

  var dismiss = function(args) {
    if (!args.id) {
      throw new Error('no id specified');
    }

    var found = queue.filter(function (entry) {
      if (entry.id === args.id) {
        return entry;
      }
    });

    found = found[0];

    found.instance.complete();
    found.instance.setHeight(0);

    var index = _.indexOf(queue, found);
    queue.splice(index, 1);

    notifyListeners();
  }

  return {
    addObserver: addObserver,
    removeObserver: removeObserver,
    clear: clear,
    complete: complete,
    dismiss: dismiss,
    edit: edit,
    fail: fail,
    start: start,
    stop: stop,
    queue: queue
  };

});