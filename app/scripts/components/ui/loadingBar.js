/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.ui.loadingbar', [
  'ngProgress'
])
  .directive('loadingBar', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/ui/loadingBar.html',
      controller: function ($rootScope, $scope, ngProgressFactory, _, LoadingBar) {

        $scope.queue = LoadingBar.queue;
        LoadingBar.addObserver(this);

        this.queueChanged = function () {
          if (LoadingBar.queue.length > 0) {
            $scope.current = LoadingBar.queue[LoadingBar.queue.length -1];
          } else {
            $scope.current = undefined;
          }
        };

        /*
         * Expose stopLoading to the scope to close the loadingbar.
         */
        $scope.dismissLoadingbar = function (id) {
          LoadingBar.dismiss({ id: id });
        };

        $scope.$on('$destroy', function () {
          LoadingBar.removeObserver(this);
        });
      }
    };
  });

