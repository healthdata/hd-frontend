/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('skryv.ui.collectionSidebar', [])
    .directive('collectionSidebar', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/ui/collectionSidebar/collectionSidebar.html',
        bindToController: {
          collections: '=',
          selectGroup: '&',
          selectVersion: '&',
          selectCollection: '&',
          openChildren: '=',
          showIcons: '='
        },
        controller: Controller,
        controllerAs: 'sidebar'
      };
    });

  var Controller = function ($scope, localStorageService) {
    var STORAGE_KEY = 'sidebar.open';
    var savedOption = JSON.parse(localStorageService.get(STORAGE_KEY));

    // Up to 1 collection: hide the sidebar by default
    if (savedOption === undefined) {
      this.isSidebarOpen = true;
    } else {
      this.isSidebarOpen = savedOption;
    }

    this.toggleSidebarOpen = function() {
      this.isSidebarOpen = !this.isSidebarOpen;
      localStorageService.set(STORAGE_KEY, this.isSidebarOpen);
    };

    /** Internal functions, shouldn't be used! **/
    this._selectVersion = function (version, group) {
      this.selectVersion({ version: version, group: group });
    };

    this._selectCollection = function(collection) {
      this.selectCollection({ collection: collection });
    };

    this._selectGroup = function (group) {
      this.selectGroup({ group: group });
    };

    // Scroll to group when selectedGroup changes
    $scope.$watch('selectedGroup', function (nextGroup) {
      setTimeout(function () {
        var list = $('.items');
        var group = $('#' + nextGroup.name);

        list.scrollTop(list.scrollTop() + group.position().top - list.height() / 3);
      });
    });

    // Scroll to version when selectedVersion changes
    $scope.$watch('selectedVersion', function (nextVersion) {
      setTimeout(function () {
        var list = $('.items');
        var group = $('#version-' + nextVersion.dataCollectionGroupId);

        list.scrollTop(list.scrollTop() + group.position().top - list.height() / 3);
      });
    });
  };

})();
