/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.editor.components')

.directive('linkLabel', [ '$window', '$timeout', function ($window, $timeout) {
  return {
    restrict: 'EA',
    transclude: true,
    scope: {
      path: '='
    },
    template: "<span class='linkLabel' ng-click='focus()' ng-transclude></span>",
    link: function (scope, element, attrs) {

      var url;
      if(attrs.type === 'field') {
        url = 'focus';
      } else if (attrs.type === 'section') {
        url = 'section';
      }

      scope.focus = function () {
        $timeout(function () {
          var elt = $window.document.getElementsByClassName(url + attrs.path);
          if (elt) {
            if (elt.length == 2) {
              $('.doc-container-middle .doc-content').scrollToElementAnimated(elt[1], 25);
            } else {
              $('.doc-container-middle .doc-content').scrollToElementAnimated(elt[0], 25);
            }
          }
        });
      };
    }
  };
} ]);