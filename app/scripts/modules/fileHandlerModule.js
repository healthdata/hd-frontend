/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var SkryvCsv = angular.module('skryv.file',
  [ 'skryv.conf', 'ngRoute', 'ngResource' ]);

SkryvCsv.factory('SkryvFile', function ($q, Notifications, $log) {

    function browserSupportFileUpload() {
      return (window.File && window.FileReader && window.FileList && window.Blob);
    }

    function upload(files) {
      var delay = $q.defer();
      if (!browserSupportFileUpload()) {
        Notifications.error('File upload not supported. Please update your browser!');
        delay.reject();
      } else {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          try {
            var reader = new FileReader();
            reader.readAsText(file);
            reader.onload = function (event) {
              delay.resolve(event.target.result);
            };
            reader.onerror = function () {
              Notifications.error('Unable to read ' + file.fileName);
              delay.reject();
            }
          } catch (err) {
            delay.resolve(err);
            Notifications.error('CSV upload failed');
            $log.log(err);
          }
        }
      }
      return delay.promise;
    }

    function download(content, name) {
      var delay = $q.defer();

      var browser = navigator.userAgent;

      if (browser.indexOf('chrome') > -1 || browser.indexOf('Chrome') > -1) {
        var hiddenElement = document.createElement('a');

        hiddenElement.href = 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURIComponent(content);
        hiddenElement.target = '_blank';
        hiddenElement.download = name;
        hiddenElement.click();
        delay.resolve('download');
      } else if (browser.indexOf('Firefox') > -1) {
        var a         = document.createElement('a');
        a.href        = 'data:attachment/csv,%EF%BB%BF' + encodeURIComponent(content);
        a.target      = '_blank';
        a.download    = name;

        document.body.appendChild(a);
        a.click();
        delay.resolve('download');
      } else if (browser.indexOf('MSIE') > -1 && browser.indexOf('MSIE 10') < 0) {
        var oWin = window.open();
        oWin.document.write('sep=,\r\n' + '%EF%BB%BF' + content);
        oWin.document.close();
        oWin.document.execCommand('SaveAs', true, name);
        oWin.close();
        delay.resolve('download');
      } else if (browser.indexOf('.NET') > -1) {
        var blob = new Blob([ '\ufeff' + content ], { type: 'application/csv;charset=utf-8;' });
        navigator.msSaveBlob(blob, name);
        delay.resolve('download');
      } else {
        var hiddenElement = document.createElement('a');

        hiddenElement.href = 'data:attachment/csv,%EF%BB%BF' + encodeURI(content);
        hiddenElement.target = '_blank';
        hiddenElement.download = name;
        hiddenElement.click();
        delay.resolve('download');
      }
      return delay.promise;
    }

    return {
      download: function (content, name) {
        return download(content, name);
      },
      upload: function (files) {
        return upload(files);
      }
    }
  });
