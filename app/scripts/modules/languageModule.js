/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
angular.module('skryv.language',
  [ 'skryv.conf' ])

.factory('SkryvLanguage', function (gettextCatalog, localStorageService, $rootScope) {

    // Translation
    gettextCatalog.debug = false;

    return {
      setLanguage: function (lang) {
        localStorageService.set('language', lang);
        gettextCatalog.setCurrentLanguage(lang);
        $rootScope.language = lang;
      },
      getLanguage: function () {
        return localStorageService.get('language');
      },
      translate: function (string) {
        return gettextCatalog.getString(string);
      },
      setDebug: function (bool) {
        gettextCatalog.debug = bool;
      },
      getDebug: function () {
        return gettextCatalog.debug;
      }
    };
  });
