/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var SkryvSession = angular.module('skryv.session',
  [ 'skryv.conf', 'ngRoute', 'ngResource', 'skryv.translation', 'skryv.collection' ]);

SkryvSession.factory('SkryvSession', function ($timeout, $filter, $log, $q, $location, $route, $window, $rootScope, SkryvUser, ngDialog, _,
                                               PLATFORM, SkryvCollection, SkryvDefinition, Notifications, gettextCatalog, localStorageService, SkryvConfig, SkryvTranslation, SkryvLanguage) {

  // TODO(arno): don't use initCollectionNames to fill dataCollectionNames
  // another remark: is caching really required here? dataCollections will never be refreshed...
  var loggedUser;
  var collections;
  var collectionNames;
  var authConfig;
  var expired;
  var installationId;
  var previousPath;
  var admin;
  var originalLoad;
  $rootScope.SessionActive = false;

  function getInstallationId() {
    return SkryvConfig.getConfigByKey('HEALTHDATA_ID_VALUE').then(function (config) {
      if (config && config.length === 1) {
        installationId = config[0].value;
      }
      return installationId;
    });
  }

  function getUserStorageType() {
    var delay = $q.defer();
    if (authConfig) {
      delay.resolve(authConfig);
    } else {
      SkryvConfig.getConfigByKey('USER_MANAGEMENT_TYPE').then(function (config) {
        authConfig = config[0].value;
        delay.resolve(authConfig);
      }, function (err) {
        delay.reject(err);
      });
    }
    return delay.promise;
  }

  function getLoggedUser() {
    var delay = $q.defer();
    if (loggedUser) {
      delay.resolve(loggedUser);
    } else {
      SkryvUser.get('me').then(function (user) {
        getAdmin(user);
        loggedUser = user;
        delay.resolve(user);
      }, function (err) {
        delay.reject(err);
      });
    }
    return delay.promise;
  }

  function initCollectionNames() {
    var names = [];

    return SkryvCollection.getCollections().then(function (response) {
      var keys = _.keys(response);
      keys.map(function (entry) {
        names.push(entry);
        names.concat(response[entry].children);
      });

      collectionNames = names;
      return names;
    });
  }

  function initCollections() {
    return SkryvCollection.getDataCollectionGroups().then(function (response) {
      var keys = _.keys(response);
      var collectionArray = [];
      keys.forEach(function (key) {
        var collection = {};
        collection.versions = response[key];
        collection.name = key;

        collectionArray.push(collection);
      });

      collectionArray.map(function (collection) {
        collection.versions.map(function (version) {
          return setVersionStatus(version);
        });
      });

      collections = collectionArray;
      return collectionArray;
    });
  }

  function getVersions(collectionName) {
    return SkryvDefinition.getVersions(collectionName);
  }

  function getAdmin(user) {
    var hasRole = _.find(user.authorities, { authority: 'ROLE_ADMIN' });
    if (hasRole) {
      admin = true;
    } else {
      admin = false;
    }
    return admin;
  }

  function updateCollections() {
    // Just call initCollections again to refresh the collections...
    return initCollections();
  }

  function getUserTokens() {
    return localStorageService.get('userInfo');
  }

  function setSessionActive() {
    ngDialog.close();
    $rootScope.SessionActive = true;
    return previousPath;
  }

  function destroyUserSession(message) {
    loggedUser = undefined;
    collections = undefined;
    admin = undefined;
    originalLoad = false;
    expired = false;

    if ($location.path() != '/login' && $location.path() != '/logout') {
      previousPath = $location.path();
    }

    $rootScope.SessionActive = false;
    localStorageService.remove('userInfo');
    ngDialog.close();
    $location.path('/login');

    if (message) {
      // Avoid multiple messages
      Notifications.clear();
      Notifications.error(message);
    }
  }

  function setVersionStatus(version) {
    version.status = $filter('collectionActive')(version);

    return version;
  };

  return {
    // Local storage
    storeUserTokens: function (accessToken, refreshToken) {
      // Set local storage tokens
      localStorageService.set('userInfo', {
        accessToken: accessToken,
        refreshToken: refreshToken
      });
    },
    getUserTokens: getUserTokens,
    destroyUserSession: destroyUserSession,

    // User
    isAdmin: function () { return admin; },
    setLoggedUser: function (u) { loggedUser = u; },
    getLoggedUser: function () { return loggedUser; },

    // Collection/Definition
    getCollections: function () {
      return collections;
    },
    getCollectionNames: function () {
      return collectionNames;
    },
    setCollections: function (c) { collections = c; },
    updateCollections: updateCollections,

    // AuthType
    getAuthType: function () { return getUserStorageType(); },
    setAuthType: function (type) { authConfig = type;  },

    // Installation ID
    getInstallationId: function () { return installationId; },

    // App
    loadApp: function () {
      var delay = $q.defer();

      // Destroy sessions if no tokens in local storage
      if (!getUserTokens()) {
        delay.reject('No local user');
        destroyUserSession();
        return delay.promise;
      }

      // Don't load when session is active
      if ($rootScope.SessionActive) {
        delay.resolve('Session active');
        return delay.promise;
      }

      // Return original call if loading is still busy
      if (originalLoad) {
        return originalLoad.promise;
      } else {
        originalLoad = delay;
      }

      // Open loading dialog
      ngDialog.open({
        template: 'views/popupLoader.html',
        showClose: false,
        controller: [ 'PLATFORM', '$scope', function (PLATFORM, $scope) {
          $scope.platform = PLATFORM;
          $scope.message = gettextCatalog.getString('Loading user data');
        } ]
      });

      // Get Translations
      if (SkryvLanguage.getLanguage() != 'en') {
        SkryvTranslation.getTranslations(SkryvLanguage.getLanguage());
      }

      // Load User and Collections
      $q.all([ getLoggedUser(), initCollectionNames(), getInstallationId() ]).then(function () {
        // See if admin
        if (!admin) {
          initCollections().then(function() {
            delay.resolve(setSessionActive());
          });
        } else {
          delay.resolve(setSessionActive());
        }
      }, function () {
        // Error when loading user and collections
        destroyUserSession('Could not retrieve user or data collections');
      });

      return delay.promise;
    }
  };
});
