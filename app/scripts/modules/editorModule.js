/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function (undefined) {
  'use strict';

angular.module('skryv.editor', [ 'documentmodel', 'ui.bootstrap', 'ui.mask', 'gettext', 'skryv.editor.components', 'services.workflow',
                                 'skryv.user', 'skryv.session', 'skryv.language', 'ngCookies', 'services.filter' ])
  .directive('skrEditorComponent', skrEditorComponentDirective)
  .directive('skrEditorOptions', skrEditorOptionsDirective)
  .directive('skrEditorInputControls', skrEditorInputControlsDirective)
  .directive('skrEditorHelpButton', skrEditorHelpButtonDirective)
  .directive('skrEditorNote', skrEditorNoteDirective)
  .directive('skrEditorSideMenu', skrEditorSideMenuDirective)
  .directive('skrTableOfContents', skrTableOfContentsDirective)
  .directive('skrDebugInfo', skrDebugInfoDirective)
  .filter('sectionNumber', sectionNumberFilter)
  .filter('assertionMessage', assertionMessageFilter)
  .filter('commentTitle', commentTitleFilter)
  .filter('groupHistory', GroupHistoryFilter)
  .controller('ProgressCtrl', ProgressController)
  .controller('FocusCtrl', FocusController)
  .controller('WorkflowOverviewCtrl', WorkflowOverviewCtrl)
  .controller('IgnoreWarningCtrl', IgnoreWarningController)
  .directive('skrValidation', skrValidationDirective)
  .directive('dateMask', dateMaskDirective)
  .directive('linkedMessage', linkedMessageDirective)
  // install the documentmodel directives
  .run(editorDirectives)
  ;

function dateMaskDirective($log, dateFilter) {
  return {
    require: 'ngModel',
    priority: 300, // run after other directives (post-link functions run highest-last)
    link: function (scope, element, attrs, ngModel) {
      if (ngModel.$parsers.length !== 2) {
        $log.error('date-mask directive expects two parsers (ui-mask and datepicker)');
        return;
      }
      // the datepicker parser will be first, the uiMask second
      // the goal if this directive is to have the uiMask parser run first
      // in such a way that it works well with the date parser

      var uiMaskParser = ngModel.$parsers.splice(1, 1)[0];
      ngModel.$parsers.unshift(function (input) {
        var maskedResult = uiMaskParser(input);
        // If the input does not yet match the mask, we do not proceed with
        // processing it as a date, so we return undefined.
        if (angular.isUndefined(maskedResult)) {
          return;
        }
        // Due to the way uiMask works, the input is not the correct date string, but
        // after invoking uiMaskParser, $viewValue will have a correct date string.
        // We do not want to use maskedResult, because it lacks the slashes in the
        // date string.
        return ngModel.$viewValue;
      });

      if (ngModel.$formatters.length !== 3) {
        $log.error('date-mask directive expects three formatter (string, datepicker, and ui-mask)');
        return;
      }

      // the datepicker and ui-mask formatters should simply swap places
      var uiMaskFormatter = ngModel.$formatters.pop();
      ngModel.$formatters.splice(1, 0, uiMaskFormatter);
    }
  };
}

var initialized = false;

function editorDirectives($q, $http, _, healthdataDocumentmodel, WORKFLOW_ENGINE_BASE_URL, SSIN_SERVER_URL, $window, $timeout, SkryvLanguage, PLATFORM) {

  // make sure we only declare the documentmodel directives once
  // multiple instantiations of this module (as may happen during testing)
  // should not duplicate directives
  if (initialized) { return; }
  initialized = true;

  // configure reference list httpGET
  healthdataDocumentmodel.referenceList.httpGET = httpGET;
  healthdataDocumentmodel.referenceList.WORKFLOW_ENGINE_BASE_URL = WORKFLOW_ENGINE_BASE_URL;
  healthdataDocumentmodel.currentLanguage.getCurrentLanguage = getCurrentLanguage;

  function getCurrentLanguage() {
    var lang = SkryvLanguage.getLanguage();
    if (lang && lang.length >= 2) {
      return lang.substr(0, 2);
    }
    return 'en';
  }

  function httpGET(url, cb) {
    $http({ method: 'GET', url: url, cache: true }).then(
      function (response) {
        cb(undefined, response.data);
      },
      function (err) {
        cb(err);
      });
  }
}

function sectionNumberFilter() {
  return function (input) {
    input = input || [];
    var out = '';
    angular.forEach(input, function (num, index) {
      if (index !== 0) { out = out + '.' ; }
      out = out + num;
    });
    return out;
  };
}

function assertionMessageFilter(gettextCatalog) {
  return function (assertion, description) {
    var message = description.messageFor(assertion) ||
                  'error code "' + assertion + '"';
    message = gettextCatalog.getString(message);
    var label = gettextCatalog.getString(description.label);
    return (label ? (label + ': ') : '') + message;
  };
}

function skrValidationDirective(_) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {
      var manipulator = scope.$eval(attrs.skrValidation);

      var errorCount = 0;
      var linkedErrors = [];
      function recomputeValidity() {
        var hasErrors = errorCount || _.some(linkedErrors);
        modelCtrl.$setValidity('skrValidation', !hasErrors);
      }

      scope.$watch(function () {
        return manipulator.errors.length;
      }, function (errs) {
        errorCount = errs;
        // WDC-1915
        // Have a generated patient ID look like an invalid field, even though it
        // is technically valid.
        if (manipulator.patientIDisGenerated && manipulator.patientIDisGenerated()) {
          errorCount += 1;
        }
        recomputeValidity();
      });

      _.each(manipulator.linked, function (linked, position) {
        scope.$watch(function () {
          return linked.manipulator.assertions[linked.assertion];
        }, function (result) {
          var assertion = linked.manipulator.description.assertions[linked.assertion];
          var isWarning = assertion.level === 'warning';
          linkedErrors[position] = !isWarning && result === 'failed';
          recomputeValidity();
        });
      });
    }
  };
}

  function skrEditorSideMenuDirective(DocumentSideMenu, ngDialog, $filter, CurrentDocument, PLATFORM, _, DocumentNotes) {
    'ngInject';
    // TODO Limit variables passed
    return {
      restrict: 'E',
      scope: {
        position: '=',
        documentModel: '=',
        registration: '=',
        dcd: '=',
        defaultTab: '=',
        validDate: '=',
        workflow: '=',
        items: '='
      },
      replace: true,
      templateUrl: 'views/document/side/sideMenu.html',
      link: function (scope, element, attrs) {
        scope.setActiveTab = function (item) {
          scope.activeTab = item;
        }
        scope.platform = PLATFORM;
        scope.menuItems = DocumentSideMenu.menuItems(scope.items);

        if (scope.defaultTab) {
          scope.setActiveTab(scope.defaultTab);
        } else {
          scope.setActiveTab(scope.items[0]);
        }
        scope.activeTabResopnsive = scope.items[0];

        // Specifics
        if (scope.dcd) {
          scope.uniqueID = scope.dcd.content.uniqueIDExplanation;
        }

        var unsubscribeDocumentNotes = DocumentNotes.listen(function (getters) {
          // Set notes on scope
          scope.notes = getters.notes();
        });

        scope.$on('$destroy', function () {
          unsubscribeDocumentNotes();
        });

        scope.getCounter = function (counters) {
          var value = 0;
          _.each(counters, function (counter) {
            value += $filter('stringToPath') (scope, counter);
          });

          if (isNaN(value)) {
            return 0;
          }

          return value;
        };

        scope.openHistory = function () {
          ngDialog.open({
            template: 'views/dialog/document/history.html',
            showClose: false,
            controller: [ '$scope', function (scope) {
              scope.history = $filter('groupHistory')(CurrentDocument.currentWorkflow().history);
            } ]
          });
        };
      }
    }
  }

function skrTableOfContentsDirective($compile, $window, $timeout) {
  // angular.js seems to have trouble with recursive directives such as
  // this. We need to do some trickery to avoid an infinite loop.
  // It seems that this trickery can be abstracted in service, see
  // http://stackoverflow.com/questions/14430655/recursion-in-angular-directives
  // but for now let's try a manual approach and introduce more complexity
  // as needed.
  return {
    restrict: 'E',
    scope: {
      sections: '='
    },
    link: function (scope, element, attrs) {
      scope.focus = function (section) {
        section.ensureIsOpen();
        // to focus on the section after it has actually been made visible, we
        // need to wrap the code in a $timeout
        $timeout(function () {
          var id = section.name;

          var elt = $window.document.querySelector('#doc-form #' + id);

          if (elt) {
            $('.doc-container-middle .doc-content').scrollToElementAnimated(elt, 0);
          }
        });
      };
      var template =
        '<div class="toc-item" ng-repeat="section in sections"' +
        ' ng-class="{tocItemGroup: section.depth == 0}"' +
        '>' +
        '  <li ng-click="focus(section)" class="toc toc-{{section.depth}}" title="{{ section.title | translate }}" >' +
        '    <a>' +
        '      {{ section.sectionNumber | sectionNumber }} {{ section.title | translate }}' +
        '    </a>' +
        '  </li>' +
        '  <skr-table-of-contents sections="section.subsections">' +
        '  </skr-table-of-contents>' +
        '</div>';
      element.html(template).show();
      $compile(element.contents())(scope);
    }
  };
}

function skrEditorInputControlsDirective(SkryvSession, PLATFORM, moment, $timeout, _, DocumentNotes) {

  return {
    restrict: 'E',
    scope: true,
    transclude: true,
    template:
      '<!--No comments present--><span class="comment-button fa fa-comment-o" ng-if="!hasNote()" ng-click="createComment()"></span><!--Comment is resolved--><span class="comment-button fa fa-comment-o" ng-if="hasNote() && hasNote.solved" ng-click="openCommentsTab()"></span><!--Comment is open--><span class="comment-button comment-read-only fa fa-comment" ng-if="hasNote() && !hasNote().solved" ng-click="openCommentsTab()"></span><div id="{{ \'editor\' + manipulator.path }}" ng-transclude class="doc-control {{ \'focus\' + manipulator.path }}"></div>',
    link: function (scope, element, attrs) {
      /* jshint unused: false */
      var notes;

      // Filters
      var unsubscribeDocumentNotes = DocumentNotes.listen(function (getters) {
        // Set notes on scope
        notes = getters.notes();
      });

      scope.$on('$destroy', function () {
        unsubscribeDocumentNotes();
      });

      scope.hasNote = function () {
        return _.find(notes, function (note) {
          return note.fieldPath === scope.manipulator.path.join('.') || note.fieldPath === scope.manipulator.path.join('|');
        });
      };

      scope.openCommentsTab = function () {
        $timeout(function () {
          var elements = angular.element('.tab-comments-link:visible');

          if (elements && elements[0]) {
            elements[0].click();
            $timeout(function () {
              var comments;
              comments = document.getElementsByClassName('note-' +  scope.manipulator.path.join('.'));
              // Try old fieldPath format...
              if (comments.length === 0) {
                comments = document.getElementsByClassName('note-' +  scope.manipulator.path.join('|'));
              }
              // Responsive menu comments are displayed: none
              _.each(comments, function(comment) {
                comment.click();
              });
            }, 500);
          }
        }, 0);
      };

      scope.createComment = function () {
        DocumentNotes.setNewFieldNote(scope.manipulator);
        scope.openCommentsTab();
      };
    }
  };
}

skrEditorHelpButtonDirective.$inject = [];
function skrEditorHelpButtonDirective() {
  return {
    restrict: 'E',
    scope: true,
    transclude: true,
    templateUrl: 'views/editor/repeatables/helpText.html',
    link: function (scope, element, attrs) {
    }
  };
}

  function skrEditorNoteDirective(DocumentNotes) {
    return {
      restrict: 'E',
      scope: {
        comment: '=',
        note: '=',
        validDate: '='
      },
      transclude: true,
      templateUrl: 'views/document/notes/note-item.html',
      link: function (scope, element, attrs) {
        // Edit reply
        scope.editComment = function (content) {
          if (content) {
            DocumentNotes.editComment(content, scope.comment, scope.note);
          }
        };

        // Delete note from workflow notes
        scope.deleteComment = function () {
          DocumentNotes.deleteComment(scope.comment, scope.note);
        };

        // Check if note can be deleted or edited by the user
        // TODO: Refactor validDate
        scope.isEditable = function () {
          return DocumentNotes.isEditable(scope.comment, scope.note, scope.validDate);
        };
      }
    };
  }

function commentTitleFilter(gettextCatalog) {
  return function (text) {
    text = text.substring(2).replace(/_/g, ' ');
    text = text.charAt(0).toUpperCase() + text.slice(1);
    text = gettextCatalog.getString(text);
    return text;
  };
}
  
function GroupHistoryFilter() {
  return function (history) {
    var grouped = [];

    if (history) {
      angular.forEach(history.reverse(), function (action) {
        if (grouped.length != 0 && grouped[grouped.length - 1].status == action.newStatus) {
          grouped[grouped.length - 1].actions.unshift(action);
        } else {
          grouped.push({
            status: action.newStatus,
            actions: [ action ],
            flags: action.flags
          });
        }
      });

      return grouped;
    }

    return history;
  };
}

skrEditorOptionsDirective.$inject = [];
function skrEditorOptionsDirective() {
  return {
    restrict: 'EA',
    controller: function ($scope, $attrs) {
      this.options = $scope.$eval($attrs.skrEditorOptions);
    }
  };
}

function skrDebugInfoDirective() {
  return {
    restrict: 'EA',
    scope: true,
    templateUrl: 'views/editor/debugInfo.html',
    link: function (scope) {
      var manipulator = scope.manipulator;
    }
  };
}

function skrEditorComponentDirective($compile, $http, $templateCache, $injector, resolveEditorComponent, _, CurrentDocument, PLATFORM) {
  return {
    restrict: 'EA',
    require: '?^skrEditorOptions',
    scope: {
      component: '=',
      manipulator: '='
    },
    link: function (scope, element, attrs, optionsCtrl) {
      scope.platform = PLATFORM;

      var component = scope.component;
      var manipulator = scope.manipulator;

      if (!component) {
        throw new Error(
          'skrEditorComponent directive ' +
          'requires a "component" attribute');
      }

      if (!manipulator) {
        throw new Error(
          'skrEditorComponent directive ' +
          'requires a "manipulator" attribute');
      }

      // If stickies are asked
      if (optionsCtrl && optionsCtrl.options.onlySticky) {
        // Always render these fields
        var showWhenNotSticky = [ 'object', 'section', 'fieldset', 'patientID' ];
        if (showWhenNotSticky.indexOf(component.description.type) === -1 && component.description && !component.description.sticky) {
          return; // Don't render
        }

        // If field is sticky
        scope.isSticky = component.description && component.description.sticky;

        // For sections rendering performance
        if (component.componentName === 'skrSection') {
          scope.isSticky = true;
        }
      }

      var tplURL;
      var resolved = resolveEditorComponent(component);
      if (resolved) {
        // resolveEditorComponent provides us with a template and link function
        if (resolved.link) {
          (resolved.link)(scope, component, manipulator, $injector, optionsCtrl && optionsCtrl.options);
        }
        tplURL = resolved.template;
      } else {
        console.log('using old: ' + component.template);
        if (component.link) {
          component.link(scope, manipulator, $injector);
        }
        tplURL = component.template;
      }

      // TODO find a better solution
      // we link the component to the manipulator such that we can open the relevant
      // section when clicking on a validation error
      manipulator.component = component;

      if (typeof tplURL !== 'string') {
        throw new Error(
          'skrEditorComponent directive ' +
          'requires a valid template URL, got ' +
          tplURL);
      }

      scope.fieldHasErrors = function () {
        return ((manipulator.errors.length > 0 && !(manipulator.errors.length === 1 && scope.hasError('required'))) || scope.linkedMessages('errors').length > 0);
      };

      scope.fieldHasWarnings = function () {
        return (manipulator.warnings.length > 0 || scope.linkedMessages('warnings').length > 0);
      };

      scope.hasError = function (error) {
        return _.indexOf(manipulator.errors, error) > -1;
      };
      
      scope.linkedMessages = function (type) {
          var linkedErrors = [];

          _.each(manipulator.linked, function (linkedRefrence) {
            if (CurrentDocument.currentDocumentModel() && CurrentDocument.currentDocumentModel().document[type].length > 0) {
              _.each(CurrentDocument.currentDocumentModel().document[type], function (error) {
                if (error.assertion === linkedRefrence.assertion) {
                  linkedErrors.push(error);
                }
              });
            }
          });

          return linkedErrors;
      };

      $http.get(tplURL, { cache: $templateCache })
        .success(function (html) {
          if (optionsCtrl && optionsCtrl.options.debug) {
            html = '<skr-debug-info></skr-debug-info>' + html;
          }
          element.html(html);
          $compile(element.contents())(scope);

          scope.$watch('manipulator.isActive()', function (isActive) {
            if (isActive) {
              element.show();
            } else {
              element.hide();
            }
          });
        });
    }
  };
}

/**
 * Use this controller when you want details about the progress of a given
 * document. It provides the following properties in the scope:
 * - progress.total: the total number of fields
 * - progress.valid: the number of valid fields
 * - progress.invalid: the number of invalid fields
 * - progress.validRatio: the ratio of valid to total #fields
 * - progress.invalidRatio: the ratio of invalid to total #fields
 *
 * Example:
 *   <div ng-controller="skrProgress" document="someDocument">
 *     There are {{ progress.invalid }} invalid fields.
 *   </div>
 **/
function ProgressController($scope, $attrs) {

  var progressExpression = $attrs.documentProgress;

  $scope.$watch(progressExpression,
    function (progress) {
        $scope.progress = progress;
        if (!progress) { return; }
        //progress.validWidth = barWidth(progress.validRatio);
        //progress.invalidWidth = barWidth(progress.invalidRatio);
        //progress.optionalWidth = barWidth(progress.optionalRatio);
    });
  //
  //function barWidth(ratio) {
  //  // ratio will be a number between 0 and 1
  //  // the floor is there to make sure that the total never exceeds 100
  //  return Math.floor(100 * ratio);
  //}
}

function WorkflowOverviewCtrl($scope, CurrentDocument, $timeout, ngDialog, $filter) {
  $scope.loading = true;

  $('body').on('click', function (e) {
    $('*[popover]').each(function () {
      //Only do this for all popovers other than the current one that cause this event
      if (!($(this).is(e.target) || $(this).has(e.target).length > 0)
        && $(this).siblings('.popover').length !== 0
        && $(this).siblings('.popover').has(e.target).length === 0)
      {
        //Remove the popover element from the DOM
        $(this).siblings('.popover').remove();
        //Set the state of the popover in the scope to reflect this
        angular.element(this).scope().tt_isOpen = false;
      }
    });
  });

  function getWorkflow() {
    $timeout(function () {
      i++;
      $scope.workflow = CurrentDocument.currentWorkflow();
      if(!$scope.workflow && i < 5){
        getWorkflow();
      }

    }, 2000);
  }

  var i = 0;
  getWorkflow();
}

/**
 * Use this controller to support scrolling to editor fields based on the paths
 * of the manipulators.
 **/
function FocusController($scope, $timeout, PLATFORM, $window, CurrentDocument) {
  'ngInject';

  $scope.focusOnField = function (manipulator) {
    manipulator.component.ensureIsOpen();
    $timeout(function () {
      var elt = $window.document.querySelector('#doc-form #editor' + manipulator.path.join('\\,'));

      if (elt) {
        $('.doc-container-middle .doc-content').scrollToElementAnimated(elt, 25);
        elt.style.backgroundColor  = "#FDFF47";
        setTimeout(function() {
          elt.style.backgroundColor = "transparent";
        }, 800)
      }
    });
  };
  $scope.platform = PLATFORM;
}

function IgnoreWarningController($scope) {
  $scope.ignoreButtonShown = false;
  $scope.showIgnoreButton = function () {
    $scope.ignoreButtonShown = true;
  };
  $scope.ignoreWarning = function (manipulator, warningName) {
    manipulator.ignoreWarning(warningName);
  };
}

function linkedMessageDirective($compile, $sanitize, gettextCatalog) {
  return {
    restrict: 'EA',
    controller: 'FocusCtrl',
    scope: {
      assertion: '=',
      manipulator: '='
    },
    link: function (scope, element, attrs) {
      var assertion = scope.assertion;
      var manipulator = scope.manipulator;

      var description = manipulator.description;
      var message = description.messageFor(assertion) ||
                    'error code "' + assertion + '"';
      message = gettextCatalog.getString(message);

      // Do not risk HTML injection.
      // Is this necessary? Is this sufficient?
      message = $sanitize(message);

      var template = message;
      var references = [];

      manipulator.processMessage(message,
        function (match, label, referencedManipulator) {
          references.push(referencedManipulator);
          template = template.replace(match,
            '<span class="interfield-reference" ng-click="focusOnReference(' +
            (references.length - 1) + ')">' +
            gettextCatalog.getString(label) + '</span>');
        });

      scope.focusOnReference = function (position) {
        var manipulator = references[position];
        scope.focusOnField(manipulator);
      };

      element.html(template);
      $compile(element.contents())(scope);
    }
  };
}

})();
