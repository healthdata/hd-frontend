/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.collection')

.config(function ($routeProvider) {
  $routeProvider
    .when('/collectiongroup', {
      templateUrl: 'views/collectionGroup/list.html',
      controller: 'collectionGroupListCtrl',
      resolve: {
        authenticate: function (platformFilter) {
          platformFilter('hd4res');
        }
      }
    })
    .when('/collectiongroup/edit/:id', {
      templateUrl: 'views/collectionGroup/edit.html',
      controller: 'collectionGroupEditCtrl',
      resolve: {
        authenticate: function (platformFilter) {
          platformFilter('hd4res');
        }
      }
    })
    .when('/collectiongroup/new', {
      templateUrl: 'views/collectionGroup/new.html',
      controller: 'collectionGroupNewCtrl',
      resolve: {
        authenticate: function (platformFilter) {
          platformFilter('hd4res');
        }
      }
    })
    .when('/collection', {
      templateUrl: 'views/collection/list.html',
      controller: 'collectionListCtrl',
      resolve: {
        authenticate: function ($q, platformFilter) {
          platformFilter('hd4res');
        }
      }
    })
    .when('/collection/new', {
      templateUrl: 'views/collection/new.html',
      controller: 'collectionNewCtrl',
      resolve: {
        authenticate: function ($q, platformFilter) {
          platformFilter('hd4res');
        }
      }
    })
    .when('/collection/edit/:id', {
      templateUrl: 'views/collection/edit.html',
      controller: 'collectionEditCtrl',
      resolve: {
        authenticate: function ($q, platformFilter) {
          platformFilter('hd4res');
        },
        skryvcollection: function (SkryvDefinitionManager, $route) {
          return SkryvDefinitionManager.get($route.current.params.id);
        }
      }
    })
    .when('/collection/raw/:id', {
      templateUrl: 'views/collection/raw.html',
      controller: 'collectionRawCtrl',
      resolve: {
        authenticate: function ($q, platformFilter, ddeFilter) {
          platformFilter('hd4res');
          ddeFilter();
        },
        skryvcollection: function (SkryvDefinitionManager, $route) {
          return SkryvDefinitionManager.get($route.current.params.id);
        }
      }
    });
});
