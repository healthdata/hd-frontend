/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.oauth')
  .config(function ($routeProvider, AUTHENTICATION) {
    'use strict';
    'ngInject';

    if (AUTHENTICATION === 'EID') {
      $routeProvider
        .when('/login', {
          templateUrl: 'views/oauth/eidLogin.html'
        })
        .when('/eid', {
          controller: 'oauthEidCtrl',
          reloadOnSearch: false, // Otherwise search will reload the page
          resolve: {
            user: function (SkryvIntro, PLATFORM, $location, $route, SkryvSession, Notifications) {
              if ($route.current.params.accessToken) {
                SkryvSession.storeUserTokens($route.current.params.accessToken, $route.current.params.refreshToken);
                $location.search('accessToken', null);

                // If access token fails, the session will destroy itself and return to login
                SkryvSession.loadApp().then(function () {
                  Notifications.success('Logged in with eID');
                  if (SkryvSession.isAdmin()) {
                    $location.path('/settings');
                  } else {
                    if (PLATFORM === 'hd4dp') { SkryvIntro.popDialogFeatures(); }
                    $location.path('/data collection');
                  }
                });
              }
            }
          }
        });
    } else {
      $routeProvider
        .when('/login', {
          templateUrl: 'views/oauth/login.html',
          controller: 'oauthLoginCtrl'
        })
        .when('/forgotPassword', {
          templateUrl: 'views/oauth/forgotPassword.html',
          controller: 'ForgotPasswordCtrl'
        })
        .when('/resetPassword', {
          templateUrl: 'views/oauth/resetPassword.html',
          controller: 'ResetPasswordCtrl'
        })
        .when('/userRequest', {
          templateUrl: 'views/user/requestNew.html',
          controller: 'requestNewCtrl'
        });
    }

    $routeProvider.when('/logout', {
      templateUrl: 'views/oauth/login.html',
      controller: 'oauthLogoutCtrl'
    });
  });
