/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* jshint node: true */

/**
 * Helpers list
 * @name Helpers
 * @const
 */
var Helpers = {

  /**
   * Returns true when the value is present in an array
   * @name inArray
   *
   * @arg  {array} array The array where the value must be present
   * @arg  {mixed} value The value that must be present in array
   *
   * @return {string} 'true' / 'false'
   */
  inArray: require('in-array'), // 3rd party dependency import

  /**
   * Transforms a date into the DD/MM/YYYY format
   * @name date
   *
   * @arg  {mixed} dte The date that must be converted ( undersood by JS Date() )
   *
   * @return {string} Date on the DD/MM/YYYY format
   */
  date: function (dte) {

    // Parse the input date
    var d = new Date(dte);

    // Return a string into the DD/MM/YYYY format
    return d.getDate() + '/' + ( d.getMonth() + 1 ) + '/' + d.getFullYear();

  },


  /**
   * Calculate an operation
   * @name math
   *
   * @arg  {mixed} value and oparztor for math function
   *
   * @return {string} list
   */
  math: function (lvalue, operator, rvalue, options) {

    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);

    return {
      "+": lvalue + rvalue,
      "-": lvalue - rvalue,
      "*": lvalue * rvalue,
      "/": lvalue / rvalue,
      "%": lvalue % rvalue
    }[operator];
  },

  /**
   * Get the first value which is not "falsy"
   * @name oneOfValues
   *
   * @arg  {mixed} ... values
   *
   * @return {mixed} First 'truthly' value
   */
  oneOfValues: function () {

    // Loop into the values and return if truhly
    for (var index in arguments)
      if (arguments[index])
        return arguments[index];

    // Otherwise, return undefined
    return undefined;

  },

  /**
   * Is one of those value defined ?
   * @name oneOfExisits
   *
   * @arg  {mixed} ... values
   */
  oneOfExists: function () {

    // Get the values (last arg is something else)
    var options = arguments[arguments.length - 1];
    delete arguments[arguments.length - 1];

    // Loop into the values and execute the block if not undefined
    for (var index in arguments)
      if (arguments[index] !== undefined)
        return options.fn(this);

    // Otherwise, return false
    return false;

  },

  /**
   * Is value 1 == value 2 ?
   * @name equals
   *
   * @arg  {mixed} ... values
   */
  equals: function (val1, val2) {

    // Just compare the 2 values
    return ( val1 == val2 );

  }

};

/**
 * Register the helpers on Handlebars engine
 * @name helpers
 * @function
 *
 * @arg {Handlebars} hbs Handlebars engine instance
 */
module.exports = function helpers(hbs) {
  Object.keys(Helpers)
    .forEach(function (key) {
      hbs.registerHelper(key, Helpers[key]);
    });
};
