/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.status')

.config(function ($routeProvider) {
  $routeProvider.when('/status/list', {
    templateUrl: 'views/statusMessaging/list.html',
    controller: 'StatusListCtrl',
    resolve: {
      authenticate: function ($q, authFilter) {
        authFilter('admin');
      }
    }
  }).when('/status/report', {
    templateUrl: 'views/statusMessaging/report.html',
    controller: 'StatusReportCtrl'
  });
})

.directive('statusBox', function (SkryvStatus) {
  return {
    restrict: 'E',
    scope: true,
    templateUrl: 'views/statusBox.html',
    link: function (scope) {
      scope.refresh = function () {
        scope.loading = true;
        scope.down = false;
        scope.esDown = false;
        scope.error = false;

        SkryvStatus.getStatusAPI().then(function (statuses) {
          scope.down = false;
          angular.forEach(statuses, function (status, $index) {
            if ($index === 'elasticsearch' && status == 'DOWN') {
              scope.esDown = true;
            } else if (status == 'DOWN') {
              scope.down = true;
            }
            scope.loading = false;
          });
          scope.statuses = statuses;
        }, function (err) {
          scope.loading = false;
          scope.error = true;
        });
      }

      scope.refresh();

    }
  };
});
