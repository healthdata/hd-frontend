/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.appConfig', [])

  // Filter by user type: Admin/User
  // Makes sure session is loaded before other modules starts
  .factory('authFilter', function (SkryvSession, $location, $q, Notifications) {
    return function (role) {
      var delay = $q.defer();

      SkryvSession.loadApp().then(function () {
        var auth = SkryvSession.isAdmin();
        // role: admin  --> only admin can access
        // role: user   --> only user can access
        // auth: if user is admin (bool)
        if ((role === 'admin' && auth) || (role === 'user' && !auth)) {
          delay.resolve(true);
        } else if ((role === 'admin' && !auth) || (role === 'user' && auth)) {
          Notifications.error('Access denied');
          $location.path('/settings');
          delay.resolve(false);
        }
      });

      return delay.promise;
    };
  })

  // When dde is diabled redirect user
  .factory('ddeFilter', function (EnvironmentConfig, $location, $q, Notifications) {
    return function () {
      var delay = $q.defer();

      EnvironmentConfig.isDdeEnabled().then(function (result) {
        if (result) {
          delay.resolve(result);
        } else {
          Notifications.error('Access denied');
          $location.path('/settings');
          delay.resolve(result);
        }
      });

      return delay.promise;
    };
  })

  // Filter if ldap or not
  .factory('authType', function (SkryvSession, $location, $q, Notifications) {
    return function () {
      var delay = $q.defer();

      SkryvSession.getAuthType().then(function (type) {
        if (type != 'DATABASE') {
          Notifications.error('Access denied');
          $location.path('/settings');
          delay.resolve(false);
        } else {
          delay.resolve(true);
        }
      });

      return delay.promise;
    };
  })

  // Filter on platform
  .factory('platformFilter', function (PLATFORM, $location, $q) {
    return function (platform) {
      var delay = $q.defer();
      if (platform == PLATFORM) {
        delay.resolve(true);
      } else {
        $location.path('/data collection');
        delay.resolve(false);
      }
      return delay.promise;
    };
  })

  // Set digest iteration limit
  .config(function ($rootScopeProvider) {
    $rootScopeProvider.digestTtl(30);
  })

  // Set prefix for local storage
  .config(function (localStorageServiceProvider, PLATFORM) {
    localStorageServiceProvider.setPrefix('htd.' + PLATFORM);
  })

  // Resolve loadApp before each route loads
  .config(function ($routeProvider, $injector) {
    // Make sure app is loaded before loading route
    var originalWhen = $routeProvider.when;
    $routeProvider.when = function (path, route) {

      if (!route.resolve) {
        route.resolve = {};
      }

      // Routes without session
      var noSession = [ 'document/hd4pc', 'login', 'logout', 'userRequest', 'forgotPassword', 'resetPassword', 'eid' ];

      angular.extend(route.resolve, {
        loadApp: [ '$q', 'SkryvSession', '$location', '_', function ($q, SkryvSession, $location, _) {
          var pathInList = _.find(noSession, function(path){
            return $location.path().indexOf(path) > -1;
          });

           if (!pathInList) {
             return SkryvSession.loadApp();
           }
        }]
      });

      return originalWhen.call($routeProvider, path, route);
    };

    // Routing
    $routeProvider
      .when('/settings', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl'
      });

    // Redirect different according to user role
    $routeProvider.otherwise({
      controller: ['SkryvSession', '$location', function (SkryvSession, $location) {
        if (SkryvSession.getLoggedUser()) {
          if (SkryvSession.isAdmin()) {
            $location.path('/settings');
          } else {
            $location.path('/data collection');
          }
        } else {
          $location.path('/');
        }
      }],
      template: '<div></div>'
    });
  })

  // Catch http calls and add bearer token if necessary
  .config(function ($httpProvider) {
    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
      $httpProvider.defaults.headers.get = {};
    }

    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';

    // Make sure data is send as utf-8
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/json; charset=utf-8';

    $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

    // Global ajax error handling
    $httpProvider.interceptors.push(function ($q, $injector, $location, $window, SINGLEFORM) {

      return {
        'response': function (response) {
          // Intercepts data if success
          return response;
        },
        'responseError': function (rejection) {
          // do something on error
          if (rejection.status === 503) {
            var win = $window.open('', '_self');
            win.document.writeln(rejection.data);
          }
          return $q.reject(rejection);
        },
        'request': function (config) {
          var deferred = $q.defer();
          config.headers = config.headers || {};
          var SkryvSession = $injector.get('SkryvSession');
          var PLATFORM = $injector.get('PLATFORM');

          // Don't pass user token to these calls
          // exclude catalogue requests
          //TODO: refactor!
          if ((config.url.indexOf('/healthdata_catalogue') > -1 && PLATFORM == 'hd4dp') ||
            (config.url.indexOf('/status') > -1 && !(config.url.indexOf('/statusmessage') > -1) && !(config.url.indexOf('/rebuild/status') > -1)) ||
            config.url.indexOf('/about/HD') > -1 ||
            (config.url.indexOf('/userrequests') > -1 && $location.path() === '/userRequest' ) ||
            config.url.indexOf('/datacollections/organization') > -1 ||
            config.url.indexOf('/translations?language') > -1 ||
            config.url.indexOf('/healthdata_hd4prc/cloud') > -1 ||
            (SINGLEFORM && config.url.indexOf('/datacollectiondefinitions')) ||
            config.url.indexOf('oauth/token') > -1 ||
              // filter out organizations call only if dataCollectionAccessRequests isn't included
            config.url.indexOf('/organizations') > -1 && config.url.indexOf('dataCollectionAccessRequests') === -1) {
            config.headers.Authorization = '';
            deferred.resolve(config);
          } else if (config.data || !config.cache) {
            // Filter refresh-token request itself
            var user = SkryvSession.getUserTokens();
            if (user) {
              config.headers.Authorization = 'Bearer ' + user.accessToken;
              deferred.resolve(config);
            } else {
              $location.path('/logout');
              deferred.reject(config);
            }
          } else {
            deferred.resolve(config);
          }

          return deferred.promise;
        }
      };
    });
  });
