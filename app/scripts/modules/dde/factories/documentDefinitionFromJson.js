/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
  'use strict';

  angular.module('skryv.dde')
    .factory('DocumentDefinitionFromJson',
    function(Field, DocumentDefinitionMetaModel, _, healthdataDocumentmodel) {
      return function documentDefinitionFromJson(dcd, params, target) {
        params = params || {};

        /** @type {DocumentDefinition.Field} */
        target = target || Field.create();

        var type = dcd.type;

        if (type === 'patientID') {
          dcd.fields = healthdataDocumentmodel.normalizeNestedPatientIDFields(dcd.fields);
        }

        // type can be overwritten by parameters
        if (params.type) {
          dcd.type = params.type;
          type = params.type;
          // reset overwrite of type parameters
          params.type = undefined;
        }

        // FIXME stijn: for some reason, we still need this code, see WDC-3062
        if (!type) {
          type = 'section';
          dcd.type = type;
        }

        // All Fields except Options (for choices), option is a hard requirement for first level elements under choices
        if (type !== 'option') {
          processProperty('type');
        }

        // List
        if (type === 'list') {
          if (has(dcd, 'computedExpressions')) {
            // Auto-fix bug where computed expressions were added to list
            // description as well as its element.
            dcd.computedExpressions = null;
          }
          // copy everything in element to root, this is a special case for lists
          if (has(dcd, 'element')) {
            if (has(dcd.element, 'conditions')) {
              dcd.conditions = dcd.element.conditions;
            }
            if (has(dcd.element, 'computedExpressions')) {
              dcd.computedExpressions = dcd.element.computedExpressions;
            }
            if (has(dcd.element, 'resolvedReferenceLists')) {
              dcd.resolvedReferenceLists = dcd.element.resolvedReferenceLists;
            }
            dcd.element = undefined;
          }
        }

        // Small tweak to allow both read-only and readOnly
        if (type !== 'section' && type !== 'fieldset' && type !== 'list') {
          processProperty('read-only', 'readOnly');
        }

        // Process all properties that we expect for this type (according to our meta model)
        if (DocumentDefinitionMetaModel.metaModel[ type ]) {
          angular.forEach(DocumentDefinitionMetaModel.metaModel[ type ], function (metaModel) {
            processProperty(metaModel.name);
          });
        }

        // Process Choices
        if (type === 'choice') {
          processNested('choices', { type: 'option' });
        }

        // Process MultiChoice
        if (type === 'multichoice') {
          processNested('choices', { type: 'option' });
        }

        // Add Custom fields (not processed above)
        processCustomFields();
        processNested('sections', { type: 'section' });
        processNested('fields');

        // helper function to check whether an object has an own property
        function has(obj, prop) {
          return Object.prototype.hasOwnProperty.call(obj, prop);
        }

        // helper function to transfer values from the dcd to the result
        function processProperty(specName, resultName) {
          resultName = specName || resultName;
          if (has(dcd, specName)) {
            target.properties[ resultName ] = dcd[ specName ];
            processDefault(specName, dcd[ specName ]);
            processValidators(specName, dcd[ specName ]);
            processComputedExpressions(specName, dcd[ specName ]);
            processOnlyWhen(specName, dcd[ specName ]);
            processResolvedReferenceLists(specName, dcd[ specName ]);
            processValues(specName, dcd[ specName ]);

            // Questionnaire
            processQuestionnaireAnswers(specName, dcd[ specName ]);
            processQuestionnaireQuestions(specName, dcd[ specName ])
          }
        }

        function processNested(nestedType, extraParams) {
          if (has(dcd, nestedType)) {
            return _.map(dcd[ nestedType ], function (dcdPart, position) {
              var model = documentDefinitionFromJson(dcdPart,
                _.extend({}, params, {
                  positionInNested: position,
                  defaultPropertyName: position.toString()
                }, extraParams));
              target.addChild(model);
              return model;
            });
          }
        }

        function processComputedExpressions(key, dcdPart) {
          if (key === 'computedExpressions' && dcdPart) {
            target.properties.computedExpressions = undefined;
            angular.forEach(dcdPart, function (value, key) {
              if (!value) {
                return;
              }
              var newComputedExpression = target.addComputedExpression();
              newComputedExpression.key = key;
              newComputedExpression.value = value;
            });
          }
        }

        function processValidators(key, dcdPart) {
          if (key === 'validators' && dcdPart) {
            target.properties.validators = undefined;
            angular.forEach(dcdPart, function (validator) {
              if (!validator) {
                return;
              }
              var newValidator = target.addValidator();
              newValidator.level = validator.level;
              newValidator.errorMessage = validator.errorMessage;
              newValidator.type = validator.hasOwnProperty('minimum') ? 'minimum' : 'maximum';
              var numberExpression = parseFloat(validator[ newValidator.type ]);
              if (isNaN(numberExpression)) {
                console.warn('Invalid number expression in validator: ', validator);
              } else {
                newValidator.expression = numberExpression;
              }
            });
          }
        }

        function processResolvedReferenceLists(key, dcdPart) {
          if (key === 'resolvedReferenceLists' && dcdPart) {
            target.properties.resolvedReferenceLists = undefined;
            angular.forEach(dcdPart, function (value, key) {
              if (!value) {
                return;
              }
              var newResolvedReferenceList = target.addResolvedReferenceList();
              newResolvedReferenceList.key = key;
              newResolvedReferenceList.referencelist = value.referencelist;
              newResolvedReferenceList.query = value.query;
            });
          }
        }

        function processOnlyWhen(key, dcdPart) {
          if (key === 'onlyWhen' && dcdPart) {
            target.properties.onlyWhen = undefined;
            if (angular.isArray(dcdPart)) {
              angular.forEach(dcdPart, function (value) {
                if (!value) {
                  return;
                }
                var newOnlyWhen = target.addOnlyWhen();
                newOnlyWhen.computedExpression = value;
              });
            } else {
              var newOnlyWhen = target.addOnlyWhen();
              newOnlyWhen.computedExpression = dcdPart;
            }
          }
        }

        function processValues(key, dcdPart) {
          if (key === 'values') {
            target.properties.values = {};
            var temp = [];
            angular.forEach(dcdPart, function (value) {
              if (!value) {
                return;
              }
              this.push(value);
            }, temp);
            target.properties.values.referencelist = temp[ 0 ];
            target.properties.values.query = temp[ 1 ];
          }
        }

        function processQuestionnaireAnswers(key, dcdPart) {
          if (key === 'answers' && dcdPart) {
            // Add Answers Node
            var answersField = target.addChild();
            answersField.properties.type = 'answers';
            answersField.properties.name = 'answers';
            answersField.properties.label = 'Answers';
            angular.forEach(dcdPart, function (answer) {
              // Add each answer beneath the answers node
              var answerField = documentDefinitionFromJson(answer, {type: 'subfield'});
              this.addChild(answerField);
            }, answersField);
          }
        }

        function processQuestionnaireQuestions(key, dcdPart) {
          if (key === 'questions' && dcdPart) {
            // Add Answers Node
            var questionsField = target.addChild();
            questionsField.properties.type = 'questions';
            questionsField.properties.name = 'questions';
            questionsField.properties.label = 'Questions';

            angular.forEach(dcdPart, function (question) {
              // Add each answer beneath the answers node
              var questionField = this.addChild();
              documentDefinitionFromJson(question, {type: 'question'}, questionField);
            }, questionsField);
          }
        }

        function processDefault(key, dcdPart) {
          // If not a number field, this is already handled by default
          // behaviour in processProperty.
          if (key === 'default' && dcd.type === 'number') {
            var numberExpression = parseFloat(dcdPart);
            if (isNaN(numberExpression)) {
              console.warn('Invalid number expression for default value: ', dcd, dcdPart);
            } else {
              target.properties.default = numberExpression;
            }
          }
        }

        // Process custom fields, but make sure fields that are not supposed to be processed are skipped
        function processCustomFields() {
          angular.forEach(dcd, function (value, key) {
            if (!value) {
              return;
            }
            if (key !== 'sections' && key !== 'fields' && key !== 'show' && key !== '$$hashKey' && key !== 'nested' &&
              key !== 'hasNested' && key !== 'choices' && !(key === 'type' && value === 'option') && !target.properties.hasOwnProperty(key)) {

              if (has(dcd, key)) {
                var newCustomProperty = target.addCustomProperty();
                newCustomProperty.key = key;
                newCustomProperty.value = value;
              }
            }
          });
        }

        return target;
      }
    });
