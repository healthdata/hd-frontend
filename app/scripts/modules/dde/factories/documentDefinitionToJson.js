/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
  'use strict';

  angular.module('skryv.dde')
    .factory('DocumentDefinitionToJson',
    function() {
      /**
       * @param {field} field
       */
      return function DocumentDefinitionToJson(field) {
        /** @type {JsonDocDef} */
        var result = {};
        var properties = field.properties;
        var nested = field.nested;
        var type = properties.type;

        preProcessQuestionnaire();

        // Filter and copy all fields
        copyFields();

        // Process Nested fields (to sections[], fields[], choices[])
        processNested();

        function copyFields() {
          angular.forEach(properties, function (value, key) {
            if (value !== undefined && value !== null && !isFilteredKeyword(key, value)) {
              result[ key ] = value;
              parseComputedExpressions(key, value);
              parseValidators(key, value);
              parseResolvedReferenceLists(key, value);
              parseOnlyWhen(key, value);
              parseValues(key, value);
              parseOrders(key, value);
              parseCustomProperties(key, value);
              parseListElement(key, value);
            }
          });
        }

        function processNested() {
          // Nested field need to be put in their correct list (sections, fields, choices) depending on their type
          angular.forEach(nested, function (nestedField) {
            var nestedType = 'fields'; // default is fields for most stuff
            if (nestedField.properties.type === 'section') {
              nestedType = 'sections';
            }
            if (nestedField.properties.type === 'option') {
              nestedType = 'choices';
            }
            if (nestedField.properties.type === 'answers') {
              parseQuestionnaireAnswers(field, nestedField);
              return;
            }
            if (nestedField.properties.type === 'questions') {
              return parseQuestionnaireQuestions(field, nestedField);
            }
            // make sure we have an initialized list for the selected type
            if (!result[ nestedType ]) {
              result[ nestedType ] = [];
            }
            // recursively parse the document definition
            result[ nestedType ].push(nestedField.toJson());
          });
        }

        /**
         * @param {string} key
         * @param {string|*} value
         */
        function isFilteredKeyword(key, value) {
          // Filter some fields out that don't need to be copied (like spec, $$hashkey, (has)nested, show)
          var filteredKeyList = [ 'spec', 'show', '$$hashKey', 'nested', 'hasNested' ];
          if (filteredKeyList.indexOf(key) !== -1) {
            return true;
          }

          // Also filter out some shizzle the docmod can't cope with
          if (key === 'type' && (value === 'section' || value === 'option' || value === 'subfield')) {
            return true;
          }
        }

        /**
         * @param {string} key
         * @param {ComputedExpression[]} value
         * @param {JsonDocDef=} target
         */
        function parseComputedExpressions(key, value, target) {
          target = target || result;
          if (key === 'computedExpressions' && angular.isArray(value) && value.length > 0) {
            target.computedExpressions = {};
            angular.forEach(value, function (computedExpression) {
              target.computedExpressions[ computedExpression.key ] = computedExpression.value;
            });
          }
        }

        /**
         * @param {string} key
         * @param {Validator[]} value
         * @param {JsonDocDef=} target
         */
        function parseValidators(key, value, target) {
          target = target || result;
          if (key === 'validators' && angular.isArray(value) && value.length > 0) {
            target.validators = [];
            angular.forEach(value, function (validator) {
              var parsedValidator = {
                name: validator.name,
                level: validator.level,
                errorMessage: validator.errorMessage
              };
              parsedValidator[ validator.type ] = validator.expression;
              this.push(parsedValidator);
            }, result.validators);
          }
        }

        /**
         * @param {string} key
         * @param {ResolvedReferenceList[]} value
         * @param {JsonDocDef=} target
         */
        function parseResolvedReferenceLists(key, value, target) {
          target = target || result;
          if (key === 'resolvedReferenceLists' && angular.isArray(value) && value.length > 0) {
            var parsed = {};
            angular.forEach(value, function (resolvedRefenceList) {
              parsed[ resolvedRefenceList.key ] = { referencelist: resolvedRefenceList.referencelist, query: resolvedRefenceList.query };
            });
            target.resolvedReferenceLists = parsed;
          }
        }

        function preProcessQuestionnaire() {
          if (type === 'questionnaire') {
            // Nested field need to be put in their correct list (sections, fields, choices) depending on their type
            angular.forEach(nested, function (nestedField) {
              if (nestedField.properties.type === 'answers') {
                return parseQuestionnaireAnswers(field, nestedField);
              }
              if (nestedField.properties.type === 'questions') {
                return parseQuestionnaireQuestions(field, nestedField);
              }
            });
          }
        }

        function parseQuestionnaireAnswers(questionnaire, answers) {
          if (questionnaire.properties.type === 'questionnaire' && answers.properties.type === 'answers') {
            // Answers for questionnaires are modelled in nested fields (questionnaire -> answers -> answer[]), but they json expects them on the root level in answer[] array
            var answerList = [];
            angular.forEach(answers.nested, function (answer) {
              this.push(answer.toJson());
            }, answerList);
            questionnaire.properties.answers = answerList;
          }
        }

        function parseQuestionnaireQuestions(questionnaire, questions) {
          if (questionnaire.properties.type === 'questionnaire' && questions.properties.type === 'questions') {
            // Answers for questionnaires are modelled in nested fields (questionnaire -> answers -> answer[]), but they json expects them on the root level in answer[] array
            var questionList = [];
            angular.forEach(questions.nested, function (question) {
              this.push(question.toJson());
            }, questionList);
            questionnaire.properties.questions = questionList;
          }
        }

        /**
         * @param {string} key
         * @param {CustomProperty[]} value
         */
        function parseCustomProperties(key, value) {
          if (key === 'custom' && angular.isArray(value) && value.length > 0) {
            // if there are custom properties, the model stores them in the .custom array, but they json expects them on the root level
            angular.forEach(value, function (customField) {
              result[ customField.key ] = customField.value;
            });
            result.custom = undefined;
          }
        }

        /**
         * @param {string} key
         * @param {OnlyWhen[]} value
         */
        function parseOnlyWhen(key, value) {
          if (key === 'onlyWhen' && angular.isArray(value) && value.length > 0) {
            result.onlyWhen = [];
            angular.forEach(value, function (onlyWhen) {
              this.push(onlyWhen.computedExpression);
            }, result.onlyWhen);
          }
        }

        /**
         * @param {string} key
         * @param {{referencelist: string, query: string}} value
         */
        function parseValues(key, value) {
          if (key === 'values' && value.referencelist && value.referencelist != null && value.query && value.query != null) {
            result.values = [ value.referencelist, value.query ];
          }
        }

        /**
         * @param {string} key
         * @param {Order[]} value
         */
        function parseOrders(key, value) {
          if (key === 'orders') {
            result.orders = value.map(function (order) {
              return { name: order.name, questions: order.questions };
            });
          }
        }

        /**
         * @param {string} key
         * @param {Condition[] |
         * Field.ComputedExpression[]|
         * ResolvedReferenceList[]} value
         */
        function parseListElement(key, value) {
          if ((type === 'list') && (key === 'conditions' || key === 'computedExpressions' || key === 'resolvedReferenceLists')) {
            if (!result.element) {
              result.element = {};
            }
            if (key === 'conditions') {
              result.element.conditions = value;
            }
            parseComputedExpressions(key, value, result.element);
            parseResolvedReferenceLists(key, value, result.element);
            result.conditions = undefined;
            result.computedExpressions = undefined;
            result.resolvedReferenceLists = undefined;
          }
        }

        return result;
      }
    });
