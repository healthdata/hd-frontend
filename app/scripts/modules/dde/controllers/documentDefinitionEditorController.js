/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde')
  .controller('DocumentDefinitionEditorCtrl',
  function ($scope, definitionId, wipId, $route, $routeParams, $location, openDocumentDefinition, openDocument, $timeout, $window, DocumentDefinitionModel, ngDialog, SkryvTranslation, $q, _, Notifications, healthdataDocumentmodel) {
    var ctrl = this;
    ctrl.documentDefinitionId = definitionId;
    ctrl.item = DocumentDefinitionModel.documentDefinition;
    ctrl.settings = DocumentDefinitionModel.settings;
    ctrl.jsonInput = '';
    ctrl.rawBasicDataSet = '';
    $scope.isLoading = true;
    $scope.readOnly = false;
    $scope.uiTreeOpts = {
      beforeDrop: onFieldMoved
    };

    var definitionSnapshot = ctrl.item.toJson();
    var confirmedExit = false;
    var unregisterRouteListener = $scope.$on('$routeChangeStart', onLocationChange);
    $window.addEventListener('beforeunload', onPageExit);

    // Load
    DocumentDefinitionModel.get(ctrl.documentDefinitionId, wipId).then(function () {
      $scope.isLoading = false;
      $scope.isLatest = ctrl.settings.isLatest;
      $scope.isPublished = ctrl.settings.isPublished;
      $scope.isEditable = ($scope.isLatest && !$scope.isPublished);
      definitionSnapshot = ctrl.item.toJson();

      if (!$scope.isEditable) {
        $timeout(function () {
          loadRaw();
          definitionSnapshot = ctrl.item.toJson();
        });
      }
    }, function (err) {
      $scope.isLoading = false;
    });

    //
    // Public
    //
    ctrl.editTitle = editTitle;
    ctrl.previewDocumentDefinition = previewDocumentDefinition;
    ctrl.openSaveDialog = openSaveDialog;
    ctrl.openRestoreDialog = openRestoreDialog;
    ctrl.openExitDialog = openExitDialog;
    ctrl.loadRaw = loadRaw;
    ctrl.parseRaw = parseRaw;
    ctrl.loadTranslations = loadTranslations;

    function editTitle(item) {
      if (!item.viewProperties) {
        item.viewProperties = {};
      }
      item.viewProperties.show = true;
      setTimeout(function () {
        document.getElementById('docDefTitle').focus();
      }, 100);
    }

    function validateJson(str) {
      $scope.jsonChecked = true;
      try {
        JSON.parse(str);
      } catch (e) {
        return false;
      }
      return true;
    }

    /**
     * Preview a Document Definition - This still requires $scope access
     * (Copied from Workbench)
     */
    function previewDocumentDefinition() {
      // First we want the clear the current document, if any. We do this by
      // setting documentLoaded to false. To make this this has any effect,
      // there must be a digest cycle before opening a new document. Therefore we
      // do the latter in a $timeout, and angular takes care of starting the
      // digest cycle.
      $scope.documentLoaded = false;
      $scope.processDocumentDefinition = null;
      $scope.documentModel = null;
      $scope.needComments = true;

      // By delaying the following code using $timeout, we make sure the old
      // document is cleared first.

      $timeout(function () {
        try {
          $scope.documentLoaded = true;
          $scope.processDocumentDefinition = openDocumentDefinition(ctrl.item.toJson());
          $scope.uniqueID = ctrl.item.toJson().uniqueIDExplanation;
          $scope.documentModel = openDocument(
            $scope.processDocumentDefinition,
            {}, // TODO: save document content in scope variable
            null, // Context: follow-up, installationId
            { readOnly: $scope.readOnly }
          );
          if (ctrl.rawBasicDataSet) {
            healthdataDocumentmodel.basicDataSet
              .importFromBasicDataSetWithCBBs(
                $scope.documentModel.document,
                ctrl.rawBasicDataSet);
            ctrl.importWithCBBsLog = $scope.documentModel.document.importWithCBBsLog;
          }
        } catch (err) {
          throw err;
        }
      });
    }

    function openSaveDialog(opts) {
      opts = opts || { preventRouteUpdate: true };
      return ngDialog.open({
        template: 'views/dde/saveDialog.html',
        controller: [ '$scope', function ($scope) {
          $scope.createWip = function () {
            confirmedExit = true;
            DocumentDefinitionModel.createWip($scope.description, opts);
            $scope.closeThisDialog('saved');
          };
        } ]
      });
    }

    function openRestoreDialog() {
      return ngDialog.open({
        template: 'views/dde/restoreDialog.html',
        controller: [ '$scope', function ($scope) {
          $scope.createWip = function () {
            confirmedExit = true;
            DocumentDefinitionModel.createWip(DocumentDefinitionModel.settings.wip.description);
            $scope.closeThisDialog('restored');
          };
        } ]
      });
    }

    function openExitDialog() {
      return ngDialog.open({
        template: 'views/dde/exitDialog.html',
        controller: [ '$scope', function ($scope) {
          $scope.saveFirst = function () {
            var saveDialog = openSaveDialog({ preventRouteUpdate: true });
            saveDialog.closePromise.then(function (result) {
              if (result && result.value === 'saved') {
                $scope.closeThisDialog('exit');
              }
            });
          }
          $scope.withoutSaving = function () {
            $scope.closeThisDialog('exit');
          }
          $scope.cancel = function () {
            $scope.closeThisDialog();
          }
        } ]
      });
    }

    function hasUnsavedChanges() {
      return !angular.equals(definitionSnapshot, ctrl.item.toJson());
    }

    function onLocationChange($event, next) {
      if (hasUnsavedChanges() && !confirmedExit) {
        var path = $location.path();
        var search = $location.search();
        $event.preventDefault();
        var dialog = openExitDialog();
        dialog.closePromise.then(function (result) {
          if (result.value == 'exit') {
            confirmedExit = true;
            $location.path(path).search(search);
          }
        })
      }
    }

    function onPageExit(event) {
      if (hasUnsavedChanges()) {
        var message = 'This data collection definition contains unsaved changes. You will lose them if you navigate away.';
        event = event || window.event;
        if (event) {
          event.returnValue = message;
        }
        return message;
      }
    }

    function onFieldMoved($event) {
      var movedField = $event.source.nodeScope.item;
      var destContainer = $event.dest.nodesScope.item;

      if (movedField.properties.type !== 'section') {
        var checkedId = movedField.properties.id;
        var checkedScope = [];
        if (destContainer) {
          checkedScope = destContainer.nameScope(ctrl.item, { includeSelf: true });
        }

        var foundFields = ctrl.item.findFieldByName(movedField.properties.name);
        // Found matching fields based on Name. Check if it is this field or other field
        for (var i = 0; i < foundFields.length; i++) {
          var foundField = foundFields[ i ];
          if (foundField.properties.type !== 'section') {
            var foundId = foundField.properties.id;
            var foundScope = foundField.nameScope(ctrl.item);
            if (!_.isEqual(foundId, checkedId) && _.isEqual(foundScope, checkedScope)) {
              Notifications.error('Another field with the same name exists in scope.');
              return false;
            }
          }
        }
      }
      return true;
    }

    function loadRaw() {
      ctrl.jsonInput = JSON.stringify(ctrl.item.toJson(), null, 2);
    }

    function parseRaw() {
      $scope.isJsonValid = validateJson(ctrl.jsonInput);
      DocumentDefinitionModel.enrichJson(JSON.parse(ctrl.jsonInput));
    }

    $scope.tabSwitch = function () {
      $scope.isJsonValid = true;
    };

    $scope.$watch('readOnly', function () {
      previewDocumentDefinition();
    });

    function getAllLabels(object) {
      var labelArray = [];

      if (object.label) {
        labelArray.push(object.label);
      }

      if (object.uniqueIDExplanation) {
        labelArray.push(object.uniqueIDExplanation);
      }

      if (object.help) {
        labelArray.push(object.help);
      }

      if (object.validators) {
        _.each(object.validators, function (validator) {
          labelArray.push(validator.errorMessage);
        });
      }

      if (object.choices) {
        _.each(object.choices, function (choice) {
          labelArray = labelArray.concat(getAllLabels(choice));
        });
      }

      if (object.answers) {
        _.each(object.answers, function (answer) {
          labelArray.push(answer.label);
        });
      }

      if (object.questions) {
        _.each(object.questions, function (question) {
          labelArray.push(question.label);
        });
      }

      if (object.sections) {
        _.each(object.sections, function (section) {
          labelArray = labelArray.concat(getAllLabels(section));
        });
      }

      if (object.fields) {
        _.each(object.fields, function (field) {
          labelArray = labelArray.concat(getAllLabels(field));
        });
      }

      if (object.conditions) {
        _.each(object.conditions, function (condition) {
          labelArray.push(condition.errorMessage);
        });
      }

      if (object.participationDefinition) {
        var labels = getAllLabels(object.participationDefinition.content);
        labelArray = labelArray.concat(labels);
      }

      //if (object.computedExpressions) {
      //  _.each(object.computedExpressions, function (computedExpression) {
      //    labelArray.push(computedExpression.errorMessage);
      //  });
      //}

      return _.uniq(labelArray);
    }

    function loadTranslations() {
      var labels = getAllLabels(ctrl.item.toJson());
      labels = labels.filter(function (n) { return n !== undefined; }); // Remove undefined

      var nlTranslations = [];
      var frTranslations = [];
      $scope.translations = [];
      $scope.toTranslate = true;
      $scope.inputFilter  = {};

      // Get dutch and french translations
      $q.all([ SkryvTranslation.getTranslations('fr', true), SkryvTranslation.getTranslations('nl', true) ]).then(function (data) {
        nlTranslations = data[1];
        frTranslations = data[0];

        // Map translations
        angular.forEach(labels, function (label) {
          $scope.translations.push({ key: label, nl: nlTranslations[label], fr: frTranslations[label] });
        });
      });

      // Op dialog for translation
      $scope.saveTranslation = function (translation) {
        SkryvTranslation.updateTranslations([ $scope.editable ]).then(function () {
          if ($scope.editable.nl) { translation.nl = $scope.editable.nl; }
          if ($scope.editable.fr) { translation.fr = $scope.editable.fr; }
          delete $scope.editable;
          Notifications.success('Translation updated');
        }, function () {
          Notifications.error('Failed updating translation');
        });
      };

      // Table configs
      $scope.editTranslation = function (translation) {
        $scope.editable = angular.copy(translation);
      };

      $scope.cancelTranslation = function () {
        delete $scope.editable;
      };
    }

    $scope.changeTranslate = function (bool) {
      $scope.toTranslate = bool;
      delete $scope.inputFilter.key;
    };

    $scope.filterTranslated = function (translation) {
      if ($scope.inputFilter.key || ($scope.toTranslate && (!translation.nl || !translation.fr)) || (!$scope.toTranslate && translation.nl && translation.fr)) {
        return true;
      } else {
        return false;
      }
    };

    $scope.$on('$destroy', function () {
      unregisterRouteListener();
      $window.removeEventListener('beforeunload', onPageExit);
    });
  });

