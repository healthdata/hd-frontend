/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
  'use strict';

  angular.module('skryv.dde')
    .factory('DocumentDefinitionModel', function (SkryvDefinitionManager, DocumentDefinition, Notifications, $location, _) {
      var docDefId;
      /** @type {DocumentDefinition.Field} */
      var documentDefinition = DocumentDefinition.createField();
      var originalDocDef = {};
      var wip = {};
      var settings = {};

      /**
       * Retrieve a Document Definition based on the given ID.
       * This will also load in the DocDef and override any current data in the model.
       * @param {string} id
       * @param {string} wipId
       * @returns {Promise<DocumentDefinition.Field>} Document Definition
       */
      function get(id, wipId) {
        docDefId = id;
        return SkryvDefinitionManager.get(id).then(function (data) {
          return SkryvDefinitionManager.getHistoryById(id).then(function (history) {
            var isLatest = false;
            var latestWip = _.max(history, function (defWip) {
              return Date.parse(defWip.createdOn);
            });

            if (wipId) {
              wip = _.find(history, function (defWip) {
                // We can't guarantee wipId is of the same type as historyId,
                // since $routeParams returns a string.
                return defWip.historyId == wipId;
              });
            } else {
              wip = latestWip;
            }

            if (wip && wipId) {
              isLatest = (wip.historyId === latestWip.historyId);
              data.content = wip.content;
            } else if (wip) {
              isLatest = true;
              data.content = wip.content;
            } else {
              Notifications.error('Could not find version');
              $location.path('/collection/edit/' + data.dataCollectionDefinitionId);
            }

            originalDocDef = data;
            var temp = DocumentDefinition.createField();
            // It is _essential_ to deep copy the content received from the backend; if not, any changes
            // to the document definition done via the DDE also appear in the history. This poses problems
            // later on, when the _original_ history must be sent back the the server, e.g., when publishing
            // a version. Modifying the history in itself is not desired behavior, but even worse, the
            // resulting JSON is not a valid document definition.
            enrichJson(angular.copy(data.content), temp);
            angular.copy(temp, documentDefinition);
            documentDefinition.properties.label = originalDocDef.dataCollectionDefinitionName;
            settings.isLatest = isLatest;
            settings.isPublished = data.published;
            settings.version = data.minorVersion;
            settings.wip = wip;
            return documentDefinition;
          });
        });
      }

      /**
       * Creates new wip from current content
       */
      function createWip(description, opts) {
        opts = opts || {};
        if (!description) {
          Notifications.error('No description was entered.');
          throw new Error('no description defined for WIP');
        }
        
        if (docDefId && documentDefinition) {
          var cp = angular.copy(originalDocDef);
          var json = documentDefinition.toJson();
          cp.content = json;
          cp.userDescription = description;
          if (settings.wip.userDescription && !description) { cp.userDescription = settings.wip.userDescription; }
          if (settings.wip.participationDefinition) { json.participationDefinition = settings.wip.participationDefinition; }

          return SkryvDefinitionManager.update(cp).then(function (wip) {
            Notifications.success('The data collection was successfully saved!');
            if (!opts.preventRouteUpdate) {
              $location.path('/dde/' + wip.dataCollectionDefinitionId); // Will load latest wip
            }
          }, function (err) {
            Notifications.error(err);
          });
        }
      }

      /**
       * Publish the currently loaded Document Definition
       */
      function publish() {
        if (docDefId && documentDefinition) {
          return SkryvDefinitionManager.publish(originalDocDef.dataCollectionDefinitionId).then(function () {
            Notifications.success('The data collection was successfully published!');
            $location.path('/collection/version/' + originalDocDef.dataCollectionDefinitionId);
          }, function (err) {
            Notifications.error(err);
          });
        }
      }

      /**
       * Enrich the json content {DocumentDefinition.Field}
       * @function parseJson
       * @param {string} json raw DocDef JSON
       * @param {Field=} field field to copy the result of the parsing to, if nothing is given, the result is copied to the loaded Document Definition
       * @returns {Field}
       */
      function enrichJson(json, field) {
        if (json) {
          var target = field || documentDefinition;
          var options = {};
          if (field) {
            documentDefinition.findFieldById(field.properties.id, function (field, parent) {
              if (parent && (parent.properties.type === 'multichoice' || parent.properties.type === 'choice' || parent.properties.type === 'patientID' || parent.properties.type === 'answers')) {
                options.type = 'subfield';
              }
              if (parent && parent.properties.type === 'questions') {
                options.type = 'question';
              }
            });
          }
          angular.copy(DocumentDefinition.createField(json, options), target);
          return target;
        }
      }

      var DocumentDefinitionModel = {
        /** @type {Field} */
        documentDefinition: documentDefinition,
        /** @type {enrichJson} */
        enrichJson: enrichJson,
        /** @type {publish} */
        publish: publish,
        /** @type {createWip} */
        createWip: createWip,
        /** @type {get} */
        get: get,
        /** @type {settings} */
        settings: settings
      };

      return DocumentDefinitionModel;
    });
