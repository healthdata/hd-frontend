/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
  'use strict';

  angular.module('skryv.dde')
    .factory('DocumentDefinitionMetaModel',
    /**
     * @returns {{}}
     * @constructor
     */
    function () {
      /**
       * Document Definition Meta Model
       * Returns an array with the available specifications for all known field types.
       *
       * e.g.
       * metaModel['text'] -> ['label','name','help','inputsize','default']
       * metaModel['section'] -> ['label', 'name', 'help']
       *
       * @type {{}}
       */
      var model = {};

      var DocumentDefinitionMetaModel = {
        metaModel: model
      };

      function createMetaModel() {
        var currentField;

        function createField(type) {
          currentField = [];
          model[ type ] = currentField;
        }

        function addSpec(spec, options) {
          currentField.push({ name: spec, options: options});
        }

        function addHelpSpec() {
          addSpec('help');
          addSpec('helpInLine', { hidden: true });
        }

        function addGeneralSpec() {
          addSpec('id');
          addSpec('type');
          addSpec('label');
          addSpec('name');
          addHelpSpec();
        }

        function addFieldSpec() {
          addSpec('required');
          addSpec('sticky');
          addSpec('readOnly');
          addSpec('onlyWhen');
        }

        createField('<none>');
        addSpec('type');

        createField('section');
        addGeneralSpec();
        addSpec('conditions');
        addSpec('computedExpressions');
        // addSpec('resolvedReferenceLists');

        createField('fieldset');
        addGeneralSpec();
        addSpec('onlyWhen');
        addSpec('conditions');
        addSpec('computedExpressions');
        // addSpec('resolvedReferenceLists');

        createField('list');
        addGeneralSpec();
        // addSpec('element');
        addSpec('initialLength');
        addSpec('onlyWhen');
        addSpec('conditions');
        addSpec('computedExpressions');
        // addSpec('resolvedReferenceLists');

        // createField('address');
        // addGeneralSpec();
        // addFieldSpec();

        // createField('attachment');
        // addGeneralSpec();
        // addFieldSpec();

        createField('boolean');
        addGeneralSpec();
        addFieldSpec();
        addSpec('default');

        createField('choice');
        addGeneralSpec();
        addFieldSpec();
        addSpec('default');
        addSpec('computedWith');

        createField('date');
        addGeneralSpec();
        addFieldSpec();
        addSpec('default');

        // createField('email');
        // addGeneralSpec();
        // addFieldSpec();
        // addSpec('computedWith');
        // addSpec('default');
        // addSpec('inputsize');

        createField('multichoice');
        addGeneralSpec();
        addFieldSpec();
        addSpec('default');
        addSpec('computedWith');

        createField('multiline');
        addGeneralSpec();
        addFieldSpec();
        addSpec('default');

        createField('number');
        addGeneralSpec();
        addFieldSpec();
        addSpec('computedWith');
        addSpec('default');
        addSpec('validators');
        // addSpec('values');

        createField('text');
        addGeneralSpec();
        addFieldSpec();
        addSpec('computedWith');
        addSpec('default');
        addSpec('inputsize');
        addSpec('referencelist');
        addSpec('binding');
        addSpec('typeaheadMinLength');
        // addSpec('values');

        createField('option');
        addSpec('id', {hidden: true});
        addSpec('type', {hidden: true});
        addSpec('label');
        addSpec('name');
        addSpec('default', {hidden: true});

        //
        // HD Specific
        //
        createField('patientID');
        addGeneralSpec();
        addFieldSpec();

        createField('questionnaire');
        addGeneralSpec();
        addFieldSpec();
        addSpec('answers');
        addSpec('orders');
        addSpec('allowRandomOrder');
        addSpec('questions');

        createField('subfield');
        addSpec('id', {hidden: true});
        addSpec('type', {hidden: true});
        addSpec('label');
        addSpec('name');
        addHelpSpec();
        addFieldSpec();
        addSpec('keep');

        createField('question');
        addSpec('id', {hidden: true});
        addSpec('type', {hidden: true});
        addSpec('label');
        addSpec('name');
        addHelpSpec();
        addFieldSpec();
        //addSpec('keep');

        createField('attachment');
        addSpec('id', {hidden: true});
        addSpec('type', {hidden: true});
        addSpec('label');
        addSpec('name');
        addHelpSpec();
        addFieldSpec();

        return model;
      }

      createMetaModel();

      return DocumentDefinitionMetaModel;
    });
