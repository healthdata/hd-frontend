/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
  'use strict';

  angular.module('skryv.dde')
    .factory('ReferenceListRepository',
    function ($log, $http, REFERENCE_SERVER_URL, REF_SERVER_URL) {
      var ReferenceListRepository = {
        getReferenceList: getReferenceList,
        getReferenceListHeaders: getReferenceListHeaders
      };

      function getReferenceList(id) {
        return $http.get(REFERENCE_SERVER_URL)
          .then(function (response) {
            return response.data;
          })
          .catch(function (failureResponse) {
            $log.error('Unable to retrieve Reference List Definitions: ' + JSON.stringify(failureResponse));
            throw 'Unable to retrieve Reference List Definitions';
          }
        );
      }

      function getReferenceListHeaders (id) {
        return $http.get(REF_SERVER_URL + '/' + id + '/headers')
          .then(function (response) {
            return response.data;
          })
          .catch(function (failureResponse) {
            $log.error('Unable to retrieve Reference List: ' + JSON.stringify(failureResponse));
            throw 'Unable to retrieve Reference List';
          }
        );
      }

      return ReferenceListRepository;
    });
