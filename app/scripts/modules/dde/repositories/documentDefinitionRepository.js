/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
  'use strict';

  angular.module('skryv.dde')
    .factory('DocumentDefinitionRepository', function ($log, $http, TEMPLATE_SERVER_URL) {
      var DocumentDefinitionRepository = {
        getDefinition: getDefinition,
        updateDefinition: updateDefinition,
        getHistory: getDefinitionHistory
      };

      /**
       * @param {string} id ID of the Document Definition to retrieve
       * @returns {Promise<string|Error>} Document Definition in JSON format
       */
      function getDefinition(id) {
        return $http.get(TEMPLATE_SERVER_URL + id)
          .then(function (response) {
            return response.data;
          })
          .catch(function (failureResponse) {
            $log.error('Unable to retrieve Registration Definition: ' + JSON.stringify(failureResponse));
            throw 'Unable to retrieve Registration Definition';
          });
      }

      /**
       * @param {string} id ID of the Registration Definition to retrieve the history for
       * @returns {Promise<string[]|Error>} History of Registration Definition in JSON format
       */
      function getDefinitionHistory(id) {
        return $http.get(TEMPLATE_SERVER_URL + id + '/versions')
          .then(function (response) {
            return response.data;
          })
          .catch(function (failureResponse) {
            $log.error('Unable to update Registration Definition: ' + JSON.stringify(failureResponse));
            throw 'Unable to retrieve Registration Definition History';
          });
      }

      /**
       * @param {string} id ID of the Registration Definition to update
       * @param {{name: {string}, content:{string}}} data the Registration Definition to save, in JSON format
       * @returns {Promise<string|Error>} The updated registration definition (or error)
       */
      function updateDefinition(id, data) {
        return $http.put(TEMPLATE_SERVER_URL + '/' + id, data)
          .then(function (response) {
            return response.data;
          })
          .catch(function (failureResponse) {
            $log.error('Unable to update Registration Definition: ' + JSON.stringify(failureResponse));
            throw 'Unable to update Registration Definition';
          });
      }

      return DocumentDefinitionRepository
    });
