/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde')
  .directive('skrDocDefEditorComponent',
  function (DocumentDefinitionModel, DocumentDefinitionMetaModel, _) {
    return {
      restrict: 'EA',
      templateUrl: 'views/dde/skrDocDefEditor.html',
      scope: {
        item: '=',
        autoUpdateName: '=',
        parentField: '=?',
        parentType: '=',
        isRoot: '='
      },
      link: function (scope) {

        /** @type {Field} */
        scope.documentDefinition = DocumentDefinitionModel.documentDefinition;
        scope.documentDefinitionMetaModel = DocumentDefinitionMetaModel;
        /** @type {Field} */
        scope.item;
        scope.rawJson = '';
        scope.templates = [];
        scope.viewProperties = {};
        scope.viewProperties.autoUpdateName = scope.autoUpdateName; // needed because we can't change values given on the directive

        if (!scope.item || !scope.item.properties) {
          throw new Error('skrDocDefEditorComponent directive requires an "item" attribute');
        }

        // When adding fields to choices the only possibility is option
        if (scope.parentType === 'choice' || scope.parentType === 'multichoice') {
          scope.item.properties.type = 'option';
        }

        // When adding fields to Questionnaire -> Questions/Answers the only possibility is subfield/question
        if (scope.parentType === 'answers') {
          scope.item.properties.type = 'subfield';
        }
        if (scope.parentType === 'questions') {
          scope.item.properties.type = 'question';
        }

        function capitalizeFirstLetter(string) {
          return string.charAt(0).toUpperCase() + string.slice(1);
        }

        function buildTemplateList(item) {
          scope.templates.length = 0;

          // Special case when we are editing the root item
          if (scope.isRoot) {
            scope.templates.push('views/dde/skrLabel.html');
            scope.templates.push('views/dde/skrComputedExpressions.html');
            scope.templates.push('views/dde/skrConditions.html');
            return;
          }

          // Iterate over the spec of the loaded Field and prepare templates
          angular.forEach(item.getSpec(), function (spec) {
            if (spec.options && spec.options.hidden) {
              return;
            }
            var tplURL = 'views/dde/skr' + capitalizeFirstLetter(spec.name) + '.html';
            if (typeof tplURL !== 'string') {
              throw new Error('skrDocDefEditorComponent directive requires a valid template URL, got ' + tplURL);
            }
            scope.templates.push(tplURL);
          });
          // Custom Properties template is always added
          scope.templates.push('views/dde/skrCustom.html');
        }

        //
        // RAW
        //
        scope.loadRawField = function () {
          scope.rawJson = JSON.stringify(scope.item.toJson(), null, 2);
        };

        scope.parseRawField = function () {
          DocumentDefinitionModel.enrichJson(scope.rawJson, scope.item);
        };

        function newPatientID(field) {
          // Last Name
          if (field.findFieldByName('name').length === 0) {
            var lastName = field.addChild();
            lastName.properties.type = 'text';
            lastName.properties.label = 'Name';
            lastName.properties.name = 'name';
            lastName.properties.required = 'noSSIN';
            lastName.properties.keep = false;
          }

          // First Name
          if (field.findFieldByName('first_name').length === 0) {
            var firstName = field.addChild();
            firstName.properties.type = 'text';
            firstName.properties.label = 'First name';
            firstName.properties.name = 'first_name';
            firstName.properties.required = 'noSSIN';
            firstName.properties.keep = false;
          }

          // date of birth
          if (field.findFieldByName('date_of_birth').length === 0) {
            var dateOfBirth = field.addChild();
            dateOfBirth.properties.type = 'date';
            dateOfBirth.properties.label = 'Date of birth';
            dateOfBirth.properties.name = 'date_of_birth';
            dateOfBirth.properties.required = 'noSSIN';
            dateOfBirth.properties.keep = true;
          }

          // Gender
          if (field.findFieldByName('sex').length === 0) {
            var gender = field.findFieldByName('sex')[ 0 ] || field.addChild();
            gender.properties.type = 'choice';
            gender.properties.label = 'Sex';
            gender.properties.name = 'sex';
            gender.properties.required = 'noSSIN';
            gender.properties.keep = true;

            var female = gender.findFieldByName('F')[ 0 ] || gender.addChild();
            female.properties.type = 'option';
            female.properties.name = 'F';
            female.properties.label = 'Female';

            var male = gender.findFieldByName('M')[ 0 ] || gender.addChild();
            male.properties.type = 'option';
            male.properties.name = 'M';
            male.properties.label = 'Male';

            var unknown = gender.findFieldByName('U')[ 0 ] || gender.addChild();
            unknown.properties.type = 'option';
            unknown.properties.name = 'U';
            unknown.properties.label = 'Unkown';
          }

          // Place of Residence
          if (field.findFieldByName('place_of_residence').length === 0) {
            var residence = field.addChild();
            residence.properties.type = 'text';
            residence.properties.label = 'Place of residence';
            residence.properties.name = 'place_of_residence';
            residence.properties.keep = true;
            residence.properties.referencelist = 'POSTAL_CODE';
          }

          // Deceased?
          if (field.findFieldByName('deceased').length === 0) {
            var deceased = field.addChild();
            deceased.properties.type = 'boolean';
            deceased.properties.label = 'Deceased?';
            deceased.properties.name = 'deceased';
            deceased.properties.keep = true;
          }

          // Date of death
          if (field.findFieldByName('date_of_death').length === 0) {
            var dateOfDeath = field.addChild();
            dateOfDeath.properties.type = 'date';
            dateOfDeath.properties.label = 'Date of death';
            dateOfDeath.properties.name = 'date_of_death';
            dateOfDeath.properties.onlyWhen = 'deceased';
            dateOfDeath.properties.keep = true;
          }
        }

        var optionsForRequiredField;
        function populateOptionsForRequiredField() {
          optionsForRequiredField = scope.item.getComputedExpressions(DocumentDefinitionModel.documentDefinition);
          angular.forEach(optionsForRequiredField, function (option) {
            option.label = option.key;
          });
          optionsForRequiredField.unshift({ label: 'false' });
          optionsForRequiredField.unshift({ label: 'true', key: true });
        }

        populateOptionsForRequiredField();
        scope.getOptionsForRequiredField = function () {
          return optionsForRequiredField;
        };
        /**
         * Triggered if the type of a field changes, this requires rebuilding the template list
         */
        scope.typeChanged = function () {
          // Special sauce for PatientID
          if (scope.item.properties.type && scope.item.properties.type === 'patientID') {
            newPatientID(scope.item);
          }

          buildTemplateList(scope.item);
        };
        // trigger manually for first window buildup
        scope.typeChanged();

        /**
         * When changing the label, the field name is checked for consistency.
         * If it is different an option is given to the user to update the name.
         * If we are creating a new field we are auto-updating the name as we type the label
         */
        scope.labelChanged = function () {
          var label = scope.item.properties.label;
          if (label) {
            // calculate name
            scope.newName = label.toLowerCase().replace(/ /g, '_');
            scope.newName = scope.newName.replace(/[^0-9a-z_]+/g, '');

            scope.newName = checkName(scope.newName);

            if (scope.item.properties.name !== scope.newName && scope.viewProperties.autoUpdateName) {
              scope.item.properties.name = scope.newName;
            }
          }
        };

        scope.addQuestionnaireOrder = function() {
          var orders = scope.item.properties.orders || [];
          orders.push({});
          scope.item.properties.orders = orders;
        };
        scope.removeQuestionnaireOrder = function(order) {
          order = order || {};
          var orders = scope.item.properties.orders || [];
          var orderInList = _.find(orders, function (o) { return o === order || o.name && o.name === order.name; });
          orders = _.without(orders, orderInList);
          scope.item.properties.orders = orders;
        };
        scope.changeQuestionnaireOrder = function(order) {
          if (order && order.questions) {
            order.questions = order.questions.split(',');
          }
        };

        scope.validateName = function () {
          var validatedName = checkName(scope.item.properties.name);
          return validatedName === scope.item.properties.name
        };

        scope.checkName = checkName;
        function checkName(currentName, appendingIndex) {
          appendingIndex = appendingIndex || 0;

          if (scope.item.properties.type !== 'section') {
            // Check if name already exists in document
            var foundFields = scope.documentDefinition.findFieldByName(currentName);
            if (foundFields.length > 0) {
              // Found matching fields based on Name. Check if it is this field or other field
              for (var i = 0; i < foundFields.length; i++) {
                var foundField = foundFields[ i ];
                if (foundField.properties.type !== 'section') {
                  var foundId = foundField.properties.id;
                  var foundScope = foundField.nameScope(scope.documentDefinition);
                  var checkedId = scope.item.properties.id;
                  var checkedScope = scope.item.nameScope(scope.documentDefinition);
                  if (_.isEmpty(checkedScope) && scope.parentField) {
                    // Can occur when adding a new field that hasn't actually been
                    // added to the definition yet
                    checkedScope = scope.parentField.nameScope(scope.documentDefinition, { includeSelf: true });
                  }

                  if (!_.isEqual(foundId, checkedId) && _.isEqual(foundScope, checkedScope)) {
                    // Found other field with same name in conflicting scope
                    // Deploy countermeasures (append _1, _2, ... to name)
                    if (appendingIndex > 0) {
                      var lastIndex = currentName.lastIndexOf('_');
                      currentName = currentName.substring(0, lastIndex);
                    }
                    currentName += '_' + ++appendingIndex;
                    // Resursively check
                    return checkName(currentName, appendingIndex);
                  }
                }
              }
            }
          }
          return currentName;
        }

        /**
         * Helper function to retrieve Reference List ID based on a given ResolvedReferenceList
         */
        scope.resolveResolvedReferenceList = function (resolvedReferenceList) {
          var result = undefined;
          angular.forEach(scope.item.getResolvedReferenceLists(scope.documentDefinition), function (refList) {
            if (refList.key === resolvedReferenceList) {
              return result = refList.referencelist;
            }
          });
          return result;
        };
      }
    }
  });
