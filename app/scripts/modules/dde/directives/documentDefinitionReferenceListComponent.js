/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
  'use strict';

  angular.module('skryv.dde')
    .directive('skrDocDefReferenceListComponent',
    function (ReferenceListRepository) {
      return {
        restrict: 'EA',
        templateUrl: 'views/dde/skrDocDefEditorReferenceList.html',
        scope: {
          selectedReferenceList: '='
        },
        link: function (scope) {
          var referenceLists = [];
          scope.referenceLists = referenceLists;

          ReferenceListRepository.getReferenceList().then(function (data) {
            referenceLists.length = 0;
            referenceLists.push({ name: '<none>', id: undefined });
            angular.forEach(data, function (refList) {
              referenceLists.push(refList);
            });
          });
        }
      }
    });
