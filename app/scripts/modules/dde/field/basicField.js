/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde.field')
  .factory('Field',
  function (DocumentDefinitionMetaModel, custom, computedExpression, condition, validator, onlyWhen, DocumentDefinitionToJson, _) {

    /**
     * Find a field within the given Document Definition by it's unique ID.
     * @param {Field} part the Document Definition (part) where to look for the field
     * @param {number} id the ID of the field to look for
     * @param {function(Field, Field, number)} func function to executed when field is found.
     * Expecting to return true/false to indicate successful/failed execution.
     * @param {Field=} parent (optional) starter parent for the search
     * @param {number=} index (optional) start index for the search
     * @returns {boolean} returns the value of the executed function, or false if the field was not found
     */
    function findFieldById(part, id, func, parent, index) {
      if (part.properties.id === id) {
        return func(part, parent, index);
      } else {
        if (part.nested) {
          for (var i = 0; i < part.nested.length; i++) {
            var result = findFieldById(part.nested[ i ], id, func, part, i);
            if (result) {
              return result;
            }
          }
        }
      }
      return false;
    }

    function findFieldsByName(part, name, foundFields) {
      foundFields = foundFields || [];
      if (part.properties.name && part.properties.name === name) {
        foundFields.push(part);
      }
      if (part.nested) {
        for (var i = 0; i < part.nested.length; i++) {
          findFieldsByName(part.nested[ i ], name, foundFields);
        }
      }
      return foundFields;
    }

    /**
     * @param {Field} documentDefinition the Document Definition (part) to search in
     * @param {Field} field the Document Definition Field that is the entry point for the search
     * @param {string} propertyName the property to look for
     * @param {Array=} target target to add the results to
     * @returns {Array} results
     */
    function gatherParentProperties(documentDefinition, field, propertyName, target) {
      target = target || [];
      documentDefinition.findFieldById(field.properties.id, function (field, parent) {
        if (field.properties[ propertyName ]) {
          target.push(field.properties[ propertyName ]);
        }
        if (parent) {
          gatherParentProperties(documentDefinition, parent, propertyName, target);
        }
      });
      return target;
    }

    function nameScope(documentDefinition, field, target) {
      var scopeContainerTypes = [ 'list', 'choice', 'multichoice', 'fieldset' ];
      target = target || [];
      documentDefinition.findFieldById(field.properties.id, function (field, parent) {
        if (_.contains(scopeContainerTypes, field.properties.type)) {
          target.push(field.properties.id);
        }
        if (parent) {
          nameScope(documentDefinition, parent, target);
        }
      });
      return target;

    }

    /**
     * Helper function to generate a GUID, used to create field id's for new fields or older template that don't have ID's yet
     * @returns {string} generated GUID
     */
    function generateUUID() {
      var d = new Date().getTime();
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
      });
    }

    /**
     * Represent a Document Definition Field, enriched with helper functions.
     * Contains children (sections, lists, fieldsets) under 'nested'
     * @constructor
     */
     function Field() {
      /**
       * @type {{
       *     key: string,
       *     type: string,
       *     custom: CustomProperty[],
       *     computedExpressions: ComputedExpression[],
       *     conditions: Condition[],
       *     validators: Validator[],
       *     resolvedReferenceLists: ResolvedReferenceList[],
       *     onlyWhen: OnlyWhen[]
       *   }}
       */
      this.properties = {};
      /** @type {Field[]} */
      this.nested = [];

      function _findFieldById(id, func) {
        return findFieldById(this, id, func);
      }

      function _findFieldByName (name) {
        return findFieldsByName(this, name);
      }

      function _nameScope (documentDefinition, opts) {
        opts = opts || {};
        var scope = nameScope(documentDefinition, this);
        if (!opts.includeSelf && scope[0] === this.properties.id) {
          scope = scope.slice(1);
        }
        return scope;
      }

      /**
       * Create a new Child Field, this field is not yet attached to this Doc Def nested fields. <br />
       * See {@link Field.addChild} for adding/creating children which are attached to this Doc Def
       * @param {string=} type type of the new field
       * @returns {Field} newly created field
       */
      function createChild(type) {
        var newChild = new Field();
        newChild.properties.type = type || '<none>';
        newChild.properties.id = generateUUID();
        return newChild;
      }

      /**
       * Add a new child to this Doc Def. If no child is provided a new one is created and directly added to this Doc Def nested fields. <br />
       * See {@link Field.createChild} for creating children without attaching them to the Doc Def
       * @param {Field=} newChild to add to this Doc Def nested fields
       * @returns {Field} newly created field
       */
      function addChild(newChild) {
        newChild = newChild || createChild();
        if (!newChild.properties.id) {
          newChild.properties.id = generateUUID();
        }
        if (newChild.properties.type === 'questionnaire' && newChild.nested.length === 0) {
          var questionsField = newChild.addChild();
          questionsField.properties.type = 'questions';
          //questionsField.properties.name = 'questions';
          questionsField.properties.label = 'Questions';
          var answersField = newChild.addChild();
          answersField.properties.type = 'answers';
          //answersField.properties.name = 'answers';
          answersField.properties.label = 'Answers';
        }
        this.nested.push(newChild);
        return newChild;
      }

      /**
       * Removes this field from the currently loaded Doc Def
       */
      function remove(documentDefinition) {
        documentDefinition.findFieldById(this.properties.id, function (field, parent, index) {
          parent.nested.splice(index, 1);
        });
      }

      /**
       * Returns the Specifications of this field
       * @returns {string[]}
       */
      function getSpec() {
        return DocumentDefinitionMetaModel.metaModel[ this.properties.type ];
      }

      function isValid () {
        var id = this.properties.id;
        var type = this.properties.type;
        var label = this.properties.label;
        var name = this.properties.name;

        return (id && label && name && type && type !== '<none>');
      }

      this.gatherParentProperties = gatherParentProperties;
      this.generateUUID = generateUUID;

      function toJson() {
        return DocumentDefinitionToJson(this);
      }

      custom(this);
      computedExpression(this);
      condition(this);
      validator(this);
      onlyWhen(this);

      // Basics
      this.findFieldById = _findFieldById;
      this.findFieldByName = _findFieldByName;
      this.nameScope = _nameScope;
      this.createChild = createChild;
      this.addChild = addChild;
      this.remove = remove;
      this.getSpec = getSpec;
      this.isValid = isValid;


      // JSON
      this.toJson = toJson;
    }

    var ConstructorFunction = {
      create : function () {
        return new Field();
      }
    };
    return ConstructorFunction;
  });
