/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde.field')
  .factory('ResolvedReferenceList',
  function(_) {
    var _field;
    function ResolvedReferenceList(field) {
      _field = field;
      return {
        addResolvedReferenceList: addResolvedReferenceList,
        removeResolvedReferenceList: removeResolvedReferenceList,
        getResolvedReferenceLists: getResolvedReferenceLists
      };
    }

    /**
     * @constructor
     */
    function ResolvedReferenceListProperty() {
      /** @type {string} */
      var key = '';
      /** @type {string} */
      var referencelist = '';
      /** @type {string} */
      var query = '';
    }

    /**
     * @returns {ResolvedReferenceListProperty}
     */
    function addResolvedReferenceList() {
      if (!_field.properties.resolvedReferenceLists) {
        _field.properties.resolvedReferenceLists = [];
      }
      var newChild = new ResolvedReferenceListProperty();
      _field.properties.resolvedReferenceLists.push(newChild);
      return newChild;
    };
    function removeResolvedReferenceList(item) {
      var index = _field.properties.resolvedReferenceLists.indexOf(item);
      if (index !== -1) {
        _field.properties.resolvedReferenceLists.splice(index, 1);
      }
    };
    // Find available Resolved Reference Lists for this field
    function getResolvedReferenceLists(documentDefinition) {
      return _.flatten(_field.gatherParentProperties(documentDefinition, this, 'resolvedReferenceLists'));
    };

    return ResolvedReferenceList
  }
);
