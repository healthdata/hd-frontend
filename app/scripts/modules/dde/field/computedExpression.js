/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde.field')
  .factory('computedExpression',
  function(_) {
    function computedExpression(field) {
      field.addComputedExpression = addComputedExpression;
      field.removeComputedExpression = removeComputedExpression;
      field.getComputedExpressions = getComputedExpressions;
      return field;
    }

    /**
     * @constructor
     */
    function ComputedExpressionProperty() {
      /** @type {string} */
      this.key = '';
      /** @type {string} */
      this.value = '';
    }

    /**
     * @returns {ComputedExpressionProperty}
     */
    function addComputedExpression() {
      if (!this.properties.computedExpressions) {
        this.properties.computedExpressions = [];
      }
      var newChild = new ComputedExpressionProperty();
      this.properties.computedExpressions.push(newChild);
      return newChild;
    }
    function removeComputedExpression(item) {
      var index = this.properties.computedExpressions.indexOf(item);
      if (index !== -1) {
        this.properties.computedExpressions.splice(index, 1);
      }
    }
    function getComputedExpressions(documentDefinition) {
      return _.flatten(this.gatherParentProperties(documentDefinition, this, 'computedExpressions'));
    }


    return computedExpression
  }
);
