/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde.field')
  .factory('condition',
  function() {
    function condition(field) {
      field.addCondition= addCondition;
      field.removeCondition=removeCondition;
      return field;
    }

    /**
     * @constructor
     */
    function ConditionProperty() {
      /** @type {string} */
      this.name = '';
      /** @type {string} */
      this.level = 'error';
      /** @type {string} */
      this.expression = '';
      /** @type {string} */
      this.errorMessage = '';
    }

    /**
     * @returns {ConditionProperty}
     */
    function addCondition() {
      if (!this.properties.conditions) {
        this.properties.conditions = [];
      }
      var newChild = new ConditionProperty();
      newChild.name = this.generateUUID();
      this.properties.conditions.push(newChild);
      return newChild;
    }
    function removeCondition(item) {
      var index = this.properties.conditions.indexOf(item);
      if (index !== -1) {
        this.properties.conditions.splice(index, 1);
      }
    }

    return condition
  }
);
