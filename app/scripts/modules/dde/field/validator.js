/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde.field')
  .factory('validator',
  function() {
    function validator(field) {
      field.addValidator= addValidator;
      field.removeValidator= removeValidator;
      return field;
    }

    /**
     * @constructor
     */
    function ValidatorProperty() {
      /** @type {string} */
      var name = '';
      /** @type {string} */
      var level = 'error';
      /** @type {string} */
      var type = 'maximum';
      /** @type {number} */
      var value = undefined;
      /** @type {string} */
      var errorMessage = '';
    }

    /**
     * @returns {ValidatorProperty}
     */
    function addValidator() {
      if (!this.properties.validators) {
        this.properties.validators = [];
      }
      var newChild = new ValidatorProperty();
      newChild.name = this.generateUUID();
      this.properties.validators.push(newChild);
      return newChild;
    }
    function removeValidator(item) {
      var index = this.properties.validators.indexOf(item);
      if (index !== -1) {
        this.properties.validators.splice(index, 1);
      }
    }

    return validator
  }
);
