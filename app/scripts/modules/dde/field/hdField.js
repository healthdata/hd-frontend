/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('skryv.dde.field')
  .factory('HDField',
  function (Field, Custom, ComputedExpression, Condition, Validator, OnlyWhen, ResolvedReferenceList) {
    function HDField() {
      var field = new Field();
      //console.log(field);
      var custom = new Custom(field);
      var computedExpression = new ComputedExpression(field);
      var condition = new Condition(field);
      var validator = new Validator(field);
      var onlyWhen = OnlyWhen(field);
      var resolvedReferenceList = new ResolvedReferenceList(field);

      return {
        // Basics
        properties: field.properties,
        nested: field.nested,
        findFieldById: field.findFieldById,
        findFieldByName: field.findFieldByName,
        createChild: function () {
          return HDField()
        },
        addChild: field.addChild,
        remove: field.remove,
        getSpec: field.getSpec,
        isValid: field.isValid,
        // JSON
        toJson: field.toJson,

        // Custom
        addCustomProperty: custom.addCustomProperty,
        removeCustomProperty: custom.removeCustomProperty,

        // Computed Expressions
        addComputedExpression: computedExpression.addComputedExpression,
        removeComputedExpression: computedExpression.removeComputedExpression,
        getComputedExpressions: computedExpression.getComputedExpressions,

        // Condition
        addCondition: condition.addCondition,
        removeCondition: condition.removeCondition,

        // Only When
        addOnlyWhen: onlyWhen.addOnlyWhen,
        removeOnlyWhen: onlyWhen.removeOnlyWhen,

        // Validator
        addValidator: validator.addValidator,
        removeValidator: validator.removeValidator,

        // Resolved Reference List
        addResolvedReferenceList: resolvedReferenceList.addResolvedReferenceList,
        removeResolvedReferenceList: resolvedReferenceList.removeResolvedReferenceList,
        getResolvedReferenceLists: resolvedReferenceList.getResolvedReferenceLists
      }
    }

    return {
      create: function () {
        return HDField()
      }
    }
  }
);