/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde.field')
  .factory('custom',
  function () {
    function custom(field) {
      field.addCustomProperty = addCustomProperty;
      field.removeCustomProperty = removeCustomProperty;
      return field;
    }

    /**
     * @constructor
     */
    function CustomProperty() {
      /** @type {string} */
      this.key = '';
      /** @type {string} */
      this.value = '';
    }

    /**
     * @returns {CustomProperty}
     */
    function addCustomProperty() {
      if (!this.properties.custom) {
        this.properties.custom = [];
      }
      var newChild = new CustomProperty();
      this.properties.custom.push(newChild);
      return newChild;
    }

    function removeCustomProperty(item) {
      var index = this.properties.custom.indexOf(item);
      if (index !== -1) {
        this.properties.custom.splice(index, 1);
      }
    }

    return custom;
  }
);
