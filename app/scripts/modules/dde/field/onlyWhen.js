/* Original work Copyright (c) 2015 Skryv
 * Modified work Copyright (c) 2015-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde.field')
  .factory('onlyWhen',
  function () {
    function onlyWhen(field) {
      field.addOnlyWhen = addOnlyWhen;
      field.removeOnlyWhen = removeOnlyWhen;
      return field;
    }

    /**
     * @constructor
     */
    function OnlyWhenProperty() {
      /** @type {string} */
      this.computedExpression = '';
    }

    /**
     * @returns {OnlyWhenProperty}
     */
    function addOnlyWhen() {
      if (!this.properties.onlyWhen) {
        this.properties.onlyWhen = [];
      }
      var newChild = new OnlyWhenProperty();
      this.properties.onlyWhen.push(newChild);
      return newChild;
    }

    /**
     * @param {OnlyWhen.OnlyWhenProperty} item
     */
    function removeOnlyWhen(item) {
      var index = this.properties.onlyWhen.indexOf(item);
      if (index !== -1) {
        this.properties.onlyWhen.splice(index, 1);
      }
    }

    return onlyWhen;
  }
);
