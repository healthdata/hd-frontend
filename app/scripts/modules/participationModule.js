/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.participation')
  .config(function ($routeProvider, PLATFORM) {

    $routeProvider.when('/participation', {
      templateUrl:  (PLATFORM === 'hd4res' ? 'views/participation/participation.html' : 'views/participation/dataProviderParticipation.html'),
      controller: (PLATFORM === 'hd4res' ? 'ParticipationCtrl' : 'DataProviderParticipationCtrl'),
      resolve: {
        authenticate: function ($q, authFilter) {
          authFilter('user');
        }
      }
    });
  });
