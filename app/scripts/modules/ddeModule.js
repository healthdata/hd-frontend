/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.dde', [ 'ngRoute', 'ngDialog', 'skryv.translation'])
  .config(function ($routeProvider) {
    $routeProvider.when('/dde/:documentDefinitionId/', {
      templateUrl: 'views/dde/edit.html',
      controller: 'DocumentDefinitionEditorCtrl',
      controllerAs: 'docDefEditor',
      resolve: {
        authenticate: function (platformFilter, ddeFilter) {
          platformFilter('hd4res');
          ddeFilter();
        },
        definitionId: function ($route) {
          return $route.current.params.documentDefinitionId;
        },
        wipId: function ($route) {
          return $route.current.params.wipId;
        }
      }
    });
  });
