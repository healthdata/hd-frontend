/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

var SkryvIntro = angular.module('skryv.intro',
  [ 'skryv.intro', 'skryv.version' ]);

  SkryvIntro.factory('SkryvIntro', function (gettextCatalog, ngDialog, PLATFORM, SkryvSession, $rootScope) {

    // ##########################
    // - Introduction Dialog
    // -------------------------

    /*  PopDialog()                      -- Show introduction dialog
     saveIntroduction(bool)           -- Close intro &| Save intro in user meta data   */

    function popDialogAdmin() {
      ngDialog.open({
        controller: [ '$scope', 'SkryvSession', 'SkryvUser', function ($scope, SkryvSession, SkryvUser) {
          // controller logic
          var loggedUser = SkryvSession.getLoggedUser();

          // Save movie is viewed
          $scope.saveIntroduction = function () {
            $scope.closeThisDialog();
            if ($scope.intro) {
              loggedUser.metaData.introductionAdmin = true;
              SkryvUser.updateMetadata(loggedUser);
            }
          };
        } ],
        template: 'views/dialog/intro/admin.html',
        showClose: false,
        className: 'ngdialog-theme-introduction ngdialog-theme-default',
      });
    }

    function popDialogUser() {
      ngDialog.open({
        controller: [ '$scope', 'SkryvSession', 'SkryvUser', function ($scope, SkryvSession, SkryvUser) {
          // controller logic
          var loggedUser = SkryvSession.getLoggedUser();

          // Save movie is viewed
          $scope.dclist =  SkryvSession.getCollections();
          $scope.saveIntroduction = function () {
            $scope.closeThisDialog();
            if ($scope.intro) {
              loggedUser.metaData.introductionUser = true;
              SkryvUser.updateMetadata(loggedUser);
            }
          };
        } ],
        template: 'views/dialog/intro/user.html',
        showClose: false,
        className: 'ngdialog-theme-introduction ngdialog-theme-default'
      });
    }

    function popDialogFeatures() {
      ngDialog.open({
        controller: [ '$scope', 'SkryvSession', 'SkryvUser', 'SkryvVersion', 'SkryvLanguage', function ($scope, SkryvSession, SkryvUser, SkryvVersion, SkryvLanguage) {
          // controller logic
          var loggedUser = SkryvSession.getLoggedUser();

          SkryvVersion.get().then(function (data) {
            $scope.version = data.value;
          });

          // Save movie is viewed
          $scope.saveBoolean = function () {
            ngDialog.close();
            if ($scope.intro) {
              loggedUser.metaData.announceFeatures = true;
              SkryvUser.updateMetadata(loggedUser);
            }
          };

          var lang = SkryvLanguage.getLanguage();
          if (lang === 'nl') {
            $scope.link = 'http://healthdataknowledge.force.com/articles/nl_NL/Support_Article/Overview-release-notes';
          } else if (lang === 'fr') {
            $scope.link = 'http://healthdataknowledge.force.com/articles/fr/Support_Article/Overview-release-notes';
          } else {
            $scope.link = 'http://healthdataknowledge.force.com/articles/en_US/Support_Article/Overview-release-notes';
          }
        } ],
        template: 'views/dialog/intro/features.html',
        showClose: false
      });
    }

    return {
      popDialogUser: function () {
        var loggedUser = SkryvSession.getLoggedUser();
        if (loggedUser) {
          if (!loggedUser.metaData.introductionUser && PLATFORM == 'hd4dp' && !SkryvSession.isAdmin()) {
            popDialogUser();
          }
        }
      },
      popDialogAdmin: function () {
        var loggedUser = SkryvSession.getLoggedUser();
        if (loggedUser) {
          if (!loggedUser.metaData.introductionAdmin && PLATFORM == 'hd4dp' && SkryvSession.isAdmin()) {
            popDialogAdmin();
          }
        }
      },
      popDialogFeatures: function () {
        var loggedUser = SkryvSession.getLoggedUser();


        if (loggedUser && !loggedUser.metaData.announceFeatures) {
          popDialogFeatures();
        }
      }
    };
  });

})();
