/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
angular.module('http-auth-interceptor', [ 'http-auth-interceptor-buffer' ])

  .run(function ($rootScope, $injector) {
    // Allow only one call for requesting a new token
    var httTry = 0;

    $rootScope.$on('event:auth-loginRequired', function () {
      var SkryvOauth = $injector.get('SkryvOauth');
      var authService = $injector.get('authService');

      if (httTry == 0) {
        httTry = 1;

        SkryvOauth.refreshAccessToken().then(function () {
          // Redo http requests
          authService.loginConfirmed();
          httTry = 0;
        }, function () {
          // Logout
          authService.loginCancelled();
          httTry = 0;
        });
      }
    });
  })

  .factory('authService', function ($rootScope, httpBuffer, SkryvSession, ngDialog) {

    return {
      /**
       * Call this function to indicate that authentication was successfull and trigger a
       * retry of all deferred requests.
       * @param data an optional argument to pass on to $broadcast which may be useful for
       * example if you need to pass through details of the user that was logged in
       * @param configUpdater an optional transformation function that can modify the
       * requests that are retried after having logged in.  This can be used for example
       * to add an authentication token.  It must return the request.
       */
      loginConfirmed: function (data, configUpdater) {
        var updater = configUpdater || function (config) {return config;};
        $rootScope.$broadcast('event:auth-loginConfirmed', data);
        httpBuffer.retryAll(updater);
      },

      /**
       * Call this function to indicate that authentication should not proceed.
       * All deferred requests will be abandoned or rejected (if reason is provided).
       * @param data an optional argument to pass on to $broadcast.
       * @param reason if provided, the requests are rejected; abandoned otherwise.
       */
      loginCancelled: function (data, reason) {
        httpBuffer.rejectAll(reason);
        SkryvSession.destroyUserSession('Session Expired');
        $rootScope.$broadcast('event:auth-loginCancelled', data);
      }
    };
  })

/**
 * $http interceptor.
 * On 401 response (without 'ignoreAuthModule' option) stores the request
 * and broadcasts 'event:auth-loginRequired'.
 * On 403 response (without 'ignoreAuthModule' option) discards the request
 * and broadcasts 'event:auth-forbidden'.
 */
  .config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($rootScope, $q, httpBuffer, $injector) {
      return {
        responseError: function (rejection) {
          if (rejection) {
            if (rejection.data) {
              // If user is not authorised by access token
              if (rejection.config.url.indexOf('refresh_token') > -1 && (rejection.status == 401 || rejection.status == 400)) {
                var SkryvSession = $injector.get('SkryvSession');
                SkryvSession.destroyUserSession('Session Expired');
              }
              // If user is not authorised by access token
              else if (rejection.data.error === 'invalid_token' && rejection.status == 401) {
                var deferred = $q.defer();
                httpBuffer.append(rejection.config, deferred);
                $rootScope.$broadcast('event:auth-loginRequired', rejection);

                return deferred.promise;
              }
            }
          }

          // otherwise, default behaviour
          return $q.reject(rejection);
        }
      };
    });
  });

/**
 * Private module, a utility, required internally by 'http-auth-interceptor'.
 */
angular.module('http-auth-interceptor-buffer', [])

  .factory('httpBuffer', function ($injector) {
    /** Holds all the requests, so they can be re-requested in future. */
    var buffer = [];

    /** Service initialized later because of circular dependency problem. */
    var $http;

    function retryHttpRequest(config, deferred) {
      function successCallback(response) {
        deferred.resolve(response);
      }
      function errorCallback(response) {
        deferred.reject(response);
      }
      $http = $http || $injector.get('$http');
      $http(config).then(successCallback, errorCallback);
    }

    return {
      /**
       * Appends HTTP request configuration object with deferred response attached to buffer.
       */
      append: function (config, deferred) {
        buffer.push({
          config: config,
          deferred: deferred
        });
      },

      /**
       * Abandon or reject (if reason provided) all the buffered requests.
       */
      rejectAll: function (reason) {
        if (reason) {
          for (var i = 0; i < buffer.length; ++i) {
            buffer[i].deferred.reject(reason);
          }
        }
        buffer = [];
      },

      /**
       * Retries all the buffered requests clears the buffer.
       */
      retryAll: function (updater) {
        for (var i = 0; i < buffer.length; ++i) {
          retryHttpRequest(updater(buffer[i].config), buffer[i].deferred);
        }
        buffer = [];
      }
    };
  });
