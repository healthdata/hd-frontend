/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.document')
  .config(function ($routeProvider, SINGLEFORM) {
    'ngInject';

    if (SINGLEFORM) {
      // Without action buttons, full-screen
      $routeProvider.when('/document/hd4pc/:uuid', {
        templateUrl: 'views/document/hd4pc.html',
        controller: 'documentHd4pcCtrl'
      });
    }

    $routeProvider.when('/document/new', {
      template: '<div></div>',
      controller: function ($location) {
        $location.path('/');
      }
    });

    // With action buttons
    $routeProvider.when('/document/:workflowId', {
      templateUrl: 'views/document/edit.html',
      controller: 'documentEditCtrl',
      resolve: {
        skryvdoc: function (SkryvDoc, authFilter, $route, Notifications) {
          if (authFilter('user')) {
            return SkryvDoc.byId($route.current.params.workflowId).then(function (doc) {
              return doc;
            }, function (err) {
              Notifications.error(err);
            });
          }
        },
        skryvdcd: function () {
          return;
        },
        skryvnotes: function (WorkflowNotes, $route, Notifications) {
            return WorkflowNotes.getAll($route.current.params.workflowId).then(function (notes) {
              return notes;
            }, function (err) {
              Notifications.error(err);
              return 'error';
            });
        }
      }
    });
    $routeProvider.when('/pdf/:workflowId', {
      templateUrl: 'views/document/pdf.html',
      controller: 'documentPdfCtrl',
      resolve: {
        skryvdoc: function (authFilter, SkryvDoc, $route) {
          if (authFilter('user')) {
            return SkryvDoc.byId($route.current.params.workflowId);
          }
        },
        skryvdcd: function () {
          return;
        }
      }
    });
    $routeProvider.when('/document/new/:dataCollectionId', {
      templateUrl: 'views/document/edit.html',
      controller: 'documentEditCtrl',
      resolve: {
        skryvdcd: function ($rootScope, $route, SkryvDefinition, PLATFORM, $location, authFilter, Notifications, gettextCatalog) {
          // Route is not available on hd4res
          if (PLATFORM === 'hd4res') {
            $location.path('/');
          }

          if (authFilter('user')) {
            return SkryvDefinition.get($route.current.params.dataCollectionId);
          }
        },
        skryvdoc: function () {
          return;
        },
        skryvnotes: function () {
          return [];
        }
      }
    });
  });
