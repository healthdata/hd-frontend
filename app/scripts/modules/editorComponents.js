/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('skryv.editor.components', [ 'services.attachment', 'services.download' ])
    .factory('resolveEditorComponent', resolveEditorComponentFactory);

  /*ngInject*/
  function resolveEditorComponentFactory($window, $q, $timeout, _, healthdataDocumentmodel, SSIN_SERVER_URL, STABLE_DATA_URL, AttachmentBackend, Download, CurrentDocument) {

    var skrBooleanInput = {
      template: 'views/editor/skrBooleanInput.html',
      link: linkWrapper(function (scope, input) {
        if (input.inputOptions) {
          scope.readOnly = input.inputOptions['read-only'];
        }
      })
    };

    var skrTextInput = {
      template: 'views/editor/skrTextInput.html',
      link: linkWrapper(function (scope, input) {
        scope.readOnly = scope.manipulator.readOnly;

        if (input.inputOptions.inputsize) {
          scope.size = input.inputOptions.inputsize;
        }
        if (input.inputOptions && input.inputOptions['read-only']) {
          scope.readOnly = input.inputOptions['read-only'];
        }
        if (input.inputOptions && input.inputOptions['computedWith']) {
          scope.computedWith = input.inputOptions.computedWith;
        }

        // Special support when this field is the internal patient id field
        // WDC-1887
        var manipulator = scope.manipulator;
        if (manipulator.parent && manipulator.parent.enricher &&
          manipulator.parent.propertyManipulators.internal_patient_id === manipulator &&
          !manipulator.parent.options.isPseudonymised) {
          manipulator.isInternalPatientID = true;
          var enricher = scope.enricher = manipulator.parent.enricher;
          scope.textFieldChanged = function () {
            enricher.enrichInternalOnChanged(manipulator.value);
          };
        }
      })
    };

    var skrMultilineInput = {
      template: 'views/editor/skrMultilineInput.html',
      link: linkWrapper(function (scope, input) {
        scope.readOnly = scope.manipulator.readOnly;

        if (input.inputOptions && input.inputOptions['read-only']) {
          scope.readOnly = input.inputOptions['read-only'];
        }
        if (input.inputOptions && input.inputOptions['computedWith']) {
          scope.computedWith = input.inputOptions.computedWith;
        }
      })
    };

    var skrNumberInput = {
      template: 'views/editor/skrNumberInput.html',
      link: linkWrapper(function linkNumberInput(scope, input) {
        scope.isFinite = $window.isFinite;

        // Used as fallback for IE9
        scope.$watch('manipulator.value', function (newValue, oldValue) {
          if ($window.isNaN(newValue)) {
            scope.manipulator.value = oldValue;
          } else {
            scope.manipulator.value = newValue;
          }
        });

        if (input.description.assertions.minimum) {
          scope.minimum = input.description.assertions.minimum.minimum;
        }
        if (input.inputOptions) {
          scope.readOnly = input.inputOptions['read-only'];
        }
        if (input.inputOptions && input.inputOptions['computedWith']) {
          scope.computedWith = input.inputOptions.computedWith;
        }
        if (input.description.assertions.maximum) {
          scope.maximum = input.description.assertions.maximum.maximum;
        }
      })
    };

    var skrDateInput = {
      template: 'views/editor/skrDateInput.html',
      link: linkWrapper(function (scope, input) {
        scope.readOnly = scope.manipulator.readOnly;

        if (input.inputOptions && input.inputOptions['read-only']) {
          scope.readOnly = input.inputOptions['read-only'];
        }
        if (input.inputOptions && input.inputOptions['computedWith']) {
          scope.computedWith = input.inputOptions.computedWith;
        }

        scope.datepicker = {
          format: input.inputOptions.format || 'dd/MM/yyyy',
          isOpen: false,
          options: {},
          open: function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            scope.datepicker.isOpen = true;
          }
        };
      })
    };

    var skrChoiceInput = {
      template: 'views/editor/skrChoiceInput.html',
      link: linkWrapper(function (scope, input) {
        if (input.inputOptions) {
          scope.display = input.inputOptions.display;
          scope.readOnly = input.inputOptions['read-only'];
        }
      })
    };

    var skrMultiChoiceInput = {
      template: 'views/editor/skrMultiChoiceInput.html',
      link: linkWrapper(function (scope, input) {
        if (input.inputOptions) {
          scope.readOnly = input.inputOptions['read-only'];
        }
      })
    };

    var skrListInput = {
      template: 'views/editor/skrListInput.html',
      link: linkWrapper(function (scope, input) {
        if (!input.listElementComponent) {
          throw new Error('skrListInput requires a list element component');
        }
        scope.listElementComponent = input.listElementComponent;
      })
    };

    function linkEditorGroup(scope, group, manipulator) {
      var pairs = scope.pairs = [];
      angular.forEach(group.components, function (nested) {
        pairs.push({
          component: nested,
          manipulator: nested.nestedManipulator(manipulator)
        });
      });
    }

    var skrSection = {
      template: 'views/editor/skrEditorSection.html',
      link: function (scope, section, manipulator) {
        linkEditorGroup(scope, section, manipulator);
        scope.toggleOpen = function () {
          if (!section.isOpen) {
            $timeout(function () {
              var id = section.name;

              var elt = $window.document.querySelector('#doc-form #' + id);

              if (elt) {
                $('.doc-container-middle .doc-content').scrollToElementAnimated(elt, 0);
              }
            });
          }
          section.toggleOpen();
        };
      }
    };

    var skrFieldSet = {
      template: 'views/editor/skrFieldSet.html',
      link: function (scope, fieldset, manipulator) {
        linkEditorGroup(scope, fieldset, manipulator);

        // A fieldset usually has only one element, namely an object containing
        // other fields. If this is not the case, we always display the fieldset,
        // but if there is only one object, and it is inactive, we hide everything.
        scope.fieldsetIsShown = true;
        if (scope.pairs.length === 1) {
          scope.$watch('pairs[0].manipulator.isActive()', function (isActive) {
            scope.fieldsetIsShown = isActive;
          });
        }
      }
    };

    var hdReferenceListInput = {
      template: 'views/editor/hdReferenceListInput.html',
      link: linkWrapper(function (scope, input, manipulator, $injector) {
        var $q = $injector.get('$q');

        var refListState = scope.refListState = manipulator.state;

        function asPromise(f) {
          var q = $q.defer();
          f(function (err, result) {
            if (err) {
              q.reject(err);
              return;
            }
            q.resolve(result);
          });
          return q.promise;
        }

        scope.autocompleteFor = function (query) {
          if (manipulator.options.readOnly) {
            return $q.when([]);
          }
          return asPromise(function (cb) {
            refListState.autocompleteFor(query, cb);
          }).then(function (results) {
            return results.slice(0, 20);
          });
        };

        scope.renderMatch = refListState.renderMatch;
        scope.selectMatch = function (match) {
          refListState.becomeMatch(match);
        };

        // This object holds the value for the dropdown used to select items
        // in the reference tree for CRAMP.
        scope.refTreeState = {
          selection: null
        };
        scope.setCurrentNode = function (node) {
          // Whenever we select a node in the reference tree, be it via the
          // dropdown or by clicking on one of the parents, we need to reset the
          // selection. Otherwise, angular gets very confused because it may
          // contain a value that does not match any of the allowed values.
          scope.refTreeState.selection = null;
          refListState.setCurrentNode(node);
        };

        scope.refTree = refListState.refTree;

        scope.isLoading = false;

        Object.defineProperty(scope, 'referenceListQuery', {
          get: function () {
            return manipulator.getQuery();
          },
          set: function (v) {
            if (manipulator.options.readOnly) { return; }
            manipulator.inputQuery(v);
          }
        });

        if (input.inputOptions.inputsize) {
          scope.size = input.inputOptions.inputsize;
        }
        if (input.inputOptions) {
          scope.readOnly = input.inputOptions['read-only'];
        }
        var typeaheadMinLength = input.inputOptions.typeaheadMinLength;
        scope.typeaheadMinLength = typeof typeaheadMinLength === 'number' ? typeaheadMinLength : 0;
      })
    };

    var hdPatientIDInput = {
      template: 'views/editor/hdPatientIDInput.html',
      link: function (scope, input, manipulator) {
        linkEditorGroup(scope, input, manipulator);
        scope.readOnly = manipulator.options.readOnly;
        // We need to make sure the component knows about the description;
        // otherwise the logic for sticky fields gets confused and thinks this
        // is a 'regular' group with nested sticky fields.
        input.description = manipulator.description;

        if (input.inputOptions.inputsize) {
          scope.size = input.inputOptions.inputsize;
        }

        scope.patientIDLabel = manipulator.options.isPseudonymised ?
          'Pseudo patient ID' :
          input.label;

        // We use an object to contain the value, to avoid problems with
        // scope inheritance.
        scope.data = {
          mask: undefined,
          allowCodepat: undefined
        };

        scope.usesCodepat = function () {
          return manipulator.getMethod() === 'codepat';
        };
        scope.$watch('usesCodepat()', function (result) {
          scope.data.allowCodepat = result;
          var useSSINMask = !manipulator.options.isPseudonymised && !result;
          scope.data.mask = useSSINMask ? '99.99.99-999.99' : '';
        });

        scope.isSSIN = function () {
          return manipulator.getMethod() === 'SSIN' ||
            manipulator.getMethod() === 'SSINbis';
        };
        scope.isGenerated = function () {
          return manipulator.patientIDisGenerated();
        };
        scope.showCodepatConfirmation = function () {
          if (!manipulator.description.allowCodepat) { return false; }
          return scope.data.allowCodepat || typeof manipulator.generatedCodepat() === 'string';
        };

        scope.allowCodepatChanged = function () {
          if (manipulator.options.isPseudonymised || manipulator.options.readOnly) {
            // In HD4RES, the change to (dis-)allow codepats may be ignored.
            // In those cases, scope.data.allowCodepat must be reset.
            scope.data.allowCodepat = scope.usesCodepat();
            return;
          }
          if (!manipulator.description.allowCodepat) { return; }
          if (scope.data.allowCodepat) {
            manipulator.acceptGeneratedCodepat();
          } else {
            // rejectGeneratedCodepat does not work well for codepats that have generated: false
            // manipulator.rejectGeneratedCodepat();
            manipulator.value = '';
          }
          // In read-only documents, the change to (dis-)allow codepats may be ignored.
          // In those cases, scope.data.allowCodepat must be reset.
          scope.data.allowCodepat = scope.usesCodepat();
          manipulator.stableDataLoader.loadStableDataOnChanged();
        };

        scope.isPseudonymised = manipulator.options.isPseudonymised;

        var enrichSSINOnChanged;
        scope.patientIDChanged = function () {
          if (manipulator.options.isPseudonymised || manipulator.options.readOnly) { return; }
          manipulator.deriveDateOfBirthFromSSIN();
          manipulator.stableDataLoader.loadStableDataOnChanged();
          if (enrichSSINOnChanged) {
            enrichSSINOnChanged();
          }
        };

        scope.stableDataApplied = false;

        var unsubDocumentListener = manipulator.document.listen(function () {
          // trick angular into refreshing the view by using $timeout
          $timeout(function () {
            var stableDataLoader = manipulator.stableDataLoader;
            scope.stableDataApplied = stableDataLoader.stableDataHasBeenApplied();
          });
        });

        scope.$on('$destroy', function () {
          unsubDocumentListener();
        });

        var skipEnrichment = typeof SSIN_SERVER_URL !== 'string' ||
          SSIN_SERVER_URL === '' ||
          manipulator.options.isPseudonymised ||
          manipulator.options.readOnly;

        if (skipEnrichment) {
          return;
        }

        var enricher = scope.enricher = manipulator.enricher;
        enrichSSINOnChanged = enricher.enrichSSINOnChanged;

        // Run it once at initialization.
        enricher.enrichSSINOnChanged();

      }
    };

    var hdQuestionnaire = {
      template: 'views/editor/hdQuestionnaire.html',
      link: linkWrapper(function (scope, input, manipulator) {
        linkEditorGroup(scope, input, manipulator);

        // We need to make sure the component knows about the description;
        // otherwise the logic for sticky fields gets confused and thinks this
        // is a 'regular' group with nested sticky fields.
        input.description = manipulator.description;

        var questionnaire = manipulator;

        var questionNames = input.hdQuestionnaire.questionNames;
        var allowRandomOrder = questionnaire.allowRandomOrder;
        var hasFixedOrder = questionnaire.hasFixedOrder;

        var originalPairs = scope.pairs.slice();

        function applyQuestionsOrder() {
          var questions = questionnaire.questionsOrder();
          questionNames.forEach(function (questionName, index) {
            scope.pairs[questions.indexOf(questionName)] = originalPairs[index];
          });
        }

        function updateQuestionsOrder(order) {
          questionnaire.updateQuestionsOrder(order);
          applyQuestionsOrder();
        }

        scope.selectedOrder = questionnaire.selectedOrder;
        scope.allowRandomOrder = allowRandomOrder;
        scope.updateQuestionsOrder = updateQuestionsOrder;
        scope.predefinedOrders = input.hdQuestionnaire.predefinedOrders;
        scope.validAnswers = input.hdQuestionnaire.validAnswers;
        scope.hasFixedOrder = hasFixedOrder;

        // run once to initialize question order
        applyQuestionsOrder();
      })
    };

    var hdQuestionInput = {
      template: 'views/editor/hdQuestionInput.html',
      link: linkWrapper(function (scope, input) {
        scope.validAnswers = input.validAnswers;
      })
    };

    var hdStaticImageInput = {
      template: 'views/editor/hdStaticImageInput.html',
      link: linkWrapper(function () {})
    };

    var attachmentInput = {
      template: 'views/editor/attachmentInput.html',
      link: linkWrapper(function (scope, input, manipulator) {
        var initAttachmentInfo = function () {
          scope.attachmentInfo = {
            attachmentID: manipulator.attachmentID(),
            hasAttachment: false,
            loadingInfo: false,
            loadingData: false,
            error: null,
            info: null
          };
        }

        initAttachmentInfo();

        // alternative for ng-model on input[file] since ng-model isn't supported
        scope.fileChanged = function (element) {
          scope.uploadedAttachment = element.files[0];
          scope.isLoading = true;
          uploadAttachment().then(function (res) {
            scope.isLoading = false;
          });
        };

        scope.readOnly = manipulator.readOnly;

        scope.downloadAttachment = downloadAttachment;

        if (!scope.readOnly) {
          scope.uploadAttachment = uploadAttachment;
          scope.deleteAttachment = deleteAttachment;
        }

        function uploadAttachment() {
          var file = scope.uploadedAttachment;
          var attachmentID = manipulator.generateID();
          scope.attachmentInfo.error = undefined;

          // check if the document has a workflowId, if not, save the document so the id gets fetched from the backend
          return $q.when(CurrentDocument.currentWorkflow().id || CurrentDocument.saveCurrentContent().then(function (res) {
              return res.id;
          })).then(function (workflowId) {
            return AttachmentBackend
              .uploadAttachment(workflowId, attachmentID, file)
              .then(
                function (response) {
                  if (response.error) {
                    scope.attachmentInfo.error = response;
                  } else {
                    scope.attachmentInfo.hasAttachment = true;
                    scope.attachmentInfo.attachmentID = attachmentID;
                    scope.attachmentInfo.info = response;
                    manipulator.setAttachmentID(attachmentID);
                  }
                });
          });
        }

        function deleteAttachment() {
          scope.uploadedAttachment = undefined;
          initAttachmentInfo();

          AttachmentBackend
            .deleteAttachment(scope.attachmentInfo.attachmentID)
            .then(
              function () {
                manipulator.clearAttachmentID();
                scope.uploadedAttachment = undefined;
                scope.attachmentInfo.hasAttachment = false;
                scope.attachmentInfo.info = null;
              },
              function (error) {
                scope.attachmentInfo.error = error;
              });
        }

        function downloadAttachment() {
          if (!scope.attachmentInfo.info) { return; }
          scope.attachmentInfo.loadingData = true;
          AttachmentBackend
            .getAttachmentData(scope.attachmentInfo.attachmentID)
            .finally(function () { scope.attachmentInfo.loadingData = false; })
            .then(
              function (data) {
                Download.download(data, {
                  name: scope.attachmentInfo.info.filename,
                  type: scope.attachmentInfo.info.mimeType
                });
              },
              function (error) {
                scope.attachmentInfo.error = error;
              });
        }

        if (!scope.attachmentInfo.attachmentID) { return; }

        scope.attachmentInfo.hasAttachment = true;
        scope.attachmentInfo.loadingInfo = true;
        AttachmentBackend
          .getAttachmentInfo(scope.attachmentInfo.attachmentID)
          .finally(function () { scope.attachmentInfo.loadingInfo = false; })
          .then(
            function (info) {
              scope.uploadedAttachment = info;
              scope.attachmentInfo.info = info;
            },
            function (err) {
              scope.attachmentInfo.error = err;
            });
      })
    };

    function linkWrapper(link) {
      return function (scope, component, manipulator, $injector) {
        if (component.description !== manipulator.description) {
          throw new Error(
            'input link: manipulator should have matching description');
        }
        link(scope, component, manipulator, $injector);
      };
    }

    var mapping = {
      attachmentInput: attachmentInput,
      skrListInput: skrListInput,
      skrChoiceInput: skrChoiceInput,
      skrMultiChoiceInput: skrMultiChoiceInput,
      hdReferenceListInput: hdReferenceListInput,
      hdPatientIDInput: hdPatientIDInput,
      hdQuestionInput: hdQuestionInput,
      hdQuestionnaire: hdQuestionnaire,
      hdStaticImageInput: hdStaticImageInput,
      skrMultilineInput: skrMultilineInput,
      skrBooleanInput: skrBooleanInput,
      skrDateInput: skrDateInput,
      skrNumberInput: skrNumberInput,
      skrTextInput: skrTextInput,
      skrSection: skrSection,
      skrFieldSet: skrFieldSet,
      skrEditorGroup: {
        template: 'views/editor/skrEditorGroup.html',
        link: linkEditorGroup
      }
    };

    return function resolveEditorComponent(component) {
      var componentName = component.componentName;
      if (!componentName) {
        return;
      }
      return mapping[componentName];
    };
  }

})();
