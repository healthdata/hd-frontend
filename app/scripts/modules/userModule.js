/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

angular.module('skryv.user')

.config(function ($routeProvider) {
  $routeProvider.when('/user/list', {
    templateUrl: 'views/user/list.html',
    controller: 'userListCtrl',
    resolve: {
      authenticate: function ($q, authFilter) {
        authFilter('admin');
      }
    }
  })
    .when('/user/requests', {
      templateUrl: 'views/user/requestList.html',
      controller: 'requestListCtrl',
      resolve: {
        authenticate: function ($q, authFilter) {
          authFilter('admin');
        }
      }
    })
    .when('/user/new', {
      templateUrl: 'views/user/new.html',
      controller: 'userNewCtrl',
      resolve: {
        authenticate: function ($q, authFilter) {
          authFilter('admin');
        },
        userManagement: function (SkryvSession, $location, $q) {
          var delay = $q.defer();
          // Redirect if LDAP
          SkryvSession.getAuthType().then(function (type) {
            if (type === 'DATABASE_LDAP') {
              $location.path('/user/search');
            } else {
              delay.resolve(true);
            }
          });
          return delay.promise;
        },
        prefilledUser: function () { return false }
      }
    })
    .when('/user/ldap/:username', {
      templateUrl: 'views/user/new.html',
      controller: 'userNewCtrl',
      resolve: {
        authenticate: function ($q, authFilter) {
          authFilter('admin');
        },
        userManagement: function (SkryvSession, $location, $q) {
          var delay = $q.defer();
          // Route not accessible for non-DATABASE-LDAP environment
          SkryvSession.getAuthType().then(function (type) {
            if (type !== 'DATABASE_LDAP') {
              $location.path('/user/list');
            } else {
              delay.resolve(true);
            }
          });
          return delay.promise;
        },
        prefilledUser: function ($route, SkryvUser, Notifications, $location) {
          return SkryvUser.getLdapUser($route.current.params.username).catch(function (err) {
            Notifications.error(err);
            $location.path('/user/list');
          });
        }
      }
    })
    .when('/user/search', {
      templateUrl: 'views/user/search_ldap.html',
      controller: 'userSearchCtrl',
      resolve: {
        authenticate: function ($q, authFilter) {
          authFilter('admin');
        },
        userManagement: function (SkryvSession, $location, $q) {
          var delay = $q.defer();
          // Route not accessible for non-DATABASE-LDAP environment
           SkryvSession.getAuthType().then(function (type) {
            if (type !== 'DATABASE_LDAP') {
              $location.path('/user/list');
            } else {
              delay.resolve(true);
            }
          });
          return delay.promise;
        }
      }
    })
    .when('/user/:id/edit', {
      templateUrl: 'views/user/edit.html',
      controller: 'userEditCtrl',
      resolve: {
        authenticate: function (authFilter) {
          authFilter('admin');
        }
      }
    })
    .when('/user/:id/request', {
      templateUrl: 'views/user/requestEdit.html',
      controller: 'requestEditCtrl',
      resolve: {
        authenticate: function (authFilter) {
          authFilter('admin');
        }
      }
    })
    .when('/dcrequests', {
      templateUrl: 'views/settings/dcrequests.html',
      controller: 'dcRequestCtrl',
      resolve: {
        authenticate: function (authFilter) {
          authFilter('admin');
        }
      }
    })
    .when('/settings/profile', {
      templateUrl: 'views/settings/profileEdit.html',
      controller: 'profileCtrl',
      resolve: {
        authenticate: function (authFilter) {
          authFilter('user');
        }
      }
    })
    .when('/admins/list', {
      templateUrl: 'views/settings/adminList.html',
      controller: 'adminListCtrl',
      resolve: {
        authenticate: function (authFilter, platformFilter) {
          authFilter('user');
          platformFilter('hd4dp');
        }
      }
    });
});
