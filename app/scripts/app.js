/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var MODE = "mock";

angular.module('frontendApp', [
  'services.workflow',
  'services.dcd',
  'services.navigation',
  'services.notification',
  'services.support',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'ui.mask',
  'gettext',
  'ui.tree',
  'monospaced.elastic',
  'ngDialog',
  'once',
  'duScroll',
  'http-auth-interceptor',
  'angularFileUpload',
  'LocalStorageModule',
  'skryv.conf',
  'skryv.appConfig',
  'skryv.language',
  'skryv.collection',
  'skryv.status',
  'skryv.ui',
  'skryv.document',
  'skryv.session',
  'skryv.oauth',
  'skryv.user',
  'skryv.participation',
  'skryv.configurations',
  'skryv.workbench',
  'skryv.intro',
  'skryv.file',
  'skryv.listView',
  'skryv.view',
  'skryv.version',
  'skryv.settings',
  'skryv.dde',
  'skryv.dde.field',
  'skryv.editor.components',
  'services.configurations',
  'components.notification',
  'isteven-multi-select'
]);
