/* jshint node: true */
'use strict';

var fs = require('fs');
var _ = require('underscore');
var request = require('request');
var async = require('async');

var argv = require('yargs')
  .demand([ 'baseURL', 'username', 'password', 'translations' ])
  .argv;

putItAllTogether(argv.baseURL, argv.username, argv.password, argv.translations, function (err, isOK) {
  if (err) {
    console.log('Error: ', err);
    return;
  }
  console.log('OK? ' + isOK);
});

function putItAllTogether(baseURL, username, password, translationsFileName, cb) {
  async.waterfall([
    function (cb) {
      async.parallel({
        translationsFile: _.partial(fs.readFile, translationsFileName, 'utf8'),
        adminToken: _.partial(requestAdminToken, baseURL, username, password)
      }, cb);
    },
    function (results, cb) {
      var adminToken = results.adminToken;
      var translations = prepareTranslations(JSON.parse(results.translationsFile));
      deployTranslations(baseURL, adminToken, translations, cb);
    }
  ], cb);
}

function prepareTranslations(translationsByLanguage) {
  var translationsByKey = {};
  _.each(translationsByLanguage, function (keyValuePairs, language) {
    _.each(keyValuePairs, function (value, key) {
      translationsByKey[key] = translationsByKey[key] || { key: key };
      translationsByKey[key][language] = value;
    });
  });
  return _.values(translationsByKey);
}

function requestAdminToken(baseURL, username, password, cb) {
  request({
    method: 'POST',
    url: baseURL + '/oauth/token',
    json: true,
    qs: {
      username: username,
      password: password,
      client_id: 'healthdata-client',
      grant_type: 'password'
    }
  }, function (err, response, body) {
    if (err) { return cb(err); }
    console.log('requestAdminToken', response.statusCode);
    return cb(null, body.access_token);
  });
}

function deployTranslations(baseURL, bearerToken, payload, cb) {
  request({
    method: 'PUT',
    url: baseURL + '/translations',
    headers: {
      'Authorization': 'Bearer ' + bearerToken
    },
    json: true,
    body: payload
  }, function (err, response) {
    if (err) { return cb(err); }
    cb(null, response.statusCode === 204);
  });
}
