# Frontend for HD4DP and HD4RES

## Prerequisites
NPM and Bower

## Building

Run `npm install` and `bower install` to install dependencies.

Run `npm test` to run the tests.

Run `grunt serve` to launch the frontend.

Run `grunt build` to build the deployable.

## Contact

For questions, please contact support@healthdata.be.

## License
Apache License, Version 2.0
