'use strict';

/* jshint node: true */

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: {
      // configurable paths
      app: 'app',
      dist: 'dist'
    },

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      compass: {
        files: [ '<%= yeoman.app %>/styles/{,*/}*.{scss,sass}' ],
        tasks: [ 'compass:serve', 'autoprefixer' ]
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/*.html',
          '<%= yeoman.app %>/views/**/*.html',
          '<%= yeoman.app %>/services/**/*.js',
          '<%= yeoman.app %>/scripts/**/*.js',
          '<%= yeoman.app %>/stylesheets/**/*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          base: [
            '.tmp',
            '<%= yeoman.app %>'
          ]
        }
      },
      test: {
        options: {
          port: 9001,
          base: [
            '.tmp',
            '<%= yeoman.app %>'
          ]
        }
      },
      dist: {
        options: {
          base: '<%= yeoman.dist %>',
          // The following middleware was added to help debugging the problem of bad image URLs (see WDC-1081).
          // The idea is that all requests should be prefixed with prefixed; requests that do not do this correctly
          // are rejected and logged.
          // Usage:
          // - run `grunt serve:dist` in a console
          // - navigate to `http://localhost:900/hd/`
          // - monitor the console output and/or network requests for errors about
          // bad routes
          middleware: function (connect, options, middlewares) {
            // inject a custom middleware into the array of default middlewares
            middlewares.unshift(function (req, res, next) {
              var originalURL = req.url;
              var prefix = '/hd';
              console.log(originalURL);
              if (originalURL.lastIndexOf(prefix, 0) !== 0) {
                return next(new Error('Bad Request: ' + originalURL + '\nURL should always be prefixed with ' + prefix));
              }
              req.url = originalURL.slice(prefix.length);
              next();
            });
            return middlewares;
          }
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: true,
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        '<%= yeoman.app %>/scripts/{,*/}*.js'
      ],
      services: [
        'test/services/**/*.js',
        '<%= yeoman.app %>/services/**/*.js'
      ],
      test: {
        options: {
          jshintrc: true
        },
        src: [ 'test/spec/{,*/}*.js' ]
      }
    },

    jscs: {
      options: {
          config: '.jscsrc'
      },
      all: [
        'Gruntfile.js', '<%= yeoman.app %>/scripts/{,*/}*.js'
      ],
      services: [
        'test/services/**/*.js',
        '<%= yeoman.app %>/services/**/*.js'
      ],
      test: {
        src: [ 'test/spec/{,*/}*.js' ]
      }
    },

    replace: {

      shared: {
        files: [ {
          expand: true,
          flatten: true,
          src: './config/configuration.js',
          dest: '<%= yeoman.app %>/services/config/'
        } ]
      },

      hd4res: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/default-hd4res.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      },

      hd4dp: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/default-hd4dp.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      },

      hd4prc: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/default-hd4pc.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      },

      intHD4DP: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/intHD4DP.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      },

      intHD4PC: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/intHD4PC.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      },

      intHD4RES: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/intHD4RES.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      },

      testHD4DP: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/testHD4DP.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      },

      accHD4DP: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/accHD4DP.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      },

      testHD4RES: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/testHD4RES.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      },

      demo: {
        options: {
          patterns: [ {
            json: grunt.file.readJSON('./config/environments/demo.json')
          } ]
        },
        files: '<%= replace.shared.files %>'
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [
          {
            dot: true,
            src: [
              '.tmp',
              '<%= yeoman.app %>/stylesheets',
              '<%= yeoman.app %>/handlebars',
              '<%= yeoman.dist %>'
            ]
          }
        ]
      },
      server: {
        files: [
          {
            dot: true,
            src: [
              '.tmp',
              '<%= yeoman.app %>/stylesheets'
            ]
          }
        ]
      }
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: [ 'last 1 version' ]
      },
      dist: {
        files: [
          {
            expand: true,
            cwd: '<%= yeoman.app %>/stylesheets',
            src: '{,*/}*.css',
            dest: '<%= yeoman.app %>/stylesheets'
          }
        ]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: '<%= yeoman.app %>/index.html',
        ignorePath: '<%= yeoman.app %>/'
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    compass: {
      options: {
        // The config here has been carefull crafted to get relative paths to images right.
        // See WDC-1081 for more info. In particular, it is necessary that the generated css
        // is placed within the app folder instead of in the .tmp folder.
        basePath: '<%= yeoman.app %>',
        sassDir: 'styles',
        cssDir: 'stylesheets',
        importPath: 'app/bower_components',
        imagesDir: 'images',
        javascriptsDir: 'scripts',
        fontsDir: 'stylesheets/fonts',
        relativeAssets: true,
        assetCacheBuster: false
      },
      dist: {
        options: {
        }
      },
      serve: {
        options: {
          debugInfo: true
        }
      }
    },

    // Renames files for browser caching purposes
    rev: {
      dist: {
        files: {
          src: [
            '<%= yeoman.dist %>/scripts/{,*/}*.js',
            '<%= yeoman.dist %>/stylesheets/{,*/}*.css',
            '<%= yeoman.dist %>/images/{,*/,*/*/}*.{png,jpg,jpeg,gif,webp,svg}',
            '<%= yeoman.dist %>/fav*.png'
          ]
        }
      }
    },

    // Browserifies only the document model code.
    // We take all files in app/scripts/modules/browserify and browserify it to
    // .tmp/scripts/bsScripts.br.js -> notice the .br to indicate that
    // it has been browserified.
    // In index.html, we then refer to scripts/bsScripts.br.js
    browserify: {
      dist: {
        files: { '.tmp/scripts/bsScripts.br.js': [ '<%= yeoman.app %>/scripts/modules/browserify/*.js' ] },
        options: { debug: true }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>'
      }
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      html: {
        src: [ '<%= yeoman.dist %>/**/*.html' ],
        options: {
          // asset URLs in html are always relative to the root
          assetsDirs: '<%= yeoman.dist %>'
        }
      },
      css: {
        src: [ '<%= yeoman.dist %>/stylesheets/{,*/}*.css' ]
        // asset URLs in css are always relative to the css file location, which
        // is the default assumption of usemin
      }
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      dist: {
        files: [
          {
            expand: true,
            cwd: '<%= yeoman.app %>/images',
            src: '{,*/}*.{png,jpg,jpeg,gif}',
            dest: '<%= yeoman.dist %>/images'
          }
        ]
      }
    },
    svgmin: {
      dist: {
        files: [
          {
            expand: true,
            cwd: '<%= yeoman.app %>/images',
            src: '{,*/}*.svg',
            dest: '<%= yeoman.dist %>/images'
          }
        ]
      }
    },

    // Allow the use of non-minsafe AngularJS files. Automatically makes it
    // minsafe compatible so Uglify does not destroy the ng references
    ngAnnotate: {
      dist: {
        files: [
          {
            expand: true,
            cwd: '.tmp/concat/scripts',
            src: '*.js',
            dest: '.tmp/concat/scripts'
          }
        ]
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      server: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= yeoman.app %>/styles',
            dest: '<%= yeoman.app %>/stylesheets',
            src: [
              'fonts/**/*'
            ]
          }
        ]
      },
      dist: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= yeoman.app %>',
            dest: '<%= yeoman.dist %>',
            src: [
              '*.{ico,png,txt}',
              '*.html',
              'scripts/**/*.html',
              'views/**/*.html',
              'files/**/*.txt',
              'bower_components/sass-bootstrap/fonts/*'
            ]
          },
          {
            expand: true,
            dot: true,
            cwd: '<%= yeoman.app %>/styles',
            dest: '<%= yeoman.dist %>/stylesheets',
            src: [
              'fonts/**/*'
            ]
          },
          {
            expand: true,
            cwd: '<%= yeoman.app %>/bower_components/font-awesome/fonts',
            dest: '<%= yeoman.dist %>/fonts',
            src: '*'
          }
        ]
      }
    },

    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    cssmin: {
      dist: {
        files: {
          '<%= yeoman.dist %>/stylesheets/platforms/hd4dp.css': [ '<%= yeoman.app %>/stylesheets/platforms/hd4dp.css' ],
          '<%= yeoman.dist %>/stylesheets/platforms/hd4res.css': [ '<%= yeoman.app %>/stylesheets/platforms/hd4res.css' ]
        }
      }
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true,
        browsers: [ 'PhantomJS' ]
      },
      all: {
        configFile: 'karma.conf.js',
        browsers: [ 'PhantomJS', 'Chrome', 'Firefox', 'IE' ]
      }
    },

    nggettext_extract: {
      pot: {
        files: {
          'app/lang/template.pot': [
            'app/scripts/**/*.html',
            'app/services/**/*.js',
            'app/views/**/*.html',
            'app/lang/translations.html',
            'app/scripts/**/*.js',
            'app/scripts/app.js'
          ]
        }
      }
    },

    nggettext_compile: {
      all: {
        options: {
          format: 'json',
          module: 'frontendApp'
        },
        files: {
          'app/lang/translations.json': [ 'app/lang/translations/*.po' ]
        }
      }
    }
  });

  grunt.registerTask('serve', function (target) {
    if (target === 'dist') {
      return grunt.task.run([ 'build', 'connect:dist:keepalive' ]);
    }

    grunt.task.run([
      'clean:server',
      'replace:intHD4DP',
      'wiredep',
      'copy:server',
      'browserify:dist',
      'compass:serve',
      'autoprefixer',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('test', [
    'clean:server',
    'replace:intHD4DP',
    'browserify:dist',
    'jshint:services',
    'jscs:services',
    // No need to compile scss files during testing. This speeds up
    // running `grunt test` significantly.
    // 'compass:serve',
    // 'autoprefixer',
    'connect:test',
    'karma:unit'
  ]);

  grunt.registerTask('testBrowsers', [
    'clean:server',
    'replace:intHD4DP',
    'browserify:dist',
    'compass:serve',
    'autoprefixer',
    'connect:test',
    'karma:all'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'wiredep',
    'useminPrepare',
    'browserify:dist',
    'compass:dist',
    'imagemin',
    'svgmin',
    'autoprefixer',
    'concat',
    'ngAnnotate',
    'copy:dist',
    'cssmin',
    'uglify',
    'rev',
    'usemin'
  ]);

  grunt.registerTask('codestyle', [
    'jshint',
    'jscs'
  ]);

  grunt.registerTask('translate', [
    'nggettext_extract'
  ]);

  // For generationg the .js file
  grunt.registerTask('translateJSON', [
    'nggettext_extract',
    'nggettext_compile'
  ]);

};
